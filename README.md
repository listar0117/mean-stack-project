# DNNae

DNNae app is build using the following stack 
> Client end build on Angular and is present in `dnnae` folder.
> Server end build using Node, Koa2 and MongoDB and is present in `server` folder.

##To Run complete Project

Make sure you have mongo, node , angularCLI installed globally on Your machine.

##Versions

Node => v10.10.0
Mongo => 4.0.10
Koa => 2.5.3

Angular CLI: 8.0.2
Node: 10.10.0
OS: linux x64
Angular: 8.0.0
... animations, cdk, common, compiler, compiler-cli, core, forms
... language-service, material, platform-browser
... platform-browser-dynamic, router

Package                           Version
-----------------------------------------------------------
@angular-devkit/architect         0.800.2
@angular-devkit/build-angular     0.800.2
@angular-devkit/build-optimizer   0.800.2
@angular-devkit/build-webpack     0.800.2
@angular-devkit/core              8.0.2
@angular-devkit/schematics        8.0.2
@angular/cli                      8.0.2
@angular/http                     7.2.15
@ngtools/webpack                  8.0.2
@schematics/angular               8.0.2
@schematics/update                0.800.2
rxjs                              6.5.2
typescript                        3.4.1
webpack                           4.30.0

## enviorments.

Create .env file inside server folder and copy contents of .env.sample in it.

## running the code.
> Go inside `server` folder and run `npm start` to run server.
> Go inside `dnnae` folder and run `ng serve` to run Angular app. 
> app will run on `http://localhost:4200`

## build

If running for the first time 
> Open package,json in main folder and change `--name` tag accordingly inside `scripts` object in key `firstStart`.
> Clone Project on server
> Go to server set up all environments, and keys as mentioned above.
> Run command `npm run-script firstStart`

If deploying regularly.
> pull Code in repo on server.
> Run command `npm run-script server`

