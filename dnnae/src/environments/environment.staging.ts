export const environment = {
  production: true,
  baseUrl: 'http://34.73.147.239:5001/api',
  founderBaseUrl: 'http://34.73.147.239:5500',
  // baseUrl: 'https://dnnae-dev.site/staging/api',
  // baseUrl: 'https://www.dnnae.com/api',
  socket:'http://localhost:5000/api/v1',
  dialogFlowToken:'20fbae890e1c412d9a06319f11a26e14',
  // nlpUrl:'http://35.237.178.130:3002',
  nlpUrl:'https://dnnae-dev.site/nlpStaging',
  mailServer:'http://34.217.69.31',
  // mapApiKey: 'AIzaSyCaKbVhcX_22R_pRKDYuNA7vox-PtGaDkI',
  mapApiKey: 'AIzaSyA6OjPEAWYRbc39TNABYSGx6p3bO-CzD2A',
};
