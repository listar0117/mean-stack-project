// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  // baseUrl: 'http://13.250.242.130:3220/api/v1/en',
  // baseUrl: 'http://34.73.147.239:5000/api',
  baseUrl: 'http://localhost:5000/api',
  // founderBaseUrl: 'http://34.73.147.239:5500',
  founderBaseUrl: 'http://localhost:5000',
  dialogFlowToken :'20fbae890e1c412d9a06319f11a26e14',
  nlpUrl:'http://35.237.178.130:3001',
  mailServer:'http://34.217.69.31',
  mapApiKey: 'AIzaSyCaKbVhcX_22R_pRKDYuNA7vox-PtGaDkI',
  // mapApiKey: 'AIzaSyA6OjPEAWYRbc39TNABYSGx6p3bO-CzD2A',
  // socket:'http://localhost:4201/api/v1',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
