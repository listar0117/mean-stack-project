import { BrowserModule, Title,HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { GestureConfig } from '@angular/material';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatCardModule } from '@angular/material/card';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import 'hammerjs';

import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AdvSearchComponent } from './adv-search/adv-search.component';
import { DialogflowService } from './services/dialogflow.service';
import { HttpRequestInterceptor } from './services/http-intercepter.service';
import { StorageService } from './services/localStorage.service';

// import { HomeComponent } from './pages/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    AdvSearchComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
    HttpModule,
    MatCardModule,
    BrowserAnimationsModule,
    NgbModule,
    MatExpansionModule,
    AppRoutingModule,
    ToastrModule.forRoot(),
  ],  
  exports:[
    MatExpansionModule,
  ],
  
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true },
    { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig },
    StorageService,
    Title,DialogflowService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
