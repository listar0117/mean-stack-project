import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http-service';
import { Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Globals } from '../../Globals';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-single-job',
  templateUrl: './single-job.component.html',
  styleUrls: ['./single-job.component.scss'],
})
export class SingleJobComponent implements OnInit {
  requestUrl: string;
  public globals = Globals;
  typeOfRequestSend: any;
  dataToSend: any;
  singleJobData: any;
  isLoadingResults: boolean = true;

  constructor(
    private service: HttpService,
    private router: Router,
    private ActivatedRoute: ActivatedRoute,
    private titleService: Title
  ) {}

  ngOnInit() {
    const val = this.ActivatedRoute.snapshot.paramMap.get('id');
    console.log(val);
    this.getJobData(val);
  }

  getJobData(val) {
    this.dataToSend = {
      jobID: val,
      // recruiterID: 2
    };
    this.requestUrl = this.globals.urls.getJobData;
    this.typeOfRequestSend = this.service.postRequest(this.requestUrl, this.dataToSend);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          this.singleJobData = res.data.job;
          this.isLoadingResults = false;
          this.titleService.setTitle(this.singleJobData.companyName);
          // this.service.showSuccess('Email sent successfully.', 'Forgot Password');
          // this.router.navigate(['/login']);
        } else if (res.data.isSuccess === false) {
          // this.service.showError(res.data.message)
          this.isLoadingResults = false;
          this.router.navigate(['/'], { queryParamsHandling: 'preserve' });
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        // this.service.showError(err.error.message);
      }
    );
  }

  goto(data) {
    // console.log(data);
    // this.router.navigate(['/',data.companyName,data.jobTitle,'apply']);
    this.router.navigate(['/job', data._id, 'apply'], { queryParamsHandling: 'preserve' });
  }
}
