import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpService } from 'src/app/services/http-service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Globals } from '../../Globals';
// import { map } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-apply-job',
  templateUrl: './apply-job.component.html',
  styleUrls: ['./apply-job.component.scss']
})
export class ApplyJobComponent implements OnInit {
  singleJobData:any;
  isLoadingResults:boolean = true;
  applyJob: FormGroup;
  isSubmitted  =  false;
  requestUrl:string;
  public globals=Globals;
  typeOfRequestSend: any;
  dataToSend: any;
  jobId:string;
  constructor(private httpService: HttpService, private applyForm: FormBuilder ,private router: Router,private ActivatedRoute: ActivatedRoute, private titleService: Title) {
  }

  ngOnInit() {
    const emailPattren = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    const phoneNumberPattren=/^(\+?(\d{1}|\d{2}|\d{3})[- ]?)?\d{3}[- ]?\d{3}[- ]?\d{4}$/
    this.applyJob = this.applyForm.group({
      name: ['', Validators.required],
      emailAddress: ['', [Validators.required, Validators.pattern(emailPattren)]],
      phoneNumber: ['', [Validators.required, Validators.pattern(phoneNumberPattren)]],
      linkedinUrl: [''],
      resume:[''],
    });
    // this.ActivatedRoute.data.pipe(map(data => data)).subscribe((res)=>{
    //   this.singleJobData=res.singleJobData;
    //   // console.log(this.singleJobData);
    // })
    const val = this.ActivatedRoute.snapshot.paramMap.get("id")
    this.jobId=val;
    console.log(val);
    this.getJobData(val)
  }
  getJobData(val){
    this.dataToSend = {
      jobID: val
      // recruiterID: 2
    }
    this.requestUrl = this.globals.urls.getJobData;
    this.typeOfRequestSend = this.httpService.postRequest(this.requestUrl, this.dataToSend);
    this.typeOfRequestSend.subscribe(
      res => {
        console.log(res);
        if (res.data.isSuccess === true) {
          this.singleJobData =  res.data.job;
          this.isLoadingResults = false;
          this.titleService.setTitle( this.singleJobData.companyName );
          // this.service.showSuccess('Email sent successfully.', 'Forgot Password');
          // this.router.navigate(['/login']);
          
        }
        else if (res.data.isSuccess === false) {
          // this.service.showError(res.data.message)
            this.isLoadingResults = false;
            this.router.navigate(['/']);
        }
      },
      err => {
        if(err.error)
        console.log(err)
        this.isLoadingResults = false;
        // this.service.showError(err.error.message);
      },
    );
  }
  get formControls() { return this.applyJob.controls; }

  onSubmit(){
    console.log(this.applyJob.value);
    this.isSubmitted = true;
    if(this.applyJob.invalid){
      return;
    }
    else {
      console.log(this.applyJob);
      // let Form = JSON.stringify(this.applyJob.value);
      this.isLoadingResults = true;
      const formData = new FormData();
      formData.append('file', this.applyJob.value.resume);
      formData.append('name', this.applyJob.value.name);
      formData.append('emailAddress', this.applyJob.value.emailAddress);
      formData.append('phoneNumber', this.applyJob.value.phoneNumber);
      formData.append('linkedinUrl', this.applyJob.value.linkedinUrl);
      formData.append('jobID', this.jobId);
      console.log(formData);
      this.requestUrl = this.globals.urls.postApplicantData;
      this.typeOfRequestSend = this.httpService.postRequest(this.requestUrl,formData,{},false);
      this.typeOfRequestSend.subscribe(
        res => {
          console.log(res);
          if (res.data.isSuccess === true) {
            this.isLoadingResults = false;
            this.httpService.showSuccess('Application Submitted', '');
            // this.router.navigate(['/login']);
            this.router.navigate(['/job',this.jobId,'verify',res.data.applicant.webId],{state: {data:res.data}});
            
          }
          else if (res.data.isSuccess === false) {
            this.httpService.showError(res.data.message)
              this.isLoadingResults = false;
              // this.router.navigate(['/']);
          }
        },
        err => {
          if(err)
          console.log(err.error)
          this.isLoadingResults = false;
          // this.service.showError(err.error.message);
        },
      );
    }
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.applyJob.get('resume').setValue(file);
    }
  }

}
