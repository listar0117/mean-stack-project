import { Component, OnInit, ViewChild } from '@angular/core';
import { Message } from '../models/message';
import { Globals } from 'src/app/Globals';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { HttpService } from 'src/app/services/http-service';
@Component({
  selector: 'app-new-job-description',
  templateUrl: './new-job-description.component.html',
  styleUrls: ['./new-job-description.component.scss', '../new-job/new-job.component.scss'],
})
export class NewJobDescriptionComponent implements OnInit {
  public message: Message;
  public messages: Message[];
  model: any = {};
  public buttons: any;
  globals = Globals;
  httpSub$: Subscription = null;
  isLoadingResults: boolean = false;

  @ViewChild('form', { static: false }) form;
  jobQuestions: any;
  constructor(private router: Router, private httpService: HttpService) {
    // this.message = new Message('', 'assets/images/user.png');
    // this.messages = [
    //   new Message(Globals.JobQuestions[0], 'assets/images/bot.png', new Date())
    // ];
  }
  ngOnInit() {
    this.jobQuestions = Globals.JobQuestions;
  }
  submit() {
    // console.log(Object.values(this.model).join(".."));
    // sessionStorage.setItem('tempJobData', JSON.stringify(this.model));
    // this.router.navigate(['/recruiter/jobDescription','text']);
    this.getExtractedJD(this.model);
  }
  getExtractedJD(data) {
    // let item = JSON.parse(sessionStorage.getItem('tempJobData'));
    let item = data;
    console.log(Object.values(item).join('..'));
    const formData = new FormData();
    formData.append('text', Object.values(item).join('..'));
    this.isLoadingResults = true;
    this.httpSub$ = this.httpService
      .postRequest(this.globals.urls.dnnaeWeb.extract, formData, {}, false)
      .pipe(map(res => res.data))
      .subscribe(
        data => {
          if (data.isSuccess == false) {
            this.isLoadingResults = false;
            this.httpService.showError('Unable to extract data');
          } else {
            this.isLoadingResults = false;
            this.createJD(data);
            // sessionStorage.setItem('textExtractedJD', JSON.stringify(data));
            // this.router.navigate(['/recruiter/jobTitle']);

            // this.httpService.showSuccess('User signed Up successfully', 'Sign Up');
            // this.router.navigate(['/verify']);
          }
        },
        err => {
          console.log(err);
          this.isLoadingResults = false;
          sessionStorage.removeItem('pdfData');
          this.router.navigate(['/recruiter/newJob'], { queryParamsHandling: 'preserve' });
          this.httpService.showError('Unable to Read Data from Document');
        }
      );
  }

  //should not be used here
  createJD(dataCopy) {
    const formData = new FormData();
    formData.append('entities', JSON.stringify(dataCopy));
    this.isLoadingResults = true;
    this.httpSub$ = this.httpService
      .postRequest(this.globals.urls.dnnaeWeb.createJD, formData, {}, false)
      .pipe(map(res => res.data))
      .subscribe(
        data => {
          if (data.isSuccess == false) {
            this.isLoadingResults = false;
            this.router.navigate(['/recruiter/newJob'], { queryParamsHandling: 'preserve' });
            this.httpService.showError('Unable to generate Job Description');
          } else {
            this.isLoadingResults = false;
            // this.jobDescription = data;
            sessionStorage.setItem('textExtractedJD', JSON.stringify(dataCopy));
            sessionStorage.setItem('textCreatedJD', JSON.stringify(data));
            sessionStorage.setItem('jobSource', JSON.stringify('QA'));
            // this.httpService.showSuccess('Job Created successfully', 'Job Created');
            sessionStorage.removeItem('stepperData');
            this.router.navigate(['/recruiter/jobTitle', 'add', 'qa'], {
              queryParamsHandling: 'preserve',
            });
          }
        },
        err => {
          this.isLoadingResults = false;
          this.httpService.showError(err);
        }
      );
  }
}
