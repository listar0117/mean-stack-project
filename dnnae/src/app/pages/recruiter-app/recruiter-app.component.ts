import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recruiter-app',
  template: `
  <app-header-recruiter></app-header-recruiter>
  <router-outlet></router-outlet>
  `,
})
export class RecruiterAppComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
