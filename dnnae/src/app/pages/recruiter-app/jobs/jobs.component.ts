import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Globals } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConfirmDialougComponent } from '../../components/confirm-dialoug/confirm-dialoug.component';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss'],
})
export class JobsComponent implements OnInit {
  jobsData: any;
  isLoadingResults = true;
  jobs: any;
  requestUrl: string;
  public globals = Globals;
  private httpSub$: Subscription = null;
  typeOfRequestSend: any;
  dataToSend: any;
  userData: any;
  calledFromExtension: string;
  showExtensionMessage: boolean = false;
  // customToolTip:string="customToolTip";
  constructor(
    private service: HttpService,
    private sanitizer: DomSanitizer,
    private router: Router,
    private ActivatedRoute: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.ActivatedRoute.queryParams.subscribe(params => {
      this.calledFromExtension = params['calledFromExtension'];
      console.log(this.calledFromExtension);
    });
    if (this.calledFromExtension == '1') {
      this.showExtensionMessage = true;
    }
  }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.jobs = this.getRecuiterJobs(this.userData.recruiterID);
    // this.jobs = [];
  }
  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
  goto(data) {
    this.router.navigate(['recruiter/jdSummary', data.jobID], { queryParamsHandling: 'preserve' });
  }
  get sortData() {
    return (
      this.jobs &&
      this.jobs.sort((a, b) => {
        return <any>new Date(b.created_at) - <any>new Date(a.created_at);
      })
    );
  }
  clearStepper() {
    sessionStorage.removeItem('stepperData');
  }

  getRecuiterJobs(recruiterID) {
    if (recruiterID) {
      this.dataToSend = {
        recruiterID: recruiterID,
        // recruiterID: 2
      };
      this.requestUrl = this.globals.urls.getRecuiterJobs;
      this.typeOfRequestSend = this.service.postRequest(this.requestUrl, this.dataToSend);
      this.typeOfRequestSend.subscribe(
        res => {
          // console.log(res);
          if (res.data.isSuccess === true) {
            this.jobs = res.data.job;
            this.isLoadingResults = false;
          } else if (res.data.isSuccess === false) {
            this.service.showError(res.data.message);
            this.isLoadingResults = false;
            this.router.navigate(['/recruiter'], { queryParamsHandling: 'preserve' });
          }
        },
        err => {
          if (err.error) console.log(err);
          // this.service.showError(err.error.message);
          this.isLoadingResults = false;
        }
      );
    }
  }
  delete(job, index): void {
    let dialogRef = this.dialog.open(ConfirmDialougComponent, {
      width: '500px',
      panelClass: 'ratingDialoug',
      data: {
        callingFromFunction: 'Delete',
        DialougHeaderText: 'Delete Job',
        DialougBodyText: 'Are you sure you want to delete this Job permanently',
        ButtonCancelText: 'Cancel',
        ButtonSubmitText: 'Delete',
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result == true) {
        const datatoSend = {
          recruiterID: this.userData.recruiterID,
          jobID: job.jobID,
        };
        let url: string = this.globals.urls.dnnaeWeb.deleteJob;
        this.isLoadingResults = true;
        this.httpSub$ = this.service
          .postRequest(url, datatoSend)
          .pipe(map(res => res.data))
          .subscribe(
            data => {
              this.isLoadingResults = false;
              this.jobs.splice(index, 1);
              this.service.showSuccess('Job deleted successfully', 'Job Deleted');
            },
            err => {
              this.isLoadingResults = false;
              this.service.showError(err);
            }
          );
      }
    });
  }
}
