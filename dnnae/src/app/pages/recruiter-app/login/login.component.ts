import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Globals } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../signup/signup.component.scss', './login.component.scss'],
})
export class LoginComponent implements OnInit {
  // Observables/Subscriptions
  httpSub$: Subscription = null;
  model: any = {};
  globals = Globals;
  userData: any;
  typeOfRequestSend: any;
  public emailPattern = this.globals.regex.email;
  @ViewChild('form', { static: false }) form;
  isLoadingResults: boolean = false;
  queryParams: string;
  constructor(
    private httpService: HttpService,
    private ActivatedRoute: ActivatedRoute,
    private router: Router
  ) {
    let item = JSON.parse(localStorage.getItem('UserData'));
    if (item) {
      this.router.navigate(['/recruiter/main'], { queryParamsHandling: 'preserve' });
    }
  }
  ngOnInit() {
    this.ActivatedRoute.queryParams.subscribe(params => {
      this.queryParams = params['calledFromExtension'];
      console.log(this.queryParams);
    });
  }
  submit() {
    const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    const params = {
      emailAddress: this.model['email'],
      type: 'EMAIL',
      calledFrom: isMobile ? 'Mobile-web' : 'Desktop-web',
      accountType: 'recruiter',
    };
     this.isLoadingResults = true;

    this.typeOfRequestSend = this.httpService.postRequest(this.globals.urls.dnnaeWeb.login, params);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        const sendParams = {
          ...params,
          queryParams: this.queryParams,
        };
        if (res.data.isSuccess === true) {
          // this.httpService.showSuccess('', 'Verify Email');
          this.router.navigate(['/recruiter/verify', 'login'], { state: { data: sendParams } });
        } else if (res.data.isSuccess === false) {
          this.httpService.showError(res.data.message);
          this.isLoadingResults = false;
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        this.httpService.showError(err.error);
      }
    );
  }
  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }
}
