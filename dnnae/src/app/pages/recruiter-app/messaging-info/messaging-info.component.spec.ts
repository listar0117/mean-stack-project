import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagingInfoComponent } from './messaging-info.component';

describe('MessagingInfoComponent', () => {
  let component: MessagingInfoComponent;
  let fixture: ComponentFixture<MessagingInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagingInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagingInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
