import { Component, OnInit } from '@angular/core';
import { Globals } from 'src/app/Globals';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-messaging-info',
  templateUrl: './messaging-info.component.html',
  styleUrls: ['../messages-main/messages-main.component.scss', './messaging-info.component.scss'],
})
export class MessagingInfoComponent implements OnInit {
  messagingTemplates: any;
  messagesVarients: any;
  prospect: any;
  prospectData: any;
  firstMessage: boolean = true;
  public globals = Globals;
  isLoadingResults = false;
  prospectValue: any;
  jobID: string;
  constructor(private ActivatedRoute: ActivatedRoute, private router: Router) {
    this.prospectValue = this.ActivatedRoute.snapshot.paramMap.get('id');
    console.log(this.prospectValue);
    this.jobID = this.ActivatedRoute.snapshot.paramMap.get('jobId');
    console.log(this.jobID);
  }

  ngOnInit() {
    this.messagingTemplates = JSON.parse(localStorage.getItem('MessageTemplates'));
    if (!this.messagingTemplates || this.messagingTemplates.length === 0) {
      this.router.navigate(['/recruiter/Message'], { queryParamsHandling: 'preserve' });
    }
    console.log(this.messagingTemplates);
    this.prospectData = this.messagingTemplates.filter(prospect => {
      return prospect.uid == this.prospectValue;
    });
    this.prospect = this.prospectData[0];
  }

  nextMessage(firstMessage) {
    if (firstMessage == true) {
      this.firstMessage = false;
    } else {
      this.router.navigate(['/recruiter/messaging_templates', this.prospect.uid, this.jobID, 1], {
        queryParamsHandling: 'preserve',
      });
    }
  }
}
