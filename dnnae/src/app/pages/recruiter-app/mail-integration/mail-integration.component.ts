import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Globals } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-mail-integration',
  templateUrl: './mail-integration.component.html',
  styleUrls: [
    '../set-connection-request/set-connection-request.component.scss',
    './mail-integration.component.scss',
  ],
})
export class MailIntegrationComponent implements OnInit {
  // Observables/Subscriptions
  httpSub$: Subscription = null;
  model: any = {};
  globals = Globals;
  userData: any;
  typeOfRequestSend: any;
  changeFelid: boolean = false;
  @ViewChild('form', { static: false }) form;
  isLoadingResults: boolean = false;
  public emailPattern = this.globals.regex.gmail;
  constructor(
    private httpService: HttpService,
    private sanitizer: DomSanitizer,
    private router: Router,
    private ActivatedRoute: ActivatedRoute
  ) {
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    if (this.userData.integratedEmailAddress) {
      this.model['email'] = this.userData.integratedEmailAddress;
    }
    // this.jobID = this.ActivatedRoute.snapshot.paramMap.get('id');
    // console.log(this.jobID);
  }
  ngOnInit() {}
  send() {
    const params = {
      loginEmail: this.model['email'],
    };
    this.typeOfRequestSend = this.httpService.postRequest(
      this.globals.urls.dnnaeWeb.emailIntegration.auth,
      params
    );
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === 'TRUE') {
          // this.httpService.showSuccess('', 'Verify Email');
          window.open(
            res.data.authLink,
            '_blank',
            'location=yes,height=570,width=520,scrollbars=yes,status=yes'
          );
          console.log(res);
          this.model['email'] = res.data.loginEmail;
          this.changeFelid = true;
        } else if (res.data.isSuccess === false) {
          this.httpService.showError(res.data.message);
          this.isLoadingResults = false;
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        this.httpService.showError(err.error);
      }
    );
  }
  submit() {
    const params = {
      recruiterID: this.userData.recruiterID,
      integratedEmailAddress: this.model['email'],
      integratedEmailToken: this.model['token'],
    };
    this.typeOfRequestSend = this.httpService.postRequest(
      this.globals.urls.dnnaeWeb.emailIntegration.saveEmailData,
      params
    );
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          this.httpService.showSuccess('Gmail Integration', 'Gmail integrate successfully');
          // console.log(res);
          // this.changeFelid = true;
          this.userData.integratedEmailAddress = this.model['email'];
          localStorage.setItem('UserData', JSON.stringify(this.userData));
          this.router.navigate(['/recruiter'], { queryParamsHandling: 'preserve' });
        } else if (res.data.isSuccess === false) {
          this.httpService.showError(res.data.message);
          this.isLoadingResults = false;
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        this.httpService.showError(err.error);
      }
    );
  }
  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }
}
