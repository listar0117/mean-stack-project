import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http-service';
import { Globals } from 'src/app/Globals';
import { map } from 'rxjs/operators';
import { Subscription, forkJoin } from 'rxjs';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss'],
})
export class InboxComponent implements OnInit {
  private httpSub$: Subscription = null;
  typeOfRequestSend: any;
  public globals = Globals;
  isLoadingResults = true;
  userData: any;
  jobsList: any;
  limit: number = 20;
  scrollDistance = 2;
  throttle = 1000;
  offset: number = 0;
  ProspectsList: any = [];
  messagesList: any;
  tempJobId: string = 'All';
  constructor(private httpService: HttpService) {
    this.userData = JSON.parse(localStorage.getItem('UserData'));
  }

  ngOnInit() {
    this.OnLoadData();
  }

  OnLoadData() {
    const jobObjToSend = {
      recruiterID: this.userData.recruiterID,
    };
    const applicantsObjToSend = {
      recruiterID: this.userData.recruiterID,
      jobID: 'All',
      limit: this.limit,
      offset: this.offset,
    };
    this.httpSub$ = forkJoin(
      this.httpService.postRequest(this.globals.urls.dnnaeWeb.inbox.getJobs, jobObjToSend),
      this.httpService.postRequest(
        this.globals.urls.dnnaeWeb.inbox.getApplicants,
        applicantsObjToSend
      )
    )
      .pipe(
        map(([jobsList, ProspectsList]) => {
          this.jobsList = jobsList.data;
          this.ProspectsList = ProspectsList.data;
        })
      )
      .subscribe(
        data => {
          this.offset = this.offset + 1;
          // this.ProspectsList.applicantArray[0].selected = true;
          if (this.ProspectsList.applicantArray.length > 0) {
            this.getAdditionalInfo(this.ProspectsList.applicantArray[0], 0);
          }
          this.isLoadingResults = false;
          console.log(this.jobsList);
          console.log(this.ProspectsList);
        },
        err => {
          this.isLoadingResults = false;
          this.httpService.showError(err);
        }
      );
  }

  selectUser(UserIndex) {
    this.ProspectsList.applicantArray.map((prospect, index) => {
      if (UserIndex == index) {
        prospect.selected = true;
      } else {
        prospect.selected = false;
      }
    });
  }

  getAdditionalInfo(prospect, ind) {
    const dataToSend = {
      recruiterID: this.userData.recruiterID,
      jobID: prospect.job.jobID,
      entity: prospect.entityUrn,
    };
    this.isLoadingResults = true;
    this.selectUser(ind);
    const requestUrl = this.globals.urls.dnnaeWeb.inbox.getMessages;
    this.typeOfRequestSend = this.httpService.postRequest(requestUrl, dataToSend);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          this.messagesList = res.data.messagesData;
          this.isLoadingResults = false;
          console.log(this.messagesList);
        } else if (res.data.isSuccess === false) {
          this.httpService.showError(res.data.message);
          this.isLoadingResults = false;
          // this.httpService.navigate(['/recruiter']);
        }
      },
      err => {
        if (err.error) console.log(err);
        // this.service.showError(err.error.message);
        this.isLoadingResults = false;
      }
    );
  }
  getJobProspects(job, comingFrom) {
    this.isLoadingResults = true;
    if (job.jobID == this.tempJobId && comingFrom == '1') {
      return;
    }
    if (
      this.ProspectsList.applicantArray.length >= this.ProspectsList.TotalApplicants &&
      comingFrom == '2'
    ) {
      return;
    }
    if (job.jobID !== this.tempJobId) {
      this.offset = 0;
    }
    const applicantsObjToSend = {
      recruiterID: this.userData.recruiterID,
      jobID: job.jobID,
      limit: this.limit,
      offset: this.offset,
    };
    const requestUrl = this.globals.urls.dnnaeWeb.inbox.getApplicants;
    this.typeOfRequestSend = this.httpService.postRequest(requestUrl, applicantsObjToSend);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          console.log(res.data);
          if (job.jobID !== this.tempJobId) {
            this.ProspectsList = res.data;
          } else {
            this.ProspectsList.applicantArray.push(...res.data.applicantArray);
          }
          if (this.ProspectsList.applicantArray.length > 0) {
            this.getAdditionalInfo(this.ProspectsList.applicantArray[0], 0);
          }
          else{
            if(this.messagesList){
              this.messagesList.messagesArray = [];
            }
          }
          this.isLoadingResults = false;
          this.offset = this.offset + 1;
          this.tempJobId = job.jobID;
          console.log(this.messagesList);
        } else if (res.data.isSuccess === false) {
          this.httpService.showError(res.data.message);
          this.isLoadingResults = false;
          // this.httpService.navigate(['/recruiter']);
        }
      },
      err => {
        if (err.error) console.log(err);
        // this.service.showError(err.error.message);
        this.isLoadingResults = false;
      }
    );
  }
}
