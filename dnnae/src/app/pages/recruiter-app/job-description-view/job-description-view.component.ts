import { Component, OnInit } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Message } from '../models/message';
import { Globals } from 'src/app/Globals';
import { Router, ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { HttpService } from 'src/app/services/http-service';
@Component({
  selector: 'app-job-description-view',
  templateUrl: './job-description-view.component.html',
  styleUrls: ['../new-job/new-job.component.scss', './job-description-view.component.scss'],
})
export class JobDescriptionViewComponent implements OnInit {
  public message: Message;
  public messages: Message[];
  public buttons: any;
  userJobDescription: any;
  globals = Globals;
  userData: any;
  PdfData: any;
  jobTitle: any;
  jobLocation: any;
  typeOfRequestSend: any;
  // Observables/Subscriptions
  httpSub$: Subscription = null;
  isLoadingResults: boolean = false;
  jobDescription: any;
  comingFrom: any;
  SliderText: String = 'None';
  constructor(
    private httpService: HttpService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.message = new Message('', 'assets/images/user.png');
    this.messages = [
      new Message('Here are the details of the job', 'assets/images/bot.png', new Date()),
    ];
  }
  ngOnInit() {
    this.route.params.subscribe(value => {
      console.log(value);
      this.comingFrom = value.from;
    });
    if (this.comingFrom == 'pdf') {
      this.PdfData = JSON.parse(sessionStorage.getItem('pdfData'));
      this.formateData(this.PdfData);
      this.createJD(this.PdfData);
    } else if (this.comingFrom == 'text') {
      this.getExtractedJD();
    }
    this.userData = JSON.parse(localStorage.getItem('UserData'));
  }

  getExtractedJD() {
    let item = JSON.parse(sessionStorage.getItem('tempJobData'));
    console.log(Object.values(item).join('..'));
    const formData = new FormData();
    formData.append('text', Object.values(item).join('..'));
    this.isLoadingResults = true;
    this.httpSub$ = this.httpService
      .postRequest(this.globals.urls.dnnaeWeb.extract, formData, {}, false)
      .pipe(map(res => res.data))
      .subscribe(
        data => {
          if (data.isSuccess == false) {
            this.isLoadingResults = false;
            sessionStorage.removeItem('pdfData');
            sessionStorage.removeItem('tempJobData');
            this.router.navigate(['/recruiter/newJob'], { queryParamsHandling: 'preserve' });
            this.httpService.showError('Unable to extract data from document');
          } else {
            this.isLoadingResults = false;
            this.createJD(data);
            console.log(data);
            this.formateData(data);
            // this.httpService.showSuccess('User signed Up successfully', 'Sign Up');
            // this.router.navigate(['/verify']);
          }
        },
        err => {
          console.log(err);
          this.isLoadingResults = false;
          sessionStorage.removeItem('pdfData');
          this.router.navigate(['/recruiter/newJob'], { queryParamsHandling: 'preserve' });
          this.httpService.showError('Unable to Read Data from Document');
        }
      );
  }
  formateData(data) {
    data.map(userData => {
      userData.tempData = [];
      userData.disabled = true;
      if (userData.data.length == 0) {
        if (userData.tag == 'TECHNICAL_SKILLS') {
          let obj = {
            data: '',
            disable: true,
            score: 0,
            scoreText: 'None',
          };
          userData.tempData.push(obj);
        } else {
          let obj = {
            data: '',
            disable: true,
          };
          userData.tempData.push(obj);
        }
      }
      userData.data.map(valueData => {
        if (userData.tag == 'TECHNICAL_SKILLS') {
          let obj = {
            ...valueData,
            disable: true,
            score: 0,
            scoreText: 'None',
          };
          userData.tempData.push(obj);
        } else {
          let obj = {
            data: valueData,
            disable: true,
          };
          userData.tempData.push(obj);
        }
      });
    });
    this.userJobDescription = data;
  }
  createJD(dataCopy) {
    const formData = new FormData();
    formData.append('entities', JSON.stringify(dataCopy));
    this.isLoadingResults = true;
    this.httpSub$ = this.httpService
      .postRequest(this.globals.urls.dnnaeWeb.createJD, formData, {}, false)
      .pipe(map(res => res.data))
      .subscribe(
        data => {
          this.isLoadingResults = false;
          this.jobDescription = data;
          // this.httpService.showSuccess('Job Created successfully', 'Job Created');
          // this.router.navigate(['/recruiter/main']);
        },
        err => {
          this.isLoadingResults = false;
          this.httpService.showError(err);
        }
      );
  }

  modifyData() {
    const dataCopy = JSON.parse(JSON.stringify(this.userJobDescription));
    const modifiedData = dataCopy.map(userData => {
      if (userData.tempData) {
        let tempArray = [];
        userData.tempData.map(valueData => {
          if (userData.tag == 'TECHNICAL_SKILLS') {
            // let tempData ={
            //   data:valueData.data,
            //   score:valueData.score,
            // }
            delete valueData.scoreText;
            tempArray.push(valueData);
            userData.data = tempArray;
          } else {
            tempArray.push(valueData.data);
            userData.data = tempArray;
          }
        });
        delete userData.disabled;
        delete userData.tempData;
        return dataCopy;
      }
    });
    console.log(dataCopy);
    this.saveJob(dataCopy);
  }

  saveJob(jobData) {
    // console.log(this.userData);
    console.log(jobData);
    this.jobTitle = jobData.filter(valueData => valueData.tag == 'JOB_POSITION');
    this.jobLocation = jobData.filter(valueData => valueData.tag == 'JOB_LOCATION');
    jobData.map(jobObject => {
      this.jobDescription.forEach(element => {
        if (element.tag == jobObject.tag) {
          if (element.tag == 'TECHNICAL_SKILLS') {
            element.data.splice(0, element.data.length);
            jobObject.data.forEach(skill => {
              element.data.push(skill.name);
            });
          } else {
            element.data = jobObject.data;
          }
        }
      });
    });
    const dataToSend = {
      recruiterID: this.userData.recruiterID,
      jobTitle: this.jobTitle[0].data[0],
      companyName: this.userData.signupCompanyName,
      jobLocation: this.jobLocation[0].data[0],
      jdType: 'TEXT',
      jobArray: jobData,
      jobDescription: this.jobDescription,
    };
    console.log('data to send', dataToSend);

    this.typeOfRequestSend = this.httpService.postRequest(
      this.globals.urls.dnnaeWeb.createJob,
      dataToSend
    );
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          this.isLoadingResults = false;
          this.httpService.showSuccess('Job created successfully.', 'Job Created');
          this.router.navigate(['/recruiter/main'], { queryParamsHandling: 'preserve' });
        } else if (res.data.isSuccess === false) {
          this.httpService.showError(res.data.message);
          this.isLoadingResults = false;
          // this.router.navigate(['/']);
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        // this.service.showError(err.error.message);
      }
    );
  }

  // enable and disbale already added fields
  editFieldsEnable(event, data, index, tempDataIndex) {
    // data.disable =  !data.disable;
    this.userJobDescription[index].tempData[tempDataIndex].disable = !this.userJobDescription[index]
      .tempData[tempDataIndex].disable;
    console.log(tempDataIndex);
  }
  addNewReason(event, data, index) {
    // data.disable =  !data.disable;
    this.userJobDescription[index].tempData.push({ data: '', disable: false });
    console.log(this.userJobDescription);
  }
  score(event, data, index, tempDataIndex) {
    console.log(event);
    switch (event.value.toString()) {
      case '0': {
        this.userJobDescription[index].tempData[tempDataIndex].scoreText = 'None';
        break;
      }
      case '1': {
        this.userJobDescription[index].tempData[tempDataIndex].scoreText = 'Not Important';
        break;
      }
      case '2': {
        this.userJobDescription[index].tempData[tempDataIndex].scoreText = 'Nice to Have';
        break;
      }
      case '3': {
        this.userJobDescription[index].tempData[tempDataIndex].scoreText = 'Important';
        break;
      }
      case '4': {
        this.userJobDescription[index].tempData[tempDataIndex].scoreText = 'Very Important';
        break;
      }
      case '5': {
        this.userJobDescription[index].tempData[tempDataIndex].scoreText = 'Must Have';
        break;
      }
      default: {
        this.userJobDescription[index].tempData[tempDataIndex].scoretext = 'None';
        break;
      }
    }
    // console.log(data);
    this.userJobDescription[index].tempData[tempDataIndex].score = event.value;
    console.log(this.userJobDescription);
  }

  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }
}
