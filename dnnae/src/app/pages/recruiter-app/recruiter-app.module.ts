import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { BrowserModule, Title, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { GestureConfig } from '@angular/material';
// import { MatToolbarModule } from '@angular/material';
import { environment } from '../../../environments/environment';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatChipsModule } from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import { Ng5SliderModule } from 'ng5-slider';
import {MatTooltipModule} from '@angular/material/tooltip';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatDialogModule } from '@angular/material/dialog';
import { NgxEditorModule } from 'ngx-editor';
import { RecruiterAppRoutingModule } from './recruiter-app-routing.module';
import { RecruiterAppComponent } from './recruiter-app.component';
import { HomeComponent } from './home/home.component';
import { SignupComponent } from './signup/signup.component';
import { MatProgressSpinnerModule } from '@angular/material';
import { MatSliderModule } from '@angular/material/slider';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainBotComponent } from './main-bot/main-bot.component';
import { MessageItemComponent } from './components/message-item/message-item.component';
import { MessageFormComponent } from './components/message-form/message-form.component';
import { MessageListComponent } from './components/message-list/message-list.component';
import { DialogflowService } from 'src/app/services/dialogflow.service';
import { ButtonsComponent } from './components/buttons/buttons.component';
import { NewJobComponent } from './new-job/new-job.component';
import { JobsComponent } from './jobs/jobs.component';
import { SettingsComponent } from './settings/settings.component';
import { ProspectsComponent } from './prospects/prospects.component';
import { NewJobDescriptionComponent } from './new-job-description/new-job-description.component';
import { UsePDFComponent } from './use-pdf/use-pdf.component';
import { JobDescriptionViewComponent } from './job-description-view/job-description-view.component';
import { SkillsListComponent } from './components/skills-list/skills-list.component';
import { LoginComponent } from './login/login.component';
import { VerifyComponent } from './verify/verify.component';
import { JobDescriptionSummaryComponent } from './job-description-summary/job-description-summary.component';
import { ProspectsListComponent } from './prospects-list/prospects-list.component';
import { ProspectsDetailsComponent } from './prospects-details/prospects-details.component';
import { HeaderComponent } from './components/header/header.component';
import { RouteGuard } from 'src/app/guards/route.guard';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { SetConnectionRequestComponent } from './set-connection-request/set-connection-request.component';
import { MessagesMainComponent } from './messages-main/messages-main.component';
import { MessagingInfoComponent } from './messaging-info/messaging-info.component';
import { MessagingTemplatesComponent } from './messaging-templates/messaging-templates.component';
import { NlpIntentComponent } from './nlp-intent/nlp-intent.component';
import { PrettyPrintPipe } from 'src/app/pipes/pretty-print.pipe';
import { NlpVectorComponent } from './nlp-vector/nlp-vector.component';
import { TextEditorComponent } from './createJobWithText/text-editor/text-editor.component';
import { JobTitleComponent } from './createJobWithText/job-title/job-title.component';
import { CompanySpecialitiesComponent } from './createJobWithText/company-specialities/company-specialities.component';
import { ExperienceComponent } from './createJobWithText/experience/experience.component';
import { DegreesComponent } from './createJobWithText/degrees/degrees.component';
import { TechnicalSkillsComponent } from './createJobWithText/technical-skills/technical-skills.component';
import { OthersComponent } from './createJobWithText/others/others.component';
import { ConfirmDialougComponent } from '../components/confirm-dialoug/confirm-dialoug.component';
import { DegreesCombinedComponent } from './createJobWithText/degrees-combined/degrees-combined.component';
import { ChatTemplateComponent } from './components/chat-template/chat-template.component';
import { CalenderLinkComponent } from './calender-link/calender-link.component';
import { CompanyNameComponent } from './createJobWithText/company-name/company-name.component';
import { GooglePlacesDirective } from 'src/app/directives/google-places.directive';
import { AgmCoreModule } from '@agm/core';
import { JobStepperComponent } from './components/job-stepper/job-stepper.component';
import { InboxComponent } from './inbox/inbox.component';
import { MailIntegrationComponent } from './mail-integration/mail-integration.component';
import { SwitchToExtensionComponent } from './switch-to-extension/switch-to-extension.component';
// import { MessageItemComponent, MessageFormComponent, MessageListComponent } from './components';

@NgModule({
  imports: [
    MatCardModule,
    CommonModule,
    InfiniteScrollModule,
    RecruiterAppRoutingModule,
    MatProgressSpinnerModule,
    FormsModule,
    Ng5SliderModule,
    NgxEditorModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatSliderModule,
    MatChipsModule,
    MatIconModule,
    NgbModule,
    MatDialogModule,
    MatTooltipModule,
    MatAutocompleteModule,
    AgmCoreModule.forRoot({
      apiKey: environment.mapApiKey,
      libraries: ['places', 'drawing', 'geometry'],
    }),
  ],
  exports: [
    MatProgressSpinnerModule,
    MatChipsModule,
    MatIconModule,
    MatExpansionModule,
    MatDialogModule,
    MatSliderModule,
    MatTooltipModule,
    MatAutocompleteModule,
    AgmCoreModule
  ],
  providers: [{ provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig }, RouteGuard],

  // providers: [
  //   DialogflowService
  // ],

  declarations: [
    HeaderComponent,
    GooglePlacesDirective,
    RecruiterAppComponent,
    HomeComponent,
    SignupComponent,
    MainBotComponent,
    MessageListComponent,
    MessageFormComponent,
    MessageItemComponent,
    ButtonsComponent,
    NewJobComponent,
    JobsComponent,
    SettingsComponent,
    ProspectsComponent,
    NewJobDescriptionComponent,
    UsePDFComponent,
    JobDescriptionViewComponent,
    SkillsListComponent,
    LoginComponent,
    VerifyComponent,
    JobDescriptionSummaryComponent,
    ProspectsListComponent,
    ProspectsDetailsComponent,
    ProfileEditComponent,
    SetConnectionRequestComponent,
    MessagesMainComponent,
    MessagingInfoComponent,
    MessagingTemplatesComponent,
    NlpIntentComponent,
    PrettyPrintPipe,
    NlpVectorComponent,
    TextEditorComponent,
    JobTitleComponent,
    CompanySpecialitiesComponent,
    ExperienceComponent,
    DegreesComponent,
    TechnicalSkillsComponent,
    OthersComponent,
    ConfirmDialougComponent,
    DegreesCombinedComponent,
    ChatTemplateComponent,
    CalenderLinkComponent,
    CompanyNameComponent,
    JobStepperComponent,
    InboxComponent,
    MailIntegrationComponent,
    SwitchToExtensionComponent,
  ],
  entryComponents: [ConfirmDialougComponent],
})
export class RecruiterAppModule {}
