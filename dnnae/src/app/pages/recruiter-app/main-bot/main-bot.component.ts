import { Component, OnInit } from '@angular/core';
import { Message } from '../models/message';
import { Buttons } from '../models';
import { Globals } from 'src/app/Globals';
import { AppLogOutService } from 'src/app/services/app-log-out.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-main-bot',
  templateUrl: './main-bot.component.html',
  styleUrls: ['./main-bot.component.scss'],
})
export class MainBotComponent implements OnInit {
  public message: Message;
  public messages: Message[];
  public buttons: any;
  globals = Globals;
  calledFromExtension: string;
  showExtensionMessage: boolean = false;
  constructor(
    private router: Router,
    private ActivatedRoute: ActivatedRoute,
    private autoLogout: AppLogOutService,
  ) {
    this.ActivatedRoute.queryParams.subscribe(params => {
      this.calledFromExtension = params['calledFromExtension'];
      console.log(this.calledFromExtension);
    });
    if (this.calledFromExtension == '1') {
      this.showExtensionMessage = true;
    }
    this.message = new Message('', 'assets/images/user.png');
    this.messages = [
      new Message(
        'Hi! I am your personal assistant. What can I help you with today? You can write it down or select one of the options below.',
        'assets/images/bot.png',
        new Date()
      ),
    ];

    this.buttons = [
      new Buttons('New Job', '/recruiter/newJob'),
      new Buttons('Jobs', '/recruiter/jobs'),
      new Buttons('Setings', '/recruiter/settings'),
      new Buttons('Prospects', '/recruiter/prospects'),
    ];
  }

  ngOnInit() {
   
  }
}
