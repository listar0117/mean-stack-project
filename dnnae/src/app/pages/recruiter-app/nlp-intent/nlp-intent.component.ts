import { Component, OnInit } from '@angular/core';
import { Globals } from 'src/app/Globals';
import { Router, ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { HttpService } from 'src/app/services/http-service';

@Component({
  selector: 'app-nlp-intent',
  templateUrl: './nlp-intent.component.html',
  styleUrls: ['./nlp-intent.component.scss']
})
export class NlpIntentComponent implements OnInit {
  httpSub$: Subscription = null;
  isLoadingResults: boolean = false;
  globals = Globals;
  textToSend:string;
  receivedData:any;
  constructor( private httpService: HttpService,) { }

  ngOnInit() {
  }
  send() {
    const formData = new FormData();
    formData.append('message',this.textToSend);
    this.isLoadingResults = true;
    this.httpSub$ = this.httpService
      .postRequest(this.globals.urls.temp.nlpIntent, formData, {}, false)
      .pipe(map(res => res.data))
      .subscribe(
        data => {
          if(data.isSuccess==false){
            this.isLoadingResults = false;
            this.httpService.showError('Unable to extract intent');
          }
          else{
            this.isLoadingResults = false;
            this.receivedData = data;
            console.log(data);
          }
        },
        err => {
          console.log(err)
          this.isLoadingResults = false;
          sessionStorage.removeItem('pdfData');
          this.httpService.showError('Unable to extract intent');
        }
      );
  }
}
