import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NlpIntentComponent } from './nlp-intent.component';

describe('NlpIntentComponent', () => {
  let component: NlpIntentComponent;
  let fixture: ComponentFixture<NlpIntentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NlpIntentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NlpIntentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
