import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsePDFComponent } from './use-pdf.component';

describe('UsePDFComponent', () => {
  let component: UsePDFComponent;
  let fixture: ComponentFixture<UsePDFComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsePDFComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsePDFComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
