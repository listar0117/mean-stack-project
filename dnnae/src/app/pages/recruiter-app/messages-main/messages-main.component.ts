import { Component, OnInit } from '@angular/core';
import { Globals } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-messages-main',
  templateUrl: './messages-main.component.html',
  styleUrls: ['./messages-main.component.scss'],
})
export class MessagesMainComponent implements OnInit {
  messagingTemplates: any;
  messagesVarients: any;
  public globals = Globals;
  isLoadingResults = false;
  userData: any;
  requestUrl: string;
  typeOfRequestSend: any;
  jobID: string;
  constructor(
    private service: HttpService,
    private router: Router,
    private ActivatedRoute: ActivatedRoute
  ) {
    this.jobID = this.ActivatedRoute.snapshot.paramMap.get('id');
    console.log(this.jobID);
  }

  ngOnInit() {
    // this.messagingTemplates = this.globals.messagingTemplates;
    // console.log(this.messagingTemplates);

    let item = JSON.parse(localStorage.getItem('UserData'));
    this.userData = item;
    this.getMessagingTemplates(item);
  }
  getMessagingTemplates(item) {
    const dataToSend = {
      jobID: this.jobID,
      recruiterID: item.recruiterID,
    };
    this.isLoadingResults = true;
    this.requestUrl = this.globals.urls.dnnaeWeb.getBatchMessageSettings;
    this.typeOfRequestSend = this.service.postRequest(this.requestUrl, dataToSend);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          console.log(res);
          this.messagingTemplates = res.data.sampleMessage;
          localStorage.setItem('MessageTemplates', JSON.stringify(this.messagingTemplates));
          this.isLoadingResults = false;
        } else if (res.data.isSuccess === false) {
          this.service.showError('Something went wrong, Please try again');
          this.isLoadingResults = false;
          this.router.navigate(['/recruiter/settings'], { queryParamsHandling: 'preserve' });
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        // this.service.showError(err.error.message);
      }
    );
  }
}
