import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobDescriptionSummaryComponent } from './job-description-summary.component';

describe('JobDescriptionSummaryComponent', () => {
  let component: JobDescriptionSummaryComponent;
  let fixture: ComponentFixture<JobDescriptionSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobDescriptionSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobDescriptionSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
