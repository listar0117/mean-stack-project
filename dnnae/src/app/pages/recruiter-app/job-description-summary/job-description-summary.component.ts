import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Globals } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-job-description-summary',
  templateUrl: './job-description-summary.component.html',
  styleUrls: ['./job-description-summary.component.scss'],
})
export class JobDescriptionSummaryComponent implements OnInit {
  jobsData: any;
  isLoadingResults = true;
  jobs: any;
  requestUrl: string;
  public globals = Globals;
  typeOfRequestSend: any;
  dataToSend: any;
  userData: any;
  job: any;
  constructor(
    private service: HttpService,
    private sanitizer: DomSanitizer,
    private router: Router,
    private ActivatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    // this.userData = JSON.parse(localStorage.getItem('UserData'));
    const val = this.ActivatedRoute.snapshot.paramMap.get('id');
    console.log(val);
    this.getJobData(val);
  }
  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
  /* To copy any Text */
  copyText(val: string) {
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.service.showSuccess('Link Copied Successfully', 'Copied to Clipboard');
  }
  getJobData(val) {
    this.dataToSend = {
      jobID: val,
      // recruiterID: 2
    };
    this.requestUrl = this.globals.urls.getJobData;
    this.typeOfRequestSend = this.service.postRequest(this.requestUrl, this.dataToSend);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          this.job = res.data.job;
          this.isLoadingResults = false;
          // this.service.showSuccess('Email sent successfully.', 'Forgot Password');
          // this.router.navigate(['/login']);
        } else if (res.data.isSuccess === false) {
          // this.service.showError(res.data.message)
          this.isLoadingResults = false;
          this.router.navigate(['/recruiter/jobs'], { queryParamsHandling: 'preserve' });
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        // this.service.showError(err.error.message);
      }
    );
  }
}
