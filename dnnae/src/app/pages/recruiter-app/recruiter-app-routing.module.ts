import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RecruiterAppComponent } from './recruiter-app.component';
import { SignupComponent } from './signup/signup.component';
import { MainBotComponent } from './main-bot/main-bot.component';
import { NewJobComponent } from './new-job/new-job.component';
import { JobsComponent } from './jobs/jobs.component';
import { SettingsComponent } from './settings/settings.component';
import { ProspectsComponent } from './prospects/prospects.component';
import { NewJobDescriptionComponent } from './new-job-description/new-job-description.component';
import { UsePDFComponent } from './use-pdf/use-pdf.component';
import { JobDescriptionViewComponent } from './job-description-view/job-description-view.component';
import { LoginComponent } from './login/login.component';
import { VerifyComponent } from './verify/verify.component';
import { JobDescriptionSummaryComponent } from './job-description-summary/job-description-summary.component';
import { ProspectsDetailsComponent } from './prospects-details/prospects-details.component';
import { RouteGuard } from 'src/app/guards/route.guard';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { SetConnectionRequestComponent } from './set-connection-request/set-connection-request.component';
import { MessagesMainComponent } from './messages-main/messages-main.component';
import { MessagingInfoComponent } from './messaging-info/messaging-info.component';
import { MessagingTemplatesComponent } from './messaging-templates/messaging-templates.component';
import { NlpIntentComponent } from './nlp-intent/nlp-intent.component';
import { NlpVectorComponent } from './nlp-vector/nlp-vector.component';
import { TextEditorComponent } from './createJobWithText/text-editor/text-editor.component';
import { JobTitleComponent } from './createJobWithText/job-title/job-title.component';
import { CompanySpecialitiesComponent } from './createJobWithText/company-specialities/company-specialities.component';
import { ExperienceComponent } from './createJobWithText/experience/experience.component';
import { DegreesComponent } from './createJobWithText/degrees/degrees.component';
import { TechnicalSkillsComponent } from './createJobWithText/technical-skills/technical-skills.component';
import { OthersComponent } from './createJobWithText/others/others.component';
import { DegreesCombinedComponent } from './createJobWithText/degrees-combined/degrees-combined.component';
import { CalenderLinkComponent } from './calender-link/calender-link.component';
import { CompanyNameComponent } from './createJobWithText/company-name/company-name.component';
import { InboxComponent } from './inbox/inbox.component';
import { MailIntegrationComponent } from './mail-integration/mail-integration.component';

const routes: Routes = [
  {
    path: '',
    component: RecruiterAppComponent,
    // canActivate: [PasswordGuardGuard],
    children: [
      {
        path: 'signup',
        component: SignupComponent,
        // pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'verify/:verifyType',
        component: VerifyComponent,
      },
      {
        path: 'home',
        component: HomeComponent,
        // canActivate: [RouteGuard],
      },
      {
        path: 'main',
        component: MainBotComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'newJob',
        component: NewJobComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'jobs',
        component: JobsComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'settings/:id',
        component: SettingsComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'Message/:id',
        component: MessagesMainComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'MessageInfo/:id/:jobId',
        component: MessagingInfoComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'messaging_templates/:id/:jobId/:step',
        component: MessagingTemplatesComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'profile',
        component: ProfileEditComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'connections/:id',
        component: SetConnectionRequestComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'calender_link',
        component: CalenderLinkComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'mail_integration',
        component: MailIntegrationComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'prospects/:id',
        component: ProspectsComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'newJobDescription',
        component: NewJobDescriptionComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'jobDescription/:from',
        component: JobDescriptionViewComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'jdSummary/:id',
        component: JobDescriptionSummaryComponent,
        canActivate: [RouteGuard],
      },
      // {
      //   path: 'prospectDetails',
      //   component: ProspectsDetailsComponent,
      //   canActivate: [RouteGuard],
      // },
      {
        path: 'prospectDetails/:jobId/:entity',
        component: ProspectsDetailsComponent,
        canActivate: [RouteGuard],
      },
      // create job with text
      {
        path: 'textJob',
        component: TextEditorComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'jobTitle/:id/:jdType',
        component: JobTitleComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'companySpecialties/:id/:jdType',
        component: CompanySpecialitiesComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'companyName/:id/:jdType',
        component: CompanyNameComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'newJobExperience/:id/:jdType',
        component: ExperienceComponent,
        canActivate: [RouteGuard],
      },
      // {
      //   path: 'newJobDegree/:id',
      //   component: DegreesComponent,
      //   canActivate: [RouteGuard],
      // },
      {
        path: 'newJobDegree/:id/:jdType',
        component: DegreesCombinedComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'technicalSkills/:id/:jdType',
        component: TechnicalSkillsComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'others/:id/:jdType',
        component: OthersComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'inbox',
        component: InboxComponent,
        canActivate: [RouteGuard],
      },
      {
        path: 'nlpIntent',
        component: NlpIntentComponent,
      },
      {
        path: 'vector',
        component: NlpVectorComponent,
      },
      {
        // need to delete might not be in use
        path: 'usePDF',
        component: UsePDFComponent,
        canActivate: [RouteGuard],
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '**',
    component: HomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecruiterAppRoutingModule {}
