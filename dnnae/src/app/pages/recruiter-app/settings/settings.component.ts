import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Globals } from 'src/app/Globals';
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['../new-job/new-job.component.scss', './settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  isLoadingResults = false;
  jobID: string;
  globals = Globals;
  constructor(private ActivatedRoute: ActivatedRoute) {
    this.jobID = this.ActivatedRoute.snapshot.paramMap.get('id');
    console.log(this.jobID);
  }

  ngOnInit() {}
}
