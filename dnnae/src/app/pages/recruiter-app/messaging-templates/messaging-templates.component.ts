import { Component, OnInit } from '@angular/core';
import { Globals } from 'src/app/Globals';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from 'src/app/services/http-service';
@Component({
  selector: 'app-messaging-templates',
  templateUrl: './messaging-templates.component.html',
  styleUrls: [
    '../messages-main/messages-main.component.scss',
    './messaging-templates.component.scss',
  ],
})
export class MessagingTemplatesComponent implements OnInit {
  prospectValue: any;
  prospect: any;
  prospectTemp: any;
  prospectData: any;
  public globals = Globals;
  messagingTemplates: any;
  isLoadingResults = false;
  userData: any;
  requestUrl: string;
  typeOfRequestSend: any;
  jobID: string;
  prospectBasicData: Object;
  messageStep: string;
  arrayToUse: any;
  titleToSend: string;
  constructor(
    private ActivatedRoute: ActivatedRoute,
    private router: Router,
    private service: HttpService
  ) {
    this.prospectValue = this.ActivatedRoute.snapshot.paramMap.get('id');
    console.log(this.prospectValue);
    this.jobID = this.ActivatedRoute.snapshot.paramMap.get('jobId');
    this.messageStep = this.ActivatedRoute.snapshot.paramMap.get('step');
    console.log(this.jobID);
  }

  ngOnInit() {
    console.log('sdsdff');
    let item = JSON.parse(localStorage.getItem('UserData'));
    this.userData = item;
    this.messagingTemplates = JSON.parse(localStorage.getItem('MessageTemplates'));

    if (!this.messagingTemplates || this.messagingTemplates.length === 0) {
      this.router.navigate(['/recruiter/Message'], { queryParamsHandling: 'preserve' });
    }
    console.log(this.messagingTemplates);
    this.prospectData = this.messagingTemplates.filter(prospect => {
      return prospect.uid == this.prospectValue;
    });
    this.prospect = this.prospectData[0];
    this.prospectBasicData = {
      name: this.prospect.name,
      uid: this.prospect.uid,
      score: this.prospect.score,
      image: this.prospect.image,
      connectionType: this.prospect.connectionType,
    };

    this.wizardStep(this.messageStep);
    // this.prospectTemp = this.prospect.interestedThread.map(messageObj => {
    //   messageObj.disabled = true;
    // });

    // if (this.prospect.NoReplyThread && this.prospect.NoReplyThread.length > 0) {
    //   this.prospectTemp = this.prospect.NoReplyThread.map(messageObj => {
    //     messageObj.disabled = true;
    //   });
    // }
    // console.log(this.prospect);
  }

  // edit(message, index) {
  //   this.prospect.interestedThread[index].disabled = !this.prospect.interestedThread[index]
  //     .disabled;
  // }
  // editNR(message, index) {
  //   this.prospect.NoReplyThread[index].disabled = !this.prospect.NoReplyThread[index].disabled;
  // }
  // save(){
  //   console.log(this.prospect);
  // }
  wizardStep(step) {
    const stepToUse = parseInt(step);
    if (stepToUse == 1) {
      // if (this.prospect.uid !== 2 && this.prospect.uid !== 3) {
      this.arrayToUse = this.prospect.NoReplyThread24;
      this.titleToSend = 'No response within 24 hours';
      // } else {
      //   this.arrayToUse = this.prospect.interestedThread;
      //   this.titleToSend='Prospect is interested';
      // }
    } else if (stepToUse == 2) {
      // if (this.prospect.uid !== 2 && this.prospect.uid !== 3) {
      this.arrayToUse = this.prospect.NoReplyThread48;
      this.titleToSend = 'No response within 48 hours';
      // } else {
      //   this.arrayToUse = this.prospect.notInterestedThread;
      //   this.titleToSend='Prospect is not interested';
      // }
    } else if (stepToUse == 3) {
      this.arrayToUse = this.prospect.NoReplyThread7;
      this.titleToSend = 'No response within 7 days';
    } else if (stepToUse == 4) {
      this.arrayToUse = this.prospect.interestedThread;
      this.titleToSend = 'Prospect is interested';
    } else if (stepToUse == 5) {
      this.arrayToUse = this.prospect.notInterestedThread;
      this.titleToSend = 'Prospect is not interested';
    }

    this.arrayToUse.map(messageObj => {
      messageObj.disabled = true;
    });
  }

  save() {
    this.messagingTemplates.map(prospect => {
      if (prospect.uid == this.prospectValue) prospect = this.prospect;
    });

    const dataToSend = {
      recruiterID: this.userData.recruiterID,
      sampleMessage: this.messagingTemplates,
      jobID: this.jobID,
    };
    this.requestUrl = this.globals.urls.dnnaeWeb.setBatchMessageSettings;
    this.typeOfRequestSend = this.service.postRequest(this.requestUrl, dataToSend);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          if (parseInt(this.messageStep) < 5) {
            localStorage.setItem('MessageTemplates', JSON.stringify(this.messagingTemplates));
            this.messageStep = JSON.stringify(parseInt(this.messageStep) + 1);
            // if (this.prospect.uid !== 2 && this.prospect.uid !== 3) {
            this.wizardStep(this.messageStep);
            this.router.navigate(
              [
                '/recruiter/messaging_templates',
                this.prospect.uid,
                this.jobID,
                parseInt(this.messageStep),
              ],
              { queryParamsHandling: 'preserve' }
            );
            // } else {
            // if (parseInt(this.messageStep) < 3) {
            //   this.wizardStep(this.messageStep);
            //   this.router.navigate([
            //     '/recruiter/messaging_templates',
            //     this.prospect.uid,
            //     this.jobID,
            //     parseInt(this.messageStep),
            //   ]);
            // } else {
            // this.service.showSuccess(
            //   'Message template updated Successfully',
            //   'Template Updated'
            // );
            // this.isLoadingResults = false;
            // localStorage.removeItem('MessageTemplates');

            // this.router.navigate(['/recruiter/Message', this.jobID]);
            // }
            // }
          } else {
            this.service.showSuccess('Message template updated Successfully', 'Template Updated');
            this.isLoadingResults = false;
            localStorage.removeItem('MessageTemplates');

            this.router.navigate(['/recruiter/Message', this.jobID], {
              queryParamsHandling: 'preserve',
            });
          }
        } else if (res.data.isSuccess === false) {
          this.service.showError('Something went wrong, Please try again');
          this.isLoadingResults = false;
          this.router.navigate(['/recruiter/Message', this.jobID], {
            queryParamsHandling: 'preserve',
          });
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        // this.service.showError(err.error.message);
      }
    );
  }
}
