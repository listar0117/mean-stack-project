import { Component, OnInit } from '@angular/core';
import { Message } from '../models/message';
import { Buttons } from '../models';
import { Globals } from 'src/app/Globals';
import { Subscription } from 'rxjs';
import { HttpService } from 'src/app/services/http-service';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-new-job',
  templateUrl: './new-job.component.html',
  styleUrls: ['./new-job.component.scss'],
})
export class NewJobComponent implements OnInit {
  public message: Message;
  public messages: Message[];
  public buttons: any;
  globals = Globals;
  // Observables/Subscriptions
  httpSub$: Subscription = null;
  // variable type null
  fileToUpload: File = null;
  isLoadingResults: boolean = false;
  constructor(private httpService: HttpService, private router: Router) {
    // this.message = new Message('', 'assets/images/user.png');
    // this.messages = [
    //   new Message('Please select one of the options below', 'assets/images/bot.png', new Date())
    // ];
    // this.buttons = [
    //   new Buttons('New Job Description','/recruiter/newJobDescription'),
    //   // new Buttons('Use PDF','/recruiter/usePDF'),
    // ]
  }

  ngOnInit() {}

  // Upload Package Image
  uploadFile(files: FileList) {
    this.fileToUpload = files.item(0);
    // var reader = new FileReader();
    // this.imagePath = files;
    // reader.readAsDataURL(files[0]);
    // reader.onload = (_event) => {
    //   this.imgURL = reader.result;
    // }
    this.extractJDFromPDF();
  }
  extractJDFromPDF() {
    const formData = new FormData();
    formData.append('file', this.fileToUpload);
    this.isLoadingResults = true;
    this.httpSub$ = this.httpService
      .postRequest(this.globals.urls.dnnaeWeb.extract, formData, {}, false)
      .pipe(map(res => res.data))
      .subscribe(
        data => {
          this.isLoadingResults = false;
          console.log(data);
          // this.createJD(data.entities);

          sessionStorage.setItem('textExtractedJD', JSON.stringify(data.entities));
          // sessionStorage.setItem('textCreatedJD', JSON.stringify(data));
          sessionStorage.setItem('jdText', JSON.stringify(data.pdf_text));
          sessionStorage.setItem('jobSource', JSON.stringify('file'));
          sessionStorage.removeItem('stepperData');
          this.router.navigate(['/recruiter/jobTitle', 'add', 'file'], {
            queryParamsHandling: 'preserve',
          });

          // sessionStorage.setItem('textExtractedJD', JSON.stringify(data));
          // this.httpService.showSuccess('User signed Up successfully, verify Email', 'Verify Email');
          // this.router.navigate(['/recruiter/verify','signUp'], { state: { data: params } });
          // this.router.navigate(['/recruiter/jobDescription','pdf']);
          // this.router.navigate(['/recruiter/jobTitle']);
        },
        err => {
          this.isLoadingResults = false;
          this.httpService.showError(err);
        }
      );
  }

  //should not be used here
  createJD(dataCopy) {
    const formData = new FormData();
    formData.append('entities', JSON.stringify(dataCopy));
    this.isLoadingResults = true;
    this.httpSub$ = this.httpService
      .postRequest(this.globals.urls.dnnaeWeb.createJD, formData, {}, false)
      .pipe(map(res => res.data))
      .subscribe(
        data => {
          if (data.isSuccess == false) {
            this.isLoadingResults = false;
            this.router.navigate(['/recruiter/newJob'], { queryParamsHandling: 'preserve' });
            this.httpService.showError('Unable to generate Job Description');
          } else {
            this.isLoadingResults = false;
            // this.jobDescription = data;
            sessionStorage.setItem('textExtractedJD', JSON.stringify(dataCopy));
            sessionStorage.setItem('textCreatedJD', JSON.stringify(data));
            sessionStorage.setItem('jobSource', JSON.stringify('file'));
            // this.httpService.showSuccess('Job Created successfully', 'Job Created');
            this.router.navigate(['/recruiter/jobTitle', 'add'], {
              queryParamsHandling: 'preserve',
            });
          }
        },
        err => {
          this.isLoadingResults = false;
          this.httpService.showError(err);
        }
      );
  }
}
