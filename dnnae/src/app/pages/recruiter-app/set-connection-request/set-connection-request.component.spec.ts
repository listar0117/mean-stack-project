import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetConnectionRequestComponent } from './set-connection-request.component';

describe('SetConnectionRequestComponent', () => {
  let component: SetConnectionRequestComponent;
  let fixture: ComponentFixture<SetConnectionRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetConnectionRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetConnectionRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
