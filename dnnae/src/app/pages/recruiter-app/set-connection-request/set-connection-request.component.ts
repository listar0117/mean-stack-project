import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Globals } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-set-connection-request',
  templateUrl: './set-connection-request.component.html',
  styleUrls: ['./set-connection-request.component.scss'],
})
export class SetConnectionRequestComponent implements OnInit {
  model: any = {};
  requestUrl: string;
  public globals = Globals;
  typeOfRequestSend: any;
  dataToSend: any;
  messageData: any;
  variable: String = '';
  caretPos: number = 0;
  rango: any;
  userData: any;
  isLoadingResults = true;
  selectionStart: any;
  selectionEnd: any;
  jobID: string;
  maxChars = 280;
  showExtensionMessage: boolean = false;
  comingFromExtension: boolean = false;
  calledFromExtension: string;
  constructor(
    private service: HttpService,
    private sanitizer: DomSanitizer,
    private router: Router,
    private ActivatedRoute: ActivatedRoute
  ) {
    this.jobID = this.ActivatedRoute.snapshot.paramMap.get('id');
    console.log(this.jobID);
  }

  ngOnInit() {
    let item = JSON.parse(localStorage.getItem('UserData'));
    this.userData = item;
    this.getConnectionData(item);
    this.ActivatedRoute.queryParams.subscribe(params => {
      this.calledFromExtension = params['calledFromExtension'];
      console.log(this.calledFromExtension);
    });
    if (this.calledFromExtension == '1') {
      this.comingFromExtension = true;
    }
  }
  getConnectionData(item) {
    this.dataToSend = {
      jobID: this.jobID,
      recruiterID: item.recruiterID,
    };
    this.isLoadingResults = true;
    this.requestUrl = this.globals.urls.dnnaeWeb.getMessageSettings;
    this.typeOfRequestSend = this.service.postRequest(this.requestUrl, this.dataToSend);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          this.messageData = res.data;
          this.model.title = this.messageData.sampleMessage;
          this.isLoadingResults = false;
        } else if (res.data.isSuccess === false) {
          this.service.showError('Something went wrong, Please try again');
          this.isLoadingResults = false;
          this.router.navigate(['/recruiter/main'], { queryParamsHandling: 'preserve' });
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        // this.service.showError(err.error.message);
      }
    );
  }
  getCursorPosition = function(oField) {
    console.log(oField);
    this.rango = oField;
    this.selectionStart = oField.selectionStart;
    this.selectionEnd = oField.selectionEnd;
    // if (this.variable) {
    //   if (oField.selectionStart || oField.selectionStart == '0') {
    //     this.caretPos = oField.selectionStart;
    //     this.model.title =
    //       this.model.title.slice(0, oField.selectionStart) +
    //       `{{.${this.variable}}}` +
    //       this.model.title.slice(oField.selectionStart + Math.abs(0));
    //     this.variable = '';
    //   }
    // }
  };
  addVariables(input, button) {
    this.variable = button;
    if (this.variable) {
      if (input.setSelectionRange) {
        input.focus();
        input.setSelectionRange(input.selectionStart, input.selectionEnd);
      } else if (input.createTextRange) {
        var range = input.createTextRange();
        range.collapse(true);
        range.moveEnd('character', input.selectionEnd);
        range.moveStart('character', input.selectionStart);
        range.select();
      }
      this.model.title =
        this.model.title.slice(0, input.selectionStart) +
        `{{.${this.variable}}}` +
        this.model.title.slice(input.selectionStart + Math.abs(0));
      // if (this.selectionStart || this.selectionStart == '0') {
      //   this.caretPos = this.selectionStart;
      //   this.model.title =
      //     this.model.title.slice(0, this.selectionStart) +
      //     `{{.${this.variable}}}` +
      //     this.model.title.slice(this.selectionStart + Math.abs(0));
      //   this.variable = '';
      // }
    }
  }

  setSelectionRange(input, selectionStart, selectionEnd) {
    if (input.setSelectionRange) {
      input.focus();
      input.setSelectionRange(selectionStart, selectionEnd);
    } else if (input.createTextRange) {
      var range = input.createTextRange();
      range.collapse(true);
      range.moveEnd('character', selectionEnd);
      range.moveStart('character', selectionStart);
      range.select();
    }
  }

  saveData() {
    this.dataToSend = {
      recruiterID: this.userData.recruiterID,
      jobID: this.jobID,
      sampleMessage: this.model.title,
    };
    this.isLoadingResults = true;
    this.requestUrl = this.globals.urls.dnnaeWeb.setMessageSettings;
    this.typeOfRequestSend = this.service.postRequest(this.requestUrl, this.dataToSend);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          this.service.showSuccess('Message updated Successfully', 'Message Updated');
          this.isLoadingResults = false;
          if (this.calledFromExtension == '1') {
            this.showExtensionMessage = true;
          } else {
            if (this.jobID !== 'All') {
              this.router.navigate(['/recruiter/jobs'], { queryParamsHandling: 'preserve' });
            } else {
              this.router.navigate(['/recruiter/main'], { queryParamsHandling: 'preserve' });
            }
          }
        } else if (res.data.isSuccess === false) {
          this.service.showError('Something went wrong, Please try again');
          this.isLoadingResults = false;
          this.router.navigate(['/recruiter/main'], { queryParamsHandling: 'preserve' });
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        // this.service.showError(err.error.message);
      }
    );
  }
}
