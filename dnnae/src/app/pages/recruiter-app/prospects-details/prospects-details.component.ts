import { Component, OnInit } from '@angular/core';
import {NgbPopoverConfig} from '@ng-bootstrap/ng-bootstrap';
import { Globals } from 'src/app/Globals';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from 'src/app/services/http-service';

@Component({
  selector: 'app-prospects-details',
  templateUrl: './prospects-details.component.html',
  styleUrls: ['../prospects/prospects.component.scss','./prospects-details.component.scss'],
  providers: [NgbPopoverConfig],
})
export class ProspectsDetailsComponent implements OnInit {
  prospect: any;
  globals = Globals;
  isLoadingResults = true;
  idsObject: any ={};
  typeOfRequestSend: any;
  userData:any;
  requestUrl: string;
  constructor(config: NgbPopoverConfig,
    private router: Router,
    private service: HttpService,
    private ActivatedRoute: ActivatedRoute ) {
    config.placement = 'right';
    config.triggers = 'click';
   }

  ngOnInit() {
    // let item = JSON.parse(sessionStorage.getItem('prospectData'));
    // this.prospect=item
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.idsObject.jobID = this.ActivatedRoute.snapshot.paramMap.get('jobId');
    this.idsObject.entity = this.ActivatedRoute.snapshot.paramMap.get('entity');
    console.log(this.idsObject);
    this.getProspectData(this.userData.recruiterID,this.idsObject.jobID,this.idsObject.entity);
  }
  calcPercentage(score, total) {
    if (total === undefined || (total.toString() === '0')) {
      total = 0.1;
    }
    let number:number =(score / total) * 100;
    // number = number.toFixed(2); 
    return parseFloat(number.toString()).toFixed(2);
  }

  getProspectData(recruiterID, jobId, entity) {
    if (recruiterID) {
      const dataToSend = {
        recruiterID: recruiterID,
        jobID: jobId,
        entity: entity,
      };
      this.requestUrl = this.globals.urls.dnnaeWeb.singleProspect;
      this.isLoadingResults = true;
      this.typeOfRequestSend = this.service.postRequest(this.requestUrl,dataToSend);
      this.typeOfRequestSend.subscribe(
        res => {
          // console.log(res);
          if (res.data.isSuccess === true) {
            this.isLoadingResults = false;
            // this.changeType(1);
            this.prospect =res.data.applicant;
            console.log(this.prospect);
          } else if (res.data.isSuccess === false) {
            this.service.showError(res.data.message);
            this.isLoadingResults = false;
            this.router.navigate(['/recruiter'], { queryParamsHandling: 'preserve' });
          }
        },
        err => {
          if (err.error) console.log(err);
          // this.service.showError(err.error.message);
          this.isLoadingResults = false;
        }
      );
    }
  }

}
