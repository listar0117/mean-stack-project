import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProspectsDetailsComponent } from './prospects-details.component';

describe('ProspectsDetailsComponent', () => {
  let component: ProspectsDetailsComponent;
  let fixture: ComponentFixture<ProspectsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProspectsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProspectsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
