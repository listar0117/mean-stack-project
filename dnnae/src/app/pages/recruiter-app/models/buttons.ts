export class Buttons {
    buttonName: string;
    buttonRoute: string;
  
    constructor(buttonName: string, buttonRoute: string){
      this.buttonName = buttonName;
      this.buttonRoute = buttonRoute;
    }
  }
  