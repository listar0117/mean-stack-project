import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DegreesCombinedComponent } from './degrees-combined.component';

describe('DegreesCombinedComponent', () => {
  let component: DegreesCombinedComponent;
  let fixture: ComponentFixture<DegreesCombinedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DegreesCombinedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DegreesCombinedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
