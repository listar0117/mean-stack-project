import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormControl } from '@angular/forms';
import { Degrees } from 'src/app/degreesList';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Options, LabelType } from 'ng5-slider';
import { Globals } from '../../../../Globals';
@Component({
  selector: 'app-degrees-combined',
  templateUrl: './degrees-combined.component.html',
  styleUrls: ['../../new-job/new-job.component.scss', './degrees-combined.component.scss'],
})
export class DegreesCombinedComponent implements OnInit {
  textExtractedJD: any;
  textCreatedJD: any;
  tempDegreeMajor: any;
  degreeMajor: any;
  tempDegree: any;
  degree: any;
  jobId: string;
  jdType: string;
  degreeCombined: any;
  sampleObj: any = {};
  tempSampleObj: any;
  degrees = Degrees;
  public globals = Globals;
  stepperData: any;
  populateStepper: boolean = true;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  degreeCtrl = new FormControl();
  filteredDegrees: Observable<any[]>;
  fruits: any[] = [];
  // allFruits: string[] = ['Apple', 'Lemon', 'Lime', 'Orange', 'Strawberry'];
  options: Options = {
    showTicksValues: true,
    showTicks: true,
    tickStep: 1,
    stepsArray: [
      { value: 1, legend: 'Nice to have' },
      { value: 3, legend: 'Important' },
      { value: 5, legend: 'Must have' },
    ],
    translate: (value: number, label: LabelType): string => {
      switch (value) {
        case 1:
          return 'Nice to have';
        case 3:
          return 'Important';
        case 5:
          return 'Must have';
        default:
          return 'Nice to have';
      }
    },
    showSelectionBar: true,
    selectionBarGradient: {
      from: 'white',
      to: '#3391fa',
    },
  };

  @ViewChild('fruitInput', { static: false }) fruitInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', { static: false }) matAutocomplete: MatAutocomplete;

  constructor(private router: Router, private ActivatedRoute: ActivatedRoute) {
    this.stepperData = {
      enable: true,
      step: 4,
    };
    this.filteredDegrees = this.degreeCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: any | null) => (fruit ? this._filter(fruit) : this.degrees.slice()))
    );

    this.jobId = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.jdType = this.ActivatedRoute.snapshot.paramMap.get('jdType');
    this.textExtractedJD = JSON.parse(sessionStorage.getItem('textExtractedJD'));
    if (this.jdType === 'qa') {
      this.textCreatedJD = JSON.parse(sessionStorage.getItem('textCreatedJD'));
    }
  }
  ngOnInit() {
    this.tempDegreeMajor = this.textExtractedJD.filter(
      valueData => valueData.tag == 'DEGREE_MAJOR'
    );
    this.degreeMajor = this.tempDegreeMajor[0];
    this.tempDegree = this.textExtractedJD.filter(valueData => valueData.tag == 'DEGREE');
    this.degree = this.tempDegree[0];
    console.log(this.degree);
    console.log(this.degreeMajor);
    const tempDegree = [];
    // this.degree.data.map(degreeVal => {
    //   this.degreeMajor.data.map(degreeMajVal => {
    //     const obj = {
    //       name: degreeVal.name || degreeMajVal.name ? `${degreeVal.name} ${degreeMajVal.name}` : '',
    //       disable: false,
    //       score: 0,
    //       scoreText: 'none',
    //     };
    //     tempDegree.push(obj);
    //   });
    // });

    for (
      let i = 0,
        len =
          this.degree.data.length < this.degreeMajor.data.length
            ? this.degree.data.length
            : this.degreeMajor.data.length;
      i < len;
      i += 1
    ) {
      let obj;
      if (
        (this.degree.data[i] && this.degree.data[i].name) ||
        (this.degree.data[i] &&
          this.degree.data[i].name !== undefined &&
          this.degree.data[i].name.length == 0)
      ) {
        obj = {
          name:
            (this.degree.data[i] && this.degree.data[i].name) ||
            (this.degreeMajor.data[i] && this.degreeMajor.data[i].name)
              ? `${this.degree.data[i] ? this.degree.data[i].name || '' : ''} ${
                  this.degreeMajor.data[i] ? this.degreeMajor.data[i].name || '' : ''
                }`
              : '',
          disable: false,
          score: 1,
          scoreText: 'Nice to have',
        };
      } else {
        obj = {
          disable: true,
          score: 1,
          scoreText: 'Nice to have',
          name: `${this.degree.data[i]} ${this.degreeMajor.data[i]}`,
        };
        // TempData.push(obj);
      }
      tempDegree.push(obj);
    }

    this.tempSampleObj = this.textExtractedJD.filter(valueData => valueData.tag == 'DEGREE_FULL');
    if (this.tempSampleObj.length > 0) {
      this.sampleObj = this.tempSampleObj[0];
    } else {
      if (tempDegree.length > 0) {
        this.sampleObj = {
          tag: 'DEGREE_FULL',
          title: 'Degree name',
          data: tempDegree,
        };
      } else {
        this.sampleObj = {
          tag: 'DEGREE_FULL',
          title: 'Degree name',
          data: [
            {
              disable: false,
              name: '',
              score: 1,
              scoreText: 'Nice to have',
            },
          ],
        };
      }
    }
    if (this.sampleObj.data.length > 0) {
      const TempData = [];
      this.sampleObj.data.map(data => {
        if (data.name || (data.name !== undefined && data.name.length == 0)) {
          data.disable = true;
          data.score = data.score || 1;
          data.scoreText = data.scoreText || 'Nice to have';
        } else {
          let obj = {
            disable: true,
            score: data.score || 1,
            scoreText: data.scoreText || 'Nice to have',
            name: data,
          };
          TempData.push(obj);
        }
      });
      if (TempData.length > 0) {
        this.sampleObj.data = TempData;
      }
    } else {
      this.sampleObj.data.push({
        disable: false,
        name: '',
        score: 1,
        scoreText: 'Nice to have',
      });
    }
    console.log(this.sampleObj);
  }
  add(event: MatChipInputEvent): void {
    // Add fruit only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      // Add our fruit
      if ((value || '').trim()) {
        let obj = {
          disable: false,
          name: value.trim(),
          score: 1,
          scoreText: 'Nice to have',
        };
        this.sampleObj.data.push(obj);
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.degreeCtrl.setValue(null);
    }
  }

  remove(degree: any): void {
    const index = this.sampleObj.data.indexOf(degree);

    if (index >= 0) {
      this.sampleObj.data.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    let obj = {
      disable: false,
      name: event.option.viewValue,
      score: 1,
      scoreText: 'Nice to have',
    };
    this.sampleObj.data.push(obj);
    this.fruitInput.nativeElement.value = '';
    this.degreeCtrl.setValue(null);
  }

  private _filter(value: any): any[] {
    const filterValue = value.toLowerCase();
    const tempArray = filterValue.split(' ');
    const regexFromMyArray = new RegExp(tempArray.join('|'), 'gi');
    const UserDegreeObj = {
      name: filterValue,
      fullName: filterValue,
      Major: '',
      Degree: '',
    };
    const filteredDegrees = this.degrees.filter(fruit =>
      regexFromMyArray.test(fruit.fullName.toLowerCase())
    );
    filteredDegrees.unshift(UserDegreeObj);
    return filteredDegrees;
    // return this.degrees.filter(fruit => fruit.fullName.toLowerCase().includes(filterValue));
  }
  chooseFirstOption(): void {
    this.matAutocomplete.options.first.select();
  }
  // editFieldsEnable(industry, i, type) {
  //   let dataSet;
  //   if (type == 'DegreeMajor') {
  //     dataSet = this.degreeMajor;
  //   } else {
  //     dataSet = this.degree;
  //   }
  //   dataSet.data[i].disable = !dataSet.data[i].disable;
  // }
  // AddNew(type) {
  //   let dataSet;
  //   if (type == 'DegreeMajor') {
  //     dataSet = this.degreeMajor;
  //   } else {
  //     dataSet = this.degree;
  //   }
  //   dataSet.data.push({
  //     disable: false,
  //     name: '',
  //     experienceCount: 0,
  //     score: 0,
  //     scoreText: 'none',
  //   });
  //   // this.sampleObj.data.push(JSON.parse(JSON.stringify(this.sampleDataObj)));
  //   console.log(this.degreeMajor);
  // }
  score(event, index) {
    let dataSet;
    // if (type == 'DegreeMajor') {
    //   dataSet = this.degreeMajor;
    // } else {
    dataSet = this.sampleObj;
    // }
    console.log(event);
    switch (event.value.toString()) {
      case '1': {
        dataSet.data[index].scoreText = 'Nice to have';
        break;
      }
      case '3': {
        dataSet.data[index].scoreText = 'Important';
        break;
      }
      case '5': {
        dataSet.data[index].scoreText = 'Must Have';
        break;
      }
      default: {
        dataSet.data[index].scoreText = 'Not important';
        break;
      }
    }
    // console.log(data);
    dataSet.data[index].score = event.value;
    // console.log(this.userJobDescription);
  }
  Next() {
    // this.degreeMajor.data.map(data => {
    //   delete data.disable;
    // });
    let tempCheck = false;
    this.sampleObj.data.map(data => {
      delete data.disable;
    });
    const tempSampleObj = this.sampleObj.data.filter(data => data.name !== '');
    this.sampleObj.data = tempSampleObj;
    this.textExtractedJD.map(data => {
      if (data.tag == 'DEGREE_FULL') {
        data = this.sampleObj;
        tempCheck = true;
      }
    });
    if (tempCheck == false) {
      this.textExtractedJD.push(this.sampleObj);
      // this.textCreatedJD.push(this.sampleObj);
    }
    // this.textExtractedJD.map(data => {
    //   if (data.tag == 'DEGREE') {
    //     data = this.degree;
    //   }
    // });
    // this.textCreatedJD.map(data => {
    //   if (data.tag == 'DEGREE_MAJOR') {
    //     data = this.degreeMajor;
    //   }
    // });

    // this.textCreatedJD.map(data => {
    //   if (data.tag == 'DEGREE_FULL') {
    //     data = this.sampleObj;
    //   }
    // });
    sessionStorage.setItem('textExtractedJD', JSON.stringify(this.textExtractedJD));
    if (this.jdType === 'qa') {
      sessionStorage.setItem('textCreatedJD', JSON.stringify(this.textCreatedJD));
    }
    this.router.navigate(['/recruiter/technicalSkills', this.jobId, this.jdType], {
      queryParamsHandling: 'preserve',
    });
  }
}
