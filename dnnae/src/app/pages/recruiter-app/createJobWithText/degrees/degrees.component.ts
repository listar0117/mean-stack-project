import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-degrees',
  templateUrl: './degrees.component.html',
  styleUrls: ['../../new-job/new-job.component.scss', './degrees.component.scss'],
})
export class DegreesComponent implements OnInit {
  textExtractedJD: any;
  textCreatedJD: any;
  tempDegreeMajor: any;
  degreeMajor: any;
  tempDegree: any;
  degree: any;
  jobId: string;
  constructor(private router: Router, private ActivatedRoute: ActivatedRoute) {
    this.jobId = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.textExtractedJD = JSON.parse(sessionStorage.getItem('textExtractedJD'));
    this.textCreatedJD = JSON.parse(sessionStorage.getItem('textCreatedJD'));
    this.tempDegreeMajor = this.textExtractedJD.filter(
      valueData => valueData.tag == 'DEGREE_MAJOR'
    );
    this.degreeMajor = this.tempDegreeMajor[0];
    this.tempDegree = this.textExtractedJD.filter(valueData => valueData.tag == 'DEGREE');
    this.degree = this.tempDegree[0];
    console.log(this.degree);
    if (this.degreeMajor.data.length > 0) {
      const TempData = [];
      this.degreeMajor.data.map(data => {
        if (data.name || (data.name !== undefined && data.name.length == 0)) {
          data.disable = true;
          data.score = data.score || 0;
          data.scoreText = data.scoreText || 'none';
        } else {
          let obj = {
            disable: true,
            score: data.score || 0,
            scoreText: data.scoreText || 'none',
            name: data,
          };
          TempData.push(obj);
        }
      });
      if (TempData.length > 0) {
        this.degreeMajor.data = TempData;
      }
    } else {
      this.degreeMajor.data.push({
        disable: false,
        name: '',
        score: 0,
        scoreText: 'none',
      });
    }
    if (this.degree.data.length > 0) {
      const TempData = [];
      this.degree.data.map(data => {
        if (data.name || (data.name !== undefined && data.name.length == 0)) {
          data.disable = true;
          data.score = data.score || 0;
          data.scoreText = data.scoreText || 'none';
        } else {
          let obj = {
            disable: true,
            score: data.score || 0,
            scoreText: data.scoreText || 'none',
            name: data,
          };
          TempData.push(obj);
        }
      });
      if (TempData.length > 0) {
        this.degree.data = TempData;
      }
      // this.degree.data =TempData
    } else {
      this.degree.data.push({
        disable: false,
        name: '',
        score: 0,
        scoreText: 'none',
      });
    }
  }
  ngOnInit() {}
  editFieldsEnable(industry, i, type) {
    let dataSet;
    if (type == 'DegreeMajor') {
      dataSet = this.degreeMajor;
    } else {
      dataSet = this.degree;
    }
    dataSet.data[i].disable = !dataSet.data[i].disable;
  }
  AddNew(type) {
    let dataSet;
    if (type == 'DegreeMajor') {
      dataSet = this.degreeMajor;
    } else {
      dataSet = this.degree;
    }
    dataSet.data.push({
      disable: false,
      name: '',
      experienceCount: 0,
      score: 0,
      scoreText: 'none',
    });
    // this.sampleObj.data.push(JSON.parse(JSON.stringify(this.sampleDataObj)));
    console.log(this.degreeMajor);
  }
  score(event, index, type) {
    let dataSet;
    if (type == 'DegreeMajor') {
      dataSet = this.degreeMajor;
    } else {
      dataSet = this.degree;
    }
    console.log(event);
    switch (event.value.toString()) {
      case '0': {
        dataSet.data[index].scoreText = 'None';
        break;
      }
      case '1': {
        dataSet.data[index].scoreText = 'Not Important';
        break;
      }
      case '2': {
        dataSet.data[index].scoreText = 'Nice to Have';
        break;
      }
      case '3': {
        dataSet.data[index].scoreText = 'Important';
        break;
      }
      case '4': {
        dataSet.data[index].scoreText = 'Very Important';
        break;
      }
      case '5': {
        dataSet.data[index].scoreText = 'Must Have';
        break;
      }
      default: {
        dataSet.data[index].scoreText = 'None';
        break;
      }
    }
    // console.log(data);
    dataSet.data[index].score = event.value;
    // console.log(this.userJobDescription);
  }
  Next() {
    this.degreeMajor.data.map(data => {
      delete data.disable;
    });
    this.degree.data.map(data => {
      delete data.disable;
    });
    this.textExtractedJD.map(data => {
      if (data.tag == 'DEGREE_MAJOR') {
        data = this.degreeMajor;
      }
    });
    this.textExtractedJD.map(data => {
      if (data.tag == 'DEGREE') {
        data = this.degree;
      }
    });
    this.textCreatedJD.map(data => {
      if (data.tag == 'DEGREE_MAJOR') {
        data = this.degreeMajor;
      }
    });
    this.textCreatedJD.map(data => {
      if (data.tag == 'DEGREE') {
        data = this.degree;
      }
    });
    sessionStorage.setItem('textExtractedJD', JSON.stringify(this.textExtractedJD));
    sessionStorage.setItem('textCreatedJD', JSON.stringify(this.textCreatedJD));
    this.router.navigate(['/recruiter/technicalSkills', this.jobId], {
      queryParamsHandling: 'preserve',
    });
  }
}
