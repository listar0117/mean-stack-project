import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Globals } from '../../../../Globals';
@Component({
  selector: 'app-company-name',
  templateUrl: './company-name.component.html',
  styleUrls: [
    '../../new-job/new-job.component.scss',
    '../company-specialities/company-specialities.component.scss',
    './company-name.component.scss',
  ],
})
export class CompanyNameComponent implements OnInit {
  company: any = {
    // disable: true,
    disable: false,
    data: [],
    companyName: '',
    tag: 'COMPANY_NAME',
    title: 'company name',
  };
  stepperData: any;
  populateStepper: boolean = true;
  public globals = Globals;
  userData: any;
  jobId: string;
  jdType: string;
  textExtractedJD: any;
  textCreatedJD: any;
  tempSampleObj: any;
  constructor(private router: Router, private ActivatedRoute: ActivatedRoute) {
    this.stepperData = {
      enable: true,
      step: 1,
    };
    this.jobId = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.jdType = this.ActivatedRoute.snapshot.paramMap.get('jdType');
    console.log(this.jobId);
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.textExtractedJD = JSON.parse(sessionStorage.getItem('textExtractedJD'));
    if (this.jdType === 'qa') {
      this.textCreatedJD = JSON.parse(sessionStorage.getItem('textCreatedJD'));
    }
    this.tempSampleObj = this.textExtractedJD.filter(valueData => valueData.tag == 'COMPANY_NAME');
  }

  ngOnInit() {
    if (this.tempSampleObj.length == 0) {
      this.company.companyName = this.userData.signupCompanyName;
      this.tempSampleObj.push(this.company);
    } else {
      this.company.companyName = this.tempSampleObj[0].companyName;
      this.company.disable = false;
    }
  }
  editFieldsEnableCompany() {
    this.company.disable = !this.company.disable;
  }

  Next() {
    this.tempSampleObj[0].companyName = this.company.companyName;
    let tempCheck = false;
    if (this.tempSampleObj.length > 0) {
      delete this.tempSampleObj[0].disable;
    }
    this.textExtractedJD.map(data => {
      if (data.tag == 'COMPANY_NAME') {
        data = this.tempSampleObj[0];
        tempCheck = true;
      }
    });
    if (tempCheck == false) {
      this.textExtractedJD.push(this.tempSampleObj[0]);
      if (this.jdType === 'qa') {
        this.textCreatedJD.push(this.tempSampleObj[0]);
      }
    }
    sessionStorage.setItem('textExtractedJD', JSON.stringify(this.textExtractedJD));
    if (this.jdType === 'qa') {
      sessionStorage.setItem('textCreatedJD', JSON.stringify(this.textCreatedJD));
    }
    this.router.navigate(['/recruiter/companySpecialties', this.jobId, this.jdType], {
      queryParamsHandling: 'preserve',
    });
  }
}
