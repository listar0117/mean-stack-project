import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormControl } from '@angular/forms';
import { industryExperience } from 'src/app/industryExperienceList';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Options, LabelType } from 'ng5-slider';
import { Globals } from '../../../../Globals';
@Component({
  selector: 'app-company-specialities',
  templateUrl: './company-specialities.component.html',
  styleUrls: [
    '../../new-job/new-job.component.scss',
    '../degrees-combined/degrees-combined.component.scss',
    './company-specialities.component.scss',
  ],
})
export class CompanySpecialitiesComponent implements OnInit {
  companiesAndIndustries: any = [];
  public globals = Globals;
  sampleObj: any = {};
  textExtractedJD: any;
  textCreatedJD: any;
  tempSampleObj: any;
  // degreeMajor: any;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  industryCtrl = new FormControl();
  filteredIndustries: Observable<any[]>;
  industries = industryExperience;
  fruits: any[] = [];
  sampleDataObj: any = {
    disable: false,
    name: '',
    score: 1,
    scoreText: 'Nice to have',
  };
  stepperData: any;
  populateStepper: boolean = true;
  // company: any = {
  //   disable: true,
  //   companyName: '',
  // };
  userData: any;
  jobId: string;
  jdType: string;
  options: Options = {
    showTicksValues: true,
    showTicks: true,
    tickStep: 1,
    stepsArray: [
      { value: 1, legend: 'Nice to have' },
      { value: 3, legend: 'Important' },
      { value: 5, legend: 'Must have' },
    ],
    translate: (value: number, label: LabelType): string => {
      switch (value) {
        case 1:
          return 'Nice to have';
        case 3:
          return 'Important';
        case 5:
          return 'Must have';
        default:
          return 'Nice to have';
      }
    },
    showSelectionBar: true,
    selectionBarGradient: {
      from: 'white',
      to: '#3391fa',
    },
  };
  @ViewChild('fruitInput', { static: false }) fruitInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', { static: false }) matAutocomplete: MatAutocomplete;
  constructor(private router: Router, private ActivatedRoute: ActivatedRoute) {
    this.stepperData = {
      enable: true,
      step: 2,
    };
    this.filteredIndustries = this.industryCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: any | null) => (fruit ? this._filter(fruit) : this.industries.slice()))
    );
    this.jobId = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.jdType = this.ActivatedRoute.snapshot.paramMap.get('jdType');
    // console.log(this.jobId);
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    // this.company.companyName = this.userData.signupCompanyName;
    this.textExtractedJD = JSON.parse(sessionStorage.getItem('textExtractedJD'));
    if (this.jdType === 'qa') {
      this.textCreatedJD = JSON.parse(sessionStorage.getItem('textCreatedJD'));
    }
    this.tempSampleObj = this.textExtractedJD.filter(
      valueData => valueData.tag == 'COMPANY_INDUSTRY_SPECIALTIES'
    );
    if (this.tempSampleObj.length > 0) {
      this.sampleObj = this.tempSampleObj[0];
    } else {
      this.sampleObj = {
        tag: 'COMPANY_INDUSTRY_SPECIALTIES',
        title: 'Company and industry specialties',
        // companyName: this.company.companyName,
        data: [
          {
            disable: false,
            name: '',
            score: 1,
            scoreText: 'Nice to have',
          },
        ],
      };
    }
    if (this.sampleObj.data.length > 0) {
      const TempData = [];
      this.sampleObj.data.map(data => {
        if (data.name || (data.name !== undefined && data.name.length == 0)) {
          data.disable = data.name.length > 0 ? true : false;
          data.score = data.score || 1;
          data.scoreText = data.scoreText || 'Nice to have';
        } else {
          let obj = {
            disable: data.name ? true : false,
            score: data.score || 1,
            scoreText: data.scoreText || 'Nice to have',
            name: data,
          };
          TempData.push(obj);
        }
      });
      if (TempData.length > 0) {
        this.sampleObj.data = TempData;
      }
    } else {
      this.sampleObj.data.push({
        disable: false,
        name: '',
        score: 1,
        scoreText: 'Nice to have',
      });
    }
  }

  ngOnInit() {
    console.log(this.sampleObj);
  }
  // editFieldsEnableCompany() {
  //   this.company.disable = !this.company.disable;
  // }
  add(event: MatChipInputEvent): void {
    // Add fruit only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      // Add our fruit
      if ((value || '').trim()) {
        let obj = {
          disable: false,
          name: value.trim(),
          score: 1,
          scoreText: 'Nice to have',
        };
        this.sampleObj.data.push(obj);
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.industryCtrl.setValue(null);
    }
  }
  editFieldsEnable(industry, i) {
    this.sampleObj.data[i].disable = !this.sampleObj.data[i].disable;
  }
  delete(industry, i) {
    this.sampleObj.data.splice(i, 1);
  }
  remove(industry: any): void {
    const index = this.sampleObj.data.indexOf(industry);

    if (index >= 0) {
      this.sampleObj.data.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    let obj = {
      disable: false,
      name: event.option.viewValue,
      score: 1,
      scoreText: 'Nice to have',
    };
    this.sampleObj.data.push(obj);
    this.fruitInput.nativeElement.value = '';
    this.industryCtrl.setValue(null);
  }
  private _filter(value: any): any[] {
    const filterValue = value.toLowerCase();
    const tempArray = filterValue.split(' ');
    const regexFromMyArray = new RegExp(tempArray.join('|'), 'gi');
    // const UserDegreeObj = {
    //   name: filterValue,
    //   fullName: filterValue,
    //   Major: '',
    //   Degree: '',
    // };
    const filteredIndustries = this.industries.filter(fruit =>
      regexFromMyArray.test(fruit.toLowerCase())
    );
    filteredIndustries.unshift(filterValue);
    return filteredIndustries;
    // return this.industries.filter(fruit => fruit.fullName.toLowerCase().includes(filterValue));
  }
  chooseFirstOption(): void {
    this.matAutocomplete.options.first.select();
  }
  AddNewIndustry() {
    this.sampleObj.data.push({
      disable: false,
      name: '',
      score: 1,
      scoreText: 'Nice to have',
    });
    // this.sampleObj.data.push(JSON.parse(JSON.stringify(this.sampleDataObj)));
    console.log(this.sampleObj);
  }
  score(event, index) {
    console.log(event);
    switch (event.value.toString()) {
      case '1': {
        this.sampleObj.data[index].scoreText = 'Nice to have';
        break;
      }
      case '3': {
        this.sampleObj.data[index].scoreText = 'Important';
        break;
      }
      case '5': {
        this.sampleObj.data[index].scoreText = 'Must Have';
        break;
      }
      default: {
        this.sampleObj.data[index].scoreText = 'Not Important';
        break;
      }
    }
    // console.log(data);
    this.sampleObj.data[index].score = event.value;
    // console.log(this.userJobDescription);
  }
  Next() {
    // const textExtractedJD = JSON.parse(sessionStorage.getItem('textExtractedJD'));
    // const textCreatedJD = JSON.parse(sessionStorage.getItem('textCreatedJD'));
    let tempCheck = false;
    let tempCheckCreatedJD = false;
    this.sampleObj.data.map(data => {
      delete data.disable;
    });
    const tempSampleObj = this.sampleObj.data.filter(data => data.name.trim() !== '');
    this.sampleObj.data = tempSampleObj;
    this.textExtractedJD.map(data => {
      if (data.tag == 'COMPANY_INDUSTRY_SPECIALTIES') {
        data = this.sampleObj;
        tempCheck = true;
      }
    });
    if (this.jdType === 'qa') {
      this.textCreatedJD.map(data => {
        if (data.tag == 'COMPANY_INDUSTRY_SPECIALTIES') {
          data = this.sampleObj;
          tempCheckCreatedJD = true;
        }
      });
    }
    if (tempCheck == false) {
      this.textExtractedJD.push(this.sampleObj);
    }
    if (this.jdType === 'qa') {
      if (tempCheckCreatedJD == false) {
        this.textCreatedJD.push(this.sampleObj);
      }
    }
    sessionStorage.setItem('textExtractedJD', JSON.stringify(this.textExtractedJD));
    if (this.jdType === 'qa') {
      sessionStorage.setItem('textCreatedJD', JSON.stringify(this.textCreatedJD));
    }
    this.router.navigate(['/recruiter/newJobExperience', this.jobId, this.jdType], {
      queryParamsHandling: 'preserve',
    });
  }
}
