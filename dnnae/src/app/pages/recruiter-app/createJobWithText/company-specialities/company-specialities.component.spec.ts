import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanySpecialitiesComponent } from './company-specialities.component';

describe('CompanySpecialitiesComponent', () => {
  let component: CompanySpecialitiesComponent;
  let fixture: ComponentFixture<CompanySpecialitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanySpecialitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanySpecialitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
