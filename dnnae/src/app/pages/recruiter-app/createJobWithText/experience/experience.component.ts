import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Options, LabelType } from 'ng5-slider';
import { Globals } from '../../../../Globals';
@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['../../new-job/new-job.component.scss', './experience.component.scss'],
})
export class ExperienceComponent implements OnInit {
  textExtractedJD: any;
  textCreatedJD: any;
  tempExperience: any;
  experience: any;
  jobId: string;
  jdType: string;
  public globals = Globals;
  stepperData: any;
  populateStepper: boolean = true;
  options: Options = {
    showTicksValues: true,
    showTicks: true,
    tickStep: 1,
    stepsArray: [
      { value: 1, legend: 'Nice to have' },
      { value: 3, legend: 'Important' },
      { value: 5, legend: 'Must have' },
    ],
    translate: (value: number, label: LabelType): string => {
      switch (value) {
        case 1:
          return 'Nice to have';
        case 3:
          return 'Important';
        case 5:
          return 'Must have';
        default:
          return 'Nice to have';
      }
    },
    showSelectionBar: true,
    selectionBarGradient: {
      from: 'white',
      to: '#3391fa',
    },
  };
  constructor(private router: Router, private ActivatedRoute: ActivatedRoute) {
    this.stepperData = {
      enable: true,
      step: 3,
    };
    this.jobId = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.jdType = this.ActivatedRoute.snapshot.paramMap.get('jdType');
    this.textExtractedJD = JSON.parse(sessionStorage.getItem('textExtractedJD'));
    if (this.jdType === 'qa') {
      this.textCreatedJD = JSON.parse(sessionStorage.getItem('textCreatedJD'));
    }
    this.tempExperience = this.textExtractedJD.filter(
      valueData => valueData.tag == 'TECHNOLOGY_WORK_EXPERIENCE'
    );
    this.experience = this.tempExperience[0];
    if (this.experience.data.length > 0) {
      const TempData = [];
      this.experience.data.map(data => {
        if (data.name || (data.name !== undefined && data.name.length > 0)) {
          // data.disable = data.name.length > 0 ? true : false;
          data.disable = false;
          data.score = data.score || 1;
          data.scoreText = data.scoreText || 'Nice to have';
          data.experienceCount = 0;
        } else {
          let obj = {
            // disable: data.name ? true : false,
            disable: false,
            score: data.score || 1,
            scoreText: data.scoreText || 'Nice to have',
            name: data,
            experienceCount: 0,
          };
          TempData.push(obj);
        }
      });
      if (TempData.length > 0) {
        this.experience.data = TempData;
      }
    } else {
      this.experience.data.push({
        disable: false,
        name: '',
        experienceCount: 0,
        score: 1,
        scoreText: 'Nice to have',
      });
    }
  }

  ngOnInit() {
    console.log(this.tempExperience);
  }
  editFieldsEnable(industry, i) {
    this.experience.data[i].disable = !this.experience.data[i].disable;
  }
  delete(industry, i) {
    this.experience.data.splice(i, 1);
  }
  AddNewIndustry() {
    this.experience.data.push({
      disable: false,
      name: '',
      experienceCount: 0,
      score: 1,
      scoreText: 'Nice to have',
    });
    // this.sampleObj.data.push(JSON.parse(JSON.stringify(this.sampleDataObj)));
    console.log(this.experience);
  }
  score(event, index) {
    console.log(event);
    switch (event.value.toString()) {
      case '1': {
        this.experience.data[index].scoreText = 'Nice to have';
        break;
      }
      case '3': {
        this.experience.data[index].scoreText = 'Important';
        break;
      }
      case '5': {
        this.experience.data[index].scoreText = 'Must Have';
        break;
      }
      default: {
        this.experience.data[index].scoreText = 'Not important';
        break;
      }
    }
    // console.log(data);
    this.experience.data[index].score = event.value;
    // console.log(this.userJobDescription);
  }
  Next() {
    this.experience.data.map(data => {
      delete data.disable;
    });
    const tempSampleObj = this.experience.data.filter(data => data.name !== '');
    this.experience.data = tempSampleObj;

    this.textExtractedJD.map(data => {
      if (data.tag == 'TECHNOLOGY_WORK_EXPERIENCE') {
        data = this.experience;
      }
    });
    if (this.jdType === 'qa') {
      this.textCreatedJD.map(data => {
        if (data.tag == 'TECHNOLOGY_WORK_EXPERIENCE') {
          data = this.experience;
        }
      });
    }
    sessionStorage.setItem('textExtractedJD', JSON.stringify(this.textExtractedJD));
    if (this.jdType === 'qa') {
      sessionStorage.setItem('textCreatedJD', JSON.stringify(this.textCreatedJD));
    }
    this.router.navigate(['/recruiter/newJobDegree', this.jobId, this.jdType], {
      queryParamsHandling: 'preserve',
    });
  }
}
