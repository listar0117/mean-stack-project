import { Component, OnInit, ViewChild } from '@angular/core';
import { Globals } from 'src/app/Globals';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { HttpService } from 'src/app/services/http-service';

@Component({
  selector: 'app-text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['../../new-job/new-job.component.scss', './text-editor.component.scss'],
})
export class TextEditorComponent implements OnInit {
  public buttons: any;
  model: any = {};
  plainTextToSend: any;
  editorConfig: any = {};
  // Observables/Subscriptions
  httpSub$: Subscription = null;
  isLoadingResults: boolean = false;
  jobDescription: any;
  globals = Globals;
  @ViewChild('form', { static: false }) form;

  constructor(
    private httpService: HttpService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: 'auto',
      minHeight: '300px',
      width: '575px',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      imageEndPoint: '',
      toolbar: [
        ['bold', 'italic', 'underline', 'strikeThrough', 'superscript', 'subscript'],
        ['fontName', 'fontSize', 'color'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', 'indent', 'outdent'],
        ['cut', 'copy', 'delete', 'removeFormat', 'undo', 'redo'],
        [
          'paragraph',
          'blockquote',
          'removeBlockquote',
          'horizontalLine',
          'orderedList',
          'unorderedList',
        ],
        // ['link', 'unlink', 'image', 'video'],
      ],
    };
  }
  paste(event) {
    const content = event.clipboardData.getData('text/html');
    this.plainTextToSend = event.clipboardData.getData('text/plain');
    console.log(content);
    event.preventDefault();
    // console.log(content);
    // const stripTagsRegex = /<(?!\/?(h1|h2|h3|h4|h5|b|br|strong)(?=>|\s?.*>))\/?.*?>/
    const stripTagsRegex = /<(?!\/?(h1|h2|h3|h4|h5|b|br|strong)(?=>|\s?.*>))\/?.*?>/gimu;
    const stripStyles = /(((color)|(font-weight)|(font-size)|(line-height)|(max-height)|(overflow)): ?[. a-zA-Z0-9\(,\)#]*)[;"']/gimu;
    const stripBrTag = /(<br>)\1+/gimu;
    const strippedData = content.replace(stripTagsRegex, '<br>');
    const strippedData1 = strippedData.replace(stripStyles, '');
    const strippedData2 = strippedData1.replace(stripBrTag, '<br>');
    console.log(strippedData2);
    this.model.title = strippedData2 ? strippedData2 : this.plainTextToSend;
    // this.model.title = content;
  }
  domParser(html) {
    const oParser = new DOMParser();
    const oDOM = oParser.parseFromString(html, 'text/html');
    const text = oDOM.body.innerText;
    return text;
  }
  submit() {
    console.log(this.model.title);
    // this.plainTextToSend ? this.plainTextToSend : this.model.title;
    this.plainTextToSend = this.domParser(this.model.title);
    const formData = new FormData();
    formData.append('text', JSON.stringify(this.plainTextToSend.replace(/\n/g, '.')));
    this.isLoadingResults = true;
    this.httpSub$ = this.httpService
      .postRequest(this.globals.urls.dnnaeWeb.extract, formData, {
        'Connection':'Keep-Alive',
        'Keep-Alive': 'timeout=300 ',
      }, true)
      .pipe(map(res => res.data))
      .subscribe(
        data => {
          if (data.isSuccess == false) {
            this.isLoadingResults = false;
            this.router.navigate(['/recruiter/newJob'], { queryParamsHandling: 'preserve' });
            this.httpService.showError('Unable to extract data');
          } else {
            this.isLoadingResults = false;
            // this.createJD(data);
            sessionStorage.setItem('textExtractedJD', JSON.stringify(data));
            sessionStorage.setItem('jdText', JSON.stringify(this.model.title));
            sessionStorage.setItem('jobSource', JSON.stringify('Text'));
            // this.httpService.showSuccess('Job Created successfully', 'Job Created');
            sessionStorage.removeItem('stepperData');
            this.router.navigate(['/recruiter/jobTitle', 'add', 'text'], {
              queryParamsHandling: 'preserve',
            });

            // sessionStorage.setItem('textExtractedJD', JSON.stringify(data));
            console.log(data);
            // this.router.navigate(['/recruiter/jobTitle']);
          }
        },
        err => {
          console.log(err);
          this.isLoadingResults = false;
          this.router.navigate(['/recruiter/newJob'], { queryParamsHandling: 'preserve' });
          this.httpService.showError('Unable to Read Data');
        }
      );
  }
  //should not be used here
  createJD(dataCopy) {
    const formData = new FormData();
    formData.append('entities', JSON.stringify(dataCopy));
    this.isLoadingResults = true;
    this.httpSub$ = this.httpService
      .postRequest(this.globals.urls.dnnaeWeb.createJD, formData, {}, false)
      .pipe(map(res => res.data))
      .subscribe(
        data => {
          if (data.isSuccess == false) {
            this.isLoadingResults = false;
            this.router.navigate(['/recruiter/newJob'], { queryParamsHandling: 'preserve' });
            this.httpService.showError('Unable to generate Job Description');
          } else {
            this.isLoadingResults = false;
            this.jobDescription = data;
            sessionStorage.setItem('textExtractedJD', JSON.stringify(dataCopy));
            sessionStorage.setItem('textCreatedJD', JSON.stringify(data));
            sessionStorage.setItem('jobSource', JSON.stringify('Text'));
            // this.httpService.showSuccess('Job Created successfully', 'Job Created');
            this.router.navigate(['/recruiter/jobTitle', 'add', 'text'], {
              queryParamsHandling: 'preserve',
            });
          }
        },
        err => {
          this.isLoadingResults = false;
          this.httpService.showError(err);
        }
      );
  }
}
