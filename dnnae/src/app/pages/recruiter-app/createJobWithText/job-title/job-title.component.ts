import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from 'src/app/services/http-service';
import { Globals } from '../../../../Globals';

@Component({
  selector: 'app-job-title',
  templateUrl: './job-title.component.html',
  styleUrls: ['../../new-job/new-job.component.scss', './job-title.component.scss'],
})
export class JobTitleComponent implements OnInit {
  textExtractedJD: any;
  textCreatedJD: any;
  model: any = {};
  tempJObTitle: any;
  jobTitle: any;
  scoreText: String;
  // changeScreen: boolean = false;
  jobId: string;
  jdType: string;
  isLoadingResults: boolean = false;
  stepperData: object;
  public globals = Globals;
  requestUrl: string;
  typeOfRequestSend: any;
  populateStepper: boolean = false;
  constructor(
    private router: Router,
    private ActivatedRoute: ActivatedRoute,
    private service: HttpService
  ) {
    this.stepperData = {
      enable: true,
      step: 0,
    };
    this.jobId = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.jdType = this.ActivatedRoute.snapshot.paramMap.get('jdType');
    console.log(this.jobId);
  }
  ngOnInit() {
    if (this.jobId !== 'add') {
      this.getJobData(this.jobId);
    } else {
      this.extractInitialPageLoadData();
    }
  }
  jsUcfirst(text) {
    let wordsArray = text.toLowerCase().split(' ');

    let capsArray = wordsArray.map(word => {
      return word.replace(word[0], word[0].toUpperCase());
    });

    return capsArray.join(' ');
  }
  extractInitialPageLoadData() {
    this.textExtractedJD = JSON.parse(sessionStorage.getItem('textExtractedJD'));
    if (this.jdType === 'qa') {
      this.textCreatedJD = JSON.parse(sessionStorage.getItem('textCreatedJD'));
    }
    this.populateStepper = true;
    this.tempJObTitle = this.textExtractedJD.filter(valueData => valueData.tag == 'JOB_POSITION');
    this.jobTitle = this.tempJObTitle[0];
    // this.jobTitle.disable = this.jobTitle.data.length > 0 ? true : false;
    this.jobTitle.disable = false;
    this.jobTitle.scoreText = this.jobTitle.scoreText || 'None';
    this.jobTitle.importance = this.jobTitle.importance || 0;
    // this.model.title=this.jobTitle.data[0];
    console.log(this.jobTitle);
  }
  editFieldsEnable() {
    this.jobTitle.disable = !this.jobTitle.disable;
  }
  getJobData(val) {
    const dataToSend = {
      jobID: val,
      // recruiterID: 2
    };
    this.requestUrl = this.globals.urls.getJobData;
    this.isLoadingResults = true;
    this.typeOfRequestSend = this.service.postRequest(this.requestUrl, dataToSend);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          // this.singleJobData =  res.data.job;
          sessionStorage.setItem('textExtractedJD', JSON.stringify(res.data.job.jobArray));
          if (this.jdType === 'qa') {
            sessionStorage.setItem('textCreatedJD', JSON.stringify(res.data.job.jobDescription));
          }
          sessionStorage.setItem('stepperData', JSON.stringify(res.data.job.stepperData));
          this.extractInitialPageLoadData();

          this.isLoadingResults = false;
        } else if (res.data.isSuccess === false) {
          // this.service.showError(res.data.message)
          this.isLoadingResults = false;
          this.router.navigate(['recruiter/jobs'], { queryParamsHandling: 'preserve' });
          this.service.showError('Some thing went wrong, unable to find the job');
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        // this.service.showError(err.error.message);
      }
    );
  }
  score(event) {
    console.log(event);
    switch (event.value.toString()) {
      case '0': {
        this.jobTitle.scoreText = 'None';
        break;
      }
      case '1': {
        this.jobTitle.scoreText = 'Not Important';
        break;
      }
      case '2': {
        this.jobTitle.scoreText = 'Nice to Have';
        break;
      }
      case '3': {
        this.jobTitle.scoreText = 'Important';
        break;
      }
      case '4': {
        this.jobTitle.scoreText = 'Very Important';
        break;
      }
      case '5': {
        this.jobTitle.scoreText = 'Must Have';
        break;
      }
      default: {
        this.jobTitle.scoretext = 'None';
        break;
      }
    }
    this.jobTitle.importance = event.value;
    console.log(this.jobTitle);
  }
  Next() {
    // if (!changeScreen) {
    //   this.changeScreen = true;
    // }
    const capitalize = this.jsUcfirst(this.tempJObTitle[0].data[0]);
    this.tempJObTitle[0].data = [];
    this.tempJObTitle[0].data.push(capitalize);

    this.textExtractedJD.map(data => {
      if (data.tag == 'JOB_POSITION') {
        data = this.tempJObTitle;
      }
    });
    if (this.jdType === 'qa') {
      this.textCreatedJD.map(data => {
        if (data.tag == 'JOB_POSITION') {
          data = this.tempJObTitle;
        }
      });
    }
    sessionStorage.setItem('textExtractedJD', JSON.stringify(this.textExtractedJD));
    if (this.jdType === 'qa') {
      sessionStorage.setItem('textCreatedJD', JSON.stringify(this.textCreatedJD));
    }

    // if (changeScreen) {
    this.router.navigate(['/recruiter/companyName', this.jobId, this.jdType], {
      queryParamsHandling: 'preserve',
    });
    // }
  }
}
