import { Component, OnInit, ViewChild } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Globals } from 'src/app/Globals';
import { Router, ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { HttpService } from 'src/app/services/http-service';

@Component({
  selector: 'app-others',
  templateUrl: './others.component.html',
  styleUrls: ['../../new-job/new-job.component.scss', './others.component.scss'],
})
export class OthersComponent implements OnInit {
  userJobDescription: any;
  globals = Globals;
  userData: any;
  jobTitle: any;
  jobLocation: any;
  typeOfRequestSend: any;
  companyName: any;
  model: any = {
    jobLocation: {
      name: '',
      disable: false,
    },
    salary: {
      name: '',
      disable: false,
    },
  };
  // Observables/Subscriptions
  httpSub$: Subscription = null;
  isLoadingResults: boolean = false;
  jobDescription: any;
  SliderText: String = 'None';
  textExtractedJD: any;
  textCreatedJD: any;
  jobId: string;
  jdType: string;
  jdText: any;
  jobSource: string;
  stepperData: any;
  populateStepper: boolean = true;
  stepperObj: any;
  @ViewChild('form', { static: false }) form;
  constructor(
    private httpService: HttpService,
    private router: Router,
    private ActivatedRoute: ActivatedRoute
  ) {
    this.stepperData = {
      enable: true,
      step: 6,
    };
  }
  ngOnInit() {
    this.jobId = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.jdType = this.ActivatedRoute.snapshot.paramMap.get('jdType');
    this.textExtractedJD = JSON.parse(sessionStorage.getItem('textExtractedJD'));
    this.jdText = JSON.parse(sessionStorage.getItem('jdText'));
    this.stepperObj = JSON.parse(sessionStorage.getItem('stepperData'));
    if (this.jdType === 'qa') {
      this.textCreatedJD = JSON.parse(sessionStorage.getItem('textCreatedJD'));
    }
    this.jobSource = sessionStorage.getItem('jobSource');
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.formateData(this.textExtractedJD);
    const salaryObj = this.textExtractedJD.filter(valueData => valueData.tag == 'SALARY');
    const jobLocation = this.textExtractedJD.filter(valueData => valueData.tag == 'JOB_LOCATION');
    const companyName = this.textExtractedJD.filter(valueData => valueData.tag == 'COMPANY_NAME');
    this.companyName = companyName[0].companyName;
    console.log('salaryObj', this.companyName);
    console.log('jobLocation', jobLocation);
    this.model = {
      jobLocation: {
        name: jobLocation[0].data[0],
        // disable: jobLocation[0].data.length > 0 ? true : false,
        disable: false,
      },
      salary: {
        name: salaryObj[0].data[0],
        // disable: salaryObj[0].data.length > 0 ? true : false,
        disable: false,
      },
    };
  }

  formateData(data) {
    data.map(userData => {
      userData.tempData = [];
      userData.disabled = true;
      if (userData.data.length == 0) {
        let obj = {
          data: '',
          // disable: true,
          disable: false,
        };
        userData.tempData.push(obj);
      }
      userData.data.map(valueData => {
        let obj = {
          data: valueData,
          // disable: true,
          disable: false,
        };
        userData.tempData.push(obj);
      });
      if (userData.tag === 'SOFT_SKILLS') {
        userData.placeHolder = 'Example: Communication skills';
      } else if (userData.tag === 'SALARY') {
        userData.placeHolder = 'Example: $10000 a month';
      } else if (userData.tag === 'JOB_LOCATION') {
        userData.placeHolder = 'Example: Palo Alto';
      }
    });
    this.userJobDescription = data;
    console.log(this.userJobDescription);
  }
  updateLocation(locationObj) {
    this.model.jobLocation.name = locationObj.formatted_address;
  }
  // not being used rigt now, but should be used here
  createJD(dataCopy) {
    const tempCopy = JSON.parse(JSON.stringify(dataCopy));
    tempCopy.map(data => {
      const tempArr = [];
      if (data.tag !== 'TECHNICAL_SKILLS')
        data.data.map(subData => {
          if (subData.name) {
            tempArr.push(subData.name);
          }
        });
      if (tempArr.length > 0) {
        data.data = tempArr;
      }
    });
    console.log(tempCopy);
    const formData = new FormData();
    formData.append('entities', JSON.stringify(tempCopy));
    this.isLoadingResults = true;
    this.httpSub$ = this.httpService
      .postRequest(this.globals.urls.dnnaeWeb.createJD, formData, {}, false)
      .pipe(map(res => res.data))
      .subscribe(
        data => {
          // this.isLoadingResults = false;
          // this.jobDescription = data;
          // this.saveJob(dataCopy);

          // this.httpService.showSuccess('Job Created successfully', 'Job Created');
          // this.router.navigate(['/recruiter/main']);
          if (data.isSuccess === false) {
            this.httpService.showError('Some thing went wrong while creating Job Description');
            this.isLoadingResults = false;
            // this.router.navigate(['/']);
          } else {
            this.isLoadingResults = false;
            this.jobDescription = data;
            // this.saveJob(dataCopy);
          }
        },
        err => {
          this.isLoadingResults = false;
          this.httpService.showError(err);
        }
      );
  }

  modifyData() {
    const dataCopy = JSON.parse(JSON.stringify(this.userJobDescription));
    // const tempCopy = JSON.parse(JSON.stringify(this.userJobDescription));
    const modifiedData = dataCopy.map(userData => {
      if (userData.tempData) {
        let tempArray = [];
        userData.tempData.map(valueData => {
          tempArray.push(valueData.data);
          userData.data = tempArray;
        });
        delete userData.disabled;
        delete userData.tempData;
        return dataCopy;
      }
    });
    if (this.jdType === 'qa') {
      this.textCreatedJD.map(data => {
        const tempArr = [];
        if (data.tag !== 'TECHNICAL_SKILLS')
          data.data.map(subData => {
            if (subData && subData.name) {
              tempArr.push(subData.name);
            }
          });
        if (tempArr.length > 0) {
          data.data = tempArr;
        }
      });
    }
    this.jobDescription = this.textCreatedJD ? this.textCreatedJD : [];
    console.log(dataCopy);
    this.saveJob(dataCopy);
    // this.createJD(dataCopy);
  }

  saveJob(jobData) {
    // console.log(this.userData);
    this.isLoadingResults = true;
    console.log(jobData);
    this.jobTitle = jobData.filter(valueData => valueData.tag == 'JOB_POSITION');
    this.jobLocation = jobData.filter(valueData => valueData.tag == 'JOB_LOCATION');

    // if (this.jdType === 'qa') {
    jobData.map(jobObject => {
      this.jobDescription.forEach(element => {
        if (element.tag == jobObject.tag) {
          if (element.tag == 'TECHNICAL_SKILLS') {
            element.data.splice(0, element.data.length);
            jobObject.data.forEach(skill => {
              element.data.push(skill.name);
            });
          } else if (element.tag == 'JOB_LOCATION') {
            element.data = [];
            element.data.push(this.model.jobLocation.name);
          } else if (element.tag == 'SALARY') {
            element.data = [];
            element.data.push(this.model.salary.name || '');
          } else {
            element.data = jobObject.data;
          }
        }
      });
      if (jobObject.tag == 'JOB_LOCATION') {
        jobObject.data = [];
        jobObject.data.push(this.model.jobLocation.name);
      } else if (jobObject.tag == 'SALARY') {
        jobObject.data = [];
        jobObject.data.push(this.model.salary.name || '');
      }
    });
    // }

    let dataToSend = {
      recruiterID: this.userData.recruiterID,
      jobTitle: this.jobTitle[0].data[0],
      companyName: this.companyName || this.userData.signupCompanyName,
      jobLocation: this.jobLocation[0].data[0],
      jdType: this.jdType || 'Edited',
      jdText: this.jdText || '',
      jobArray: jobData,
      jobDescription: this.jobDescription,
      jobID: this.jobId,
      stepperData: this.stepperObj,
    };
    // if(this.jobId!=='add'){
    //   dataToSend.jobID = this.jobId
    // }
    console.log('data to send', dataToSend);

    this.typeOfRequestSend = this.httpService.postRequest(
      this.globals.urls.dnnaeWeb.createJob,
      dataToSend
    );
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          this.isLoadingResults = false;
          sessionStorage.removeItem('textExtractedJD');
          sessionStorage.removeItem('textCreatedJD');
          sessionStorage.removeItem('jobSource');
          sessionStorage.removeItem('jdText');
          if (this.jobId == 'add') {
            this.httpService.showSuccess('Please launch extension', 'Job Created');
          } else {
            this.httpService.showSuccess('Please launch extension', 'Job Updated');
          }
          // if (!this.userData.calendlyLink) {
          //   this.httpService.showError('Please update Clandely Link', 'Job Updated');
          //   this.router.navigate(['/recruiter/calender_link']);
          // } else {
          this.router.navigate(['/recruiter/jobs'], { queryParamsHandling: 'preserve' });
          // }
        } else if (res.data.isSuccess === false) {
          this.httpService.showError(res.data.message);
          this.isLoadingResults = false;
          // this.router.navigate(['/']);
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        // this.service.showError(err.error.message);
      }
    );
  }

  // enable and disbale already added fields
  editFieldsEnable(event, data, index, tempDataIndex) {
    // data.disable =  !data.disable;
    this.userJobDescription[index].tempData[tempDataIndex].disable = !this.userJobDescription[index]
      .tempData[tempDataIndex].disable;
    console.log(tempDataIndex);
  }
  editSingleFieldsEnable(event, model, type) {
    if (type === 1) {
      this.model.salary.disable = !this.model.salary.disable;
    } else if (type == 2) {
      this.model.jobLocation.disable = !this.model.jobLocation.disable;
    }
  }

  delete(event, data, index, tempDataIndex) {
    this.userJobDescription[index].tempData.splice(tempDataIndex, 1);
  }
  addNewReason(event, data, index) {
    // data.disable =  !data.disable;
    this.userJobDescription[index].tempData.push({ data: '', disable: false });
    console.log(this.userJobDescription);
  }
  // not being used now
  score(event, data, index, tempDataIndex) {
    console.log(event);
    switch (event.value.toString()) {
      case '0': {
        this.userJobDescription[index].tempData[tempDataIndex].scoreText = 'None';
        break;
      }
      case '1': {
        this.userJobDescription[index].tempData[tempDataIndex].scoreText = 'Not Important';
        break;
      }
      case '2': {
        this.userJobDescription[index].tempData[tempDataIndex].scoreText = 'Nice to Have';
        break;
      }
      case '3': {
        this.userJobDescription[index].tempData[tempDataIndex].scoreText = 'Important';
        break;
      }
      case '4': {
        this.userJobDescription[index].tempData[tempDataIndex].scoreText = 'Very Important';
        break;
      }
      case '5': {
        this.userJobDescription[index].tempData[tempDataIndex].scoreText = 'Must Have';
        break;
      }
      default: {
        this.userJobDescription[index].tempData[tempDataIndex].scoretext = 'None';
        break;
      }
    }
    // console.log(data);
    this.userJobDescription[index].tempData[tempDataIndex].score = event.value;
    console.log(this.userJobDescription);
  }

  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }
}
