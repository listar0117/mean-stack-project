import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Options, LabelType } from 'ng5-slider';
import { Globals } from 'src/app/Globals';
@Component({
  selector: 'app-technical-skills',
  templateUrl: './technical-skills.component.html',
  styleUrls: ['../../new-job/new-job.component.scss', './technical-skills.component.scss'],
})
export class TechnicalSkillsComponent implements OnInit {
  textExtractedJD: any;
  textCreatedJD: any;
  tempTechnicalSkills: any;
  technicalSkills: any;
  jobId: string;
  jdType: string;
  stepperData: any;
  populateStepper: boolean = true;
  globals = Globals;
  options: Options = {
    showTicksValues: true,
    showTicks: true,
    tickStep: 1,
    stepsArray: [
      { value: 1, legend: 'Nice to have' },
      { value: 3, legend: 'Important' },
      { value: 5, legend: 'Must have' },
    ],
    translate: (value: number, label: LabelType): string => {
      switch (value) {
        case 1:
          return 'Nice to have';
        case 3:
          return 'Important';
        case 5:
          return 'Must have';
        default:
          return 'Nice to have';
      }
    },
    showSelectionBar: true,
    selectionBarGradient: {
      from: 'white',
      to: '#3391fa',
    },
  };
  constructor(private router: Router, private ActivatedRoute: ActivatedRoute) {
    this.stepperData = {
      enable: true,
      step: 5,
    };
    this.jobId = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.jdType = this.ActivatedRoute.snapshot.paramMap.get('jdType');
    this.textExtractedJD = JSON.parse(sessionStorage.getItem('textExtractedJD'));
    if (this.jdType === 'qa') {
      this.textCreatedJD = JSON.parse(sessionStorage.getItem('textCreatedJD'));
    }
    this.tempTechnicalSkills = this.textExtractedJD.filter(
      valueData => valueData.tag == 'TECHNICAL_SKILLS'
    );
    const cleanSkills = this.tempTechnicalSkills[0].data.filter(
      skills => skills.name.length > 0 && skills.name !== undefined
    );
    this.tempTechnicalSkills[0].data = cleanSkills;
    this.technicalSkills = this.tempTechnicalSkills[0];
    if (this.technicalSkills.data.length > 0) {
      this.technicalSkills.data.map(data => {
        data.disable = true;
        // data.disable = false;
        data.name = data.name || '';
        data.score = data.score || 1;
        data.scoreText = data.scoreText || 'Nice to have';
      });
    } else {
      this.technicalSkills.data.push({
        disable: false,
        name: '',
        score: 1,
        scoreText: 'Nice to have',
      });
    }
  }

  ngOnInit() {
    console.log(this.technicalSkills);
  }
  editFieldsEnable(industry, i) {
    this.technicalSkills.data[i].disable = !this.technicalSkills.data[i].disable;
  }
  delete(skill, i) {
    this.technicalSkills.data.splice(i, 1);
  }
  AddNew() {
    this.technicalSkills.data.push({
      disable: false,
      name: '',
      score: 1,
      scoreText: 'Nice to have',
    });
    // this.sampleObj.data.push(JSON.parse(JSON.stringify(this.sampleDataObj)));
    console.log(this.technicalSkills);
  }
  score(event, index) {
    console.log(event);
    switch (event.value.toString()) {
      case '1': {
        this.technicalSkills.data[index].scoreText = 'Nice to have';
        break;
      }
      case '3': {
        this.technicalSkills.data[index].scoreText = 'Important';
        break;
      }
      case '5': {
        this.technicalSkills.data[index].scoreText = 'Must Have';
        break;
      }
      default: {
        this.technicalSkills.data[index].scoreText = 'Not Important';
        break;
      }
    }
    // console.log(data);
    this.technicalSkills.data[index].score = event.value;
    // console.log(this.userJobDescription);
  }
  Next() {
    this.technicalSkills.data.map(data => {
      delete data.disable;
    });
    const tempSampleObj = this.technicalSkills.data.filter(data => data.name !== '');
    this.technicalSkills.data = tempSampleObj;
    this.textExtractedJD.map(data => {
      if (data.tag == 'TECHNICAL_SKILLS') {
        data = this.technicalSkills;
      }
    });
    if (this.jdType === 'qa') {
      this.textCreatedJD.map(data => {
        if (data.tag == 'TECHNICAL_SKILLS') {
          data = this.technicalSkills;
        }
      });
    }
    sessionStorage.setItem('textExtractedJD', JSON.stringify(this.textExtractedJD));
    if (this.jdType === 'qa') {
      sessionStorage.setItem('textCreatedJD', JSON.stringify(this.textCreatedJD));
    }
    this.router.navigate(['/recruiter/others', this.jobId, this.jdType], {
      queryParamsHandling: 'preserve',
    });
  }
}
