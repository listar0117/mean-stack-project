import { Component, OnInit } from '@angular/core';
import { Globals } from 'src/app/Globals';
@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss'],
})
export class ProfileEditComponent implements OnInit {
  userData: any;
  public globals = Globals;
  constructor() {}

  ngOnInit() {
    console.log('asdasdasdasd');
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    if (
      !this.userData.imageURL ||
      this.userData.imageURL == null ||
      this.userData.imageURL == 'null'
    ) {
      this.userData.imageURL = this.globals.defaultImage;
    }
  }
}
