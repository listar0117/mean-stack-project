import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Globals } from '../../../../Globals';
@Component({
  selector: 'app-job-stepper',
  templateUrl: './job-stepper.component.html',
  styleUrls: ['./job-stepper.component.scss'],
})
export class JobStepperComponent implements OnInit {
  public globals = Globals;
  stepperObj: any;
  jobId: string;
  jdType: string;
  @Input() stepperInput: any;
  @Input() populate: boolean;
  constructor(private router: Router, private ActivatedRoute: ActivatedRoute) {
    this.jobId = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.jdType = this.ActivatedRoute.snapshot.paramMap.get('jdType');
    // this.textExtractedJD = JSON.parse(sessionStorage.getItem('textExtractedJD'));
    // if (this.populate) {
    const stepperData = JSON.parse(sessionStorage.getItem('stepperData'));
    this.stepperObj = stepperData && stepperData.length > 0 ? stepperData : Globals.jobStepper;
    // }
  }
  ngOnInit() {
    // if (this.populate) {
    this.stepperObj[this.stepperInput.step].enable = this.stepperInput.enable;
    sessionStorage.setItem('stepperData', JSON.stringify(this.stepperObj));
    // }
  }
  ngOnChanges(changes) {
    // Extract changes to the input property by its name
    let stepperObj: boolean = changes.populate.currentValue;
    const stepperData = JSON.parse(sessionStorage.getItem('stepperData'));
    this.stepperObj = stepperData && stepperData.length > 0 ? stepperData : Globals.jobStepper;
    this.stepperObj[this.stepperInput.step].enable = this.stepperInput.enable;
    sessionStorage.setItem('stepperData', JSON.stringify(this.stepperObj));
  }

  goto(step) {
    if (step.enable) {
      this.router.navigate([step.route, this.jobId, this.jdType], {
        queryParamsHandling: 'preserve',
      });
    }
  }
}
