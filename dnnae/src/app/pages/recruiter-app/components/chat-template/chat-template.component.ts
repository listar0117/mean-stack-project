import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-chat-template',
  templateUrl: './chat-template.component.html',
  styleUrls: ['./chat-template.component.scss'],
})
export class ChatTemplateComponent implements OnInit {
  @Input() messagesArray: any;
  @Input() prospectData: object;
  @Input() title: String;
  constructor() {}

  ngOnInit() {
  }
  edit(message, index) {
    this.messagesArray[index].disabled = !this.messagesArray[index]
      .disabled;
  }
}
