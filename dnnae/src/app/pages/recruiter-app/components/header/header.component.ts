import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../../Globals';
import { StorageService } from '../../../../services/localStorage.service';
import { Router } from '@angular/router';
import { AppLogOutService } from 'src/app/services/app-log-out.service';
@Component({
  selector: 'app-header-recruiter',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public globals = Globals;
  userData: any;
  hideTabs: boolean;
  constructor(
    private router: Router,
    public storageService: StorageService,
    private autoLogout: AppLogOutService
  ) {
    this.storageService.changes.subscribe(res => {
      this.userData = JSON.parse(localStorage.getItem('UserData'));
      // this.token = JSON.parse(localStorage.getItem('token'));
      if (this.userData) {
        this.hideTabs = true;
      } else {
        this.hideTabs = false;
      }
    });
  }
  ngOnInit() {
    console.log(this.router.url);
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    if (this.userData) {
      this.hideTabs = true;
    } else {
      this.hideTabs = false;
    }
    this.storageService.changes.subscribe(res => {
      this.userData = JSON.parse(localStorage.getItem('UserData'));
      // this.token = JSON.parse(localStorage.getItem('token'));
      if (this.userData) {
        this.hideTabs = true;
      } else {
        this.hideTabs = false;
      }
    });
    // if(this.userData){
    //   this.hideTabs = false
    // }
    // else {
    //   this.hideTabs=true;
    // }
  }

  logOut() {
    sessionStorage.clear();
    localStorage.clear();
    this.userData = '';
    this.storageService.clear('UserData');
    this.hideTabs = false;
    this.router.navigate(['recruiter/login'], { queryParamsHandling: 'preserve' });
  }
}
