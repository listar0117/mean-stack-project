import { Component, OnInit, Input } from '@angular/core';
import { Message } from '../../models';
import { DialogflowService } from '../../../../services/dialogflow.service';
import { Globals } from 'src/app/Globals';
import{Router} from '@angular/router';
@Component({
  selector: 'message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.scss']
})
export class MessageFormComponent implements OnInit {

  @Input('message')
  public message : Message;

  @Input('messages')
  public messages : Message[];
  public counter: any;
  public UserResponse: any;
  constructor(private dialogFlowService: DialogflowService,private router: Router) { }

  ngOnInit() {
    console.log(this.router.url)
    this.counter =1;
    this.UserResponse=[];
  }

  public sendMessage(): void {
    this.message.timestamp = new Date();
    this.messages.push(this.message);
    if(this.router.url==='/recruiter/newJobDescription' && this.counter <= Globals.JobQuestions.length ){
      this.messages.push(
        // new Message(Globals.JobQuestions[this.counter], 'assets/images/bot.png', new Date())
      );
      this.UserResponse.push (this.message.content);
      this.counter+=1;
      if(this.counter ==Globals.JobQuestions.length){
        sessionStorage.setItem('tempJobData', JSON.stringify(this.UserResponse));
        this.router.navigate(['/recruiter/jobDescription'], { queryParamsHandling: 'preserve' });
      }
    }
    else{
      this.dialogFlowService.getResponse(this.message.content).subscribe(res => {
        this.messages.push(
          new Message(res.result.fulfillment.speech, 'assets/images/bot.png', res.timestamp)
        );
      });
    }

    this.message = new Message('', 'assets/images/user.png');
  }

}
