import { Component, OnInit,Input } from '@angular/core';
import { Buttons } from '../../models';

@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.scss']
})
export class ButtonsComponent implements OnInit {
  @Input('buttons')
  // @Input() buttons:any;
  public buttons: Buttons[];
  constructor() { }

  ngOnInit() {
  }

}
