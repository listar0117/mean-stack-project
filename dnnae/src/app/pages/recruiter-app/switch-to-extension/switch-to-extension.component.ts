import { Component, OnInit } from '@angular/core';
import { Globals } from 'src/app/Globals';

@Component({
  selector: 'app-switch-to-extension',
  templateUrl: './switch-to-extension.component.html',
  styleUrls: [
    '../set-connection-request/set-connection-request.component.scss',
    './switch-to-extension.component.scss',
  ],
})
export class SwitchToExtensionComponent implements OnInit {
  public globals = Globals;
  constructor() {}

  ngOnInit() {}
}
