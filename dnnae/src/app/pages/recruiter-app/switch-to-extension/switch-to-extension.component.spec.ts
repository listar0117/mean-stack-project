import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchToExtensionComponent } from './switch-to-extension.component';

describe('SwitchToExtensionComponent', () => {
  let component: SwitchToExtensionComponent;
  let fixture: ComponentFixture<SwitchToExtensionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwitchToExtensionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchToExtensionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
