import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Globals } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbPopoverConfig } from '@ng-bootstrap/ng-bootstrap';
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'app-prospects',
  templateUrl: './prospects.component.html',
  styleUrls: ['./prospects.component.scss'],
  providers: [NgbPopoverConfig],
})
export class ProspectsComponent implements OnInit {
  isLoadingResults = true;
  prospects: any;
  requestUrl: string;
  public globals = Globals;
  typeOfRequestSend: any;
  throttle = 3000;
  scrollDistance = 2;
  dataToSend: any;
  userData: any;
  originalData: any;
  limit: number = 20;
  offset: number = 0;
  prospectValue: any;
  totalProspects: number;
  pageTitle: string;
  statusFilterText = { name: 'Score', id: 1 };
  constructor(
    private service: HttpService,
    private sanitizer: DomSanitizer,
    private router: Router,
    private ActivatedRoute: ActivatedRoute,
    config: NgbPopoverConfig
  ) {
    config.placement = 'right';
    config.triggers = 'click';
  }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.prospectValue = this.ActivatedRoute.snapshot.paramMap.get('id');
    console.log(this.prospectValue);
    this.prospects = [];
    this.getProspects(this.userData.recruiterID, this.prospectValue);
  }
  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
  goto(data) {
    // sessionStorage.setItem('prospectData', JSON.stringify(data));
    this.router.navigate(['recruiter/prospectDetails',data.job._id,data.entityUrn], { queryParamsHandling: 'preserve' });
  }

  // change type complete or incomplete
  changeType(type) {
    // 1 sort by score 2 sort by date
    if (type === 1) {
      this.prospects.sort((a, b) => b.score.final_score - a.score.final_score);
      this.statusFilterText = { name: 'Score', id: 1 };
    } else if (type === 2) {
      this.prospects = JSON.parse(JSON.stringify(this.originalData)).reverse();
      this.statusFilterText = { name: 'Date', id: 2 };
    }
  }
  calcPercentage(score, total) {
    if (total === undefined || total.toString() === '0') {
      total = 0.1;
    }
    let number: number = (score / total) * 100;
    // number = number.toFixed(2);
    return parseFloat(number.toString()).toFixed(2);
  }

  getProspects(recruiterID, val) {
    if (recruiterID) {
      this.dataToSend = {
        recruiterID: recruiterID,
        jobID: val,
        limit: this.limit,
        offset: this.offset,
      };
      this.requestUrl = this.globals.urls.dnnaeWeb.prospectsList;
      this.isLoadingResults = true;
      this.typeOfRequestSend = this.service.postRequest(this.requestUrl, this.dataToSend);
      this.typeOfRequestSend.subscribe(
        res => {
          // console.log(res);
          if (res.data.isSuccess === true) {
            this.prospects.push(...res.data.applicantArray);
            console.log(this.prospects);
            this.originalData = JSON.parse(JSON.stringify(this.prospects));
            this.offset = this.offset + 1;
            this.totalProspects = res.data.TotalApplicants;
            if (val === `All`) {
              this.pageTitle = `Prospects List: All jobs`;
            } else if (this.prospects.length > 0) {
              this.pageTitle = `Prospects List: ${this.prospects[0].job.jobTitle}`;
            } else {
              this.pageTitle = `No prospects found`;
            }
            this.isLoadingResults = false;
            this.changeType(1);
          } else if (res.data.isSuccess === false) {
            this.service.showError(res.data.message);
            this.isLoadingResults = false;
            this.router.navigate(['/recruiter'], { queryParamsHandling: 'preserve' });
          }
        },
        err => {
          if (err.error) console.log(err);
          // this.service.showError(err.error.message);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onScrollDown() {
    // console.log('scrolled down!!', ev);
    if (this.prospects.length < this.totalProspects) {
      this.getProspects(this.userData.recruiterID, this.prospectValue);
    }
    // add another 20 items
    // const start = this.sum;
    // this.sum += 20;
    // this.appendItems(start, this.sum);

    // this.direction = 'down'
  }
}
