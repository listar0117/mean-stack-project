import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NlpVectorComponent } from './nlp-vector.component';

describe('NlpVectorComponent', () => {
  let component: NlpVectorComponent;
  let fixture: ComponentFixture<NlpVectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NlpVectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NlpVectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
