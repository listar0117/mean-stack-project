import { Component, OnInit } from '@angular/core';
import { Globals } from 'src/app/Globals';
import { Router, ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { HttpService } from 'src/app/services/http-service';

@Component({
  selector: 'app-nlp-vector',
  templateUrl: './nlp-vector.component.html',
  styleUrls: ['../nlp-intent/nlp-intent.component.scss', './nlp-vector.component.scss'],
})
export class NlpVectorComponent implements OnInit {
  httpSub$: Subscription = null;
  isLoadingResults: boolean = false;
  globals = Globals;
  textToSend: any = {
    title:'',
    summary:'',
    skills:'',
    experience:'',
    education:'',
    location:'',
  };
  receivedData: any;
  constructor(private httpService: HttpService) {}

  ngOnInit() {}
  send() {
    const tempData = {
      title:this.textToSend.title || 'nlp or machine learning engineer',
      summary:this.textToSend.summary || 'nan',
      skills:this.textToSend.skills || 'nan',
      experience:this.textToSend.experience || 'nan',
      education:this.textToSend.education || 'nan',
      location:this.textToSend.location || 'nan',
    }
    const formData = new FormData();
    formData.append('search_profile', JSON.stringify(tempData));
    this.isLoadingResults = true;
    this.httpSub$ = this.httpService
      .postRequest(this.globals.urls.temp.vector, formData, {}, false)
      .pipe(map(res => res.data))
      .subscribe(
        data => {
          if (data.isSuccess == false) {
            this.isLoadingResults = false;
            this.httpService.showError('Unable to extract intent');
          } else {
            this.isLoadingResults = false;
            this.receivedData = data;
            console.log(data);
          }
        },
        err => {
          console.log(err);
          this.isLoadingResults = false;
          sessionStorage.removeItem('pdfData');
          this.httpService.showError('Unable to extract intent');
        }
      );
  }
}
