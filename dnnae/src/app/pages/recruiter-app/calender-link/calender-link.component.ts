import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Globals } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-calender-link',
  templateUrl: './calender-link.component.html',
  styleUrls: [
    '../set-connection-request/set-connection-request.component.scss',
    './calender-link.component.scss',
  ],
})
export class CalenderLinkComponent implements OnInit {
  model: any = {};
  requestUrl: string;
  public globals = Globals;
  typeOfRequestSend: any;
  dataToSend: any;
  messageData: any;
  variable: String = '';
  caretPos: number = 0;
  rango: any;
  userData: any;
  isLoadingResults = true;
  selectionStart: any;
  selectionEnd: any;
  // jobID: string;
  constructor(
    private service: HttpService,
    private sanitizer: DomSanitizer,
    private router: Router,
    private ActivatedRoute: ActivatedRoute
  ) {
    // this.jobID = this.ActivatedRoute.snapshot.paramMap.get('id');
    // console.log(this.jobID);
  }

  ngOnInit() {
    let item = JSON.parse(localStorage.getItem('UserData'));
    this.userData = item;
    this.getConnectionData(item);
  }

  getConnectionData(item) {
    this.dataToSend = {
      recruiterID: item.recruiterID,
    };
    this.isLoadingResults = true;
    this.requestUrl = this.globals.urls.dnnaeWeb.getCalendlySettings;
    this.typeOfRequestSend = this.service.postRequest(this.requestUrl, this.dataToSend);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          this.messageData = res.data;
          this.model.title = this.messageData.link;
          this.isLoadingResults = false;
        } else if (res.data.isSuccess === false) {
          this.service.showError('Something went wrong, Please try again');
          this.isLoadingResults = false;
          this.router.navigate(['/recruiter/main'], { queryParamsHandling: 'preserve' });
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        // this.service.showError(err.error.message);
      }
    );
  }

  saveData() {
    this.dataToSend = {
      recruiterID: this.userData.recruiterID,
      link: this.model.title,
    };
    this.isLoadingResults = true;
    this.requestUrl = this.globals.urls.dnnaeWeb.setCalendlySettings;
    this.typeOfRequestSend = this.service.postRequest(this.requestUrl, this.dataToSend);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          this.service.showSuccess('Calendly link updated Successfully', 'Calendly Link Updated');
          this.isLoadingResults = false;
          this.userData.calendlyLink = this.model.title;
          localStorage.setItem('UserData', JSON.stringify(this.userData));
          // this.router.navigate(['/recruiter/main'], { queryParamsHandling: 'preserve' });
          this.router.navigate(['/recruiter/mail_integration'], { queryParamsHandling: 'preserve' });
        } else if (res.data.isSuccess === false) {
          this.service.showError('Something went wrong, Please try again');
          this.isLoadingResults = false;
          this.router.navigate(['/recruiter/main'], { queryParamsHandling: 'preserve' });
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        // this.service.showError(err.error.message);
      }
    );
  }
}
