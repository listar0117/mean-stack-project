import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalenderLinkComponent } from './calender-link.component';

describe('CalenderLinkComponent', () => {
  let component: CalenderLinkComponent;
  let fixture: ComponentFixture<CalenderLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalenderLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalenderLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
