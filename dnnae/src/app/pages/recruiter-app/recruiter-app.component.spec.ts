import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterAppComponent } from './recruiter-app.component';

describe('RecruiterAppComponent', () => {
  let component: RecruiterAppComponent;
  let fixture: ComponentFixture<RecruiterAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecruiterAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
