import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpService } from 'src/app/services/http-service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Globals } from '../../Globals';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public globals = Globals;
  private httpSub$: Subscription = null;
  // Type boolean variables
  isLoadingResults = false;
  signUp: FormGroup;
  isSubmitted = false;
  // previewDataFromServer = [];
  // companiesData=[];
  constructor(
    private httpService: HttpService,
    private applyForm: FormBuilder,
    private router: Router,
    private ActivatedRoute: ActivatedRoute
  ) {}
  ngOnInit() {
    const emailPattren = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    this.signUp = this.applyForm.group({
      emailAddress: ['', [Validators.required, Validators.pattern(emailPattren)]],
    });
    // this.ActivatedRoute.data.pipe(map(data => data)).subscribe((res)=>{
    //   this.companiesData=res.companyData;
    //   // console.log(this.companiesData);
    // })
  }
  get formControls() {
    return this.signUp.controls;
  }
  onSubmit() {
    console.log(this.signUp.value);
    this.isSubmitted = true;
    if (this.signUp.invalid) {
      return;
    }
    this.postData(this.signUp.value);
  }
  postData(data) {
    this.httpSub$ = this.httpService
      .postRequest(this.globals.urls.postLead, data)
      .pipe(map(res => res.data))
      .subscribe(
        data => {
          console.log(data);
          if (data.isSuccess === true) {
            this.isLoadingResults = false;
            this.httpService.showSuccess('email Signedup successfully.', 'Signed Up');
          } else if (data.isSuccess === false) {
            this.isLoadingResults = false;
            this.httpService.showError(data.message, '');
          }
        },
        err => {
          this.isLoadingResults = false;
          this.httpService.showError(err);
        }
      );
  }
  goto(data) {
    this.router.navigate(['/', data.companyName, 'jobs'], { queryParamsHandling: 'preserve' });
  }
}
