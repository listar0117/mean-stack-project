import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TractionAppRoutingModule } from './traction-app-routing.module';
import { TractionAppComponent } from './traction-app.component';
import { HomeComponent } from './home/home.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { VerifyComponent } from './verify/verify.component';

@NgModule({
  declarations: [
    TractionAppComponent,
    HomeComponent,
    SignupComponent,
    LoginComponent,
    VerifyComponent,
  ],
  imports: [
    CommonModule,
    TractionAppRoutingModule,
    MatCardModule,
    MatProgressSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [MatProgressSpinnerModule],
})
export class TractionAppModule {}
