import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-traction-app',
  template: `
  <router-outlet></router-outlet>
  `,
})
export class TractionAppComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
