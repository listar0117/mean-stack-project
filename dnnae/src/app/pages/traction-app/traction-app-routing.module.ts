import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TractionAppComponent } from './traction-app.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { VerifyComponent } from './verify/verify.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '',
    component: TractionAppComponent,
    children: [
      {
        path: 'signup',
        component: SignupComponent,
        // pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'verify/:verifyType',
        component: VerifyComponent,
      },
      {
        path: 'home',
        component: HomeComponent,
        // canActivate: [RouteGuard],
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '**',
    component: HomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TractionAppRoutingModule {}
