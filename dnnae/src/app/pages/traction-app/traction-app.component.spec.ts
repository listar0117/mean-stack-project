import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TractionAppComponent } from './traction-app.component';

describe('TractionAppComponent', () => {
  let component: TractionAppComponent;
  let fixture: ComponentFixture<TractionAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TractionAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TractionAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
