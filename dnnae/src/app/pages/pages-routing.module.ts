import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';
import { ApplyJobComponent } from './apply-job/apply-job.component';
import { RecruiterPageComponent } from './recruiter-page/recruiter-page.component';
import { SingleJobComponent } from './single-job/single-job.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { ChatBotProfilesComponent } from './chat-bot-profiles/chat-bot-profiles.component';
import { ChatBotMessagingComponent } from './chat-bot-messaging/chat-bot-messaging.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  // canActivate: [AuthGuard],
  children: [
    {
      path: '',
      redirectTo: 'earlySignUp',
      pathMatch: 'full'
    }, 
    {
      path: 'earlySignUp',
      component: HomeComponent,
      // resolve:{
      //   companyData:companyDataResolver
      // }
    },
    {
      path: 'privacy-policy',
      component: PrivacyPolicyComponent,
    },
    {
      path: ':recruiterUserName',
      component: RecruiterPageComponent,
      // resolve:{
      //   jobsData:recruiterDataResolver
      // }
    },
    {
      path: 'company/:companyName/job/:id/:jobName',
      component: SingleJobComponent,
    },
    {
      path: 'job/:id',
      component: SingleJobComponent,
    },
    {
      path: 'job/:id/apply',
      component: ApplyJobComponent,
    },
    {
      path: 'job/:id/verify/:webUserId',
      component: VerifyEmailComponent,
    },
    {
      path: 'bot/profiles',
      component: ChatBotProfilesComponent,
    },
    {
      path: 'bot/messaging/:prospectId',
      component: ChatBotMessagingComponent,
    },
    // Module for recruiters, replica of android App
    // { path: 'recruiter',
    // loadChildren: './recruiter-app/recruiter-app.module#RecruiterAppModule',
    // //  pathMatch: 'full'
    // },
    // {
    //   path: ':companyName/:job/apply',
    //   component: ApplyJobComponent,
    //   resolve:{
    //     singleJobData:jobDetailsResolver
    //   }
    // },
    // {
    //   path: '',
    //   redirectTo: 'main',
    //   pathMatch: 'full'
    // },  
  ],
  
},
{
  path: '**',
    component: HomeComponent

}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  // providers:[recruiterDataResolver,companyDataResolver, companyJobsResolver, jobDetailsResolver],
})
export class PagesRoutingModule {
}
