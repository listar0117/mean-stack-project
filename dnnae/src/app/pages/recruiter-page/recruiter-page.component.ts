import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http-service';
import { Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Globals } from '../../Globals';

@Component({
  selector: 'app-recruiter-page',
  templateUrl: './recruiter-page.component.html',
  styleUrls: ['./recruiter-page.component.scss'],
})
export class RecruiterPageComponent implements OnInit {
  jobsData: any;
  isLoadingResults = true;
  jobs: any;
  userId: number;
  requestUrl: string;
  public globals = Globals;
  typeOfRequestSend: any;
  dataToSend: any;
  constructor(
    private service: HttpService,
    private router: Router,
    private ActivatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    // console.log(this.ActivatedRoute.data);
    // this.ActivatedRoute.data.pipe(map(data => data)).subscribe((res)=>{
    //   this.jobsData=res.jobsData;
    //   console.log(this.jobsData);
    // })
    const val = this.ActivatedRoute.snapshot.paramMap.get('recruiterUserName');
    console.log(val);
    this.jobs = this.getRecuiterJobs(val);
  }

  getRecuiterJobs(recruiterUserName) {
    const userNameArray = recruiterUserName.match(/\d+$/);
    if (userNameArray) {
      this.userId = parseInt(userNameArray[0]);
      this.dataToSend = {
        recruiterID: this.userId,
        // recruiterID: 2
      };
      this.requestUrl = this.globals.urls.getRecuiterJobs;
      this.typeOfRequestSend = this.service.postRequest(this.requestUrl, this.dataToSend);
      this.typeOfRequestSend.subscribe(
        res => {
          // console.log(res);
          if (res.data.isSuccess === true) {
            this.jobs = res.data.job;
            this.isLoadingResults = false;
            // this.service.showSuccess('Email sent successfully.', 'Forgot Password');
            // this.router.navigate(['/login']);
          } else if (res.data.isSuccess === false) {
            this.service.showError(res.data.message);
            this.isLoadingResults = false;
            this.router.navigate(['/'], { queryParamsHandling: 'preserve' });
          }
        },
        err => {
          if (err.error) console.log(err);
          // this.service.showError(err.error.message);
          this.isLoadingResults = false;
        }
      );
    }

    console.log(this.userId);
  }

  goto(data) {
    this.router.navigate(['/job', data.jobID], { queryParamsHandling: 'preserve' });
  }
}
