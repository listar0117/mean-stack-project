import { Component, OnInit } from '@angular/core';
// import { Subscription } from 'rxjs';
import { HttpService } from 'src/app/services/http-service';
import { HttpParams } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Globals } from '../../Globals';
import { map } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { Subscription, forkJoin } from 'rxjs';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss'],
})
export class VerifyEmailComponent implements OnInit {
  // Observables/Subscriptions
  httpSub$: Subscription = null;
  singleJobData: any;
  isLoadingResults = false;
  applyJob: FormGroup;
  isSubmitted = false;
  requestUrl: string;
  public globals = Globals;
  typeOfRequestSend: any;
  dataToSend: any;
  jobId: string;
  webId: string;
  userEmail: String;
  allUserData: any;
  allJobData: any;
  userData: any;
  constructor(
    public httpService: HttpService,
    private applyForm: FormBuilder,
    private router: Router,
    private ActivatedRoute: ActivatedRoute,
    private titleService: Title
  ) {}

  ngOnInit() {
    this.applyJob = this.applyForm.group({
      pinCode: ['', [Validators.required]],
    });
    // this.ActivatedRoute.data.pipe(map(data => data)).subscribe((res)=>{
    //   this.singleJobData=res.singleJobData;
    //   // console.log(this.singleJobData);
    // })
    const val = this.ActivatedRoute.snapshot.paramMap.get('id');
    const userId = this.ActivatedRoute.snapshot.paramMap.get('webUserId');
    this.webId = userId;
    this.jobId = val;
    console.log(val);
    // this.getJobData(val);
    this.getInitialRequests(val, userId);
    // this.userEmail= history.state.data && history.state.data.applicant.emailAddress;
    // console.log(history.state.data);
  }
  // getJobData(val){
  //   this.dataToSend = {
  //     jobID: val
  //     // recruiterID: 2
  //   }
  //   this.requestUrl = this.globals.urls.getJobData;
  //   this.typeOfRequestSend = this.httpService.postRequest(this.requestUrl, this.dataToSend);
  //   this.typeOfRequestSend.subscribe(
  //     res => {
  //       console.log(res);
  //       if (res.data.isSuccess === true) {
  //         this.singleJobData =  res.data.job;
  //         this.isLoadingResults = false;
  //         this.titleService.setTitle( this.singleJobData.companyName );
  //         // this.service.showSuccess('Email sent successfully.', 'Forgot Password');
  //         // this.router.navigate(['/login']);

  //       }
  //       else if (res.data.isSuccess === false) {
  //         // this.service.showError(res.data.message)
  //           this.isLoadingResults = false;
  //           this.router.navigate(['/']);
  //       }
  //     },
  //     err => {
  //       if(err.error)
  //       console.log(err)
  //       this.isLoadingResults = false;
  //       // this.service.showError(err.error.message);
  //     },
  //   );
  // }

  getInitialRequests(jobId, webUserId) {
    this.isLoadingResults = true;
    this.dataToSend = {
      jobID: jobId,
    };
    let params = new HttpParams().set('id', webUserId.toString());
    this.httpSub$ = forkJoin(
      this.httpService.getRequest(this.globals.urls.getSingleApplicant, params),
      this.httpService.postRequest(this.globals.urls.getJobData, this.dataToSend)
    )
      .pipe(
        map(([userData, jobData]) => {
          this.allUserData = userData;
          this.allJobData = jobData;
        })
      )
      .subscribe(
        data => {
          this.isLoadingResults = false;
          console.log(this.allUserData);
          console.log(this.allJobData);
          if (this.allJobData.data.isSuccess === true) {
            this.singleJobData = this.allJobData.data.job;
            this.isLoadingResults = false;
            this.titleService.setTitle(this.singleJobData.companyName);
            // this.httpService.showSuccess('Email sent successfully.', 'Forgot Password');
            // this.router.navigate(['/login']);
          } else if (this.allJobData.data.isSuccess === false) {
            // this.httpService.showError(res.data.message)
            this.isLoadingResults = false;
            this.router.navigate(['/'], { queryParamsHandling: 'preserve' });
          }
          if (this.allUserData.data.isSuccess === true) {
            this.userData = this.allUserData.data.applicant;
          } else if (this.allUserData.data.isSuccess === false) {
            this.httpService.showError(this.allUserData.data.message);
            this.router.navigate(['job', this.singleJobData._id, 'apply'], {
              queryParamsHandling: 'preserve',
            });
          }
        },
        err => {
          this.isLoadingResults = false;
          this.httpService.showError(err.error);
        }
      );
  }
  get formControls() {
    return this.applyJob.controls;
  }
  onSubmit() {
    console.log(this.applyJob.value);
    this.isSubmitted = true;
    if (this.applyJob.invalid) {
      return;
    } else {
      this.dataToSend = {
        verificationCode: parseInt(this.applyJob.value.pinCode, 10),
        webId: this.webId,
      };
      this.requestUrl = this.globals.urls.verifyApplicant;
      this.typeOfRequestSend = this.httpService.putRequest(this.requestUrl, this.dataToSend);
      this.typeOfRequestSend.subscribe(
        res => {
          console.log(res);
          if (res.data.isSuccess === true) {
            this.isLoadingResults = false;
            this.httpService.showSuccess(res.message, 'User Verified');
            this.router.navigate(['job', this.singleJobData._id, 'apply'], {
              queryParamsHandling: 'preserve',
            });
          } else if (res.data.isSuccess === false) {
            this.isLoadingResults = false;
            this.httpService.showError(res.message);
          }
        },
        err => {
          if (err.error) console.log(err);
          this.isLoadingResults = false;
          this.httpService.showError(err.error);
        }
      );
    }
  }
}
