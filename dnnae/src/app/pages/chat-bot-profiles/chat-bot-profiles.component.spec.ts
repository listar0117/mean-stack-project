import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatBotProfilesComponent } from './chat-bot-profiles.component';

describe('ChatBotProfilesComponent', () => {
  let component: ChatBotProfilesComponent;
  let fixture: ComponentFixture<ChatBotProfilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatBotProfilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatBotProfilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
