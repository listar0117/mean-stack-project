import { Component, OnInit } from '@angular/core';
import { Globals } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-chat-bot-profiles',
  templateUrl: './chat-bot-profiles.component.html',
  styleUrls: [
    '../recruiter-app/messages-main/messages-main.component.scss',
    './chat-bot-profiles.component.scss',
  ],
})
export class ChatBotProfilesComponent implements OnInit {
  messagingTemplates: any;
  messagesVarients: any;
  public globals = Globals;
  isLoadingResults = false;
  userData: any;
  requestUrl: string;
  typeOfRequestSend: any;
  jobID: string;
  constructor(
    private service: HttpService,
    private router: Router,
    private ActivatedRoute: ActivatedRoute
  ) {
    // this.jobID = this.ActivatedRoute.snapshot.paramMap.get('id');
    // console.log(this.jobID);
  }

  ngOnInit() {
    // this.messagingTemplates = this.globals.messagingTemplates;
    // console.log(this.messagingTemplates);

    // let item = JSON.parse(localStorage.getItem('UserData'));
    // this.userData = item;
    this.getMessagingTemplates();
  }
  getMessagingTemplates() {
    const dataToSend = {
      jobID: '5d7f449ea9a38a3a59b2427a',
      recruiterID: 9,
      bot:true
    };
    this.isLoadingResults = true;
    this.requestUrl = this.globals.urls.dnnaeWeb.getBatchMessageSettings;
    this.typeOfRequestSend = this.service.postRequest(this.requestUrl, dataToSend);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          console.log(res);
          this.messagingTemplates = res.data.sampleMessage;
          // localStorage.setItem('MessageTemplates', JSON.stringify(this.messagingTemplates));
          this.isLoadingResults = false;
        } else if (res.data.isSuccess === false) {
          this.service.showError('Something went wrong, Please try again');
          this.isLoadingResults = false;
          // this.router.navigate(['/recruiter/settings']);
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        // this.service.showError(err.error.message);
      }
    );
  }
  createFakeProfile(prospect) {
    const dataToSend = {
      firstName: prospect.name.split(' ').slice(0, -1).join(' '),
      lastName: prospect.name.split(' ').slice(-1).join(' '),
      connection_degree: prospect.connectionType === 'Old connection' ? '1st' : '2nd',
      status:  prospect.connectionType === 'Old connection' ? '' : 'ACCEPTED',
      final_score: prospect.score ==='High score' ? 50 : 1,
    };
    this.isLoadingResults = true;
    this.requestUrl = this.globals.urls.temp.createFakeProfile;
    this.typeOfRequestSend = this.service.postRequest(this.requestUrl, dataToSend);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          console.log(res);
          this.router.navigate(['/bot/messaging',res.data.id]);
          this.isLoadingResults = false;
        } else if (res.data.isSuccess === false) {
          this.service.showError('Something went wrong, Please try again');
          this.isLoadingResults = false;
          // this.router.navigate(['/recruiter/settings']);
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        // this.service.showError(err.error.message);
      }
    );
  }
}
