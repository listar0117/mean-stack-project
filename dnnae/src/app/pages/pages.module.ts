import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { CommonModule } from '@angular/common'; 
import { PagesRoutingModule } from './pages-routing.module';
import { HeaderComponent } from 'src/app/pages/components/header/header.component';
import {
  MatProgressSpinnerModule,
} from '@angular/material';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatCardModule } from '@angular/material/card';
// import { EnvServiceProvider } from '../services/env.service.provider';
// import { environment } from '../../environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { ApplyJobComponent } from './apply-job/apply-job.component';
import { FoterComponent } from './components/foter/foter.component';
import { RecruiterPageComponent } from './recruiter-page/recruiter-page.component';
import { SingleJobComponent } from './single-job/single-job.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { ChatBotProfilesComponent } from './chat-bot-profiles/chat-bot-profiles.component';
import { ChatBotMessagingComponent } from './chat-bot-messaging/chat-bot-messaging.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
// import { CommonPagesModule } from './common-pages/common-pages.module';
const PAGES_COMPONENTS = [
  PagesComponent,
  HeaderComponent,
  HomeComponent,
];
@NgModule({
  imports: [
    NgbModule,
    FormsModule,
    MatCardModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    PagesRoutingModule,
    CommonModule,
  ],
  exports: [
    MatProgressSpinnerModule,
  ],

  providers: [],

  declarations: [
    ...PAGES_COMPONENTS,
    ApplyJobComponent,
    FoterComponent,
    RecruiterPageComponent,
    SingleJobComponent,
    VerifyEmailComponent,
    ChatBotProfilesComponent,
    ChatBotMessagingComponent,
    PrivacyPolicyComponent,
  ],
})
export class PagesModule {
}
