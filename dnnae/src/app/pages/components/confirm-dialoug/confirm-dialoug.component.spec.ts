import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmDialougComponent } from './confirm-dialoug.component';

describe('ConfirmDialougComponent', () => {
  let component: ConfirmDialougComponent;
  let fixture: ComponentFixture<ConfirmDialougComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmDialougComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmDialougComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
