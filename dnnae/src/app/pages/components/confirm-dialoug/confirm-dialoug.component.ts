import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-confirm-dialoug',
  templateUrl: './confirm-dialoug.component.html',
  styleUrls: ['./confirm-dialoug.component.scss']
})
export class ConfirmDialougComponent implements OnInit {
  populateText: any = {}
  constructor(
    public dialogRef: MatDialogRef<ConfirmDialougComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }
  ngOnInit() {
    this.populateText = this.data;
  }
  onNoClick(id): void {
    this.dialogRef.close(id);
  }
}
