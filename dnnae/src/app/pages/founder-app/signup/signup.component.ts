import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Globals } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  // Observables/Subscriptions
  httpSub$: Subscription = null;

  //
  model: any = {};
  globals = Globals;
  userData: any;

  @ViewChild('form', { static: false }) form;
  isLoadingResults: boolean = false;
  // variable type null
  fileToUpload: File = null;
  // spId: string = null;
  buttonText: string = 'Next';
  public emailPattern = this.globals.regex.email;
  imagePath: any;
  imgURL: any;
  queryParams: string;
  // public passowrdPattren =this.globals.regex.passwordStrength;
  constructor(
    private httpService: HttpService,
    private ActivatedRoute: ActivatedRoute,
    private router: Router
  ) {
    // when editing, snapshot params will be required from '/:spId'
    // this.spId = this.route.snapshot.params.spId;
  }

  ngOnInit() {
    this.ActivatedRoute.queryParams.subscribe(params => {
      this.queryParams = params['calledFromExtension'];
      console.log(this.queryParams);
    });
  }

  // Upload Package Image
  uploadImage(files: FileList) {
    this.fileToUpload = files.item(0);
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = _event => {
      this.imgURL = reader.result;
    };
  }

  submit() {
    const nameString = `${this.model['firstName']} ${this.model['LastName']}`;
    const hasImage = this.fileToUpload ? 1 : 0;
    const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    const calledFrom = isMobile ? 'Mobile-web' : 'Desktop-web';
    // const params = {
    //   name:nameString,
    //   signupCompanyName: this.model['companyName'],
    //   emailAddress:this.model['email'],
    //   profileImage: this.model['imageString'],
    //   type:'EMAIL',
    //   calledFrom:'Web',
    //   image: this.fileToUpload,
    //   hasImage: this.fileToUpload ? 1:0
    // };
    const params = {
      name: nameString,
      signupCompanyName: this.model['companyName'],
      emailAddress: this.model['email'],
      profileImage: this.model['imageString'],
      type: 'EMAIL',
      calledFrom: 'Web',
      image: this.fileToUpload,
      hasImage: this.fileToUpload ? 1 : 0,
    };
    const formData = new FormData();
    formData.append('image', this.fileToUpload);
    formData.append('name', nameString);
    formData.append('signupCompanyName', this.model['companyName']);
    formData.append('emailAddress', this.model['email']);
    formData.append('hasImage', hasImage.toString());
    formData.append('type', 'EMAIL');
    formData.append('calledFrom', calledFrom);
    formData.append('accountType','founder');

    this.isLoadingResults = true;
    this.httpSub$ = this.httpService
      .postRequest(this.globals.urls.dnnaeWeb.signUp, formData, {}, false)
      .pipe(map(res => res.data))
      .subscribe(
        data => {
          const sendParams = {
            ...params,
            queryParams: this.queryParams,
          };
          this.isLoadingResults = false;
          this.httpService.showSuccess('User signed Up successfully, verify Email', 'Verify Email');
          this.router.navigate(['/founder/verify', 'signUp'], { state: { data: sendParams } });
          // this.router.navigate(['/verify']);
        },
        err => {
          this.isLoadingResults = false;
          this.httpService.showError(err);
        }
      );
  }

  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }
}
