import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RouteFounderGuard } from 'src/app/guards/route-founder.guard';

import { FounderAppComponent } from './founder-app.component';

import { SearchComponent } from './search/search.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { VerifyComponent } from './verify/verify.component';

const routes: Routes = [
  {
    path: '',
    component: FounderAppComponent,
    children: [
      {
        path: 'signup',
        component: SignupComponent,
        // pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'verify/:verifyType',
        component: VerifyComponent,
      },
      {
        path: 'home',
        component: HomeComponent,
      },
      {
        path: 'search',
        component: SearchComponent,
        canActivate: [RouteFounderGuard]
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '**',
    component: HomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FoundersRoutingModule {}
