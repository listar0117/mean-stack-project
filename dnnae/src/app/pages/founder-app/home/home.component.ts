import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Globals } from 'src/app/Globals';
@Component({
  selector: 'founder-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  userData: any;
  globals = Globals;
  constructor(private router: Router) {
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    if (this.userData) {
      this.router.navigate(['founder/search'], { queryParamsHandling: 'preserve' });
    }
  }

  ngOnInit() {}
}
