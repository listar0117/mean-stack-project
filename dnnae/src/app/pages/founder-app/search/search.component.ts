import { Component, OnInit, ViewChild, AfterViewInit, ElementRef, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/services/http-service';

import { ToastrService } from 'ngx-toastr';
import { Globals } from 'src/app/Globals';
import { Subscription, fromEvent } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  title: string;
  model: any = {};
  
  @ViewChild('form', { static: false }) form;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  // Observables/Subscriptions
  httpSub$: Subscription = null;
  // httpSearch$: Subscription = null;
  // Public Depedency
  public globals = Globals;
  // Type boolean variables
  isLoadingResults = false;
  totalRecords: any;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  searchData: any;
  columnsCompany: any;
  columnsPerson: any;
  companyData: any;
  personData: any;
  flagAdvSearch = 0;
  userData: any;
  typeOfRequestSend: any;
  allIndustries: string[];
  allInvestStage: string[];
  allInvestorType: string[];
  allTitles: string[];
  titleCtrl = new FormControl();
  industryCtrl = new FormControl();
  stageCtrl = new FormControl();
  investorCtrl = new FormControl();
  filteredTitles: Observable<string[]>;
  filteredIndustries: Observable<string[]>;
  filteredInvestStage: Observable<string[]>;
  filteredInvestorType: Observable<string[]>;
  @ViewChild('titleInput', {static: false}) titleInput: ElementRef<HTMLInputElement>;
  @ViewChild('industryInput', {static: false}) industryInput: ElementRef<HTMLInputElement>;
  @ViewChild('investorInput', {static: false}) investorInput: ElementRef<HTMLInputElement>;
  @ViewChild('stageInput', {static: false}) stageInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;
  @ViewChild('auto_1', {static: false}) matAutocomplete_1: MatAutocomplete;
  @ViewChild('auto_investor', {static: false}) matAutocomplete_investor: MatAutocomplete;
  @ViewChild('auto_stage', {static: false}) matAutocomplete_stage: MatAutocomplete;
  constructor(
    public service: HttpService,
    public toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  addType(event: MatChipInputEvent): void {
    if (!this.matAutocomplete_investor.isOpen) {
      const input = event.input;
      const cur_type = event.value;

      if ((cur_type || '').trim()) {
        this.searchData.types.push(cur_type);
      }

      if (input) {
        input.value = '';
      }
      this.investorCtrl.setValue(null);
    }
  }
  removeType(cur_type: String): void {
    const index = this.searchData.types.indexOf(cur_type);

    if (index >= 0) {
      this.searchData.types.splice(index, 1);
    }
  }

  selectedInvestor(event: MatAutocompleteSelectedEvent): void {
    this.searchData.types.push(event.option.viewValue);
    this.investorInput.nativeElement.value = '';
    this.investorCtrl.setValue(null);
  }

  addTitle(event: MatChipInputEvent): void {
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const cur_title = event.value;

      if ((cur_title || '').trim()) {
        this.searchData.titles.push(cur_title);
      }

      if (input) {
        input.value = '';
      }

      this.titleCtrl.setValue(null);
    }
  }
  removeTitle(cur_title: String): void {
    const index = this.searchData.titles.indexOf(cur_title);

    if (index >= 0) {
      this.searchData.titles.splice(index, 1);
    }
  }

  selectedTitle(event: MatAutocompleteSelectedEvent): void {
    this.searchData.titles.push(event.option.viewValue);
    this.titleInput.nativeElement.value = '';
    this.titleCtrl.setValue(null);
  }

  addIndustry(event: MatChipInputEvent): void {
    if (!this.matAutocomplete_1.isOpen) {
      const input = event.input;
      const cur_industry = event.value;

      if ((cur_industry || '').trim()) {
        this.searchData.industries.push(cur_industry);
      }

      if (input) {
        input.value = '';
      }
      this.industryCtrl.setValue(null);
    }
  }
  removeIndustry(cur_industry: String): void {
    const index = this.searchData.industries.indexOf(cur_industry);

    if (index >= 0) {
      this.searchData.industries.splice(index, 1);
    }
  }

  selectedIndustry(event: MatAutocompleteSelectedEvent): void {
    this.searchData.industries.push(event.option.viewValue);
    this.industryInput.nativeElement.value = '';
    this.industryCtrl.setValue(null);
  }

  addStage(event: MatChipInputEvent): void {
    if (!this.matAutocomplete_stage.isOpen) {
      const input = event.input;
      const cur_stage = event.value;

      if ((cur_stage || '').trim()) {
        this.searchData.stages.push(cur_stage);
      }

      if (input) {
        input.value = '';
      }
      this.stageCtrl.setValue(null);
    }
  }
  removeStage(cur_stage: String): void {
    const index = this.searchData.stages.indexOf(cur_stage);

    if (index >= 0) {
      this.searchData.stages.splice(index, 1);
    }
  }
  selectedStage(event: MatAutocompleteSelectedEvent): void {
    this.searchData.stages.push(event.option.viewValue);
    this.stageInput.nativeElement.value = '';
    this.stageCtrl.setValue(null);
  }

  addLocation(event: MatChipInputEvent): void {
    const input = event.input;
    const cur_location = event.value;

    if ((cur_location || '').trim()) {
      this.searchData.locations.push(cur_location);
    }

    if (input) {
      input.value = '';
    }
  }
  removeLocation(cur_location: String): void {
    const index = this.searchData.locations.indexOf(cur_location);

    if (index >= 0) {
      this.searchData.locations.splice(index, 1);
    }
  }

  addAvoid(event: MatChipInputEvent): void {
    const input = event.input;
    const cur_avoid = event.value;

    if ((cur_avoid || '').trim()) {
      this.searchData.avoids.push(cur_avoid);
    }

    if (input) {
      input.value = '';
    }
  }
  removeAvoid(cur_avoid: String): void {
    const index = this.searchData.avoids.indexOf(cur_avoid);

    if (index >= 0) {
      this.searchData.avoids.splice(index, 1);
    }
  }

  ngOnInit() {
    this.title = 'Founders Listing';
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.searchData = {
      types: [],
      titles: [],
      industries: [],
      stages: [],
      locations: [],
      avoids: []
    }
    this.flagAdvSearch = 0;
    this.initRecommend();
  }

  // ngAfterViewInit() {
  //   this.getNewRequests();
  // }

  query() {
    const dataToSend = {
      query: this.model.search,
    };
    this.isLoadingResults = true;
    this.httpSub$ = this.service
      .postRequest(this.globals.urls.founders.founderData, dataToSend)
      .pipe(map(res => res.data))
      .subscribe(
        data => {
          this.isLoadingResults = false;
          if (data.isSuccess === false) {
            this.service.showError(data.error);
          } else {
            console.log(data);
            this.searchData = {
              types: data.inv_type ? [data.inv_type] : [],
              titles: [],
              industries: data.category,
              stages: data.inv_stage ? [data.inv_stage] : [],
              locations: data.city ? [data.city] : [],
              avoids: []
            }
            this.flagAdvSearch = 1
          }
        },
        err => {
          // this.dataSource = [];
          this.isLoadingResults = false;
          this.searchData = {};
        }
    );
  }
  search() {
    let params= {
      investor_type: this.searchData.types.length > 0 ? this.searchData.types : [""],
      titles: this.searchData.titles.length > 0 ? this.searchData.titles : [""],
      industries: this.searchData.industries.length > 0 ? this.searchData.industries : [""],
      invest_stage: this.searchData.stages.length > 0 ? this.searchData.stages : [""],
      location: this.searchData.locations.length > 0 ? this.searchData.locations : [""],
      avoid_list: this.searchData.avoids.length > 0 ? this.searchData.avoids : [""]
    }
    console.log(params)
    this.isLoadingResults = true;
    let vm= this
    this.httpSub$ = this.service
      .postRequest(this.globals.urls.founders.founderSearch, params)
      .pipe(map(res => res.data))
      .subscribe(
        data => {
          this.flagAdvSearch = 2
          vm.isLoadingResults = false;
          if (data.isSuccess === false) {
            vm.service.showError(data.error);
          } else {
            vm.columnsCompany = []
            vm.companyData = []
            if (data.company.length>0) {
              let firstData= data.company[0].search_details
              firstData.forEach( item => {
                vm.columnsCompany.push(item.name)
              })
              data.company.forEach(company_item => {
                let cur_data = []
                company_item.search_details.forEach(item =>{
                  cur_data.push(item.data.join())
                })
                vm.companyData.push(cur_data)
              })
            }
            vm.columnsPerson = []
            if (data.person.length>0) {
              let firstData= data.person[0].search_details
              firstData.forEach( item => {
                vm.columnsPerson.push(item.name)
              })
            }
            console.log(vm.companyData)
          }
        },
        err => {
          // this.dataSource = [];
          this.isLoadingResults = false;
        }
      );
  }

  saveCampaign() {
    let params= {
      founderID: this.userData.recruiterID,
      campaignTitle: this.searchData.titles,
      companyName: '',
      campaignLocation: this.searchData.locations.join(","),
      investorType: this.searchData.types,
      industries: this.searchData.industries.join(","),
      invest_stage: this.searchData.stages,
    };
    this.typeOfRequestSend = this.service.postRequest(
      this.globals.urls.dnnaeWeb.createCampaign,
      params
    );
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          this.isLoadingResults = false;
          this.service.showSuccess('Campaign created successfully.', 'Campaign Created');
          this.router.navigate(['/founder/main'], { queryParamsHandling: 'preserve' });
        } else if (res.data.isSuccess === false) {
          this.service.showError(res.data.message);
          this.isLoadingResults = false;
          // this.router.navigate(['/']);
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        // this.service.showError(err.error.message);
      }
    );
  }
  initRecommend() {
    let vm= this
    this.httpSub$ = this.service
      .postRequest(this.globals.urls.founders.recommend,{})
      .pipe(map(res => res.data))
      .subscribe(
        data => {
          console.log(data)
          vm.allIndustries = data.industries
          vm.allInvestStage = data.invest_stage
          vm.allInvestorType = data.investor_type
          vm.allTitles = data.titles

          vm.filteredTitles = vm.titleCtrl.valueChanges.pipe(
            startWith(null),
            map((title: string | null) => title ? vm._filterTitle(title) : vm.allTitles.slice()));

          vm.filteredIndustries = vm.industryCtrl.valueChanges.pipe(
            startWith(null),
            map((industry: string | null) => industry ? vm._filterIndustry(industry) : vm.allIndustries.slice()));

          vm.filteredInvestStage = vm.stageCtrl.valueChanges.pipe(
            startWith(null),
            map((stage: string | null) => stage ? vm._filterStage(stage) : vm.allInvestStage.slice()));

          vm.filteredInvestorType = vm.investorCtrl.valueChanges.pipe(
            startWith(null),
            map((investor: string | null) => investor ? vm._filterInvestor(investor) : vm.allInvestorType.slice()));
        },
        err => {
          console.log(err)
        }
      );
  }

  private _filterTitle(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allTitles.filter(title => title.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filterIndustry(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allIndustries.filter(industry => industry.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filterStage(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allInvestStage.filter(stage => stage.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filterInvestor(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allInvestorType.filter(investor => investor.toLowerCase().indexOf(filterValue) === 0);
  }
}

