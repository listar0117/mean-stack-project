import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Globals } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { StorageService } from '../../../services/localStorage.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['../home/home.component.scss', './verify.component.scss'],
})
export class VerifyComponent implements OnInit {
  rawUserData: any;
  userData: any;
  // Observables/Subscriptions
  httpSub$: Subscription = null;
  typeOfRequestSend: any;
  verifyType: any;
  queryParams: string;
  @ViewChild('form', { static: false }) form;
  //
  model: any = {};
  globals = Globals;
  isLoadingResults: boolean = false;
  constructor(
    private router: Router,
    private httpService: HttpService,
    private route: ActivatedRoute,
    public storageService: StorageService
  ) {
    let item = JSON.parse(localStorage.getItem('UserData'));
    if (item) {
      this.router.navigate(['/founder/search'], { queryParamsHandling: 'preserve' });
    }
    console.log(this.router.getCurrentNavigation()); // should log out 'bar'
    // console.log(route.params.value.verifyType);
    this.route.params.subscribe(value => {
      console.log(value);
      this.verifyType = value.verifyType;
      // this.queryParams = value.queryParams;
    });
    this.rawUserData = this.router.getCurrentNavigation();
    this.userData = this.rawUserData.extras.state.data;
    this.queryParams =this.rawUserData.extras.state.data.queryParams
  }

  ngOnInit() {}
  submit() {
    const params = {
      emailAddress: this.userData.emailAddress,
      verificationCode: this.model['verify'],
      calledFrom: this.userData.calledFrom,
      type: 'EMAIL',
    };
    this.isLoadingResults = true;
    let sendUrl = '';
    if (this.verifyType == 'login') {
      sendUrl = this.globals.urls.dnnaeWeb.loginVerify;
    } else if (this.verifyType == 'signUp') {
      sendUrl = this.globals.urls.dnnaeWeb.signUpVerify;
    }

    this.typeOfRequestSend = this.httpService.postRequest(sendUrl, params);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          this.storageService.store('UserData', res.data);
          // localStorage.setItem('UserData', JSON.stringify(res.data));
          this.httpService.showSuccess('Email Verified Successfully', 'Email Verified');
          // this.router.navigate(['/recruiter/main'], { queryParamsHandling: 'preserve' });
          if (this.queryParams) {
            this.router.navigate(['/founder/search'], {
              queryParams: { calledFromExtension: this.queryParams },
            });
          } else {
            this.router.navigate(['/founder/search'], { queryParamsHandling: 'preserve' });
          }
        } else if (res.data.isSuccess === false) {
          this.httpService.showError(res.data.message);
          this.isLoadingResults = false;
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        this.httpService.showError(err.error);
      }
    );
  }
  resend() {
    const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    const params = {
      emailAddress: this.userData.emailAddress,
      type: 'EMAIL',
      calledFrom: isMobile ? 'Mobile-web' : 'Desktop-web',
    };
    this.isLoadingResults = true;
    this.typeOfRequestSend = this.httpService.postRequest(this.globals.urls.dnnaeWeb.login, params);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          this.httpService.showSuccess('Email Resent successfully', 'Resent Email');
          // this.router.navigate(['/recruiter/verify','login'], { state: { data: params } });
        } else if (res.data.isSuccess === false) {
          this.httpService.showError(res.data.message);
          this.isLoadingResults = false;
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        this.httpService.showError(err.error);
      }
    );
  }
  ngOnDestroy(): void {
    if (this.httpSub$) this.httpSub$.unsubscribe();
  }
}
