import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-founder-app',
  template: `
  <app-header-recruiter></app-header-recruiter>
  <router-outlet></router-outlet>
  `,
})
export class FounderAppComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
