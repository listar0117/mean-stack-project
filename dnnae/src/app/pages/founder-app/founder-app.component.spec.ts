import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FounderAppComponent } from './founder-app.component';

describe('FounderAppComponent', () => {
  let component: FounderAppComponent;
  let fixture: ComponentFixture<FounderAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FounderAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FounderAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
