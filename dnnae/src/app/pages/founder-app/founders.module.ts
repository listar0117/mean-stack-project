import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressSpinnerModule, MatFormFieldModule } from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FounderAppComponent } from './founder-app.component';
import { FoundersRoutingModule } from './founders-routing.module';
import { SearchComponent } from './search/search.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { TableTestComponent } from './components/table-test/table-test.component';
import { HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { GestureConfig } from '@angular/material';
import { RouteFounderGuard } from 'src/app/guards/route-founder.guard';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { VerifyComponent } from './verify/verify.component';

@NgModule({
  declarations: [
    FounderAppComponent,
    SearchComponent,
    HeaderComponent,
    TableTestComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent,
    VerifyComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FoundersRoutingModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    MatChipsModule,
    MatFormFieldModule,
    MatListModule,
    MatPaginatorModule,
    MatAutocompleteModule,
    MatTooltipModule
  ],
  exports:[
    MatProgressSpinnerModule,
    MatTableModule,
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
  ],
  providers: [{ provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig }, RouteFounderGuard],
})
export class FoundersModule { }
