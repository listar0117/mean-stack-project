import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatBotMessagingComponent } from './chat-bot-messaging.component';

describe('ChatBotMessagingComponent', () => {
  let component: ChatBotMessagingComponent;
  let fixture: ComponentFixture<ChatBotMessagingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatBotMessagingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatBotMessagingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
