import { Component, OnInit } from '@angular/core';
import { Globals } from 'src/app/Globals';
import { HttpService } from 'src/app/services/http-service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-chat-bot-messaging',
  templateUrl: './chat-bot-messaging.component.html',
  styleUrls: [
    '../recruiter-app/messages-main/messages-main.component.scss',
    './chat-bot-messaging.component.scss',
  ],
  providers: [NgbRatingConfig],
})
export class ChatBotMessagingComponent implements OnInit {
  public globals = Globals;
  isLoadingResults = false;
  typeOfRequestSend: any;
  requestUrl: string;
  profileId: String;
  allMessages: any;
  prospectMessages: any;
  currentRate: any;
  model: any;
  constructor(
    private service: HttpService,
    private router: Router,
    private ActivatedRoute: ActivatedRoute,
    config: NgbRatingConfig
  ) {
    this.profileId = this.ActivatedRoute.snapshot.paramMap.get('prospectId');
    config.max = 5;
  }

  ngOnInit() {
    this.getMessages();
    this.model = {
      messageText: '',
      type: 'SENT',
      linkedinMessageID: '123456',
      serverMessageID: '',
    };
  }

  getMessages() {
    const dataToSend = {
      recruiterID: 9,
      jobID: '5d7f449ea9a38a3a59b2427a',
      bot: true,
    };
    this.isLoadingResults = true;
    this.requestUrl = this.globals.urls.temp.getMessages;
    this.typeOfRequestSend = this.service.postRequest(this.requestUrl, dataToSend);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          console.log(res);
          this.allMessages = res.data.profileArray;
          const messages = this.allMessages.filter(profile => profile.entityUrn === this.profileId);
          console.log(messages);
          if (messages[0].intent_object) {
            // messages[0].intent_object.text = JSON.parse(messages[0].intent_object.text);
            messages[0].intent_object.text = messages[0].intent_object.text;
          }
          this.prospectMessages = messages[0];
          this.prospectMessages.messageArray.map(messages => {
            messages.rating = messages.rating ? messages.rating : 0;
          });
          // localStorage.setItem('MessageTemplates', JSON.stringify(this.messagingTemplates));
          this.isLoadingResults = false;
        } else if (res.data.isSuccess === false) {
          this.service.showError('Something went wrong, Please try again');
          this.isLoadingResults = false;
          // this.router.navigate(['/recruiter/settings']);
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        // this.service.showError(err.error.message);
      }
    );
  }
  send() {
    if (this.model.messageText.length > 0) {
      this.prospectMessages.messageArray.push(this.model);
    }
    const dataToSend = {
      recruiterID: 9,
      jobID: '5d7f449ea9a38a3a59b2427a',
      profileArray: [
        {
          entityUrn: this.prospectMessages.entityUrn,
          profileUrl: this.prospectMessages.profileUrl,
          messageArray: this.prospectMessages.messageArray,
        },
      ],
    };
    this.isLoadingResults = true;
    this.requestUrl = this.globals.urls.temp.setMessages;
    this.typeOfRequestSend = this.service.postRequest(this.requestUrl, dataToSend);
    this.typeOfRequestSend.subscribe(
      res => {
        // console.log(res);
        if (res.data.isSuccess === true) {
          this.getMessages();
          // localStorage.setItem('MessageTemplates', JSON.stringify(this.messagingTemplates));
          // this.isLoadingResults = false;
        } else if (res.data.isSuccess === false) {
          this.service.showError('Something went wrong, Please try again');
          this.isLoadingResults = false;
          // this.router.navigate(['/recruiter/settings']);
        }
      },
      err => {
        if (err.error) console.log(err);
        this.isLoadingResults = false;
        // this.service.showError(err.error.message);
      }
    );
  }
}
