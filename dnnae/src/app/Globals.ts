import { environment } from '../environments/environment';
// const baseUrl = environment.baseUrl;
const baseUrl = environment.baseUrl;
const founderBaseUrl = environment.founderBaseUrl;
const NlpbaseUrl = environment.nlpUrl;
const mailServerBaseUrl = environment.mailServer;
// console.log(EnvService);
const messagesVarients = {
  OPLS: `Hi {First_Name}, Hope things are well. I wanted to quickly message you to let you know of a new role I am searching for. If you or anyone in your network seems like a good fit, please let me know. Here are the details: {Job_link}. Talk to you soon {First_Name}`,
  OPHS: `Hi {First_Name}, Hope you are doing well. I am recruiting for this new {Job_Title} role and it seems like you are a really good fit. Please let me know if you have some time to speak about this? Here is the link: {Job_Link}`,
  NPNI: `Hey {First_Name}, thanks for writing back. Completely understand that this role might not be a fit right now. Shall I check back in a few months? Do you know anyone in your network that might be interested in this role? Here is the link again: {Job_Link}`,
  NPIHS: `Thanks {First_Name}, I look forward to speaking with you. Will any of these time slots work for you? If you book a time using the link below, it will book your selected slot on both our calendars with the call-in details. Speak soon. {Link_to_calendar}`,
  NPILS: `Thanks {First_Name}. If you could kindly apply at this link with your resume? {Job_link}. I look forward to reviewing it and discussing with the team. Speak soon`,
  NPHSNM: `Thanks for connecting with me {First_Name}. I am looking to recruit for this new {job_title} role and I believe you are a great fit. Do you have some time to speak this coming week or next?`,
  NPLSNM: `Thanks for connecting with me {First_Name}.
      I wanted to quickly message you to let you know of a new role I am searching for. If you or anyone in your network seems like a good fit, please let me know. Here are the details: {Job_link}. Talk to you soon {First_Name}`,
  Interested: 'I am Interested',
  NotInterested: 'Not Interested',
};
export const Globals = {
  appName: 'dnNae',
  defaultImage: 'https://storage.googleapis.com/dnnae/assets/dummy.png',
  desktopGif: 'https://storage.googleapis.com/dnnae/assets/desktop.gif',
  // dnNaeLogo:'https://storage.googleapis.com/dnnae/assets/download.png',
  dnNaeLogo: 'assets/download.png',

  token: '20fbae890e1c412d9a06319f11a26e14',
  regex: {
    // email: /^\w+(\+[\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
    email: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$/i,
    gmail: /^[A-Z0-9._%+-]+@g(oogle)?mail\.com$/i,
    // gmail: /^[A-Z0-9._%+-](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/i,
  },
  urls: {
    getRecuiterJobs: baseUrl + '/user/getJobs',
    getJobData: baseUrl + '/get/Job',
    getSingleSongData: baseUrl + '/list/:id',
    postApplicantData: baseUrl + '/post/applicants',
    postLead: baseUrl + '/post/lead',
    getSingleApplicant: baseUrl + '/get/webApplicant',
    verifyApplicant: baseUrl + '/put/VerifyjobApplicant',
    dnnaeWeb: {
      signUp: baseUrl + '/user/register',
      signUpVerify: baseUrl + '/user/verify',
      login: baseUrl + '/user/login',
      loginVerify: baseUrl + '/user/verify_email',
      extract: NlpbaseUrl + '/extract',
      createJD: NlpbaseUrl + '/generate',
      createJob: baseUrl + '/user/AddJob',
      createCampaign: baseUrl + '/user/AddCampaign',
      getCampaigns: baseUrl + '/user/getCampaigns',
      prospectsList: baseUrl + '/get-applicants',
      singleProspect: baseUrl + '/get/singleApplicant',
      getMessageSettings: baseUrl + '/post/get-message-settings',
      setMessageSettings: baseUrl + '/post/set-message-settings',
      getCalendlySettings: baseUrl + '/post/get-calendly-link',
      setCalendlySettings: baseUrl + '/post/set-calendly-link',
      getBatchMessageSettings: baseUrl + '/post/get-batchMessage-settings',
      setBatchMessageSettings: baseUrl + '/post/set-batchMessage-settings',
      deleteJob: baseUrl + '/delete/job',
      inbox: {
        getJobs: baseUrl + '/user/inbox/getJobsList',
        getApplicants: baseUrl + '/user/inbox/get-applicants',
        getMessages: baseUrl + '/user/inbox/get-Messages',
      },
      emailIntegration: {
        auth: mailServerBaseUrl + '/gmail/get_auth_link.php',
        saveEmailData: baseUrl + '/post/save-email-data',
      },
    },
    temp: {
      nlpIntent: NlpbaseUrl + '/detect_intent',
      vector: NlpbaseUrl + '/vector_search',
      createFakeProfile: baseUrl + '/post/chatbot/fakeProfiles',
      getMessages: baseUrl + '/get/applicant-messaging-from-bot',
      setMessages: baseUrl + '/post/applicant-messaging',
    },
    founders: {
      founderData: 'http://34.83.217.131:5000/api/predict',
      founderSearch: 'http://34.83.217.131:5000/api/advSearch',
      recommend: 'http://34.83.217.131:5000/api/recommend',
    },
  },
  jobStepper: [
    {
      id: 0,
      name: 'Job Title',
      enable: true,
      route: 'recruiter/jobTitle',
    },
    {
      id: 1,
      name: 'Company Name',
      enable: false,
      route: 'recruiter/companyName',
    },
    {
      id: 2,
      name: 'Industry Specialties',
      enable: false,
      route: 'recruiter/companySpecialties',
    },
    {
      id: 3,
      name: 'Required Experience',
      enable: false,
      route: 'recruiter/newJobExperience',
    },
    {
      id: 4,
      name: 'Required Education',
      enable: false,
      route: 'recruiter/newJobDegree',
    },
    {
      id: 5,
      name: 'Technical Skills',
      enable: false,
      route: 'recruiter/technicalSkills',
    },
    {
      id: 6,
      name: 'Additional Information',
      enable: false,
      route: 'recruiter/others',
    },
  ],
  JobQuestions: [
    { question: 'What is the job title of this new role?', tag: 'title' },
    { question: 'Got it. What type of work experience is required?', tag: 'experience' },
    {
      question: 'Thank you. What specific technical skills are required for this position?',
      tag: 'techSkills',
    },
    { question: 'Alright, Now what are the education requirements?', tag: 'education' },
    { question: 'Ok, How about soft skills required for this role?', tag: 'softSkills' },
    { question: 'And where will this position be located?', tag: 'location' },
    {
      question:
        'OK, let’s quickly talk about responsibilities. What are the day to day responsibilities?',
      tag: 'responsibility',
    },
    { question: 'Are there any responsibilities towards the team?', tag: 'teamResponsibility' },
    {
      question: 'Last question, What are the salary and benefits for this position?',
      tag: 'salaryBenefits',
    },
  ],
  messagingTemplates: [
    {
      name: 'Mary Fields',
      image: '',
      uid: 0,
      connectionType: 'New connection',
      score: 'High score',
      messagesThread: [
        {
          type: 'user',
          message: messagesVarients.Interested,
        },
        {
          type: 'dnnae',
          message: messagesVarients.NPIHS,
        },
        {
          type: 'user',
          message: messagesVarients.NotInterested,
        },
        {
          type: 'dnnae',
          message: messagesVarients.NPNI,
        },
      ],
      NoReplyThread: [
        {
          type: 'dnnae',
          message: messagesVarients.NPHSNM,
        },
        {
          type: 'user',
          message: messagesVarients.Interested,
        },
        {
          type: 'dnnae',
          message: messagesVarients.NPIHS,
        },
        {
          type: 'user',
          message: messagesVarients.NotInterested,
        },
        {
          type: 'dnnae',
          message: messagesVarients.NPNI,
        },
      ],
    },
    {
      name: 'John Johnson',
      image: '',
      uid: 1,
      connectionType: 'New connection',
      score: 'Low score',
      messagesThread: [
        {
          type: 'user',
          message: messagesVarients.Interested,
        },
        {
          type: 'dnnae',
          message: messagesVarients.NPILS,
        },
        {
          type: 'user',
          message: messagesVarients.NotInterested,
        },
        {
          type: 'dnnae',
          message: messagesVarients.NPNI,
        },
      ],
      NoReplyThread: [
        {
          type: 'dnnae',
          message: messagesVarients.NPLSNM,
        },
        {
          type: 'user',
          message: messagesVarients.Interested,
        },
        {
          type: 'dnnae',
          message: messagesVarients.NPILS,
        },
        {
          type: 'user',
          message: messagesVarients.NotInterested,
        },
        {
          type: 'dnnae',
          message: messagesVarients.NPNI,
        },
      ],
    },
    {
      name: 'Lisa Doe',
      image: '',
      uid: 2,
      connectionType: 'Old connection',
      score: 'High score',
      messagesThread: [
        {
          type: 'user',
          message: messagesVarients.Interested,
        },
        {
          type: 'dnnae',
          message: messagesVarients.OPHS,
        },
        {
          type: 'user',
          message: messagesVarients.NotInterested,
        },
        {
          type: 'dnnae',
          message: messagesVarients.NPNI,
        },
      ],
    },
    {
      name: 'Bill Smith',
      image: '',
      uid: 3,
      connectionType: 'Old connection',
      score: 'Low score',
      messagesThread: [
        {
          type: 'user',
          message: messagesVarients.Interested,
        },
        {
          type: 'dnnae',
          message: messagesVarients.OPLS,
        },
        {
          type: 'user',
          message: messagesVarients.NotInterested,
        },
        {
          type: 'dnnae',
          message: messagesVarients.NPNI,
        },
      ],
    },
  ],
};
