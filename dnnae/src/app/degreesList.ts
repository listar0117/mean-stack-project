export const Degrees=[
  {
    "name": "Accounting",
    "fullName": "BS Accounting",
    "Major": "Accounting",
    "Degree": "BS"
  },
  {
    "name": "Accounting",
    "fullName": "Bachelor of Accounting",
    "Major": "Accounting",
    "Degree": "Bachelor"
  },
  {
    "name": "Aerospace Studies",
    "fullName": "Bachelor of Aerospace Studies",
    "Major": "Aerospace Studies",
    "Degree": "Air Force ROTC"
  },
  {
    "name": "Air Force ROTC",
    "fullName": "Air Force ROTC",
    "Major": "Aerospace Studies",
    "Degree": "Air Force ROTC"
  },
  {
    "name": "Agribusiness",
    "fullName": "Bachelor of Agribusiness",
    "Major": "Agribusiness",
    "Degree": "Bachelor"
  },
  {
    "name": "Agribusiness",
    "fullName": "BS Agribusiness",
    "Major": "Agribusiness",
    "Degree": "BS"
  },
  {
    "name": "Agricultural Communication",
    "fullName": "BS Agricultural Communication",
    "Major": "Agricultural Communication",
    "Degree": "BS"
  },
  {
    "name": "Agricultural Communication",
    "fullName": "Bachelor of Agricultural Communication",
    "Major": "Agricultural Communication",
    "Degree": "Bachelor"
  },
  {
    "name": "Agricultural Education",
    "fullName": "BS Agricultural Education",
    "Major": "Agricultural Education",
    "Degree": "BS"
  },
  {
    "name": "Agricultural Education",
    "fullName": "Bachelor of Agricultural Education",
    "Major": "Agricultural Education",
    "Degree": "Bachelor"
  },
  {
    "name": "Agricultural Science",
    "fullName": "Bachelor of Agricultural Science",
    "Major": "Agricultural Science",
    "Degree": "Bachelor"
  },
  {
    "name": "Agricultural Science",
    "fullName": "BS Agricultural Science",
    "Major": "Agricultural Science",
    "Degree": "BS"
  },
  {
    "name": "Agricultural Systems Technology",
    "fullName": "BS Agricultural Systems Technology",
    "Major": "Agricultural Systems Technology",
    "Degree": "BS"
  },
  {
    "name": "Agricultural Systems Technology",
    "fullName": "Bachelor of Agricultural Systems Technology",
    "Major": "Agricultural Systems Technology",
    "Degree": "Bachelor"
  },
  {
    "name": "Agricultural Systems Technology and Agribusiness (Composite)",
    "fullName": "Bachelor of Agricultural Systems Technology and Agribusiness (Composite)",
    "Major": "Agricultural Systems Technology and Agribusiness (Composite)",
    "Degree": "Bachelor"
  },
  {
    "name": "Agricultural Systems Technology and Agribusiness (Composite)",
    "fullName": "BS Agricultural Systems Technology and Agribusiness (Composite)",
    "Major": "Agricultural Systems Technology and Agribusiness (Composite)",
    "Degree": "BS"
  },
  {
    "name": "American Studies",
    "fullName": "BS American Studies",
    "Major": "American Studies",
    "Degree": "BS"
  },
  {
    "name": "American Studies",
    "fullName": "BA American Studies",
    "Major": "American Studies",
    "Degree": "BA"
  },
  {
    "name": "Animal, Dairy, and Veterinary Sciences",
    "fullName": "BS Animal, Dairy, and Veterinary Sciences",
    "Major": "Animal, Dairy, and Veterinary Sciences",
    "Degree": "BS"
  },
  {
    "name": "Animal, Dairy, and Veterinary Sciences",
    "fullName": "Bachelor of Animal, Dairy, and Veterinary Sciences",
    "Major": "Animal, Dairy, and Veterinary Sciences",
    "Degree": "Bachelor"
  },
  {
    "name": "Anthropology",
    "fullName": "Bachelor of Anthropology",
    "Major": "Anthropology",
    "Degree": "Bachelor"
  },
  {
    "name": "Anthropology",
    "fullName": "BS of Anthropology",
    "Major": "Anthropology",
    "Degree": "BS"
  },
  {
    "name": "Anthropology",
    "fullName": "BA Anthropology",
    "Major": "Anthropology",
    "Degree": "BA"
  },
  {
    "name": "Applied Economics",
    "fullName": "BS Applied Economics",
    "Major": "Applied Economics",
    "Degree": "BS"
  },
  {
    "name": "Applied Economics",
    "fullName": "Bachelor of Applied Economics",
    "Major": "Applied Economics",
    "Degree": "Bachelor"
  },
  {
    "name": "Art",
    "fullName": "Bachelor of Art",
    "Major": "Art",
    "Degree": "Bachelor"
  },
  {
    "name": "Art",
    "fullName": "BA Art",
    "Major": "Art",
    "Degree": "BA"
  },
  {
    "name": "Art",
    "fullName": "BFA",
    "Major": "Art",
    "Degree": "BFA"
  },
  {
    "name": "Art History",
    "fullName": "BA Art History",
    "Major": "Art History",
    "Degree": "BA"
  },
  {
    "name": "Art History",
    "fullName": "Bachelor of Art History",
    "Major": "Art History",
    "Degree": "Bachelor"
  },
  {
    "name": "Asian Studies",
    "fullName": "BA Asian Studies",
    "Major": "Asian Studies",
    "Degree": "BA"
  },
  {
    "name": "Asian Studies",
    "fullName": "Bachelor of Asian Studies",
    "Major": "Asian Studies",
    "Degree": "Bachelor"
  },
  {
    "name": "Aviation Technology - Maintenance Management",
    "fullName": "Bachelor of Aviation Technology - Maintenance Management",
    "Major": "Aviation Technology - Maintenance Management",
    "Degree": "Bachelor"
  },
  {
    "name": "Aviation Technology - Maintenance Management",
    "fullName": "BS Aviation Technology - Maintenance Management",
    "Major": "Aviation Technology - Maintenance Management",
    "Degree": "BS"
  },
  {
    "name": "Aviation Technology - Professional Pilot",
    "fullName": "Bachelor of Aviation Technology - Professional Pilot",
    "Major": "Aviation Technology - Professional Pilot",
    "Degree": "Bachelor"
  },
  {
    "name": "Aviation Technology - Professional Pilot",
    "fullName": "BS Aviation Technology - Professional Pilot",
    "Major": "Aviation Technology - Professional Pilot",
    "Degree": "BS"
  },
  {
    "name": "Biochemistry",
    "fullName": "Bachelor of Biochemistry",
    "Major": "Biochemistry",
    "Degree": "Bachelor"
  },
  {
    "name": "Biochemistry",
    "fullName": "BS Biochemistry",
    "Major": "Biochemistry",
    "Degree": "BS"
  },
  {
    "name": "Biological Engineering",
    "fullName": "Bachelor of Biological Engineering",
    "Major": "Biological Engineering",
    "Degree": "Bachelor"
  },
  {
    "name": "Biological Engineering",
    "fullName": "BS Biological Engineering",
    "Major": "Biological Engineering",
    "Degree": "BS"
  },
  {
    "name": "Biological Science",
    "fullName": "Bachelor of Biological Science",
    "Major": "Biological Science",
    "Degree": "Bachelor"
  },
  {
    "name": "Biological Science",
    "fullName": "BS Biological Science",
    "Major": "Biological Science",
    "Degree": "BS"
  },
  {
    "name": "Biological Science",
    "fullName": "BA Biological Science",
    "Major": "Biological Science",
    "Degree": "BA"
  },
  {
    "name": "Biology",
    "fullName": "Bachelor of Biology",
    "Major": "Biology",
    "Degree": "Bachelor"
  },
  {
    "name": "Biology",
    "fullName": "BS Biology",
    "Major": "Biology",
    "Degree": "BS"
  },
  {
    "name": "Biology",
    "fullName": "BA Biology",
    "Major": "Biology",
    "Degree": "BA"
  },
  {
    "name": "Building Construction and Construction Management Certificate",
    "fullName": "Building Construction and Construction Management Certificate",
    "Major": "Building Construction and Construction Management Certificate",
    "Degree": "CC"
  },
  {
    "name": "CC",
    "fullName": "CC",
    "Major": "CC",
    "Degree": "CC"
  },
  {
    "name": "Business Education",
    "fullName": "BS Business Education",
    "Major": "Business Education",
    "Degree": "BS"
  },
  {
    "name": "Business Education",
    "fullName": "BA Business Education",
    "Major": "Business Education",
    "Degree": "BA"
  },
  {
    "name": "Business Education",
    "fullName": "Bachelor of  Business Education",
    "Major": "Business Education",
    "Degree": "Bachelor"
  },
  {
    "name": "Certified Nursing Assistant Certificate",
    "fullName": "Certified Nursing Assistant Certificate",
    "Major": "Certified Nursing Assistant Certificate",
    "Degree": "CNA"
  },
  {
    "name": "CNA",
    "fullName": "CNA",
    "Major": "CNA",
    "Degree": "CNA"
  },
  {
    "name": "Chemistry",
    "fullName": "BS Chemistry",
    "Major": "Chemistry",
    "Degree": "BS"
  },
  {
    "name": "Chemistry",
    "fullName": "Bachelor of Chemistry",
    "Major": "Chemistry",
    "Degree": "Bachelor"
  },
  {
    "name": "Chemistry Teaching",
    "fullName": "Bachelor of Chemistry Teaching",
    "Major": "Chemistry Teaching",
    "Degree": "Bachelor"
  },
  {
    "name": "Chemistry Teaching",
    "fullName": "BS Chemistry Teaching",
    "Major": "Chemistry Teaching",
    "Degree": "BS"
  },
  {
    "name": "Civil Engineering",
    "fullName": "BS Civil Engineering",
    "Major": "Civil Engineering",
    "Degree": "BS"
  },
  {
    "name": "Civil Engineering",
    "fullName": "Bachelor of Civil Engineering",
    "Major": "Civil Engineering",
    "Degree": "Bachelor"
  },
  {
    "name": "Climate Science",
    "fullName": "BS Climate Science",
    "Major": "Climate Science",
    "Degree": "BS"
  },
  {
    "name": "Climate Science",
    "fullName": "Bachelor of Climate Science",
    "Major": "Climate Science",
    "Degree": "Bachelor"
  },
  {
    "name": "Communication Studies",
    "fullName": "BA Communication Studies",
    "Major": "Communication Studies",
    "Degree": "BA"
  },
  {
    "name": "Communication Studies",
    "fullName": "BS Communication Studies",
    "Major": "Communication Studies",
    "Degree": "BS"
  },
  {
    "name": "Communication Studies",
    "fullName": "Bachelor of Communication Studies",
    "Major": "Communication Studies",
    "Degree": "Bachelor"
  },
  {
    "name": "Communicative Disorders and Deaf Education",
    "fullName": "Bachelor of Communicative Disorders and Deaf Education",
    "Major": "Communicative Disorders and Deaf Education",
    "Degree": "Bachelor"
  },
  {
    "name": "Communicative Disorders and Deaf Education",
    "fullName": "BS Communicative Disorders and Deaf Education",
    "Major": "Communicative Disorders and Deaf Education",
    "Degree": "BS"
  },
  {
    "name": "Communicative Disorders and Deaf Education",
    "fullName": "BA Communicative Disorders and Deaf Education",
    "Major": "Communicative Disorders and Deaf Education",
    "Degree": "BA"
  },
  {
    "name": "Computer Sciences",
    "fullName": "BS Computer Sciences",
    "Major": "Computer Sciences",
    "Degree": "BS"
  },
  {
    "name": "Computer Sciences",
    "fullName": "MS Computer Sciences",
    "Major": "Computer Sciences",
    "Degree": "MS"
  },
  {
    "name": "Computer Sciences",
    "fullName": "Bachelor in Computer Sciences",
    "Major": "Computer Sciences",
    "Degree": "Bachelor"
  },
  {
    "name": "Computer Sciences",
    "fullName": "Masters in Computer Sciences",
    "Major": "Computer Sciences",
    "Degree": "Masters"
  },
  {
    "name": "Computer Sciences",
    "fullName": "BCS",
    "Major": "Computer Sciences",
    "Degree": "Masters"
  },
  {
    "name": "Computer Sciences",
    "fullName": "MCS",
    "Major": "Computer Sciences",
    "Degree": "Masters"
  },
  {
    "name": "Computer Sciences",
    "fullName": "PHD in Computer Sciences",
    "Major": "Computer Sciences",
    "Degree": "PHD"
  }
]
