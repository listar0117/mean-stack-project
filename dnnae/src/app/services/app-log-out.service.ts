import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AppLogOutService {
  constructor(private router: Router) {
    console.log("here");
    this.logOut();
  }
  logOut() {
    console.log("logOut service");
    window.addEventListener(
      'storage',
      event => {
        if (event.storageArea == localStorage) {
          let token = localStorage.getItem('UserData');
          if (token == undefined) {
            // you can update this as per your key
            // DO LOGOUT FROM THIS TAB AS WELL
            this.router.navigate(['recruiter/login'], { queryParamsHandling: 'preserve' });
          }
        }
      },
      false
    );
  }
}
