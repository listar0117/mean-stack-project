
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PreloadSelectedModulesList } from './services/preload_selected_modules_list';
// import { AuthGuard } from './guards/auth-guard.service';

const routes: Routes = [

  // place: canActivate: [AuthGuard] when connecting full app
 
  // Module for recruiters, replica of android App
  { path: 'recruiter',
   loadChildren: './pages/recruiter-app/recruiter-app.module#RecruiterAppModule',
  //  pathMatch: 'full'
  },
  { path: 'founder',
   loadChildren: './pages/founder-app/founders.module#FoundersModule',
  //  pathMatch: 'full'
  },
  { path: 'traction',
   loadChildren: './pages/traction-app/traction-app.module#TractionAppModule',
  //  pathMatch: 'full'
  },
 // web module for all users
 { path: '', loadChildren: './pages/pages.module#PagesModule'},
  // { path: '', component: HomeComponent },
  { path: '**', redirectTo: 'dashboard' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { initialNavigation: 'enabled' })],  //initialNavigation: 'enabled' for removing flick
  exports: [RouterModule],
})
export class AppRoutingModule {
}
