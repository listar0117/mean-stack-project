import { Injectable } from '@angular/core';

import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { Observable, of } from 'rxjs';
import { Globals } from '../Globals';
import { HttpService } from '../services/http-service';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class RouteGuard implements CanActivate {
  constructor(private httpService: HttpService, private _router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    let userData: any = JSON.parse(localStorage.getItem('UserData'));
    let jobData: any = JSON.parse(sessionStorage.getItem('tempJobData'));
    let pdfData: any = JSON.parse(sessionStorage.getItem('pdfData'));
    let prospectData: any = JSON.parse(sessionStorage.getItem('prospectData'));
    // let role = route.data['roles'] as string;
    console.log(route);

    if (userData) {
      if (route.routeConfig.path == 'jobDescription/:from') {
        if (jobData || pdfData) {
          return true;
        } else {
          this._router.navigate(['recruiter/newJob']);
        }
      } else if (route.routeConfig.path == 'prospectDetails') {
        if (prospectData) {
          return true;
        } else {
          this._router.navigate(['recruiter/prospects']);
        }
      }
      // else if(route.routeConfig.path == 'home'){
      //   this._router.navigate(['recruiter/main']);
      // }
      return true;
      console.log('true i am in');
      // let adminData = JSON.parse(userData);

      // let returnValue = true;

      // if (!adminData[role]) {

      //   returnValue = false;

      // }

      // return returnValue;
    } else {
      this._router.navigate(['recruiter/home']);
      return false;
      console.log('false i am out');
    }
  }
}
