import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { HttpService } from '../services/http-service';

@Injectable()
export class RouteFounderGuard implements CanActivate {
  constructor(private httpService: HttpService, private _router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    let userData: any = JSON.parse(localStorage.getItem('UserData'));
    if (userData) {
      return true;
    } else {
      this._router.navigate(['founder/home']);
      return false;
    }
  }
}
