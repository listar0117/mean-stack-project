const fs = require('fs');
const mongoose = require('mongoose');
// const option = {
//   socketTimeoutMS: 30000,
//   keepAlive: true,
//   reconnectTries: 30000,
//   useNewUrlParser: true,
// };

const options = { keepAlive: 300000, connectTimeoutMS: 30000, useNewUrlParser: true };
const applicantsModel = require('./models/applicants');

let dev_db_url = 'mongodb://localhost:27017/dnNae';
let mongoDB = process.env.MONGODB_URI || dev_db_url;
// mongoose.connect(mongoDB, { useNewUrlParser: true });

mongoose.connect(mongoDB, options);
mongoose.Promise = global.Promise;

console.log('here');

// get conversations
applicantsModel.find({ _id: '5dafacaa14657649cbdbaf3b' }).then(applicants => {
  console.log('total applicants: ', applicants[0].profileArray.length);
//   let profileArrays = [];
  // console.log('applicants>>>>>>.....', applicants);
//   applicants.map(applicantData => {
//     applicantData.profileArray.map(applicant => {
//       const messages = {
//         entityUrn: applicant.entityUrn,
//         messageArray: applicant.messageArray,
//       };
//       console.log('=========================================');
//       // profileArrays = profileArrays.concat(messages);
//       profileArrays.push(messages);
//     });
//   });
//   console.log(profileArrays.length);
//   let removedDupes = [];
  // removedDupes = getUnique(profileArrays, 'entityUrn');
  const removedDupes = applicants;
  console.log(removedDupes.length);
  fs.writeFile('recruiters.json', JSON.stringify(removedDupes), 'utf8', function(err) {
    console.log(err);
  });
});
