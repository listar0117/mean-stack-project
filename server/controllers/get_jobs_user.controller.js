const commonService = require('../services/comon.service');
const jobService = require('../services/get_jobs_user.service');

const getJobs = async ctx => {
  if (!ctx.request.body.recruiterID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }
  const job = await jobService.getJobs(ctx.request.body);
  if (job && job !== null) {
    ctx.status = 200;
    ctx.body = {
      job,
      isSuccess: true,
      action: 'NA',
      message: 'Jobs fetched Successfully',
    };
    return;
  }
  if (job === null) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      action: '',
      message: 'No Jobs Found',
    };
    return;
  }
};

// get single job making for web
const getJob = async ctx => {
  // if (!ctx.request.body.recruiterID) {
  //   ctx.status = 400;
  //   ctx.body = {
  //     isSuccess: false,
  //     message: 'Please provide recuriter Id',
  //   };
  //   return;
  // }
  const job = await jobService.getJob(ctx.request.body);
  // console.log('got Job', job);
  if (job && job.err) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      action: '',
      message: 'Job not found',
    };
    return;
  }
  if (job && job !== null) {
    ctx.status = 200;
    ctx.body = {
      job,
      isSuccess: true,
      action: 'NA',
      message: 'Job fetched Successfully',
    };
    return;
  }
  if (job === null) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      action: '',
      message: 'No Jobs Found',
    };
    return;
  }
};
module.exports = {
  getJobs,
  getJob,
};
