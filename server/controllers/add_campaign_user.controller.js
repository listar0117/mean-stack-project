const commonService = require('../services/comon.service');
const loggedinUser = require('../services/add_campaign_user.service');

// Add Campaign
const addCampaign = async ctx => {
  // console.log('paramsssssssssssss', ctx.request.body);
  const body = ctx.request.body;
  if (!body.founderID.toString()) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide Founder Id',
    };
    return;
  }
  if (body.campaignTitle.length == 0) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide Campaign Title',
    };
    return;
  }
  
  
  // if (!body.jobLinkedInSearchKeyword) {
  //   ctx.status = 400;
  //   ctx.body = {
  //     isSuccess: false,
  //     message: 'Please provide jobLinkedInSearchKeyword field',
  //   };
  //   return;
  // }
  
  const campaign = await loggedinUser.createCampaign(body);

  // if (job && job.DialogErr) {
  //   ctx.status = 200;
  //   ctx.body = {
  //     isSuccess: false,
  //     // message: job.err,
  //     message: job.DialogErr,
  //     action: '',
  //   };
  //   return;
  // }
  // if (job && job.err) {
  //   ctx.status = 200;
  //   ctx.body = {
  //     isSuccess: false,
  //     // message: job.err,
  //     message: 'Something went wrong',
  //     action: '',
  //   };
  //   return;
  // }
  // if (job && job.noJob === true) {
  //   ctx.status = 200;
  //   ctx.body = {
  //     isSuccess: false,
  //     // message: job.err,
  //     message: 'Job Not found',
  //     action: '',
  //   };
  //   return;
  // }
  ctx.status = 200;
  ctx.body = {
    ...campaign,
    isSuccess: true,
    action: 'NA',
    message: 'New campaign created successfully',
  };
  return;
};

module.exports = {
  addCampaign,
};
