const integratedEmailService = require('../../services/mailIntegration/set_mailIntegration_settings.service');

const setMail = async ctx => {
  if (!ctx.request.body.recruiterID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }
  if (!ctx.request.body.integratedEmailAddress) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide email address.',
    };
    return;
  }
  if (!ctx.request.body.integratedEmailToken) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide Token provided by Gmail.',
    };
    return;
  }
  //   if (!ctx.request.body.jobID) {
  //     ctx.status = 400;
  //     ctx.body = {
  //       isSuccess: false,
  //       message: 'Please provide job Id',
  //     };
  //     return;
  //   }

  const emailData = await integratedEmailService.setMail(ctx.request.body);
  // console.log('Applicant populated', populateApplicant);
  if (emailData && emailData.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'Some thing went wrong',
      action: '',
    };
    return;
  }
  if (emailData && emailData.mailError) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'Some thing went wrong at Gmail end. Unable to integrate Gmail',
      action: '',
    };
    return;
  }
  if (emailData.updated === true) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: true,
      message: 'Email integrated successfully',
      action: '',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'unable to integrate recruiterID not correct',
    action: '',
  };
  return;
};

module.exports = {
  setMail,
};
