const applicantMessages = require('../../services/applicants_messages/add_applicants_message.service');

const addMessage = async ctx => {
  if (!ctx.request.body.recruiterID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }
  if (!ctx.request.body.jobID && !ctx.request.body.jobID.toString() === '0') {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide jobID',
    };
    return;
  }
  if (
    !ctx.request.body.profileArray ||
    (ctx.request.body.profileArray && ctx.request.body.profileArray.length <= 0)
  ) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please populate profileArray',
    };
    return;
  }
  const populateMessages = await applicantMessages.addMessages(ctx.request.body);
  // console.log('Applicant populated', populateApplicant);
  if (populateMessages && populateMessages.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'JobId Incorrect. Unable to add messages1',
      action: '',
    };
    return;
  }
  if (populateMessages && populateMessages.notFound) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'JobId Incorrect. Unable to add messages2',
      action: '',
    };
    return;
  }
  if (populateMessages !== null) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: true,
      message: 'New messages added successfully',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'unable to add messages recruiterID not correct',
    action: '',
  };
  return;
};

module.exports = {
  addMessage,
};
