// const commonService = require('../../services/comon.service');
const messages = require('../../services/applicants_messages/get_inbox_messages.service');

const getMessages = async ctx => {
  if (!ctx.request.body.recruiterID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }
  if (!ctx.request.body.jobID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide Job Id',
    };
    return;
  }
  if (!ctx.request.body.entity) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide user entity',
    };
    return;
  }

  const getMessages = await messages.getMessages(ctx.request.body);
  // console.log('get Applicants', getApplicantsResponse);
  if (getMessages && getMessages.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: getMessages.err,
      action: '',
    };
    return;
  }
  if (getMessages && getMessages.notFound) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'No messages found',
      action: '',
    };
    return;
  }
  if (getMessages !== null) {
    ctx.status = 200;
    ctx.body = {
      messagesData: getMessages,
      isSuccess: true,
      message: 'Got new messages successfully',
      action: '',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'recruiterId Incorrect',
    action: '',
  };
  return;
};

module.exports = {
  getMessages,
};
