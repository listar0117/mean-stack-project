// const commonService = require('../../services/comon.service');
const messages = require('../../services/applicants_messages/get_applicants_message.service');

const getMessage = async ctx => {
  if (!ctx.request.body.recruiterID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }
  if (!ctx.request.body.jobID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide Job Id',
    };
    return;
  }

  const getMessagesResponse = await messages.getMessages(ctx.request.body);
  // console.log('get Applicants', getApplicantsResponse);
  if (getMessagesResponse && getMessagesResponse.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: getMessagesResponse.err,
      action: '',
    };
    return;
  }
  if (getMessagesResponse && getMessagesResponse.notFound) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'No messages found',
      action: '',
    };
    return;
  }
  if (getMessagesResponse && getMessagesResponse.nlpError) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'Some thing went wrong at NLP end. Unable to detect intent',
      action: '',
    };
    return;
  }
  if (getMessagesResponse !== null) {
    ctx.status = 200;
    ctx.body = {
      profileArray: getMessagesResponse.messagesArray,
      isSuccess: true,
      message: 'Got new messages successfully',
      action: '',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'recruiterId Incorrect',
    action: '',
  };
  return;
};

module.exports = {
  getMessage,
};
