const linkSettingsService = require('../../services/calendly_settings.service');

const setLink = async ctx => {
  if (!ctx.request.body.recruiterID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }
  if (!ctx.request.body.link) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide calendly link.',
    };
    return;
  }
  //   if (!ctx.request.body.jobID) {
  //     ctx.status = 400;
  //     ctx.body = {
  //       isSuccess: false,
  //       message: 'Please provide job Id',
  //     };
  //     return;
  //   }

  const linkData = await linkSettingsService.setLink(ctx.request.body);
  // console.log('Applicant populated', populateApplicant);
  if (linkData && linkData.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'Some thing went wrong',
      action: '',
    };
    return;
  }
  if (linkData.updated === true) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: true,
      message: 'Calendly link updated successfully',
      action: '',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'unable to add link recruiterID not correct',
    action: '',
  };
  return;
};

module.exports = {
  setLink,
};
