const settingsService = require('../../services/messageSettings.service');

const getMessageSettings = async ctx => {
  if (!ctx.request.body.recruiterID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }
  if (!ctx.request.body.jobID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide job Id',
    };
    return;
  }

  const messageData = await settingsService.getSettings(ctx.request.body);
  // console.log('Applicant populated', populateApplicant);
  if (messageData && messageData.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'Some thing went wrong',
      action: '',
    };
    return;
  }
  if (messageData !== null) {
    ctx.status = 200;
    ctx.body = {
      ...messageData,
      isSuccess: true,
      message: 'Message settings fetched successfully',
      action: 'set_message_settings',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'unable to add applicants recruiterID not correct',
    action: '',
  };
  return;
};

module.exports = {
  getMessageSettings,
};
