const linkSettingsService = require('../../services/calendly_settings.service');

const getLink = async ctx => {
  if (!ctx.request.body.recruiterID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }
  // if (!ctx.request.body.jobID) {
  //   ctx.status = 400;
  //   ctx.body = {
  //     isSuccess: false,
  //     message: 'Please provide job Id',
  //   };
  //   return;
  // }

  const linkData = await linkSettingsService.getLink(ctx.request.body);
  // console.log('Applicant populated', populateApplicant);
  if (linkData && linkData.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'Some thing went wrong',
      action: '',
    };
    return;
  }
  if (linkData !== null) {
    ctx.status = 200;
    ctx.body = {
      ...linkData,
      isSuccess: true,
      message: 'Calendly link fetched successfully',
      action: 'set_message_settings',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'unable to add link recruiterID not correct',
    action: '',
  };
  return;
};

module.exports = {
  getLink,
};
