const settingsService = require('../../services/messageSettings.service');

const setMessageSettings = async ctx => {
  if (!ctx.request.body.recruiterID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }
  if (!ctx.request.body.sampleMessage) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide sample Message',
    };
    return;
  }
  if (!ctx.request.body.jobID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide job Id',
    };
    return;
  }

  const messageData = await settingsService.setSettings(ctx.request.body);
  // console.log('Applicant populated', populateApplicant);
  if (messageData && messageData.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'Some thing went wrong',
      action: '',
    };
    return;
  }
  if (messageData.updated === true) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: true,
      message: 'Message settings updated successfully',
      action: '',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'unable to add applicants recruiterID not correct',
    action: '',
  };
  return;
};

module.exports = {
  setMessageSettings,
};
