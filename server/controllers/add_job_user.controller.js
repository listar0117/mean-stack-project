const commonService = require('../services/comon.service');
const loggedinUser = require('../services/add_job_user.service');

// Add Job
const addJob = async ctx => {
  // console.log('paramsssssssssssss', ctx.request.body);
  const body = ctx.request.body;
  if (!body.recruiterID.toString()) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }
  if (!body.jobTitle) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide JobTitle',
    };
    return;
  }
  // if (!body.companyName) {
  //   ctx.status = 400;
  //   ctx.body = {
  //     isSuccess: false,
  //     message: 'Please provide Company Name',
  //   };
  //   return;
  // }
  // if (!body.jobLocation) {
  //   ctx.status = 400;
  //   ctx.body = {
  //     isSuccess: false,
  //     message: 'Please provide Job Location',
  //   };
  //   return;
  // }
  if (!body.jdType) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide jdType i.e TEXT or PDF',
    };
    return;
  }
  if (body.jdType === 'PDF' && !body.jdFile) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide jdFile url',
    };
    return;
  }
  // if (!body.jobLinkedInSearchKeyword) {
  //   ctx.status = 400;
  //   ctx.body = {
  //     isSuccess: false,
  //     message: 'Please provide jobLinkedInSearchKeyword field',
  //   };
  //   return;
  // }
  // if ((body.jobDescription && body.jobDescription.length <= 0) || !body.jobDescription) {
  //   ctx.status = 400;
  //   ctx.body = {
  //     isSuccess: false,
  //     message: 'Please provide job description',
  //   };
  //   return;
  // }
  if ((body.jobArray && body.jobArray.length <= 0) || !body.jobArray) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide job Array',
    };
    return;
  }
  const job = await loggedinUser.createjob(body);

  if (job && job.DialogErr) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      // message: job.err,
      message: job.DialogErr,
      action: '',
    };
    return;
  }
  if (job && job.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      // message: job.err,
      message: 'Something went wrong',
      action: '',
    };
    return;
  }
  if (job && job.noJob === true) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      // message: job.err,
      message: 'Job Not found',
      action: '',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    ...job,
    isSuccess: true,
    action: 'NA',
    message: 'New job created successfully',
  };
  return;
};

module.exports = {
  addJob,
};
