const fakeProfileService = require('../../services/chatbot/chatbot_and_fakeProfile.service');

const addProfile = async ctx => {
  //   if (!ctx.request.body.recruiterID) {
  //     ctx.status = 400;
  //     ctx.body = {
  //       isSuccess: false,
  //       message: 'Please provide recruiter Id',
  //     };
  //     return;
  //   }

  //   if (!ctx.request.body.jobID) {
  //     ctx.status = 400;
  //     ctx.body = {
  //       isSuccess: false,
  //       message: 'Please provide job Id',
  //     };
  //     return;
  //   }
  ctx.request.body.recruiterID = 9;
  ctx.request.body.jobID = '5d7f449ea9a38a3a59b2427a';

  const fakeProfileData = await fakeProfileService.addProfile(ctx.request.body);
  // console.log('Applicant populated', populateApplicant);
  if (fakeProfileData && fakeProfileData.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'Some thing went wrong',
      action: '',
    };
    return;
  }
  if (fakeProfileData.updated === true) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: true,
      id: fakeProfileData.id,
      message: 'Profile created successfully',
      action: '',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'unable to create profile',
    action: '',
  };
  return;
};

module.exports = {
  addProfile,
};
