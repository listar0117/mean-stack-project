// import asyncBusboy from 'async-busboy';
const asyncBusboy = require('async-busboy');

const UploadService = require('../services/upload.service');

const UserService = require('../services/user.service');
const commonService = require('../services/comon.service');
const User = require('../models/user');

// testing function needs to b deleted
const GetUsers = function GetUsers(ctx) {
  console.log('ctx: ', ctx);
  // ctx.body = {users:'users'};
  const users = UserService.getAllUsers();
  console.log(users);
  ctx.body = users;
  console.log('the response status is ', ctx.status);
  // return ctx;
};

// sign up user
const registerUser = async ctx => {
  const { files, fields } = await asyncBusboy(ctx.req);
  const body = fields;
  body.hasImage = parseInt(body.hasImage, 10);
  const pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

  // if (!pattern.test(body.image)) {
  if (!body.image && body.hasImage === 1 && files.length > 0) {
    const date = Date.now().toString();
    const gcsname = `${date}${files[0].filename}`;
    files[0].newFileName = gcsname;
    try {
      const uploadImage = await UploadService.uploadImage(files);
      // const publicUrl = UploadService.getPublicUrl(`signUpImages/${gcsname}`);
      body.image = uploadImage;
    } catch (err) {
      console.log('error =====>>>>>', JSON.stringify(err));
      console.log('error on uploading picture', err);
      ctx.body = {
        isSuccess: false,
        message: 'unable to upload file, error occurred while uploading file',
      };
      ctx.status = 500;
      return;
    }
  }

  if (!body.emailAddress) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide an Email Address',
    };
    return;
  }
  if (!body.calledFrom) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'calledFrom field is required',
    };
    return;
  }
  if (!body.name) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide name',
    };
    return;
  }
  if (!body.hasImage === 0 || !body.hasImage === 1) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide hasImage field',
    };
    return;
  }
  if (body.hasImage === 1 && !body.image) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide an image',
    };
    return;
  }
  if (!body.type) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide Signup type field',
    };
    return;
  }
  if (body.type === 'LINKEDIN') {
    if (!body.linkedInAccessToken) {
      ctx.status = 400;
      ctx.body = {
        isSuccess: false,
        message: 'Please provide Linkedin Access Token',
      };
      return;
    }
    if (!body.linkedInPersonID) {
      ctx.status = 400;
      ctx.body = {
        isSuccess: false,
        message: 'Please provide Linkedin Person Id',
      };
      return;
    }
  }
  const user = await UserService.createUser(body);
  if (user) {
    if (user.type === 'new' && user.LoginType.toUpperCase() === 'EMAIL') {
      ctx.body = {
        // ...user.user.toObject(),
        isSuccess: true,
        message: 'User registered successfully',
        action: 'EMAIL_VERIFICATION',
      };
      ctx.status = 200;
    } else if (user.type === 'new' && user.LoginType.toUpperCase() === 'LINKEDIN') {
      ctx.body = {
        ...user.user,
        isSuccess: true,
        message: 'User registered successfully',
        action: 'SIGN_UP_SUCCESS',
      };
      ctx.status = 200;
    } else if (user.type === 'existing') {
      ctx.body = {
        data: user.user.email,
        isSuccess: false,
        message: 'User already exists on the application',
        action: '',
      };
      ctx.status = 200;
    } else if (user.err) {
      ctx.body = {
        data: user.err,
        isSuccess: false,
        message: user.err,
        action: '',
      };
      ctx.status = 500;
    }
  } else {
    ctx.body = {
      isSuccess: false,
      message: 'Unable to sign up user',
    };
    ctx.status = 500;
  }
};
// verify User after SignUp
const verifyRegisteredUser = async ctx => {
  if (!ctx.request.body.emailAddress) {
    ctx.status = 400;
    ctx.body = {
      error: 'Email Expected',
      isSuccess: false,
      message: 'Please Provide an Email',
    };
    return;
  }
  if (!ctx.request.body.calledFrom) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'calledFrom field is required',
    };
    return;
  }
  if (!ctx.request.body.type) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide Signup type field',
    };
    return;
  }
  const authenticated = await UserService.verifyUser(ctx.request.body);
  if (!authenticated.err) {
    ctx.body = {
      ...authenticated,
      isSuccess: true,
      message: authenticated.err,
      action: 'SIGN_UP_SUCCESS',
    };
    ctx.status = 200;
    return;
  }
  if (authenticated.err) {
    ctx.body = {
      //   ...authenticated,
      isSuccess: false,
      message: 'You have entered an incorrect verification code. Please try again.',
    };
    ctx.status = 200;
  }
};

// login User
const login = async ctx => {
  if (!ctx.request.body.emailAddress) {
    ctx.status = 400;
    ctx.body = {
      error: 'Email Expected',
      isSuccess: false,
      message: 'Please Provide an Email',
    };
    return;
  }
  if (!ctx.request.body.calledFrom) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'calledFrom field is required',
    };
    return;
  }
  if (!ctx.request.body.type) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide Signin type field',
    };
    return;
  }
  if (ctx.request.body.type === 'LINKEDIN') {
    if (!ctx.request.body.linkedInAccessToken) {
      ctx.status = 400;
      ctx.body = {
        isSuccess: false,
        message: 'Please provide Linkedin Access Token',
      };
      return;
    }
    if (!ctx.request.body.linkedInPersonID) {
      ctx.status = 400;
      ctx.body = {
        isSuccess: false,
        message: 'Please provide Linkedin Person Id',
      };
      return;
    }
  }
  const verifySignUp = await UserService.signdupAndVerified(ctx.request.body);
  // console.log(verifySignUp.user.name);
  if (verifySignUp && verifySignUp.type === 'old') {
    if (verifySignUp.LoginType === 'EMAIL') {
      const verificationCode = await commonService.sendEmailOnLogin(
        ctx.request.body.emailAddress,
        verifySignUp.user.name
      );
      const enterVerificationCode = await UserService.addVerificationCode(
        verificationCode,
        ctx.request.body.emailAddress
      );
      ctx.status = 200;
      ctx.body = {
        isSuccess: true,
        message: 'An email contaning pin code is sent on this email address.',
        action: 'EMAIL_VERIFICATION',
      };
      return;
    }
    if (verifySignUp.LoginType.toUpperCase() === 'LINKEDIN') {
      ctx.status = 200;
      ctx.body = {
        ...verifySignUp.user.toObject(),
        isSuccess: true,
        message: 'User verified successfully.',
        action: 'SIGN_IN_SUCCESS',
      };
      return;
    }
  }
  if (
    verifySignUp &&
    verifySignUp.type === 'new' &&
    verifySignUp.LoginType.toUpperCase() === 'LINKEDIN'
  ) {
    ctx.body = {
      ...verifySignUp.allData,
      isSuccess: true,
      message: 'User verified successfully',
      action: '',
    };
    ctx.status = 200;
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'User does not exist Please Signup',
    action: 'SIGNUP',
  };
};
const verifyEmailLogin = async ctx => {
  if (!ctx.request.body.emailAddress || !ctx.request.body.verificationCode) {
    ctx.status = 400;
    ctx.body = {
      error: 'Email Expected',
      isSuccess: false,
      message: 'Email address or verification code missing',
    };
  } else {
    const verifyCode = await UserService.verifyCode(
      ctx.request.body.emailAddress,
      ctx.request.body.verificationCode
    );
    if (verifyCode.err) {
      ctx.body = {
        ...verifyCode,
        isSuccess: false,
        message: 'You have entered an incorrect verification code. Please try again.',
        action: '',
      };
      ctx.status = 200;
    } else {
      ctx.body = {
        ...verifyCode,
        isSuccess: true,
        message: 'User verified successfully',
        action: 'SIGN_IN_SUCCESS',
      };
      ctx.status = 200;
    }
  }
};

module.exports = {
  GetUsers,
  registerUser,
  verifyRegisteredUser,
  login,
  verifyEmailLogin,
};
