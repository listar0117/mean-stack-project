// const commonService = require('../../services/comon.service');
const lead = require('../services/leads.service');

const Postlead = async ctx => {
  if (!ctx.request.body.emailAddress) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide email Address',
    };
    return;
  }
  const populateLead = await lead.postLead(ctx.request.body);
  // console.log('Applicant populated', populateApplicant);
  if (populateLead && populateLead.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'Some thing went wrong. Please try again Later',
      action: '',
    };
    return;
  }
  if (populateLead && populateLead.excist) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'Email already registered',
      action: '',
    };
    return;
  }
  if (populateLead && !populateLead.excist && populateLead.updated === true) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: true,
      message: 'Signed Up successfully',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'Some thing went wrong. Sign up again Later',
    action: '',
  };
  return;
};

module.exports = {
  Postlead,
};
