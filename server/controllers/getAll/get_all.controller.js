const getAll = require('../../services/getAll/get_all.service');

const getAllJobs = async ctx => {
  const getjobData = await getAll.getJobs(ctx.request.body);
  // console.log('get Applicants', getApplicantsResponse);
  if (getjobData && getjobData.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: getjobData.err.toString(),
      action: '',
    };
    return;
  }
  if (getjobData && getjobData.notFound) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'No Applicants found',
      action: '',
    };
    return;
  }
  if (getjobData !== null) {
    ctx.status = 200;
    ctx.body = {
      jobsArray: getjobData,
      isSuccess: true,
      message: 'Got new applicants successfully',
      action: '',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'Something went wrong',
    action: '',
  };
  return;
};

const getAllApplicants = async ctx => {
  const getApplicantsData = await getAll.getApplicants(ctx.request.body);
  // console.log('get Applicants', getApplicantsResponse);
  if (getApplicantsData && getApplicantsData.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: getApplicantsData.err.toString(),
      action: '',
    };
    return;
  }
  if (getApplicantsData && getApplicantsData.notFound) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'No Applicants found',
      action: '',
    };
    return;
  }
  if (getApplicantsData !== null) {
    ctx.status = 200;
    ctx.body = {
      applicantArray: getApplicantsData,
      isSuccess: true,
      message: 'Got new applicants successfully',
      action: '',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'Something went wrong',
    action: '',
  };
  return;
};

module.exports = {
  getAllJobs,
  getAllApplicants,
};
