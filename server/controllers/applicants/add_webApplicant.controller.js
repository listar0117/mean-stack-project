const asyncBusboy = require('async-busboy');
const UploadService = require('../../services/upload.service');

// const commonService = require('../../services/comon.service');
const webApplicantService = require('../../services/applicants/webApplicants.service');

const saveApplicant = async ctx => {
  const { files, fields } = await asyncBusboy(ctx.req);
  const body = fields;
  if (files.length > 0) {
    try {
      const uploadCv = await UploadService.uploadResume(files);
      body.cvUrl = uploadCv;
    } catch (err) {
      console.log('error =====>>>>>', JSON.stringify(err));
      console.log('error on uploading resume', err);
      ctx.body = {
        isSuccess: false,
        message: 'unable to upload resume, error occurred while uploading file',
      };
      ctx.status = 200;
      return;
    }
  }

  if (!body.jobID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide jobID',
    };
    return;
  }
  if (!body.emailAddress) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide an Email Address',
    };
    return;
  }
  if (!body.name) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide name',
    };
    return;
  }
  if (!body.phoneNumber) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide Phone Number',
    };
    return;
  }
  const applicant = await webApplicantService.saveApplicant(body);
  if (applicant.saved) {
    ctx.body = {
      applicant: applicant.data,
      isSuccess: true,
      message: 'CV submitted successfully',
      action: 'NA',
    };
    ctx.status = 200;
    return;
  }
  if (applicant === null) {
    ctx.body = {
      // ...user.user.toObject(),
      isSuccess: false,
      message: 'No job found. ',
      action: 'NA',
    };
    ctx.status = 400;
    return;
  }
  if (applicant.err) {
    ctx.body = {
      // ...user.user.toObject(),
      isSuccess: false,
      message: 'Some thing went wrong, Unable to process application',
      action: 'NA',
    };
    ctx.status = 400;
    return;
  }
};
module.exports = {
  saveApplicant,
};
