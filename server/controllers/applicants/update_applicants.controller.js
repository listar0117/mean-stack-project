const applicant = require('../../services/applicants/applicants_status.service');

const updateApplicant = async ctx => {
  if (!ctx.request.body.recruiterID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }
  if (
    !ctx.request.body.requestArray ||
    (ctx.request.body.requestArray && ctx.request.body.requestArray.length <= 0)
  ) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please populate requestArray',
    };
    return;
  }
  const populateApplicant = await applicant.updateStatus(ctx.request.body);
  // console.log('Applicant populated', populateApplicant);
  if (populateApplicant && populateApplicant.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'Some thing went wrong',
      action: '',
    };
    return;
  }
  if (populateApplicant && populateApplicant.notFound) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'JobId Incorrect. Unable to update applicants status',
      action: '',
    };
    return;
  }
  if (populateApplicant.updated === false) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'No applicants added against this job',
    };
    return;
  }
  if (populateApplicant.updated === true) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: true,
      message: 'Applicants updated successfully',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'unable to update applicants recruiterID not correct',
    action: '',
  };
  return;
};

module.exports = {
  updateApplicant,
};
