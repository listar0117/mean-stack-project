// const commonService = require('../../services/comon.service');
const applicant = require('../../services/applicants/applicants.service');

const getApplicants = async ctx => {
  if (!ctx.request.body.recruiterID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }
  if (!ctx.request.body.jobID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide Job Id',
    };
    return;
  }

  const getApplicantsResponse = await applicant.getApplicants(ctx.request.body);
  // console.log('get Applicants', getApplicantsResponse);
  if (getApplicantsResponse && getApplicantsResponse.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: getApplicantsResponse.err,
      action: '',
    };
    return;
  }
  if (getApplicantsResponse && getApplicantsResponse.notFound) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'No Applicants found',
      action: '',
    };
    return;
  }
  if (getApplicantsResponse !== null) {
    ctx.status = 200;
    ctx.body = {
      applicantArray: getApplicantsResponse.applicants,
      TotalApplicants: getApplicantsResponse.TotalApplicants,
      isSuccess: true,
      message: 'Got new applicants successfully',
      action: '',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'recruiterId Incorrect',
    action: '',
  };
  return;
};

// get a single applicant against a job using entityurn
const getApplicant = async ctx => {
  if (!ctx.request.body.recruiterID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }
  if (!ctx.request.body.jobID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide Job Id',
    };
    return;
  }
  if (!ctx.request.body.entity) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide user entity',
    };
    return;
  }

  const getApplicantsResponse = await applicant.getApplicant(ctx.request.body);
  // console.log('get Applicants', getApplicantsResponse);
  if (getApplicantsResponse && getApplicantsResponse.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: getApplicantsResponse.err,
      action: '',
    };
    return;
  }
  if (getApplicantsResponse && getApplicantsResponse.notFound) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'unable to find applicant data',
      action: '',
    };
    return;
  }
  if (getApplicantsResponse !== null) {
    ctx.status = 200;
    ctx.body = {
      applicant: getApplicantsResponse,
      // TotalApplicants: getApplicantsResponse.TotalApplicants,
      isSuccess: true,
      message: 'Got new applicants successfully',
      action: '',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'recruiterId Incorrect',
    action: '',
  };
  return;
};

module.exports = {
  getApplicants,
  getApplicant,
};
