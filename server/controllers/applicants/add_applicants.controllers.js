const commonService = require('../../services/comon.service');
const applicant = require('../../services/applicants/applicants.service');

const addApplicant = async ctx => {
  if (!ctx.request.body.recruiterID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }
  if (
    !ctx.request.body.profileArray ||
    (ctx.request.body.profileArray && ctx.request.body.profileArray.length <= 0)
  ) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please populate profiles Array',
    };
    return;
  }
  const populateApplicant = await applicant.addApplicant(ctx.request.body);
  // console.log('Applicant populated', populateApplicant);
  if (populateApplicant && populateApplicant.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'JobId Incorrect. Unable to add applicants',
      action: '',
    };
    return;
  }
  if (populateApplicant && populateApplicant.notFound) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'JobId Incorrect. Unable to add applicants',
      action: '',
    };
    return;
  }
  if (populateApplicant && populateApplicant.nlpError) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'Some thing went wrong at NLP end. Unable to add applicants',
      action: '',
    };
    return;
  }
  if (populateApplicant !== null) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: true,
      message: 'New applicant added successfully',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'unable to add applicants recruiterID not correct',
    action: '',
  };
  return;
};

module.exports = {
  addApplicant,
};
