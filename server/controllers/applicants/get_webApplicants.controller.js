const applicant = require('../../services/applicants/webApplicants.service');

const getApplicants = async ctx => {
  if (!ctx.request.body.recruiterID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }
  if (!ctx.request.body.jobID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide Job Id',
    };
    return;
  }

  const getApplicantsResponse = await applicant.getApplicants(ctx.request.body);
  // console.log('get Applicants', getApplicantsResponse);
  if (getApplicantsResponse && getApplicantsResponse.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: getApplicantsResponse.err.toString(),
      action: '',
    };
    return;
  }
  if (getApplicantsResponse && getApplicantsResponse.notFound) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'No Applicants found',
      action: '',
    };
    return;
  }
  if (getApplicantsResponse !== null) {
    ctx.status = 200;
    ctx.body = {
      applicantArray: getApplicantsResponse.applicants,
      TotalApplicants: getApplicantsResponse.TotalApplicants,
      isSuccess: true,
      message: 'Got new applicants successfully',
      action: '',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'recruiterId Incorrect',
    action: '',
  };
  return;
};
const getSingleApplicant = async ctx => {
  console.log(ctx.query.id);
  if (!ctx.query.id) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide Applicant Id',
    };
    return;
  }
  const getApplicantResponse = await applicant.getApplicant(ctx.query);
  if (getApplicantResponse && getApplicantResponse.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: getApplicantResponse.err,
      action: '',
    };
    return;
  }
  if (getApplicantResponse && getApplicantResponse.found === true) {
    ctx.status = 200;
    ctx.body = {
      applicant: getApplicantResponse.sendData,
      isSuccess: true,
      message: 'Applicant found',
      action: '',
    };
    return;
  }
};

module.exports = {
  getApplicants,
  getSingleApplicant,
};
