const applicant = require('../../services/applicants/applicants_status.service');

const getApplicants = async ctx => {
  if (!ctx.request.body.recruiterID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }
  if (!ctx.request.body.jobID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide job Id',
    };
    return;
  }
  const filteredApplicants = await applicant.getTopApplicants(ctx.request.body);
  // console.log('Applicant populated', populateApplicant);
  if (filteredApplicants && filteredApplicants.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'Some thing went wrong',
      action: '',
    };
    return;
  }
  if (filteredApplicants && filteredApplicants.notFound) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'JobId Incorrect. Unable to get Applicants',
      action: '',
    };
    return;
  }
  if (filteredApplicants && filteredApplicants.found === true) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: true,
      message: 'Applicants fetched successfully',
      action: '',
      numberOfRequests: filteredApplicants.dataToSend.sendData.length,
      requestArray: filteredApplicants.dataToSend.sendData,
      messageTemplate: filteredApplicants.dataToSend.messageTemplate,
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'unable to get applicants recruiterID not correct',
    action: '',
  };
  return;
};

module.exports = {
  getApplicants,
};
