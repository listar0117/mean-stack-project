// const commonService = require('../../services/comon.service');
const applicant = require('../../services/applicants/webApplicants.service');

const verify = async ctx => {
  if (!ctx.request.body.verificationCode) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please enter verification code',
    };
    return;
  }
  if (!ctx.request.body.webId) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide webId',
    };
    return;
  }

  const verificationResponse = await applicant.verifyApplicant(ctx.request.body);
  // console.log('get Applicants', getApplicantsResponse);
  if (verificationResponse && verificationResponse.err) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: verificationResponse.err.toString(),
      action: '',
    };
    return;
  }
  if (verificationResponse && verificationResponse.verified === false) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      message: 'Invalid verification code',
      action: '',
    };
    return;
  }
  if (verificationResponse && verificationResponse.verified === true) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: true,
      message: 'Email verified successfully',
      action: '',
    };
    return;
  }
  if (verificationResponse && verificationResponse.alreadyVerified === true) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: true,
      message: 'Email already verified',
      action: '',
    };
    return;
  }
  ctx.status = 200;
  ctx.body = {
    isSuccess: false,
    message: 'Applicant not registered',
    action: '',
  };
  return;
};

module.exports = {
    verify,
};
