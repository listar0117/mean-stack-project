const jobService = require('../services/deleteJob.service');

const deleteJob = async ctx => {
  if (!ctx.request.body.recruiterID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }
  if (!ctx.request.body.jobID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide job Id',
    };
    return;
  }
  const job = await jobService.deleteJob(ctx.request.body);

  if (job === null) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      action: '',
      message: 'No Job Found to delete',
    };
    return;
  }
  if (job === 'wrongRecruiter') {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      action: '',
      message: 'Wrong recruiterID',
    };
    return;
  }
  if (job && job !== null) {
    ctx.status = 200;
    ctx.body = {
      //   job,
      isSuccess: true,
      action: 'NA',
      message: 'Job deleted Successfully',
    };
    return;
  }
};

module.exports = {
  deleteJob,
};
