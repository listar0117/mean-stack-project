const commonService = require('../services/comon.service');
const campaignService = require('../services/get_campaigns_user.service');

const getCampaigns = async ctx => {
  if (!ctx.request.body.recruiterID) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      message: 'Please provide recruiter Id',
    };
    return;
  }

  const campaign = await campaignService.getCampaigns(ctx.request.body);
  if (campaign && campaign !== null) {
    ctx.status = 200;
    ctx.body = {
      campaign,
      isSuccess: true,
      action: 'NA',
      message: 'Campaigns fetched Successfully',
    };
    return;
  }
  if (campaign === null) {
    ctx.status = 200;
    ctx.body = {
      isSuccess: false,
      action: '',
      message: 'No Campaigns Found',
    };
    return;
  }
};

// get single campaign making for web
const getCampaign = async ctx => {
  const campaign = await campaignService.getCampaign(ctx.request.body);
  // console.log('got Job', job);
  if (campaign && campaign.err) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      action: '',
      message: 'Campaign not found',
    };
    return;
  }
  if (campaign && campaign !== null) {
    ctx.status = 200;
    ctx.body = {
      campaign,
      isSuccess: true,
      action: 'NA',
      message: 'Campaign fetched Successfully',
    };
    return;
  }
  if (campaign === null) {
    ctx.status = 400;
    ctx.body = {
      isSuccess: false,
      action: '',
      message: 'No Campaigns Found',
    };
    return;
  }
};
module.exports = {
  getCampaigns,
  getCampaign,
};
