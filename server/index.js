const compress = require('koa-compress');
const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-body');
const formidable = require('koa2-formidable');
const zlib = require('zlib').Z_SYNC_FLUSH;

// const multer = require('koa-multer');
// const upload = multer({ dest: 'uploads/' });
const path = require('path');
const fs = require('fs');
const send = require('koa-send');
const mongoose = require('mongoose');
const http = require('http');
const serve = require('koa-static');
const cors = require('koa2-cors');
const jwt = require('koa-jwt');
const loggerSimple = require('koa-logger');
// const requestId = require('@kasa/koa-request-id');
// const logging = require('@kasa/koa-logging');
// const pino = require('pino');
const respond = require('koa-respond');
const app = new Koa();
const logger = require('koa-pino-logger')();
const router = new Router();
const nodeCron = require('node-cron');
const options = {
  origin: '*',
};

const getAnalytics = require('./crons/get_analytics.service');
if (process.env.NODE_ENV === 'production') {
  nodeCron.schedule(
    '0 1 * * *',
    () => {
      // nodeCron.schedule('5 * * * * *', () => {
      console.log('in cron job: ', new Date());
      getAnalytics.getAnalytics();
    },
    {
      scheduled: true,
      timezone: 'America/Sao_Paulo',
    }
  );
}
if (process.env.NODE_ENV !== 'test') {
  app.use(
    loggerSimple()
    // transporter: (str, args) => {
    //   console.log(str);
    //   console.log(args);
    // },
  );
}

// app.use(requestId());
// app.use(logging({ logger: pino() }));

app.use(logger);

app.use(cors(options));
const distFolder = path.resolve(__dirname, '/public/dnnae');
app.use(
  compress({
    filter: content_type => {
      return /text/i.test(content_type);
    },
    threshold: 2048,
    flush: zlib,
  })
);
// app.use(formidable({}));
app.use(bodyParser());
app.use(serve(__dirname + '/public/dnnae'));
const routes = require('./routes/routes');
// app.use(tasks.routes())
app.use(router.allowedMethods());
const secret = process.env.JWT_SECRET || 'jwt_secret';

// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
//   });
app.use(routes.routes());

// app.use(jwt({
//   secret: secret
// }).unless({
//   path: [/^\/public/, "/"]
// }));

app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set('X-Response-Time', `${ms}ms`);
});

app.use(respond());
// app.use(function* index() {
//   yield send (this, __dirname + '/public/dnnae');
// });

// Set up mongoose connection
let dev_db_url = 'mongodb://localhost/dnNae';
let mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
let version = '/api/v1';

let port = process.env.internalPort || 5000;
// Catch all other routes and return the index file

// app.get('/*', (req, res) => {
//   res.sendFile(path.join(__dirname));
// });
const server = http.createServer(app.callback());

// app.use(async (ctx) => {
//   if (!ctx.path.startsWith("/api")){
//     console.log("here");
//     console.log(distFolder);
//     console.log("dirName",__dirname);
//     // return ctx.body = serve(__dirname + '/public/dnnae');
//     ctx.type = 'html';
//     ctx.body = fs.createReadStream(distFolder + '/index.html');
//   }
//   await send(ctx, ctx.path);
// });

// if nothing founf redirect to angular app build index
app.use(async function(ctx, next) {
  return send(ctx, distFolder + '/index.html').then(() => next());
});

server.listen(port, () => {
  console.log('Server is up and running on port number ' + port);
});
