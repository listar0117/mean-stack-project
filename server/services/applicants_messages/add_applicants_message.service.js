require('dotenv').config();

const userModel = require('../../models/user.js');
const jobModel = require('../../models/job');
const applicantModel = require('../../models/applicants');
const applicantMessagesModel = require('../../models/applicantMessage');

const getUnique = otherArray => {
  return current => {
    return (
      otherArray.filter(other => {
        return other.entityUrn === current.entityUrn;
      }).length === 0
    );
  };
};

const updateExistingObj = async (datafromDB, apiData) => {
  const dbData = JSON.parse(JSON.stringify(datafromDB));

  if (datafromDB.profileArray.length > 0 && apiData.profileArray.length > 0) {
    const newArray = datafromDB.profileArray.map(dbentity => {
      apiData.profileArray.map(apiEntity => {
        if (dbentity.entityUrn === apiEntity.entityUrn) {
          // if (dbentity.messageArray) {
          //   dbentity.messageArray.push(...apiEntity.messageArray);
          // } else {
          dbentity.messageArray = apiEntity.messageArray;
          if (apiEntity.rating) {
            dbentity.rating = apiEntity.rating;
          }

          // }
        }
        return dbentity;
      });
      return dbentity;
    });
    console.log(newArray);
    console.log(datafromDB.profileArray);
    const updatedMessages = await applicantModel.update(
      { _id: datafromDB._id },
      { $set: { profileArray: newArray } }
    );
  }

  const uniqueProfiles = apiData.profileArray.filter(getUnique(dbData.profileArray));
  if (uniqueProfiles.length > 0) {
    const UpdateProfileData = await applicantModel.updateOne(
      { _id: datafromDB._id },
      { $push: { profileArray: { $each: uniqueProfiles } } }
    );
  }
};

// add Applicants service
const addMessages = async applicantData => {
  const user = await userModel.findOne({ recruiterID: applicantData.recruiterID });
  let jobFound = {};
  try {
    let checkApplicantsMessagesData;
    // let checkTempApplicantsMessagesData;
    // check user exist
    if (user) {
      // check job exist and job id not 0
      if (applicantData.jobID && applicantData.jobID.toLowerCase() !== 'all') {
        jobFound = await jobModel.findOne({ _id: applicantData.jobID });
        if (!jobFound) {
          return { notFound: true };
        }
      }
      // check if job already present in applicant model
      if (applicantData.jobID && applicantData.jobID.toLowerCase() !== 'all') {
        checkApplicantsMessagesData = await applicantModel.findOne({
          jobID: applicantData.jobID,
        });
      }
      // if job exist get unique applicants messages and add in db
      if (
        checkApplicantsMessagesData &&
        checkApplicantsMessagesData.recruiterID === applicantData.recruiterID &&
        applicantData.jobID.toLowerCase() !== 'all'
      ) {
        updateExistingObj(checkApplicantsMessagesData, applicantData);
        return applicantData;
      }

      if (applicantData.jobID && applicantData.jobID.toLowerCase() === 'all') {
        let applicantsObjects = await applicantModel.find({
          recruiterID: applicantData.recruiterID,
        });
        if (applicantsObjects) {
          applicantsObjects = JSON.parse(JSON.stringify(applicantsObjects));
          try {
            applicantsObjects.map(async obj => {
              obj.profileArray.map(async dbProfile => {
                for (const apiProfile of applicantData.profileArray) {
                  // applicantData.requestArray.map(async apiProfile => {
                  if (apiProfile.entityUrn === dbProfile.entityUrn) {
                    const messageArrayUpdated = Date.now();
                    const updateData = await applicantModel.update(
                      { 'profileArray.entityUrn': apiProfile.entityUrn },
                      {
                        $set: {
                          'profileArray.$.messageArray': apiProfile.messageArray,
                          'profileArray.$.messageArrayUpdated': messageArrayUpdated,
                        },
                      }
                    );
                    console.log(updateData);
                  }
                  // })
                }
              });
            });
            // return { updated: true };
            return applicantData;
          } catch (err) {
            return { err };
          }
        }
      }
      // check if temp job exist
      // if (applicantData.jobID === 0 || applicantData.jobID === '0') {
      //   checkTempApplicantsMessagesData = await applicantMessagesModel.findOne({
      //     tempID: 0,
      //     recruiterID: applicantData.recruiterID,
      //   });
      // }

      // if (checkTempApplicantsMessagesData) {
      //   updateExistingObj(checkTempApplicantsMessagesData, applicantData);
      //   return applicantData;
      // }

      // if job id 0 and did not exist in db add as tempID,JOBID is mongoose obj, 0 is not acceptable
      // if (applicantData.jobID === 0 || applicantData.jobID === '0') {
      //   delete applicantData.jobID;
      //   applicantData.tempID = 0;
      // }

      // const newApplicant = await applicantMessagesModel.create({ ...applicantData });
      // add ids in jobs model
      // if (applicantData.jobID && applicantData.jobID.toLowerCase() !== 'all') {
      //   const job = await jobModel.findOne({ _id: applicantData.jobID });
      //   if (job) {
      //     const updatedjob = await jobModel.update(
      //       { _id: job._id },
      //       { $addToSet: { applicantMessages: [newApplicant._id] } }
      //     );
      //   }
      // }

      // add ids in applicants model
      // if (applicantData.jobID && applicantData.jobID !== 0 && applicantData.jobID !== '0') {
      //   const applicants = await applicantModel.findOne({ jobID: applicantData.jobID });
      //   if (applicants) {
      //     const updatedapplicants = await applicantModel.update(
      //       { _id: applicants._id },
      //       { $addToSet: { applicantMessages: [newApplicant._id] } }
      //     );
      //   }
      // }
      // return newApplicant;
    }
    return null;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    console.log('error on add messages', err);
    return { err };
  }
};

module.exports = {
  addMessages,
};
