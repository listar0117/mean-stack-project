const request = require('request-promise-native');
require('dotenv').config();

const moment = require('moment');
const mongoose = require('mongoose');
const userModel = require('../../models/user.js');
const jobModel = require('../../models/job');
const applicantModel = require('../../models/applicants');
const applicantMessagesModel = require('../../models/applicantMessage');
const defaultMessages = require('../batchMessaging_keys_obj');

const intentInfo = `This shows the current intent of an applicant. They might be interested in applying for the position posted, interested in learning more or not interested.`;
const messageInfo = `This shows the template that the system is about to send to a user. This is based on scoring, intent and other attributes the system detects.`;
const median = values => {
  if (values.length === 0) return 0;
  values.sort((a, b) => {
    return a - b;
  });
  const half = Math.floor(values.length / 2);
  if (values.length % 2) return values[half];
  return (values[half - 1] + values[half]) / 2.0;
};

const scoreProfiles = async (AllProfiles, sendingProfiles) => {
  const finalScores = AllProfiles.map(profile => {
    if (profile.score) {
      return profile.score.final_score;
    }
  });
  const refineFinalScore = finalScores.filter(data => data !== undefined);
  const calcMedian = median(refineFinalScore);
  console.log(calcMedian);

  // match scores
  sendingProfiles.map(profile => {
    let tempScore = true;
    if (profile.score && profile.score.final_score >= calcMedian) {
      profile.scoreType = 'High Score';
      profile.score_quality = 'high_score';
      // profile.messageArray.map(message => {
      //   message.messageType = 'High Score';
      //   if (tempScore === true) {
      //     message.isDefault = 1;
      //     tempScore = false;
      //   } else {
      //     message.isDefault = 0;
      //   }
      // });
    }
    if (profile.score && profile.score.final_score < calcMedian) {
      profile.scoreType = 'Low Score';
      profile.score_quality = 'low_score';
      // profile.messageArray.map(message => {
      //   message.messageType = 'Low Score';
      //   message.isDefault = 0;
      // });
    }
    if (!profile.score || !profile.score.final_score) {
      profile.score_quality = 'low_score';
    }
    // else {
    //   profile.score_quality = 'low_score';
    // }
  });
  return sendingProfiles;
};

// check how much time is passed
const checkTimePassed = initialTime => {
  const currentTime = moment();

  const timePassed = moment(initialTime);
  const hoursToReturn = currentTime.diff(timePassed, 'h');
  return hoursToReturn;
};

// find Intent
const findIntent = async filteredProfiles => {
  // filteredProfiles.map(profile => {
  //   profile.intent = 'Interested';
  // });
  let response;
  try {
    response = await request({
      method: 'POST',
      // url: `http://34.67.207.137:3002/score_profile`,
      url: `${process.env.nlpUrl}${process.env.nlpPort}/detect_profile_intent`,
      form: {
        intent_input: JSON.stringify(filteredProfiles),
      },
    });
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    console.log('error on nlp', err);
    return { nlpError: true };
  }
  if (response.isSuccess === false) {
    console.log('error =====>>>>>', JSON.stringify(response));
    console.log('error on nlp intent api', response);
    return { nlpError: true };
  }
  return JSON.parse(response);
};

const createMessageVariants = (messageArray, user, recruiterData) => {
  messageArray.map(message => {
    let UserMessage = message.value.message;
    if (UserMessage) {
      UserMessage = UserMessage.replace(/{First_Name}/g, user.firstName);
      UserMessage = UserMessage.replace(/{Last_Name}/g, user.lastName);
      UserMessage = UserMessage.replace(/{Company_Name}/g, user.companyName);
      UserMessage = UserMessage.replace(/{School_Name}/g, user.schoolName);
      UserMessage = UserMessage.replace(/{Job_Link}/g, user.jobLink);
      UserMessage = UserMessage.replace(/{Job_Title}/g, user.jobTitle);
      UserMessage = UserMessage.replace(/{Link_to_calendar}/g, recruiterData.calendlyLink);
      UserMessage = UserMessage.replace(/{User_Name}/g, user.name);
      UserMessage.replace(/ +/g, ' ');
    }
    // const objToSend = {
    message.type = 'RECEIVED';
    message.linkedinMessageID = '';
    // message.serverMessageID = mongoose.Types.ObjectId(),
    message.messageText = UserMessage || '';
    message.dateTime = moment();
    message.messageType = user.scoreType;
    message.isDefault = 1;
    // };
    // return objToSend;
    delete message.value;
  });
  return messageArray;
};
const sendEmail = async (emailSubject, message, from, to, token) => {
  console.log('1111', process.env.mailServer);
  let response;
  try {
    response = await request({
      method: 'POST',
      // url: `http://34.67.207.137:3002/score_profile`,
      url: `${process.env.mailServer}/gmail/send_email_messages.php`,
      body: {
        loginEmail: from,
        accessToken: token,
        subject: emailSubject,
        toEmail: to,
        body: message,
        _comment: '',
      },
      json: true,
    });
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    console.log('error on messaging Api', err);
    return { mailError: true };
  }
  console.log('resssssssssssssssssssssssss', response);
  return response;
};
const createMessageText = async (message, user, recruiterData, templateKey) => {
  let UserMessage = message.replace(`"`, `'`);
  let mailSend;
  const temKeyArray = templateKey.split('-');
  const emailKey = temKeyArray.includes('EM');
  // const emailKey = true;
  const emailSubject = defaultMessages.messagingTemplates.filter(messageTemp =>
    messageTemp.keys.includes('subject')
  );
  const afterEmailText = defaultMessages.messagingTemplates.filter(messageTemp =>
    messageTemp.keys.includes('ELM')
  );
  if (UserMessage) {
    UserMessage = UserMessage.replace(/{First_Name}/g, user.firstName);
    UserMessage = UserMessage.replace(/{Last_Name}/g, user.lastName);
    UserMessage = UserMessage.replace(/{Company_Name}/g, user.companyName);
    UserMessage = UserMessage.replace(/{School_Name}/g, user.schoolName);
    UserMessage = UserMessage.replace(/{Job_Link}/g, user.jobLink);
    UserMessage = UserMessage.replace(/{Job_Title}/g, user.jobTitle);
    UserMessage = UserMessage.replace(/{Link_to_calendar}/g, recruiterData.calendlyLink);
    UserMessage = UserMessage.replace(/{User_Name}/g, user.name);
    if (user.intent_object && user.intent_object.datetime) {
      UserMessage = UserMessage.replace(
        /{Date_Time}/g,
        moment(user.intent_object.datetime).format('dddd, MMMM Do YYYY, h:mm:ss a')
      );
    }
    UserMessage.replace(/ +/g, ' ');
  }
  if (emailKey) {
    mailSend = await sendEmail(
      emailSubject[0].message.replace(/{Job_Link}/g, user.jobLink),
      UserMessage,
      recruiterData.integratedEmailAddress,
      user.intent_object.email,
      recruiterData.gmailAccessToken
    );
  }
  const objToSend = {
    type: 'RECEIVED',
    linkedinMessageID: '',
    serverMessageID: mongoose.Types.ObjectId(),
    messageText: emailKey ? afterEmailText[0].message : UserMessage || '',
    templateKey,
    dateTime: moment(),
    connectionType:
      user.connection_degree === '1st'
        ? `Old connection ${user.scoreType}`
        : `New connection ${user.scoreType}`,
    messageType: user.scoreType,
    isDefault: 1,
  };
  user.lastMessageGenerated = objToSend;
  return objToSend;
};

const findTemplateMessage = (userData, keyToFind) => {
  let messageTemplate = '';
  userData = JSON.parse(JSON.stringify(userData));
  const TemplateToUse =
    userData.messageTemplatesArray && userData.messageTemplatesArray.length > 0
      ? userData.messageTemplatesArray
      : defaultMessages.messagingTemplates;

  TemplateToUse.map(object => {
    object.keys.map(key => {
      if (key === keyToFind) {
        messageTemplate = object.message;
      }
    });
  });
  return messageTemplate;
};
const populateMessages = async (filteredProfiles, applicantData, userData, jobData) => {
  for (const profile of filteredProfiles) {
    // filteredProfiles.map(profile => {
    let template;
    let templateKey;
    if (profile.messageArrayHistory.length === 0) {
      if (profile.connection_state === 'old_connection') {
        if (profile.score_quality === 'high_score') {
          template = findTemplateMessage(userData, 'OCHS');
          templateKey = 'OCHS';
        }
        if (profile.score_quality === 'low_score') {
          template = findTemplateMessage(userData, 'OCLS');
          templateKey = 'OCLS';
        }
      }
    }
    if (profile.messageArrayHistory.length > 0) {
      const replyExists = profile.messageArrayHistory.filter(message => message.type === 'SENT');
      if (replyExists.length <= 0) {
        if (profile.hoursPassed >= 24 && profile.hoursPassed < 48) {
          if (
            profile.connection_state === 'new_connection' &&
            profile.score_quality === 'low_score'
          ) {
            template = findTemplateMessage(userData, 'NCLSNM24');
            templateKey = 'NCLSNM24';
          }
          if (
            profile.connection_state === 'new_connection' &&
            profile.score_quality === 'low_score'
          ) {
            template = findTemplateMessage(userData, 'INCHING24');
            templateKey = 'INCHING24';
          }
          profile.messageNumber = '2nd';
        }
        if (profile.hoursPassed >= 48 && profile.hoursPassed < 168) {
          if (
            profile.connection_state === 'new_connection' &&
            profile.score_quality === 'low_score'
          ) {
            template = findTemplateMessage(userData, 'NCLSNM48');
            templateKey = 'INCLINE48';
          }
          if (
            profile.connection_state === 'new_connection' &&
            profile.score_quality === 'low_score'
          ) {
            template = findTemplateMessage(userData, 'NCHSNM48');
            templateKey = 'NCHSNM48';
          }
          profile.messageNumber = '3rd';
        }
        if (profile.hoursPassed >= 168 && profile.hoursPassed < 174) {
          if (
            profile.connection_state === 'new_connection' &&
            profile.score_quality === 'low_score'
          ) {
            template = findTemplateMessage(userData, 'NCLSNM7d');
            templateKey = 'NCLSNM7d';
          }
          if (
            profile.connection_state === 'new_connection' &&
            profile.score_quality === 'low_score'
          ) {
            template = findTemplateMessage(userData, 'NCHSNM7d');
            templateKey = 'NCHSNM7d';
          }
          profile.messageNumber = '4th';
        }
      }
      if (
        replyExists.length > 0 &&
        profile.messageArrayHistory[profile.messageArrayHistory.length - 1].message_response_key &&
        profile.messageArrayHistory[profile.messageArrayHistory.length - 1].type === 'SENT'
      ) {
        template = findTemplateMessage(
          userData,
          profile.messageArrayHistory[profile.messageArrayHistory.length - 1].message_response_key
        );
        templateKey =
          profile.messageArrayHistory[profile.messageArrayHistory.length - 1].message_response_key;
      }
    }
    if (template) {
      const messageObj = await createMessageText(template, profile, userData, templateKey);
      profile.messageArray.push(messageObj);
      profile.messageArrayHistory.push(messageObj);
    }
  }
  // });
};

// Get messages service
const getMessages = async applicantData => {
  const user = await userModel.findOne({ recruiterID: applicantData.recruiterID });
  try {
    if (user) {
      const sendApplicants = {
        // TotalApplicants: applicantsLength,
        messagesArray: [],
      };

      return sendApplicants;
      if (applicantData.jobID.toLowerCase() === 'all') {
        let applicants = await applicantModel
          .find({
            recruiterID: applicantData.recruiterID,
          })
          .populate('jobID');
        applicants = JSON.parse(JSON.stringify(applicants));
        let tempData = [];
        // let AllProfiles =[]
        const modApplicants = applicants.map(entity => {
          entity.profileArray.map(profile => {
            profile.job = entity.jobID;
          });
          tempData.push(...entity.profileArray);
          // AllProfiles.push();
          //   if (entity.tempID === 0) {
          //     entity.jobID = 0;
          //   }
          //   return entity;
        });
        const refinedData = tempData.map(applicant => {
          if (
            (applicant.connection_degree === '1st' ||
              applicant.status === 'ACCEPTED' ||
              applicant.status === 'SENT') &&
            applicant.score.final_score > 0
          ) {
            const timePassed = checkTimePassed(applicant.requestTime);
            // checkTimePassed(applicant.requestTime).then(data => {
            //   timePassed = data;
            // });
            const dataToReturn = {
              entityUrn: applicant.entityUrn,
              profileUrl: applicant.profileUrl,
              full_name: applicant.full_name,
              image_url: applicant.image_url,
              score: applicant.score,
              intent_info: intentInfo,
              intent_object: applicant.intent_object,
              message_info: messageInfo,
              messageNumber: applicant.messageNumber ? applicant.messageNumber : '1st',
              intent: applicant.intent ? applicant.intent : 'None',
              readMessages: applicant.messageArray && applicant.messageArray.length > 0 ? 1 : 1,
              sendMessages: applicant.messageArray && applicant.messageArray.length > 0 ? 1 : 0,
              // readMessages: applicant.batchMessagesCompleted ? 0 : 1,
              // sendMessages: applicant.batchMessagesCompleted ? 0 : 1,
              lastMessageGenerated: applicant.lastMessageGenerated
                ? applicant.lastMessageGenerated
                : {},
              messageArray: applicant.messageArray ? applicant.messageArray : [],
              messageArrayHistory: applicant.messageArrayHistory
                ? applicant.messageArrayHistory
                : applicant.messageArray,
              messageVariantsArray: applicant.messageVariantsArray
                ? applicant.messageVariantsArray
                : [],
              requestTime: applicant.requestTime,
              hoursPassed: timePassed,
              connection_degree: applicant.connection_degree,
              status: applicant.status,
              firstName: applicant.firstName,
              lastName: applicant.lastName,
              companyName: applicant.companyName,
              schoolName: applicant.schoolName,
              name: applicant.name,
              jobLink: applicant.job.jobLink,
              jobTitle: applicant.job.jobTitle,
              currentTitle: applicant.title,
              sendTemplateMessage: false,
              connection_state:
                applicant.connection_degree === '1st' ? 'old_connection' : 'new_connection',
              batchMessagesCompleted: applicant.batchMessagesCompleted
                ? applicant.batchMessagesCompleted
                : false,
            };
            if (!dataToReturn.messageArrayHistory) {
              dataToReturn.messageArrayHistory = [];
            }
            return dataToReturn;
          }
        });
        let descardNULL = refinedData.filter(data => data !== undefined);
        scoreProfiles(tempData, descardNULL);
        // find intent creating nlp obj
        descardNULL.map(profile => {
          if (profile.messageArray && profile.messageArrayHistory) {
            if (profile.messageArray.length > profile.messageArrayHistory.length) {
              const length = profile.messageArray.length - profile.messageArrayHistory.length;
              const elements = profile.messageArray.splice(
                profile.messageArrayHistory.length,
                length
              );
              profile.messageArray.push(...elements);
              profile.messageArrayHistory.push(...elements);
              profile.sendTemplateMessage = true;
            } else {
              profile.sendTemplateMessage = false;
            }
          }
        });
        const nlpObj = {
          profileArray: descardNULL,
        };
        console.log(nlpObj);
        const parsedFromIntent = await findIntent(nlpObj);
        // assign messages based on intent and connection type
        descardNULL = parsedFromIntent.profileArray;
        // assignMessages(descardNULL, applicantData, user);
        await populateMessages(descardNULL, applicantData, user);

        // update db with added messages and fields
        try {
          applicants.map(async obj => {
            obj.profileArray.map(async originalProfile => {
              for (const updatedProfile of descardNULL) {
                // delete updatedProfile.requestTime;
                // delete updatedProfile.connection_degree;
                // delete updatedProfile.hoursPassed;
                // delete updatedProfile.firstName;
                // delete updatedProfile.lastName;
                // delete updatedProfile.companyName;
                // delete updatedProfile.schoolName;
                // delete updatedProfile.name;
                // delete updatedProfile.jobLink;
                // delete updatedProfile.batchMessagesCompleted;

                // applicantData.requestArray.map(async updatedProfile => {
                if (updatedProfile.entityUrn === originalProfile.entityUrn) {
                  console.log("intent>>>>>>>>>>>>", updatedProfile.intent_object);
                  if (updatedProfile.intent_object && updatedProfile.intent_object.text) {
                    const intentTextObjType = typeof updatedProfile.intent_object.text === 'string';
                    if (updatedProfile.intent_object && intentTextObjType === true) {
                      updatedProfile.intent_object.text = JSON.parse(
                        updatedProfile.intent_object.text
                      );
                    }
                  }

                  const updateData = await applicantModel.update(
                    { 'profileArray.entityUrn': updatedProfile.entityUrn },
                    {
                      $set: {
                        'profileArray.$.intent_info': updatedProfile.intent_info,
                        'profileArray.$.message_info': updatedProfile.message_info,
                        'profileArray.$.messageNumber': updatedProfile.messageNumber,
                        'profileArray.$.intent': updatedProfile.intent,
                        'profileArray.$.intent_object': updatedProfile.intent_object,
                        'profileArray.$.readMessages': updatedProfile.readMessages,
                        'profileArray.$.sendMessages': updatedProfile.sendMessages,
                        // 'profileArray.$.messageArray': updatedProfile.messageArray,
                        'profileArray.$.messageArrayHistory': updatedProfile.messageArrayHistory,
                        'profileArray.$.messageVariantsArray': updatedProfile.messageVariantsArray,
                        'profileArray.$.requestTime': updatedProfile.requestTime,
                        'profileArray.$.connection_state': updatedProfile.connection_state,
                        'profileArray.$.score_quality': updatedProfile.score_quality,
                        'profileArray.$.hoursPassed': updatedProfile.hoursPassed,
                        'profileArray.$.batchMessagesCompleted':
                          updatedProfile.batchMessagesCompleted,
                        'profileArray.$.lastMessageGenerated': updatedProfile.lastMessageGenerated,
                      },
                    }
                  );
                  console.log(updateData);
                }

                // })
              }
            });
          });
        } catch (err) {
          console.error(err);
          return { err };
          // process.exit(1);
          // next(err);
        }
        if (!applicantData.bot) {
          descardNULL.map(applicant => {
            if (applicant.messageArray.length > 0) {
              const lastElement = applicant.messageArray[applicant.messageArray.length - 1];
              if (
                lastElement.type.toLowerCase() === 'sent' &&
                lastElement.linkedinMessageID === ''
              ) {
                applicant.readMessages = 1;
                applicant.sendMessages = 1;
                const lastMessage = JSON.parse(
                  JSON.stringify(applicant.messageArray[applicant.messageArray.length - 1])
                );
                applicant.messageArray = [];
                applicant.messageArray.push(lastMessage);
              } else {
                applicant.readMessages = 1;
                applicant.sendMessages = 1;
                applicant.messageArray = [];
                applicant.lastMessageGenerated.messageText
                  ? applicant.messageArray.push(applicant.lastMessageGenerated)
                  : '';
              }
            }
            //  else {
            //   const objToSend = {
            //     type: 'RECEIVED',
            //     linkedinMessageID: '',
            //     serverMessageID: mongoose.Types.ObjectId(),
            //     messageText: '',
            //     dateTime: moment(),
            //     messageType: applicant.scoreType,
            //     isDefault: 1,
            //   };
            //   applicant.readMessages = 1;
            //   applicant.sendMessages = 1;
            //   applicant.messageArray.push(objToSend);
            // }
          });
        }
        let messageArrayApplicants;

        if (!applicantData.bot) {
          messageArrayApplicants = descardNULL.filter(
            data =>
              data.intent.toLowerCase() !== 'do not know' &&
              data.intent.toLowerCase() !== 'dont_know'
          );
        } else {
          messageArrayApplicants = descardNULL;
        }

        const sortedByScore = messageArrayApplicants.sort(
          (a, b) => parseFloat(b.score.final_score) - parseFloat(a.score.final_score)
        );
        const sendApplicants = {
          messagesArray: sortedByScore,
          // tempData,
          //   total: modApplicants.length,
        };
        return sendApplicants;
      }
      if (applicantData.jobID !== 0 && applicantData.jobID !== '0') {
        let jobExist;
        try {
          jobExist = await jobModel.findOne({ _id: applicantData.jobID });
        } catch (err) {
          console.log('error =====>>>>>', JSON.stringify(err));
          return { notFound: true };
        }
        if (!jobExist || jobExist === null) {
          return { notFound: true };
        }
        // console.log(jobExist);
        if (jobExist && jobExist.recruiterID === applicantData.recruiterID) {
          let messages = await applicantModel.findOne({ jobID: applicantData.jobID });
          messages = JSON.parse(JSON.stringify(messages));
          const refinedData = messages.profileArray.map(applicant => {
            if (
              (applicant.connection_degree === '1st' ||
                applicant.status === 'ACCEPTED' ||
                applicant.status === 'SENT') &&
              applicant.score.final_score > 0
            ) {
              const timePassed = checkTimePassed(applicant.requestTime);
              const dataToReturn = {
                entityUrn: applicant.entityUrn,
                profileUrl: applicant.profileUrl,
                full_name: applicant.full_name,
                image_url: applicant.image_url,
                score: applicant.score,
                intent_info: intentInfo,
                intent_object: applicant.intent_object,
                message_info: messageInfo,
                messageNumber: applicant.messageNumber ? applicant.messageNumber : '1st',
                intent: applicant.intent ? applicant.intent : 'None',
                readMessages: applicant.messageArray && applicant.messageArray.length > 0 ? 1 : 1,
                sendMessages: applicant.messageArray && applicant.messageArray.length > 0 ? 1 : 0,
                // readMessages: applicant.batchMessagesCompleted ? 0 : 1,
                // sendMessages: applicant.batchMessagesCompleted ? 0 : 1,
                lastMessageGenerated: applicant.lastMessageGenerated
                  ? applicant.lastMessageGenerated
                  : {},
                messageArray: applicant.messageArray ? applicant.messageArray : [],
                messageArrayHistory: applicant.messageArrayHistory
                  ? applicant.messageArrayHistory
                  : applicant.messageArray,
                messageVariantsArray: applicant.messageVariantsArray
                  ? applicant.messageVariantsArray
                  : [],
                requestTime: applicant.requestTime,
                hoursPassed: timePassed,
                connection_degree: applicant.connection_degree,
                status: applicant.status,
                firstName: applicant.firstName,
                lastName: applicant.lastName,
                companyName: applicant.companyName,
                schoolName: applicant.schoolName,
                name: applicant.name,
                jobLink: jobExist.jobLink,
                jobTitle: jobExist.jobTitle,
                currentTitle: applicant.title,
                connection_state:
                  applicant.connection_degree === '1st' ? 'old_connection' : 'new_connection',
                batchMessagesCompleted: applicant.batchMessagesCompleted
                  ? applicant.batchMessagesCompleted
                  : false,
              };
              if (!dataToReturn.messageArrayHistory) {
                dataToReturn.messageArrayHistory = [];
              }
              return dataToReturn;
            }
          });
          let descardNULL = refinedData.filter(data => data !== undefined);
          // 1st arg=>> all profiles,  2nd Arg=>> data we are sending
          scoreProfiles(messages.profileArray, descardNULL);
          // find intent  creating nlp obj
          descardNULL.map(profile => {
            if (profile.messageArray.length > profile.messageArrayHistory.length) {
              const length = profile.messageArray.length - profile.messageArrayHistory.length;
              const elements = profile.messageArray.splice(
                profile.messageArrayHistory.length,
                length
              );
              profile.messageArray.push(...elements);
              profile.messageArrayHistory.push(...elements);
              profile.sendTemplateMessage = true;
            } else {
              profile.sendTemplateMessage = false;
            }
          });
          const nlpObj = {
            profileArray: descardNULL,
          };
          console.log(nlpObj);
          const parsedFromIntent = await findIntent(nlpObj);
          // assign messages based on intent and connection type
          descardNULL = parsedFromIntent.profileArray;
          console.log('<<<<<<<<discardednullAfterIntent>>>>>>', descardNULL);
          console.log('discardednullAfterIntent>>>>>>', descardNULL);
          console.log('discardednullAfterIntent>>>>>>', descardNULL);
          // assign messages based on intent and connection type
          // assignMessages(descardNULL, applicantData, user);
          await populateMessages(descardNULL, applicantData, user);
          // update db obj
          try {
            // messages.map(async obj => {
            messages.profileArray.map(async originalProfile => {
              for (const updatedProfile of descardNULL) {
                // delete updatedProfile.requestTime;
                // delete updatedProfile.connection_degree;
                // delete updatedProfile.hoursPassed;
                // delete updatedProfile.firstName;
                // delete updatedProfile.lastName;
                // delete updatedProfile.companyName;
                // delete updatedProfile.schoolName;
                // delete updatedProfile.name;
                // delete updatedProfile.jobLink;
                // delete updatedProfile.batchMessagesCompleted;
                // applicantData.requestArray.map(async updatedProfile => {
                if (updatedProfile.entityUrn === originalProfile.entityUrn) {
                  console.log("intent>>>>>>>>>>>>", updatedProfile.intent_object);
                  if (updatedProfile.intent_object && updatedProfile.intent_object.text) {
                    const intentTextObjType = typeof updatedProfile.intent_object.text === 'string';
                    if (updatedProfile.intent_object && intentTextObjType === true) {
                      updatedProfile.intent_object.text = JSON.parse(
                        updatedProfile.intent_object.text
                      );
                    }
                  }
                  const updateData = await applicantModel.update(
                    { 'profileArray.entityUrn': updatedProfile.entityUrn },
                    {
                      $set: {
                        'profileArray.$.intent_info': updatedProfile.intent_info,
                        'profileArray.$.message_info': updatedProfile.message_info,
                        'profileArray.$.messageNumber': updatedProfile.messageNumber,
                        'profileArray.$.intent': updatedProfile.intent,
                        'profileArray.$.intent_object': updatedProfile.intent_object,
                        'profileArray.$.readMessages': updatedProfile.readMessages,
                        'profileArray.$.sendMessages': updatedProfile.sendMessages,
                        // 'profileArray.$.messageArray': updatedProfile.messageArray,
                        'profileArray.$.messageArrayHistory': updatedProfile.messageArrayHistory,
                        'profileArray.$.messageVariantsArray': updatedProfile.messageVariantsArray,
                        'profileArray.$.connection_state': updatedProfile.connection_state,
                        'profileArray.$.score_quality': updatedProfile.score_quality,
                        'profileArray.$.requestTime': updatedProfile.requestTime,
                        'profileArray.$.hoursPassed': updatedProfile.hoursPassed,
                        'profileArray.$.batchMessagesCompleted':
                          updatedProfile.batchMessagesCompleted,
                        'profileArray.$.lastMessageGenerated': updatedProfile.lastMessageGenerated,
                      },
                    }
                  );
                  console.log(updateData);
                }
                // })
              }
            });
            // });
          } catch (err) {
            return { err };
          }
          if (!applicantData.bot) {
            descardNULL.map(applicant => {
              if (applicant.messageArray.length > 0) {
                const lastElement = applicant.messageArray[applicant.messageArray.length - 1];
                if (
                  lastElement.type.toLowerCase() === 'sent' &&
                  lastElement.linkedinMessageID === ''
                ) {
                  applicant.readMessages = 1;
                  applicant.sendMessages = 1;
                  const lastMessage = JSON.parse(
                    JSON.stringify(applicant.messageArray[applicant.messageArray.length - 1])
                  );
                  applicant.messageArray = [];
                  applicant.messageArray.push(lastMessage);
                } else {
                  applicant.readMessages = 1;
                  applicant.sendMessages = 1;
                  applicant.messageArray = [];
                  applicant.lastMessageGenerated.messageText
                    ? applicant.messageArray.push(applicant.lastMessageGenerated)
                    : '';
                }
              }
              // else {
              //   const objToSend = {
              //     type: 'RECEIVED',
              //     linkedinMessageID: '',
              //     serverMessageID: mongoose.Types.ObjectId(),
              //     messageText: '',
              //     dateTime: moment(),
              //     messageType: applicant.scoreType,
              //     isDefault: 1,
              //   };
              //   applicant.readMessages = 1;
              //   applicant.sendMessages = 1;
              //   applicant.messageArray.push(objToSend);
              // }
            });
          }
          let messageArrayApplicants;

          if (!applicantData.bot) {
            messageArrayApplicants = descardNULL.filter(
              data =>
                data.intent.toLowerCase() !== 'do not know' &&
                data.intent.toLowerCase() !== 'dont_know'
            );
          } else {
            messageArrayApplicants = descardNULL;
          }

          const sortedByScore = messageArrayApplicants.sort(
            (a, b) => parseFloat(b.score.final_score) - parseFloat(a.score.final_score)
          );
          const sendApplicants = {
            // TotalApplicants: applicantsLength,
            messagesArray: sortedByScore,
          };
          return sendApplicants;
        }
        return { notFound: true };
      }
      if (applicantData.jobID === 0 || applicantData.jobID === '0') {
        const messages = await applicantMessagesModel.findOne({
          tempID: 0,
          recruiterID: applicantData.recruiterID,
        });
        if (messages && messages.recruiterID === applicantData.recruiterID) {
          const sendApplicants = {
            // TotalApplicants: applicantsLength,
            messagesArray: [...messages.profileArray],
          };
          return sendApplicants;
        }
        return { notFound: true };
      }
    }
    return null;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};
module.exports = {
  getMessages,
};
