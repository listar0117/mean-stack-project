const moment = require('moment');
const mongoose = require('mongoose');

const userModel = require('../../models/user.js');
const jobModel = require('../../models/job');
const applicantModel = require('../../models/applicants');

const getMessages = async applicantData => {
  const user = await userModel.findOne({ recruiterID: applicantData.recruiterID });
  try {
    if (user) {
      let jobExist;
      try {
        jobExist = await jobModel.findOne({ _id: applicantData.jobID });
      } catch (err) {
        console.log('error =====>>>>>', JSON.stringify(err));
        return { notFound: true };
      }
      if (jobExist && jobExist.recruiterID === applicantData.recruiterID) {
        let messages = await applicantModel.findOne({ jobID: applicantData.jobID });
        messages = JSON.parse(JSON.stringify(messages));
        const filteredMessagesArray = messages.profileArray.filter(
          data => data.entityUrn === applicantData.entity
        );
        const sendMessagesArray = {
          // TotalApplicants: applicantsLength,
          messagesArray: filteredMessagesArray[0].messageArray,
          full_name: filteredMessagesArray[0].full_name,
        };
        return sendMessagesArray;
      }
      return { notFound: true };
    }
  } catch (err) {
    return { err };
  }
};

module.exports = {
  getMessages,
};
