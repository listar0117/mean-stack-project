const request = require('request-promise-native');
require('dotenv').config();

const moment = require('moment');
const mongoose = require('mongoose');
const userModel = require('../../models/user.js');
const jobModel = require('../../models/job');
const applicantModel = require('../../models/applicants');
const applicantMessagesModel = require('../../models/applicantMessage');
const defaultMessage = require('../batchMessagingObj');
// this service+ attached api is no longer being used.
const intentInfo = `This shows the current intent of an applicant. They might be interested in applying for the position posted, interested in learning more or not interested.`;
const messageInfo = `This shows the template that the system is about to send to a user. This is based on scoring, intent and other attributes the system detects.`;
const median = values => {
  if (values.length === 0) return 0;
  values.sort((a, b) => {
    return a - b;
  });
  const half = Math.floor(values.length / 2);
  if (values.length % 2) return values[half];
  return (values[half - 1] + values[half]) / 2.0;
};

const scoreProfiles = async (AllProfiles, sendingProfiles) => {
  const finalScores = AllProfiles.map(profile => {
    if (profile.score) {
      return profile.score.final_score;
    }
  });
  const refineFinalScore = finalScores.filter(data => data !== undefined);
  const calcMedian = median(refineFinalScore);
  console.log(calcMedian);

  // match scores
  sendingProfiles.map(profile => {
    let tempScore = true;
    if (profile.score && profile.score.final_score >= calcMedian) {
      profile.scoreType = 'High Score';
      // profile.messageArray.map(message => {
      //   message.messageType = 'High Score';
      //   if (tempScore === true) {
      //     message.isDefault = 1;
      //     tempScore = false;
      //   } else {
      //     message.isDefault = 0;
      //   }
      // });
    }
    if (profile.score && profile.score.final_score < calcMedian) {
      profile.scoreType = 'Low Score';
      // profile.messageArray.map(message => {
      //   message.messageType = 'Low Score';
      //   message.isDefault = 0;
      // });
    }
  });
  return sendingProfiles;
};

// check how much time is passed
const checkTimePassed = initialTime => {
  const currentTime = moment();

  const timePassed = moment(initialTime);
  const hoursToReturn = currentTime.diff(timePassed, 'h');
  return hoursToReturn;
};

// find Intent
const findIntent = async filteredProfiles => {
  // filteredProfiles.map(profile => {
  //   profile.intent = 'Interested';
  // });
  let response;
  try {
    response = await request({
      method: 'POST',
      // url: `http://34.67.207.137:3002/score_profile`,
      url: `${process.env.nlpUrl}${process.env.nlpPort}/detect_profile_intent`,
      form: {
        intent_input: JSON.stringify(filteredProfiles),
      },
    });
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    console.log('error on nlp', err);
    return { nlpError: true };
  }
  if (response.isSuccess === false) {
    console.log('error =====>>>>>', JSON.stringify(response));
    console.log('error on nlp intent api', response);
    return { nlpError: true };
  }
  return JSON.parse(response);
};
// const addMessageObj = profile => {
//   if (profile.scoreType.toLowerCase() === 'high') {
//     let tempScore = true;
//     profile.messageArray.map(message => {
//       message.messageType = 'High Score';
//       if (tempScore === true) {
//         message.isDefault = 1;
//         tempScore = false;
//       } else {
//         message.isDefault = 0;
//       }
//     });
//   }
//   if (profile.scoreType.toLowerCase() === 'low') {
//     let tempScore = true;
//     profile.messageArray.map(message => {
//       message.messageType = 'Low Score';
//       if (tempScore === true) {
//         message.isDefault = 1;
//         tempScore = false;
//       } else {
//         message.isDefault = 0;
//       }
//     });
//   }
// };

const createMessageVariants = (messageArray, user, recruiterData) => {
  messageArray.map(message => {
    let UserMessage = message.value.message;
    if (UserMessage) {
      UserMessage = UserMessage.replace(/{First_Name}/g, user.firstName);
      UserMessage = UserMessage.replace(/{Last_Name}/g, user.lastName);
      UserMessage = UserMessage.replace(/{Company_Name}/g, user.companyName);
      UserMessage = UserMessage.replace(/{School_Name}/g, user.schoolName);
      UserMessage = UserMessage.replace(/{Job_Link}/g, user.jobLink);
      UserMessage = UserMessage.replace(/{Job_Title}/g, user.jobTitle);
      UserMessage = UserMessage.replace(/{Link_to_calendar}/g, recruiterData.calendlyLink);
      UserMessage = UserMessage.replace(/{User_Name}/g, user.name);
      UserMessage.replace(/ +/g, ' ');
    }
    // const objToSend = {
    message.type = 'SENT';
    message.linkedinMessageID = '';
    // message.serverMessageID = mongoose.Types.ObjectId(),
    message.messageText = UserMessage || '';
    message.dateTime = moment();
    message.messageType = user.scoreType;
    message.isDefault = 1;
    // };
    // return objToSend;
    delete message.value;
  });
  return messageArray;
};

const createMessageText = (message, user, recruiterData) => {
  let UserMessage = message;
  if (UserMessage) {
    UserMessage = UserMessage.replace(/{First_Name}/g, user.firstName);
    UserMessage = UserMessage.replace(/{Last_Name}/g, user.lastName);
    UserMessage = UserMessage.replace(/{Company_Name}/g, user.companyName);
    UserMessage = UserMessage.replace(/{School_Name}/g, user.schoolName);
    UserMessage = UserMessage.replace(/{Job_Link}/g, user.jobLink);
    UserMessage = UserMessage.replace(/{Job_Title}/g, user.jobTitle);
    UserMessage = UserMessage.replace(/{Link_to_calendar}/g, recruiterData.calendlyLink);
    UserMessage = UserMessage.replace(/{User_Name}/g, user.name);
    UserMessage.replace(/ +/g, ' ');
  }
  const objToSend = {
    type: 'SENT',
    linkedinMessageID: '',
    serverMessageID: mongoose.Types.ObjectId(),
    messageText: UserMessage || '',
    dateTime: moment(),
    connectionType:
      user.connection_degree === '1st'
        ? `Old connection ${user.scoreType}`
        : `New connection ${user.scoreType}`,
    messageType: user.scoreType,
    isDefault: 1,
  };
  user.lastMessageGenerated = objToSend;
  return objToSend;
};
const addRelevantMessage = (profile, messageUID, threadID, thread, userData, jobData) => {
  // if (profile.messageArray.length === 0) {
  let defaultmessagesVal = false;
  if (jobData && jobData.defaultBatchMessages && jobData.defaultBatchMessages.length > 0) {
    // addMessageObj(profile);
    const message = jobData.defaultBatchMessages.filter(
      messageObj => messageObj.uid === messageUID
    );
    let messageThread;
    const messageVariantArray = [];
    if (thread === 'interested') {
      messageThread = message[0].interestedThread;
      jobData.defaultBatchMessages.map(messageObj => {
        if (messageObj.connectionType === 'New connection') {
          const objToPush = {
            connectionType: `${messageObj.connectionType} ${messageObj.score}`,
            value: messageObj.interestedThread[1],
          };
          messageVariantArray.push(objToPush);
        }
        if (messageObj.connectionType === 'Old connection') {
          const objToPush = {
            connectionType: `${messageObj.connectionType} ${messageObj.score}`,
            value: messageObj.interestedThread[2],
          };
          messageVariantArray.push(objToPush);
        }
      });
    }
    if (thread === 'notInterested') {
      messageThread = message[0].notInterestedThread;
      jobData.defaultBatchMessages.map(messageObj => {
        if (messageObj.connectionType === 'New connection') {
          const objToPush = {
            connectionType: `${messageObj.connectionType} ${messageObj.score}`,
            value: messageObj.notInterestedThread[1],
          };
          messageVariantArray.push(objToPush);
        }
        if (messageObj.connectionType === 'Old connection') {
          const objToPush = {
            connectionType: `${messageObj.connectionType} ${messageObj.score}`,
            value: messageObj.notInterestedThread[2],
          };
          messageVariantArray.push(objToPush);
        }
      });
    }
    if (thread === '24') {
      messageThread = message[0].NoReplyThread24;
    }
    if (thread === '48') {
      messageThread = message[0].NoReplyThread48;
    }
    if (thread === '7') {
      messageThread = message[0].NoReplyThread7;
    }

    const messageObj = createMessageText(messageThread[threadID].message, profile, userData);
    const messageVariants = createMessageVariants(messageVariantArray, profile, userData);
    profile.messageArray.push(messageObj);
    profile.messageVariantsArray = messageVariants;
    defaultmessagesVal = true;
    return profile;
  }
  if (userData && userData.defaultBatchMessages && userData.defaultBatchMessages.length > 0) {
    const message = userData.defaultBatchMessages.filter(
      messageObj => messageObj.uid === messageUID
    );
    let messageThread;
    const messageVariantArray = [];
    if (thread === 'interested') {
      messageThread = message[0].interestedThread;
      userData.defaultBatchMessages.map(messageObj => {
        if (messageObj.connectionType === 'New connection') {
          const objToPush = {
            connectionType: `${messageObj.connectionType} ${messageObj.score}`,
            value: messageObj.interestedThread[1],
          };
          messageVariantArray.push(objToPush);
        }
        if (messageObj.connectionType === 'Old connection') {
          const objToPush = {
            connectionType: `${messageObj.connectionType} ${messageObj.score}`,
            value: messageObj.interestedThread[0],
          };
          messageVariantArray.push(objToPush);
        }
      });
    }
    if (thread === 'notInterested') {
      messageThread = message[0].notInterestedThread;
      userData.defaultBatchMessages.map(messageObj => {
        if (messageObj.connectionType === 'New connection') {
          const objToPush = {
            connectionType: `${messageObj.connectionType} ${messageObj.score}`,
            value: messageObj.notInterestedThread[1],
          };
          messageVariantArray.push(objToPush);
        }
        if (messageObj.connectionType === 'Old connection') {
          const objToPush = {
            connectionType: `${messageObj.connectionType} ${messageObj.score}`,
            value: messageObj.notInterestedThread[0],
          };
          messageVariantArray.push(objToPush);
        }
      });
    }
    if (thread === '24') {
      messageThread = message[0].NoReplyThread24;
    }
    if (thread === '48') {
      messageThread = message[0].NoReplyThread48;
    }
    if (thread === '7') {
      messageThread = message[0].NoReplyThread7;
    }
    const messageObj = createMessageText(messageThread[threadID].message, profile, userData);
    const messageVariants = createMessageVariants(messageVariantArray, profile, userData);
    profile.messageArray.push(messageObj);
    profile.messageVariantsArray = messageVariants;
    defaultmessagesVal = true;
    return profile;
  }
  if (defaultmessagesVal === false) {
    const message = defaultMessage.messagingTemplates.filter(
      messageObj => messageObj.uid === messageUID
    );
    let messageThread;
    const messageVariantArray = [];
    if (thread === 'interested') {
      messageThread = message[0].interestedThread;
      defaultMessage.messagingTemplates.map(messageObj => {
        if (messageObj.connectionType === 'New connection') {
          const objToPush = {
            connectionType: `${messageObj.connectionType} ${messageObj.score}`,
            value: messageObj.interestedThread[1],
          };
          messageVariantArray.push(objToPush);
        }
        if (messageObj.connectionType === 'Old connection') {
          const objToPush = {
            connectionType: `${messageObj.connectionType} ${messageObj.score}`,
            value: messageObj.interestedThread[0],
          };
          messageVariantArray.push(objToPush);
        }
      });
    }
    if (thread === 'notInterested') {
      messageThread = message[0].notInterestedThread;
      jobData.defaultBatchMessages.map(messageObj => {
        if (messageObj.connectionType === 'New connection') {
          const objToPush = {
            connectionType: `${messageObj.connectionType} ${messageObj.score}`,
            value: messageObj.notInterestedThread[1],
          };
          messageVariantArray.push(objToPush);
        }
        if (messageObj.connectionType === 'Old connection') {
          const objToPush = {
            connectionType: `${messageObj.connectionType} ${messageObj.score}`,
            value: messageObj.notInterestedThread[0],
          };
          messageVariantArray.push(objToPush);
        }
      });
    }
    if (thread === '24') {
      messageThread = message[0].NoReplyThread24;
    }
    if (thread === '48') {
      messageThread = message[0].NoReplyThread48;
    }
    if (thread === '7') {
      messageThread = message[0].NoReplyThread7;
    }
    const messageObj = createMessageText(messageThread[threadID].message, profile, userData);
    const messageVariants = createMessageVariants(messageVariantArray, profile, userData);
    profile.messageArray.push(messageObj);
    profile.messageVariantsArray = messageVariants;
    return profile;
  }

  // }
};
// assign Messages
// MessageData >> in case of All, receive userData,
// incase of ID receive jobData to get Messages entered by user

// sequence of numbers in addRelevantMessage function
// MessageArrayIndex,[deleted]
// messageUID,
// threadID,

const assignMessages = async (filteredProfiles, applicantData, userData, jobData) => {
  filteredProfiles.map(profile => {
    // new connections
    if (profile.batchMessagesCompleted !== true) {
      if (profile.connection_degree !== '1st') {
        // check intent
        // if (profile.intent.toLowerCase() === 'interested') {
        if (profile.scoreType.toLowerCase() === 'high score') {
          if (profile.intent.toLowerCase() === 'interested') {
            profile = addRelevantMessage(profile, 0, 1, 'interested', userData, jobData);
            profile.batchMessagesCompleted = true;
            profile.messageNumber = '1st';
            return;
          }
          if (profile.intent.toLowerCase() === 'not interested') {
            profile = addRelevantMessage(profile, 0, 1, 'notInterested', userData, jobData);
            profile.messageNumber = '1st';
            profile.batchMessagesCompleted = true;
            return;
          }
          if (profile.hoursPassed >= 24 && profile.hoursPassed < 48) {
            profile = addRelevantMessage(profile, 0, 0, '24', userData, jobData);
            profile.messageNumber = '2nd';
            return;
          }
          if (profile.hoursPassed >= 48 && profile.hoursPassed < 168) {
            profile = addRelevantMessage(profile, 0, 0, '48', userData, jobData);
            profile.messageNumber = '3rd';
            return;
          }
          if (profile.hoursPassed >= 168 && profile.hoursPassed < 174) {
            profile = addRelevantMessage(profile, 0, 0, '7', userData, jobData);
            profile.batchMessagesCompleted = true;
            profile.messageNumber = '4th';
            return;
          }
        }
        if (profile.scoreType.toLowerCase() === 'low score') {
          if (profile.intent.toLowerCase() === 'interested') {
            profile = addRelevantMessage(profile, 1, 1, 'interested', userData, jobData);
            profile.messageNumber = '1st';
            profile.batchMessagesCompleted = true;
            return;
          }
          if (profile.intent.toLowerCase() === 'not interested') {
            profile = addRelevantMessage(profile, 1, 1, 'notInterested', userData, jobData);
            profile.messageNumber = '1st';
            profile.batchMessagesCompleted = true;
            return;
          }
          if (profile.hoursPassed >= 24 && profile.hoursPassed < 48) {
            profile = addRelevantMessage(profile, 1, 0, '24', userData, jobData);
            profile.messageNumber = '2nd';
            return;
          }
          if (profile.hoursPassed >= 48 && profile.hoursPassed < 168) {
            profile = addRelevantMessage(profile, 1, 0, '48', userData, jobData);
            profile.messageNumber = '3rd';
            return;
          }
          if (profile.hoursPassed >= 168 && profile.hoursPassed < 1744) {
            profile = addRelevantMessage(profile, 1, 0, '7', userData, jobData);
            profile.batchMessagesCompleted = true;
            profile.messageNumber = '4th';
            return;
          }
        }
      }
      // old connections
      if (profile.connection_degree === '1st') {
        // check intent
        // if (profile.intent.toLowerCase() === 'interested') {
        if (
          profile.scoreType.toLowerCase() === 'high score' &&
          profile.batchMessagesCompleted !== true
        ) {
          if (profile.messageArray.length === 0) {
            // bool 'interested as the interested obj has value '
            profile = addRelevantMessage(profile, 2, 0, 'interested', userData, jobData);
            profile.messageNumber = '1st';
            return;
          }
          if (profile.intent.toLowerCase() === 'interested') {
            profile = addRelevantMessage(profile, 2, 2, 'interested', userData, jobData);
            profile.batchMessagesCompleted = true;
            profile.messageNumber = '2nd';
            return;
          }
          if (profile.intent.toLowerCase() === 'not interested') {
            profile = addRelevantMessage(profile, 2, 2, 'notInterested', userData, jobData);
            profile.messageNumber = '2nd';
            profile.batchMessagesCompleted = true;
            return;
          }
          if (profile.hoursPassed >= 24 && profile.hoursPassed < 48) {
            profile = addRelevantMessage(profile, 2, 0, '24', userData, jobData);
            profile.messageNumber = '3rd';
            return;
          }
          if (profile.hoursPassed >= 48 && profile.hoursPassed < 168) {
            profile = addRelevantMessage(profile, 2, 0, '48', userData, jobData);
            profile.messageNumber = '4th';
            return;
          }
          if (profile.hoursPassed >= 168 && profile.hoursPassed < 174) {
            profile = addRelevantMessage(profile, 2, 0, '7', userData, jobData);
            profile.batchMessagesCompleted = true;
            profile.messageNumber = '5th';
            return;
          }
        }
        if (
          profile.scoreType.toLowerCase() === 'low score' &&
          profile.batchMessagesCompleted !== true
        ) {
          if (profile.messageArray.length === 0) {
            // bool 'interested as the interested obj has value '
            profile = addRelevantMessage(profile, 3, 0, 'interested', userData, jobData);
            profile.messageNumber = '1st';
            return;
          }
          if (profile.intent.toLowerCase() === 'interested') {
            profile = addRelevantMessage(profile, 3, 2, 'interested', userData, jobData);
            profile.batchMessagesCompleted = true;
            profile.messageNumber = '2nd';
            return;
          }
          if (profile.intent.toLowerCase() === 'not interested') {
            profile = addRelevantMessage(profile, 3, 2, 'notInterested', userData, jobData);
            profile.batchMessagesCompleted = true;
            profile.messageNumber = '2nd';
            return;
          }
          if (profile.hoursPassed >= 24 && profile.hoursPassed < 48) {
            profile = addRelevantMessage(profile, 3, 0, '24', userData, jobData);
            profile.messageNumber = '3rd';
            return;
          }
          if (profile.hoursPassed >= 48 && profile.hoursPassed < 168) {
            profile = addRelevantMessage(profile, 3, 0, '48', userData, jobData);
            profile.messageNumber = '4th';
            return;
          }
          if (profile.hoursPassed >= 168 && profile.hoursPassed < 1744) {
            profile = addRelevantMessage(profile, 3, 0, '7', userData, jobData);
            profile.batchMessagesCompleted = true;
            profile.messageNumber = '5th';
            return;
          }
        }
      }
    }
  });
  return filteredProfiles;
};

// Get messages service
const getMessages = async applicantData => {
  const user = await userModel.findOne({ recruiterID: applicantData.recruiterID });
  try {
    if (user) {
      if (applicantData.jobID.toLowerCase() === 'all') {
        let applicants = await applicantModel
          .find({
            recruiterID: applicantData.recruiterID,
          })
          .populate('jobID');
        applicants = JSON.parse(JSON.stringify(applicants));
        let tempData = [];
        // let AllProfiles =[]
        const modApplicants = applicants.map(entity => {
          entity.profileArray.map(profile => {
            profile.job = entity.jobID;
          });
          tempData.push(...entity.profileArray);
          // AllProfiles.push();
          //   if (entity.tempID === 0) {
          //     entity.jobID = 0;
          //   }
          //   return entity;
        });
        const refinedData = tempData.map(applicant => {
          if (
            (applicant.connection_degree === '1st' ||
              applicant.status === 'ACCEPTED' ||
              applicant.status === 'SENT') &&
            applicant.score.final_score > 0
          ) {
            const timePassed = checkTimePassed(applicant.requestTime);
            // checkTimePassed(applicant.requestTime).then(data => {
            //   timePassed = data;
            // });
            const dataToReturn = {
              entityUrn: applicant.entityUrn,
              profileUrl: applicant.profileUrl,
              full_name: applicant.full_name,
              image_url: applicant.image_url,
              score: applicant.score,
              intent_info: intentInfo,
              message_info: messageInfo,
              messageNumber: applicant.messageNumber ? applicant.messageNumber : '1st',
              intent: applicant.intent ? applicant.intent : 'None',
              readMessages:
                (applicant.messageArray && applicant.messageArray.length > 0) ||
                !applicant.batchMessagesCompleted
                  ? 1
                  : 0,
              sendMessages: applicant.messageArray && applicant.messageArray.length > 0 ? 1 : 0,
              // readMessages: applicant.batchMessagesCompleted ? 0 : 1,
              // sendMessages: applicant.batchMessagesCompleted ? 0 : 1,
              lastMessageGenerated: applicant.lastMessageGenerated
                ? applicant.lastMessageGenerated
                : {},
              messageArray: applicant.messageArray ? applicant.messageArray : [],
              messageVariantsArray: applicant.messageVariantsArray
                ? applicant.messageVariantsArray
                : [],
              requestTime: applicant.requestTime,
              hoursPassed: timePassed,
              connection_degree: applicant.connection_degree,
              status: applicant.status,
              firstName: applicant.firstName,
              lastName: applicant.lastName,
              companyName: applicant.companyName,
              schoolName: applicant.schoolName,
              name: applicant.name,
              jobLink: applicant.job.jobLink,
              jobTitle: applicant.job.jobTitle,
              currentTitle: applicant.title,
              batchMessagesCompleted: applicant.batchMessagesCompleted
                ? applicant.batchMessagesCompleted
                : false,
            };
            return dataToReturn;
          }
        });
        let descardNULL = refinedData.filter(data => data !== undefined);
        scoreProfiles(tempData, descardNULL);
        // find intent creating nlp obj
        const nlpObj = {
          profileArray: descardNULL,
        };
        const parsedFromIntent = await findIntent(nlpObj);
        // assign messages based on intent and connection type
        descardNULL = parsedFromIntent.profileArray;
        assignMessages(descardNULL, applicantData, user);

        // update db with added messages and fields
        try {
          applicants.map(async obj => {
            obj.profileArray.map(async originalProfile => {
              for (const updatedProfile of descardNULL) {
                // delete updatedProfile.requestTime;
                // delete updatedProfile.connection_degree;
                // delete updatedProfile.hoursPassed;
                // delete updatedProfile.firstName;
                // delete updatedProfile.lastName;
                // delete updatedProfile.companyName;
                // delete updatedProfile.schoolName;
                // delete updatedProfile.name;
                // delete updatedProfile.jobLink;
                // delete updatedProfile.batchMessagesCompleted;

                // applicantData.requestArray.map(async updatedProfile => {
                if (updatedProfile.entityUrn === originalProfile.entityUrn) {
                  const updateData = await applicantModel.update(
                    { 'profileArray.entityUrn': updatedProfile.entityUrn },
                    {
                      $set: {
                        'profileArray.$.intent_info': updatedProfile.intent_info,
                        'profileArray.$.message_info': updatedProfile.message_info,
                        'profileArray.$.messageNumber': updatedProfile.messageNumber,
                        'profileArray.$.intent': updatedProfile.intent,
                        'profileArray.$.readMessages': updatedProfile.readMessages,
                        'profileArray.$.sendMessages': updatedProfile.sendMessages,
                        'profileArray.$.messageArray': updatedProfile.messageArray,
                        'profileArray.$.messageVariantsArray': updatedProfile.messageVariantsArray,
                        'profileArray.$.requestTime': updatedProfile.requestTime,
                        'profileArray.$.hoursPassed': updatedProfile.hoursPassed,
                        'profileArray.$.batchMessagesCompleted':
                          updatedProfile.batchMessagesCompleted,
                        'profileArray.$.lastMessageGenerated': updatedProfile.lastMessageGenerated,
                      },
                    }
                  );
                  console.log(updateData);
                }
                // })
              }
            });
          });
        } catch (err) {
          return { err };
        }
        if (!applicantData.bot) {
          descardNULL.map(applicant => {
            if (applicant.messageArray.length > 0) {
              const lastElement = applicant.messageArray[applicant.messageArray.length - 1];
              if (
                lastElement.type.toLowerCase() === 'sent' &&
                lastElement.linkedinMessageID === ''
              ) {
                applicant.readMessages = 1;
                applicant.sendMessages = 1;
                const lastMessage = JSON.parse(
                  JSON.stringify(applicant.messageArray[applicant.messageArray.length - 1])
                );
                applicant.messageArray = [];
                applicant.messageArray.push(lastMessage);
              } else {
                applicant.readMessages = 1;
                applicant.sendMessages = 1;
                applicant.messageArray = [];
                applicant.lastMessageGenerated.messageText
                  ? applicant.messageArray.push(applicant.lastMessageGenerated)
                  : '';
              }
            } else {
              const objToSend = {
                type: 'SENT',
                linkedinMessageID: '',
                serverMessageID: mongoose.Types.ObjectId(),
                messageText: '',
                dateTime: moment(),
                messageType: applicant.scoreType,
                isDefault: 1,
              };
              applicant.readMessages = 1;
              applicant.sendMessages = 1;
              applicant.messageArray.push(objToSend);
            }
          });
        }
        const sortedByScore = descardNULL.sort(
          (a, b) => parseFloat(b.score.final_score) - parseFloat(a.score.final_score)
        );
        const sendApplicants = {
          messagesArray: sortedByScore,
          // tempData,
          //   total: modApplicants.length,
        };
        return sendApplicants;
      }
      if (applicantData.jobID !== 0 && applicantData.jobID !== '0') {
        let jobExist;
        try {
          jobExist = await jobModel.findOne({ _id: applicantData.jobID });
        } catch (err) {
          console.log('error =====>>>>>', JSON.stringify(err));
          return { notFound: true };
        }
        // console.log(jobExist);
        if (jobExist && jobExist.recruiterID === applicantData.recruiterID) {
          let messages = await applicantModel.findOne({ jobID: applicantData.jobID });
          messages = JSON.parse(JSON.stringify(messages));
          const refinedData = messages.profileArray.map(applicant => {
            if (
              (applicant.connection_degree === '1st' ||
                applicant.status === 'ACCEPTED' ||
                applicant.status === 'SENT') &&
              applicant.score.final_score > 0
            ) {
              const timePassed = checkTimePassed(applicant.requestTime);
              const dataToReturn = {
                entityUrn: applicant.entityUrn,
                profileUrl: applicant.profileUrl,
                full_name: applicant.full_name,
                image_url: applicant.image_url,
                score: applicant.score,
                intent_info: intentInfo,
                message_info: messageInfo,
                messageNumber: applicant.messageNumber ? applicant.messageNumber : '1st',
                intent: applicant.intent ? applicant.intent : 'None',
                readMessages:
                  (applicant.messageArray && applicant.messageArray.length > 0) ||
                  !applicant.batchMessagesCompleted
                    ? 1
                    : 0,
                sendMessages: applicant.messageArray && applicant.messageArray.length > 0 ? 1 : 0,
                // readMessages: applicant.batchMessagesCompleted ? 0 : 1,
                // sendMessages: applicant.batchMessagesCompleted ? 0 : 1,
                lastMessageGenerated: applicant.lastMessageGenerated
                  ? applicant.lastMessageGenerated
                  : {},
                messageArray: applicant.messageArray ? applicant.messageArray : [],
                messageVariantsArray: applicant.messageVariantsArray
                  ? applicant.messageVariantsArray
                  : [],
                requestTime: applicant.requestTime,
                hoursPassed: timePassed,
                connection_degree: applicant.connection_degree,
                status: applicant.status,
                firstName: applicant.firstName,
                lastName: applicant.lastName,
                companyName: applicant.companyName,
                schoolName: applicant.schoolName,
                name: applicant.name,
                jobLink: jobExist.jobLink,
                jobTitle: jobExist.jobTitle,
                currentTitle: applicant.title,
                batchMessagesCompleted: applicant.batchMessagesCompleted
                  ? applicant.batchMessagesCompleted
                  : false,
              };
              return dataToReturn;
            }
          });
          let descardNULL = refinedData.filter(data => data !== undefined);
          // 1st arg=>> all profiles,  2nd Arg=>> data we are sending
          scoreProfiles(messages.profileArray, descardNULL);
          // find intent  creating nlp obj
          const nlpObj = {
            profileArray: descardNULL,
          };
          const parsedFromIntent = await findIntent(nlpObj);
          // assign messages based on intent and connection type
          descardNULL = parsedFromIntent.profileArray;
          // assign messages based on intent and connection type
          assignMessages(descardNULL, applicantData, user);
          // update db obj
          try {
            // messages.map(async obj => {
            messages.profileArray.map(async originalProfile => {
              for (const updatedProfile of descardNULL) {
                // delete updatedProfile.requestTime;
                // delete updatedProfile.connection_degree;
                // delete updatedProfile.hoursPassed;
                // delete updatedProfile.firstName;
                // delete updatedProfile.lastName;
                // delete updatedProfile.companyName;
                // delete updatedProfile.schoolName;
                // delete updatedProfile.name;
                // delete updatedProfile.jobLink;
                // delete updatedProfile.batchMessagesCompleted;
                // applicantData.requestArray.map(async updatedProfile => {
                if (updatedProfile.entityUrn === originalProfile.entityUrn) {
                  const updateData = await applicantModel.update(
                    { 'profileArray.entityUrn': updatedProfile.entityUrn },
                    {
                      $set: {
                        'profileArray.$.intent_info': updatedProfile.intent_info,
                        'profileArray.$.message_info': updatedProfile.message_info,
                        'profileArray.$.messageNumber': updatedProfile.messageNumber,
                        'profileArray.$.intent': updatedProfile.intent,
                        'profileArray.$.readMessages': updatedProfile.readMessages,
                        'profileArray.$.sendMessages': updatedProfile.sendMessages,
                        'profileArray.$.messageArray': updatedProfile.messageArray,
                        'profileArray.$.messageVariantsArray': updatedProfile.messageVariantsArray,
                        'profileArray.$.requestTime': updatedProfile.requestTime,
                        'profileArray.$.hoursPassed': updatedProfile.hoursPassed,
                        'profileArray.$.batchMessagesCompleted':
                          updatedProfile.batchMessagesCompleted,
                        'profileArray.$.lastMessageGenerated': updatedProfile.lastMessageGenerated,
                      },
                    }
                  );
                  console.log(updateData);
                }
                // })
              }
            });
            // });
          } catch (err) {
            return { err };
          }
          if (!applicantData.bot) {
            descardNULL.map(applicant => {
              if (applicant.messageArray.length > 0) {
                const lastElement = applicant.messageArray[applicant.messageArray.length - 1];
                if (
                  lastElement.type.toLowerCase() === 'sent' &&
                  lastElement.linkedinMessageID === ''
                ) {
                  applicant.readMessages = 1;
                  applicant.sendMessages = 1;
                  const lastMessage = JSON.parse(
                    JSON.stringify(applicant.messageArray[applicant.messageArray.length - 1])
                  );
                  applicant.messageArray = [];
                  applicant.messageArray.push(lastMessage);
                } else {
                  applicant.readMessages = 1;
                  applicant.sendMessages = 1;
                  applicant.messageArray = [];
                  applicant.lastMessageGenerated.messageText
                    ? applicant.messageArray.push(applicant.lastMessageGenerated)
                    : '';
                }
              } else {
                const objToSend = {
                  type: 'SENT',
                  linkedinMessageID: '',
                  serverMessageID: mongoose.Types.ObjectId(),
                  messageText: '',
                  dateTime: moment(),
                  messageType: applicant.scoreType,
                  isDefault: 1,
                };
                applicant.readMessages = 1;
                applicant.sendMessages = 1;
                applicant.messageArray.push(objToSend);
              }
            });
          }
          const sortedByScore = descardNULL.sort(
            (a, b) => parseFloat(b.score.final_score) - parseFloat(a.score.final_score)
          );
          const sendApplicants = {
            // TotalApplicants: applicantsLength,
            messagesArray: sortedByScore,
          };
          return sendApplicants;
        }
        return { notFound: true };
      }
      if (applicantData.jobID === 0 || applicantData.jobID === '0') {
        const messages = await applicantMessagesModel.findOne({
          tempID: 0,
          recruiterID: applicantData.recruiterID,
        });
        if (messages && messages.recruiterID === applicantData.recruiterID) {
          const sendApplicants = {
            // TotalApplicants: applicantsLength,
            messagesArray: [...messages.profileArray],
          };
          return sendApplicants;
        }
        return { notFound: true };
      }
    }
    return null;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};
module.exports = {
  getMessages,
};
