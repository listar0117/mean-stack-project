const jobModel = require('../../models/job');
const applicantModel = require('../../models/applicants');
// Get All jobs
const getJobs = async jobData => {
  const jobs = await jobModel.find({});
  return jobs;
};
// Get All applicants
const getApplicants = async applicantData => {
  const applicants = await applicantModel.find({});
  return applicants;
};

module.exports = {
  getJobs,
  getApplicants,
};
