// OPLS==> old prospect low score
// OPHS==> old prospect high score
// NPNI==> new prospect not intrested
// NPIHS==> new prospect interested high score
// NPILS==> new prospect interested low score
// NPHSNM24==> new prospect high score no message for 24 hours
// NPLSNM24==> new prospect low score no message for 24 hours
// NPHSNM48==> new prospect high score no message for 24 hours
// NPLSNM48==> new prospect low score no message for 24 hours
// NPHSNM7d==> new prospect high score no message for 7 days
// NPLSNM7d==> new prospect low score no message for 7 days
const messagesVarients = {
  OPLS: `Hi {First_Name}, Hope things are well. I wanted to quickly message you to let you know of a new role I am searching for. If you or anyone in your network seems like a good fit, please let me know. Here are the details: {Job_Link}. Talk to you soon {First_Name}`,
  OPHS: `Hi {First_Name}, Hope you are doing well. I am recruiting for this new {Job_Title} role and it seems like you are a really good fit. Please let me know if you have some time to speak about this? Here is the link: {Job_Link}`,
  NPNI: `Hey {First_Name}, thanks for writing back. Completely understand that this role might not be a fit right now. Shall I check back in a few months? Do you know anyone in your network that might be interested in this role? Here is the link again: {Job_Link}`,
  NPIHS: `Thanks {First_Name}, I look forward to speaking with you. Will any of these time slots work for you? If you book a time using the link below, it will book your selected slot on both our calendars with the call-in details. Speak soon. {Link_to_calendar}`,
  OPIHS: `Thanks for responding, {First_Name}. I look forward to speaking with you and discussing more about the role. Will any of these time slots work for you? If you book a time using the link below, it will book your selected slot on both our calendars with the call-in details. Speak soon. {Link_to_calendar}`,
  NPILS: `Thanks {First_Name}. If you could kindly apply at this link with your resume? {Job_Link}. I look forward to reviewing it and discussing with the team. Speak soon`,
  OPILS: `Thanks for responding, {First_Name}. If you could kindly apply at this link with your resume? {Job_Link}. I look forward to reviewing it and discussing with the team. Speak soon`,
  NPHSNM24: `Thanks for connecting with me {First_Name}. I am looking to recruit for this new {Job_Title} role and I believe you are a great fit. Do you have some time to speak this coming week or next?`,
  NPLSNM24: `Thanks for connecting with me {First_Name}.
        I wanted to quickly message you to let you know of a new role I am searching for. If you or anyone in your network seems like a good fit, please let me know. Here are the details: {Job_Link}. Talk to you soon {First_Name}`,
  NPHSNM48: `Hi {First_Name}, I hope this note finds you well. As you know, I am looking to recruit for this new {Job_Title} role and I believe you are a great fit. Do you have some time to speak this coming week or next?`,
  NPLSNM48: `Hi {First_Name}, I hope this note finds you well. As you know, I am searching for a new role. If you or anyone in your network seems like a good fit, please let me know. Here are the details: {Job_Link}. Talk to you soon {First_Name}`,
  NPHSNM7d: `Hi {First_Name}, I promise I haven’t just connected with you to increase my connections on LinkedIn. As you know, I am looking to recruit for this new {Job_Title} role and I believe you are a great fit. Do you have some time to speak this coming week or next?`,
  NPLSNM7d: `Hi {First_Name}, I promise I haven’t just connected with you increase my connections on LinkedIn. As you know,  I am searching for a new role. If you or anyone in your network seems like a good fit, please let me know. Here are the details: {Job_Link}. Thanks.`,
  Interested: 'I am Interested',
  NotInterested: 'Not Interested',
};
const messagingTemplates = [
  {
    name: 'Mary Fields',
    image: '',
    uid: 0,
    connectionType: 'New connection',
    score: 'High score',
    interestedThread: [
      {
        type: 'user',
        newtemplateKey: '',
        message: messagesVarients.Interested,
      },
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'NPIHS',
        message: messagesVarients.NPIHS,
      },
    ],
    notInterestedThread: [
      {
        type: 'user',
        newtemplateKey: '',
        message: messagesVarients.NotInterested,
      },
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'NPHSNI',
        message: messagesVarients.NPNI,
      },
    ],
    NoReplyThread24: [
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'NPHSNM24',
        message: messagesVarients.NPHSNM24,
      },
      // {
      //   type: 'user',
      //   message: messagesVarients.Interested,
      // },
      // {
      //   type: 'dnnae',
      //   message: messagesVarients.NPIHS,
      // },
      // {
      //   type: 'user',
      //   message: messagesVarients.NotInterested,
      // },
      // {
      //   type: 'dnnae',
      //   message: messagesVarients.NPNI,
      // },
    ],
    NoReplyThread48: [
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'NPHSNM48',
        message: messagesVarients.NPHSNM48,
      },
    ],
    NoReplyThread7: [
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'NPHSNM7d',
        message: messagesVarients.NPHSNM7d,
      },
    ],
  },
  {
    name: 'John Johnson',
    image: '',
    uid: 1,
    connectionType: 'New connection',
    score: 'Low score',
    interestedThread: [
      {
        type: 'user',
        newtemplateKey: '',
        message: messagesVarients.Interested,
      },
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'NPILS',
        message: messagesVarients.NPILS,
      },
    ],
    notInterestedThread: [
      {
        type: 'user',
        newtemplateKey: '',
        message: messagesVarients.NotInterested,
      },
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'NPLSNI',
        message: messagesVarients.NPNI,
      },
    ],
    NoReplyThread24: [
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'NPLSNM24',
        message: messagesVarients.NPLSNM24,
      },
      // {
      //   type: 'user',
      //   message: messagesVarients.Interested,
      // },
      // {
      //   type: 'dnnae',
      //   message: messagesVarients.NPILS,
      // },
      // {
      //   type: 'user',
      //   message: messagesVarients.NotInterested,
      // },
      // {
      //   type: 'dnnae',
      //   message: messagesVarients.NPNI,
      // },
    ],
    NoReplyThread48: [
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'NPLSNM48',
        message: messagesVarients.NPLSNM48,
      },
    ],
    NoReplyThread7: [
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'NPLSNM7d',
        message: messagesVarients.NPLSNM7d,
      },
    ],
  },
  {
    name: 'Lisa Doe',
    image: '',
    uid: 2,
    connectionType: 'Old connection',
    score: 'High score',
    interestedThread: [
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'OPHS',
        message: messagesVarients.OPHS,
      },
      {
        type: 'user',
        newtemplateKey: '',
        message: messagesVarients.Interested,
      },
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'OPIHS',
        message: messagesVarients.OPIHS,
      },
    ],
    notInterestedThread: [
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'OPHS',
        message: messagesVarients.OPHS,
      },
      {
        type: 'user',
        newtemplateKey: '',
        message: messagesVarients.NotInterested,
      },
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'OPHSNI',
        message: messagesVarients.NPNI,
      },
    ],
    NoReplyThread24: [
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'OPHSNM24',
        message: messagesVarients.NPLSNM24,
      },
      // {
      //   type: 'user',
      //   message: messagesVarients.Interested,
      // },
      // {
      //   type: 'dnnae',
      //   message: messagesVarients.NPILS,
      // },
      // {
      //   type: 'user',
      //   message: messagesVarients.NotInterested,
      // },
      // {
      //   type: 'dnnae',
      //   message: messagesVarients.NPNI,
      // },
    ],
    NoReplyThread48: [
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'OPHSNM48',
        message: messagesVarients.NPLSNM48,
      },
    ],
    NoReplyThread7: [
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'OPHSNM7d',
        message: messagesVarients.NPLSNM7d,
      },
    ],
  },
  {
    name: 'Bill Smith',
    image: '',
    uid: 3,
    connectionType: 'Old connection',
    score: 'Low score',
    interestedThread: [
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'OPLS',
        message: messagesVarients.OPLS,
      },
      {
        type: 'user',
        newtemplateKey: '',
        message: messagesVarients.Interested,
      },
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'OPILS',
        message: messagesVarients.OPILS,
      },
    ],
    notInterestedThread: [
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'OPLS',
        message: messagesVarients.OPLS,
      },
      {
        type: 'user',
        message: messagesVarients.NotInterested,
      },
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'OPLSNI',
        message: messagesVarients.NPNI,
      },
    ],
    NoReplyThread24: [
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'OPLSNM24',
        message: messagesVarients.NPLSNM24,
      },
      // {
      //   type: 'user',
      //   message: messagesVarients.Interested,
      // },
      // {
      //   type: 'dnnae',
      //   message: messagesVarients.NPILS,
      // },
      // {
      //   type: 'user',
      //   message: messagesVarients.NotInterested,
      // },
      // {
      //   type: 'dnnae',
      //   message: messagesVarients.NPNI,
      // },
    ],
    NoReplyThread48: [
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'OPLSNM48',
        message: messagesVarients.NPLSNM48,
      },
    ],
    NoReplyThread7: [
      {
        type: 'dnnae',
        newtemplateKey: '',
        oldKeyMap: 'OPLSNM7d',
        message: messagesVarients.NPLSNM7d,
      },
    ],
  },
];
module.exports = {
  messagingTemplates,
};
