const userModel = require('../models/user.js');
const campaignModel = require('../models/campaign');
const commonService = require('./comon.service');
const linkedinString = require('./linkedinKeywordString.service');

const getCampaigns = async userData => {
  console.log('get campaigns User INPUT ====>>>>>', JSON.stringify(userData));
  try {
    const user = await userModel
      .findOne({ recruiterID: userData.recruiterID })
      .populate('campaigns', null);
    if (user) {
      const returnCampaigns = user.campaigns;
      const linkedinArray = returnCampaigns.map(eachCampaign => {
        // const getSearchData = eachCampaign.jobArray.filter(
        //   jobArray => jobArray.tag === 'TECHNICAL_SKILLS'
        // );
        // const JobTitle = eachJob.jobArray.filter(jobArray => jobArray.tag === 'JOB_POSITION');
        // const addedScore = getSearchData[0].data.map(data =>
        //   data.score || data.score === 0 ? data : { ...data, score: 0 }
        // );
        // const uriString1 = linkedinString.keywordsExtractor(addedScore, JobTitle);
        // eachJob.jobLinkedInSearchKeyword = uriString1;
        return eachCampaign;
      });
      const campaigns = linkedinArray.map(campaign => ({
        ...campaign.toObject(),
        campaignID: campaign._id,
        _id: undefined,
      }));
      return campaigns;
    }
    return null;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};
// get single job making for web
const getCampaign = async userData => {
  console.log('get single campaign User INPUT ====>>>>>', JSON.stringify(userData));
  try {
    const campaign = await campaignModel.findOne({ _id: userData.campaignID });
    // console.log(job);
    const returnCampaign = campaign || null;
    return returnCampaign;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};
// get jobs list for inbox may be add search job as well .
const getCampaignsList = async userData => {
  console.log('get jobs User INPUT ====>>>>>', JSON.stringify(userData));
  try {
    const user = await userModel
      .findOne({ recruiterID: userData.recruiterID })
      .populate('campaigns', null, { sort: { created_at: -1 } });
    if (user) {
      const returnCampaigns = user.campaigns;
      // const linkedinArray = returnJobs.map(eachJob => {
      //   const getSearchData = eachJob.jobArray.filter(
      //     jobArray => jobArray.tag === 'TECHNICAL_SKILLS'
      //   );
      //   const JobTitle = eachJob.jobArray.filter(jobArray => jobArray.tag === 'JOB_POSITION');
      //   const addedScore = getSearchData[0].data.map(data =>
      //     data.score || data.score === 0 ? data : { ...data, score: 0 }
      //   );
      //   const uriString1 = linkedinString.keywordsExtractor(addedScore, JobTitle);
      //   eachJob.jobLinkedInSearchKeyword = uriString1;
      //   return eachJob;
      // });
      const campaigns = returnCampaigns.map(campaign => ({
        // ...job.toObject(),
        campaignID: campaign._id,
        _id: undefined,
        campaignTitle: campaign.campaignTitle,
      }));
      return campaigns;
    }
    return null;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};
module.exports = {
  getCampaigns,
  getCampaign,
  getCampaignsList,
};
