const userModel = require('../models/user.js');
const jobModel = require('../models/job');
const commonService = require('./comon.service');
const linkedinString = require('./linkedinKeywordString.service');

const getJobs = async userData => {
  console.log('get jobs User INPUT ====>>>>>', JSON.stringify(userData));
  try {
    const user = await userModel
      .findOne({ recruiterID: userData.recruiterID, jobStatus: { $ne: 'DELETED' } })
      .populate('jobs', null, { jobStatus: { $ne: 'DELETED' } });
    if (user) {
      const returnJobs = user.jobs;
      const linkedinArray = returnJobs.map(eachJob => {
        const getSearchData = eachJob.jobArray.filter(
          jobArray => jobArray.tag === 'TECHNICAL_SKILLS'
        );
        const JobTitle = eachJob.jobArray.filter(jobArray => jobArray.tag === 'JOB_POSITION');
        const addedScore = getSearchData[0].data.map(data =>
          data.score || data.score === 0 ? data : { ...data, score: 0 }
        );
        const uriString1 = linkedinString.keywordsExtractor(addedScore, JobTitle);
        eachJob.jobLinkedInSearchKeyword = uriString1;
        return eachJob;
      });
      const jobs = linkedinArray.map(job => ({
        ...job.toObject(),
        jobID: job._id,
        _id: undefined,
      }));
      return jobs;
    }
    return null;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};
// get single job making for web
const getJob = async userData => {
  console.log('get single job User INPUT ====>>>>>', JSON.stringify(userData));
  try {
    const job = await jobModel.findOne({ _id: userData.jobID });
    // console.log(job);
    const returnJob = job || null;
    return returnJob;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};
// get jobs list for inbox may be add search job as well .
const getJobsList = async userData => {
  console.log('get jobs User INPUT ====>>>>>', JSON.stringify(userData));
  try {
    const user = await userModel
      .findOne({ recruiterID: userData.recruiterID, jobStatus: { $ne: 'DELETED' } })
      .populate('jobs', null, { jobStatus: { $ne: 'DELETED' } }, { sort: { created_at: -1 } });
    if (user) {
      const returnJobs = user.jobs;
      // const linkedinArray = returnJobs.map(eachJob => {
      //   const getSearchData = eachJob.jobArray.filter(
      //     jobArray => jobArray.tag === 'TECHNICAL_SKILLS'
      //   );
      //   const JobTitle = eachJob.jobArray.filter(jobArray => jobArray.tag === 'JOB_POSITION');
      //   const addedScore = getSearchData[0].data.map(data =>
      //     data.score || data.score === 0 ? data : { ...data, score: 0 }
      //   );
      //   const uriString1 = linkedinString.keywordsExtractor(addedScore, JobTitle);
      //   eachJob.jobLinkedInSearchKeyword = uriString1;
      //   return eachJob;
      // });
      const jobs = returnJobs.map(job => ({
        // ...job.toObject(),
        jobID: job._id,
        _id: undefined,
        jobTitle: job.jobTitle,
      }));
      return jobs;
    }
    return null;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};
module.exports = {
  getJobs,
  getJob,
  getJobsList,
};
