const userModel = require('../models/user.js');
const jobModel = require('../models/job');

const getLink = async userData => {
  console.log('get calendlyLink User INPUT ====>>>>>', JSON.stringify(userData));
  const user = await userModel.findOne({ recruiterID: userData.recruiterID });
  let getJob;
  try {
    // if (userData.jobID.toLowerCase() !== 'all') {
    //   getJob = await jobModel.findOne({ _id: userData.jobID });
    // }
    if (user) {
      let link = '';
      if (user.calendlyLink) {
        link = user.calendlyLink;
      }
      return {
        link,
      };
    }
    return null;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};

// set settings message coming from api for the user

const setLink = async userData => {
  const user = await userModel.findOne({ recruiterID: userData.recruiterID });
  try {
    if (user) {
      const updatedUser = await userModel.updateOne(
        { _id: user._id },
        { calendlyLink: userData.link }
      );
      return { updated: true };
    }
    return null;
  } catch (err) {
    return { err };
  }
};

module.exports = {
  getLink,
  setLink,
};
