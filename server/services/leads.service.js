const leadModel = require('../models/homeLeads');
// const commonService = require('./comon.service');

const postLead = async leadData => {
  console.log('Lead Data User INPUT ====>>>>>', JSON.stringify(leadData));
  const checkLead = await leadModel.findOne({ emailAddress: leadData.emailAddress });
  try {
    if (!checkLead) {
      const applicantAdded = await leadModel.create({ ...leadData });
      return { excist: false, updated: true };
    }
    return { excist: true };
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};

module.exports = {
  postLead,
};
