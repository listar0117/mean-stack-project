const userModel = require('../../models/user.js');
const jobModel = require('../../models/job');
const applicantModel = require('../../models/applicants');

// update applicants statuses
const updateStatus = async applicantData => {
  const user = await userModel.findOne({ recruiterID: applicantData.recruiterID });
  try {
    if (user) {
      if (applicantData.jobID && applicantData.jobID.toLowerCase() !== 'all') {
        const jobExist = await jobModel.findOne({ _id: applicantData.jobID });
        // console.log(jobExist);
        if (jobExist && jobExist.recruiterID === applicantData.recruiterID) {
          const applicantDBData = await applicantModel.findOne({ jobID: applicantData.jobID });
          if (applicantDBData) {
            const connectionDate = Date.now();
            const updatedArray = applicantDBData.profileArray.filter(dbApplicant =>
              applicantData.requestArray.every(apiApplicant => {
                if (dbApplicant.profileUrl === apiApplicant.profileUrl) {
                  dbApplicant.status = apiApplicant.status;
                  if (!dbApplicant.requestTime) {
                    dbApplicant.requestTime = connectionDate;
                  }
                }
                return dbApplicant;
              })
            );
            console.log(updatedArray);
            const updateData = await applicantModel.update(
              { _id: applicantDBData._id },
              { $set: { profileArray: updatedArray } }
            );
            console.log(updateData);
            return { updated: true };
          }
          return { updated: false };
        }
      }

      // when no jobId is present
      if (applicantData.jobID && applicantData.jobID.toLowerCase() === 'all') {
        let applicantsObjects = await applicantModel.find({
          recruiterID: applicantData.recruiterID,
        });
        if (applicantsObjects) {
          applicantsObjects = JSON.parse(JSON.stringify(applicantsObjects));
          try {
            applicantsObjects.map(async obj => {
              obj.profileArray.map(async dbProfile => {
                for (const apiProfile of applicantData.requestArray) {
                  // applicantData.requestArray.map(async apiProfile => {
                  if (apiProfile.entityUrn === dbProfile.entityUrn) {
                    const connectionDate = Date.now();
                    const requestTime = dbProfile.requestTime
                      ? dbProfile.requestTime
                      : connectionDate;
                    const updateData = await applicantModel.update(
                      { 'profileArray.entityUrn': apiProfile.entityUrn },
                      {
                        $set: {
                          'profileArray.$.status': apiProfile.status,
                          'profileArray.$.connectionDate': connectionDate,
                          'profileArray.$.requestTime': requestTime,
                        },
                      }
                    );
                    console.log(updateData);
                  }
                  // })
                }
              });
            });

            return { updated: true };
          } catch (err) {
            return { err };
          }
        }
        return { updated: false };
      }

      return { notFound: true };
    }
    return null;
  } catch (err) {
    return { err };
  }
};

// fetch Top applicants to connect
const getTopApplicants = async applicantData => {
  const user = await userModel.findOne({ recruiterID: applicantData.recruiterID });
  try {
    if (user) {
      const jobExist = await jobModel.findOne({ _id: applicantData.jobID });
      // console.log(jobExist);
      if (jobExist && jobExist.recruiterID === applicantData.recruiterID) {
        const applicantDBData = await applicantModel.findOne({ jobID: applicantData.jobID });
        if (applicantDBData) {
          const scoredElements = applicantDBData.profileArray.filter(profile => profile.score);
          const sortedByScore = scoredElements.sort(
            (a, b) => parseFloat(b.score.final_score) - parseFloat(a.score.final_score)
          );
          const filteredConnection = sortedByScore.filter(
            applicant =>
              applicant.connection_degree !== '1st' &&
              applicant.status !== 'SENT' &&
              applicant.status !== 'ACCEPTED' &&
              applicant.status !== 'REFUSED' &&
              applicant.score &&
              applicant.score.final_score > 0
          );
          // let Top = [];
          // if (filteredConnection.length > 100) {
          //   Top = filteredConnection.slice(0, 100);
          // } else {
          //   Top = filteredConnection;
          // }
          const Top = filteredConnection;
          const sendData = Top.map(
            ({
              profileUrl,
              entityUrn,
              score,
              firstName,
              lastName,
              companyName,
              schoolName,
              full_name,
              image_url,
              headline,
              locationName,
              locality,
              connection_degree,
            }) => {
              // real time logic
              let UserMessage = user.defaultMesage;
              if (jobExist && jobExist.defaultMesage) {
                UserMessage = jobExist.defaultMesage;
              }
              if (UserMessage) {
                UserMessage = UserMessage.replace(/{{.First_Name}}/g, firstName);
                UserMessage = UserMessage.replace(/{{.Last_Name}}/g, lastName);
                UserMessage = UserMessage.replace(/{{.Company_Name}}/g, companyName);
                UserMessage = UserMessage.replace(/{{.School_Name}}/g, schoolName);
                UserMessage = UserMessage.replace(/{{.Job_Title}}/g, jobExist.jobTitle);
                UserMessage = UserMessage.replace(/{{.User_Name}}/g, user.name);
              }
              const DefaultMessage = `Hi ${firstName},
              ${
                companyName || schoolName
                  ? `I see that you ${companyName ? `work with ${companyName} and` : ``} ${
                      schoolName ? `went to ${schoolName}, ` : ``
                    } and`
                  : ``
              }I’m impressed with your work and education. My business is looking for ${
                jobExist.jobTitle
              }, and you appear to be a great fit. Let me know if you’re interested.
              Thanks,
              ${user.name}`;
              return {
                profileUrl,
                entityUrn,
                full_name,
                image_url,
                score,
                headline,
                locationName,
                locality,
                connection_degree,
                jobTitle: jobExist.jobTitle,
                message:
                  user.defaultMesage || (jobExist && jobExist.defaultMesage)
                    ? UserMessage.replace(/ +/g, ' ')
                    : // /[ |\t]+/g
                      DefaultMessage.replace(/ +/g, ' '),
              };
            }
          );
          console.log(sendData);
          let messageTemplate = `Hi {{.First_Name}},
          I see that you work with {{.Company_Name}} and went to {{.School_Name}}, and I’m impressed with your work and education. My business is looking for {{.Job_Title}}, and you appear to be a great fit. Let me know if you’re interested.
          Thanks,
          {{.User_Name}}`;
          if (user.defaultMesage) {
            messageTemplate = user.defaultMesage;
          }
          if (jobExist && jobExist.defaultMesage) {
            messageTemplate = jobExist.defaultMesage;
          }

          const dataToSend = {
            sendData,
            messageTemplate,
          };
          return { found: true, dataToSend };
        }
        return { updated: false };
      }

      return { notFound: true };
    }
    return null;
  } catch (err) {
    console.log(err);
    return { err };
  }
};
module.exports = {
  updateStatus,
  getTopApplicants,
};
