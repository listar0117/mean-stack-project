const request = require('request-promise-native');
require('dotenv').config();

const userModel = require('../../models/user.js');
const jobModel = require('../../models/job');
const applicantModel = require('../../models/applicants');
const quickApplicantModel = require('../../models/quickApplicants');
const commonService = require('../comon.service');

const comparer = otherArray => {
  return current => {
    return (
      otherArray.filter(other => {
        return other.entityUrn === current.entityUrn;
      }).length === 0
    );
  };
};
const getUnique = (arr, comp) => {
  // const unique = arr
  //   .map(e => e[comp])
  //   // store the keys of the unique objects
  //   .map((e, i, final) => final.indexOf(e) === i && i)
  //   // eliminate the dead keys & store unique objects
  //   .filter(e => arr[e])
  //   .map(e => arr[e]);
  // return unique;
  const listOfTags = arr;
  const keys = [comp];
  const filtered = listOfTags.filter(
    (s => o => (k => !s.has(k) && s.add(k))(keys.map(k => o[k]).join('|')))(new Set())
  );
  return filtered;
};
// send data to common db;
const commonDB = async data => {
  let response;
  try {
    response = await request({
      method: 'POST',
      url: `http://34.82.130.16:3001/save`,
      headers: {
        'Content-Type': 'application/json',
      },
      // url: `${process.env.nlpUrl}${process.env.nlpPort}/score_profile1`,
      body: data,
      json: true,
    });
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    console.log('error while saving Data in commonDB', err);
    // return { nlpError: true };
  }
  console.log('resssssssssssssssssssssssss', response);
};

// add Applicants service
const addApplicant = async applicantData => {
  const user = await userModel.findOne({ recruiterID: applicantData.recruiterID });
  let jobFound = {};
  try {
    if (user) {
      if (applicantData.jobID) {
        jobFound = await jobModel.findOne({ _id: applicantData.jobID });
        if (!jobFound) {
          return { notFound: true };
        }
      }
      // const checkApplicantsData = await applicantModel.findOne({ jobID: applicantData.jobID });
      // if (checkApplicantsData) {
      // filter out undefined profiles
      const validQuickProfiles = [];
      const validDeepProfiles = applicantData.profileArray.filter(applicant => {
        if (
          applicant.profileUrl !== 'https://www.linkedin.com/undefined' &&
          applicant.profileUrl !== 'https://www.linkedin.com/undefined/' &&
          applicant.profileUrl !== 'https://www.linkedin.com/in/undefined' &&
          applicant.profileUrl !== 'https://www.linkedin.com/in/undefined/' &&
          applicant.profileUrl !== '' &&
          applicant.profileUrl !== null &&
          applicant.profileUrl !== 'NULL' &&
          applicant.profileUrl !== undefined
        ) {
          if (applicant.scrapeType === 'DEEP') {
            return applicant;
          }
          if (applicant.scrapeType === 'QUICK') {
            validQuickProfiles.push(applicant);
          }
        }
      });
      applicantData.profileArray = getUnique(validDeepProfiles, 'entityUrn');
      if (process.env.NODE_ENV !== 'local') {
        // if (!applicantData.jobID === '5db9ba264dc72b663deef9db') {
          const sendToDb = await commonDB(applicantData.profileArray);
        // }
        if (user.recruiterID === 66) {
          const addtoDB = {
            added: true,
          };
          return addtoDB;
        }
      }
      const newApplicantData = {
        ...applicantData,
      };
      delete newApplicantData.recruiterID;
      newApplicantData.recruiter = user._id;
      newApplicantData.recruiterID = user.recruiterID;
      // const deepApplicants = newApplicantData.profileArray.filter(
      //   applicant => applicant.scrapeType === 'DEEP'
      // );

      const quickApplicantsObj = JSON.parse(JSON.stringify(newApplicantData));
      quickApplicantsObj.profileArray = validQuickProfiles;
      const applicantArray = { applicantArray: newApplicantData.profileArray };
      // }
      console.log('applicant Array', applicantArray);
      console.log('quick applicant Array', validQuickProfiles);
      // if (!applicantData.jobID === '5db9ba264dc72b663deef9db') {
        if (applicantArray.applicantArray.length > 0) {
          applicantArray.applicantArray.forEach(applicant => {
            if (!applicant.job) {
              applicant.job = jobFound;
            }
          });
          console.log('1111', process.env.nlpUrl);
          console.log('1111', process.env.nlpPort);
          let response;
          try {
            response = await request({
              method: 'POST',
              // url: `http://34.67.207.137:3002/score_profile`,
              url: `${process.env.nlpUrl}${process.env.nlpPort}/score_profile1`,
              form: {
                applicant_obj: JSON.stringify(applicantArray),
              },
            });
          } catch (err) {
            console.log('error =====>>>>>', JSON.stringify(err));
            console.log('error on nlp', err);
            return { nlpError: true };
          }
          console.log('resssssssssssssssssssssssss', response);
          const recivedData = JSON.parse(response);
          if (recivedData.isSuccess === false) {
            console.log('error =====>>>>>', JSON.stringify(recivedData));
            console.log('error on nlp api', recivedData);
            return { nlpError: true };
          }
          const ScoreArray = Object.keys(recivedData[0]).map(function(key) {
            return { profileUrl: key, score: recivedData[0][key] };
          });
          newApplicantData.profileArray.forEach(applicant => {
            ScoreArray.forEach(scoreObj => {
              if (!applicant.score) {
                if (applicant.profileUrl === scoreObj.profileUrl) {
                  applicant.score = scoreObj.score;
                }
                //  else {
                //   applicant.score = {};
                // }
              }
              delete applicant.job;
              const date = new Date();
              const timestamp = date.getTime();
              applicant.createdAt = timestamp;
              applicant.updatedAt = timestamp;
            });
          });
          console.log('nayyyyayyyyyy', newApplicantData);
          // console.log('response.statusCode: ' + response);
        }
      // }
      console.log('movedon');
      const checkApplicantsData = await applicantModel.findOne({ jobID: applicantData.jobID });
      if (checkApplicantsData && checkApplicantsData.recruiterID === applicantData.recruiterID) {
        // Find values that are in apiProfiles but not in dbProfiles
        const dbData = JSON.parse(JSON.stringify(checkApplicantsData));
        const uniqueProfiles = newApplicantData.profileArray.filter(comparer(dbData.profileArray));
        if (uniqueProfiles.length > 0) {
          // const date = new Date();
          // const timestamp = date.getTime();
          // uniqueProfiles.map(profile => {
          //   const o = Object.assign({}, profile);
          //   o.createdAt = timestamp;
          //   o.updatedAt = timestamp;
          //   return o;
          // });
          // update Data
          const UpdateProfileData = await applicantModel.updateOne(
            { _id: checkApplicantsData._id },
            { $push: { profileArray: { $each: uniqueProfiles } } }
          );
        }
        if (quickApplicantsObj.profileArray && quickApplicantsObj.profileArray.length > 0) {
          const checkQuickApplicantsData = await quickApplicantModel.findOne({
            jobID: applicantData.jobID,
          });
          if (
            checkQuickApplicantsData &&
            checkQuickApplicantsData.recruiterID === applicantData.recruiterID
          ) {
            const quickDbData = JSON.parse(JSON.stringify(checkQuickApplicantsData));
            const quickUniqueProfiles = quickApplicantsObj.profileArray.filter(
              comparer(quickDbData.profileArray)
            );
            if (quickUniqueProfiles.length > 0) {
              // update quick applicant model Data
              const UpdateQuickApplicantsData = await quickApplicantModel.updateOne(
                { _id: checkQuickApplicantsData._id },
                { $push: { profileArray: { $each: quickUniqueProfiles } } }
              );
            }
          } else {
            const quickApplicant = await quickApplicantModel.create({ ...quickApplicantsObj });
            const job = await jobModel.findOne({ _id: applicantData.jobID });
            const updatedjob = await jobModel.update(
              { _id: job._id },
              { $addToSet: { quickApplicants: [quickApplicant._id] } }
            );
            const updatedapplicant = await applicantModel.update(
              { _id: checkApplicantsData._id },
              { $addToSet: { quickApplicants: [quickApplicant._id] } }
            );
          }
        }
        // if job id excist update id's in jobs model
        if (applicantData.jobID) {
          const job = await jobModel.findOne({ _id: applicantData.jobID });
          if (job) {
            const updatedjob = await jobModel.update(
              { _id: job._id },
              { $addToSet: { applicants: [checkApplicantsData._id] } }
            );
          }
        }
        return newApplicantData;
      }
      const newApplicant = await applicantModel.create({ ...newApplicantData });
      let quickApplicant = {};
      if (quickApplicantsObj.profileArray && quickApplicantsObj.profileArray.length > 0) {
        quickApplicant = await quickApplicantModel.create({ ...quickApplicantsObj });
      }
      if (applicantData.jobID) {
        const job = await jobModel.findOne({ _id: applicantData.jobID });
        if (job) {
          const updatedjob = await jobModel.update(
            { _id: job._id },
            { $addToSet: { applicants: [newApplicant._id] } }
          );
          if (quickApplicant) {
            const updatedjob = await jobModel.update(
              { _id: job._id },
              { $addToSet: { quickApplicants: [quickApplicant._id] } }
            );
            const updatedapplicant = await applicantModel.update(
              { _id: newApplicant._id },
              { $addToSet: { quickApplicants: [quickApplicant._id] } }
            );
          }
        }
      }
      return newApplicant;
    }
    return null;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    console.log('error on add applicants', err);
    return { err };
  }
};

// Get Applicants service
const getApplicants = async applicantData => {
  const user = await userModel.findOne({ recruiterID: applicantData.recruiterID });
  try {
    if (user) {
      let start;
      let end;
      let pagination = false;
      if (
        applicantData.limit &&
        (applicantData.offset || applicantData.offset.toString() === '0')
      ) {
        start = applicantData.limit * applicantData.offset;
        end = applicantData.limit * applicantData.offset + applicantData.limit;
        pagination = true;
      }
      if (applicantData.jobID === 'All') {
        let applicants = await applicantModel
          .find({ recruiterID: applicantData.recruiterID })
          .populate(
            'jobID',
            '_id jobArray jobType jobStatus recruiterID jobTitle companyName jobLocation jdType jobLinkedInSearchKeyword created_at updatedAt jobLink'
          );
        // const TotalApplicants = await applicantModel.count({
        //   recruiterID: applicantData.recruiterID,
        // });
        applicants = JSON.parse(JSON.stringify(applicants));
        // delete applicants._id;
        // const applicantsNew = applicants.map(
        //   data =>
        //     data.profileArray.map(jobApplicant => {
        //       jobApplicant.job ? jobApplicant : {...data,data}
        //     })
        //   // data.job ? data : { ...data, jobData }
        // );
        const tempData = [];
        const applicantsNew = applicants.forEach(data => {
          data.profileArray.forEach(tempApplicant => {
            const applicant = {
              ...tempApplicant,
              job: data.jobID,
            };
            // delete applicant.job.applicants;
            if (applicant) {
              // get single array of relevent skills either >0 (must have) or most relevent based on searchstring
              const skillsArray = applicant.job.jobArray.filter(
                skill => skill.tag === 'TECHNICAL_SKILLS'
              );
              const SkillsArrayUnfiltered = skillsArray[0].data.map(skill => {
                if (skill.score > 0) {
                  return skill.name;
                }
              });
              let skillsArrayFiltered = SkillsArrayUnfiltered.filter(skill => skill !== undefined);
              if (skillsArrayFiltered.length === 0) {
                const getReleventSkills = applicant.job.jobLinkedInSearchKeyword.split('%20OR%20');
                let skilledArray = [];
                getReleventSkills.map((skill, index) => {
                  if (index !== 0) {
                    skill = skill.replace('(', '');
                    skill = skill.replace(')', '');
                    const values = skill.split('%20');
                    const lowerCaseVal = values.map(value => value.toLowerCase());
                    skilledArray = [...skilledArray, lowerCaseVal];
                    skillsArrayFiltered = skilledArray[0];
                  }
                });
              }
              // replace skills with string in applicants
              if (applicant.skills && applicant.skills.length > 0) {
                applicant.skills = applicant.skills.map(skill => {
                  if (skillsArrayFiltered.includes(skill.toLowerCase())) {
                    return commonService.generateHighlightBlock(skill);
                  }
                  return skill;
                });
              }
              // replace summary with highlighted skills
              if (applicant.summary) {
                const summaryArray = applicant.summary.split(' ');
                applicant.summary = summaryArray
                  .map(skill => {
                    if (skillsArrayFiltered.includes(skill.toLowerCase())) {
                      return commonService.generateHighlightBlock(skill);
                    }
                    return skill;
                  })
                  .join(' ');
              }

              tempData.push(applicant);
            }
          });
        });

        const sendApplicants = {
          applicants: pagination ? tempData.slice(start, end) : tempData,
          // tempData,
          TotalApplicants: tempData.length,
        };
        return sendApplicants;
      }
      const jobExist = await jobModel.findOne({ _id: applicantData.jobID });
      // console.log(jobExist);
      if (jobExist && jobExist.recruiterID === applicantData.recruiterID) {
        let job = await jobModel.findOne({ _id: applicantData.jobID }).populate('applicants');
        const jobData = await jobModel
          .findOne({ _id: applicantData.jobID })
          .select(
            '_id jobType jobStatus recruiterID jobTitle companyName jobLocation jdType jobLinkedInSearchKeyword created_at updatedAt jobLink'
          );
        job = JSON.parse(JSON.stringify(job));
        const jobArray = JSON.parse(JSON.stringify(job.jobArray));
        const skillsArray = jobArray.filter(skill => skill.tag === 'TECHNICAL_SKILLS');
        const SkillsArrayUnfiltered = skillsArray[0].data.map(skill => {
          if (skill.score > 0) {
            return skill.name;
          }
        });
        let skillsArrayFiltered = SkillsArrayUnfiltered.filter(skill => skill !== undefined);
        if (skillsArrayFiltered.length === 0) {
          const getReleventSkills = job.jobLinkedInSearchKeyword.split('%20OR%20');
          let skilledArray = [];
          getReleventSkills.map((skill, index) => {
            if (index !== 0) {
              skill = skill.replace('(', '');
              skill = skill.replace(')', '');
              const values = skill.split('%20');
              const lowerCaseVal = values.map(value => value.toLowerCase());
              skilledArray = [...skilledArray, lowerCaseVal];
              skillsArrayFiltered = skilledArray[0];
            }
          });

          console.log(getReleventSkills);
        }
        const applicantsLength =
          job.applicants.length > 0 ? job.applicants[0].profileArray.length : 0;
        const applicants =
          job.applicants.length > 0
            ? job.applicants[0].profileArray.map(data => ({ ...data, job: jobData }))
            : [];
        // delete job.applicants;
        const skillHighlightedApplicants = applicants.map(data => {
          const result = JSON.parse(JSON.stringify(data));

          if (result.skills && result.skills.length > 0) {
            result.skills = result.skills.map(skill => {
              if (skillsArrayFiltered.includes(skill.toLowerCase())) {
                return commonService.generateHighlightBlock(skill);
              }
              return skill;
            });
          }

          if (result.summary) {
            const summaryArray = result.summary.split(' ');

            result.summary = summaryArray
              .map(skill => {
                if (skillsArrayFiltered.includes(skill.toLowerCase())) {
                  return commonService.generateHighlightBlock(skill);
                }

                return skill;
              })
              .join(' ');
          }

          return result;
        });
        const sendApplicants = {
          TotalApplicants: applicantsLength,
          applicants: pagination
            ? skillHighlightedApplicants.slice(start, end)
            : skillHighlightedApplicants,
        };
        return sendApplicants;
      }
      return { notFound: true };
    }
    return null;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};

// Get Applicants service
const getApplicant = async applicantData => {
  const user = await userModel.findOne({ recruiterID: applicantData.recruiterID });
  try {
    if (user) {
      let jobExist = await jobModel.findOne({ _id: applicantData.jobID });
      if (jobExist && jobExist.recruiterID === applicantData.recruiterID) {
        let applicants = await applicantModel.findOne({ jobID: applicantData.jobID });
        applicants = JSON.parse(JSON.stringify(applicants));
        const singleProspect = applicants.profileArray.filter(
          prospect => prospect.entityUrn === applicantData.entity
        );
        let sendApplicants = singleProspect[0];
        jobExist = JSON.parse(JSON.stringify(jobExist));
        sendApplicants.job = jobExist;
        sendApplicants = JSON.parse(JSON.stringify(sendApplicants));
        return sendApplicants;
      }
      return { notFound: true };
    }
    return null;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};
module.exports = {
  addApplicant,
  getApplicants,
  getApplicant,
};
