const userModel = require('../../models/user.js');
const jobModel = require('../../models/job');
const applicantModel = require('../../models/applicants');
const commonService = require('../comon.service');

// Get Applicants service for inbox
const getApplicants = async applicantData => {
  const user = await userModel.findOne({ recruiterID: applicantData.recruiterID });
  try {
    if (user) {
      let start;
      let end;
      let pagination = false;
      if (
        applicantData.limit &&
        (applicantData.offset || applicantData.offset.toString() === '0')
      ) {
        start = applicantData.limit * applicantData.offset;
        end = applicantData.limit * applicantData.offset + applicantData.limit;
        pagination = true;
      }
      if (applicantData.jobID === 'All') {
        let applicants = await applicantModel
          .find({ recruiterID: applicantData.recruiterID })
          .populate('jobID', '_id  recruiterID jobLocation  created_at updatedAt');
        // const TotalApplicants = await applicantModel.count({
        //   recruiterID: applicantData.recruiterID,
        // });
        applicants = JSON.parse(JSON.stringify(applicants));
        // delete applicants._id;
        // const applicantsNew = applicants.map(
        //   data =>
        //     data.profileArray.map(jobApplicant => {
        //       jobApplicant.job ? jobApplicant : {...data,data}
        //     })
        //   // data.job ? data : { ...data, jobData }
        // );
        const tempData = [];
        let sortedByScore = [];
        const applicantsNew = applicants.forEach(data => {
          data.profileArray.forEach((tempApplicant, index) => {
            let applicant;
            if (tempApplicant.messageArray && tempApplicant.messageArray.length > 0) {
              applicant = {
                //   ...tempApplicant,
                full_name: tempApplicant.full_name,
                entityUrn: tempApplicant.entityUrn,
                image_url: tempApplicant.image_url,
                score: tempApplicant.score,
                locality: tempApplicant.locality,
                selected: false,
                lastMessage: tempApplicant.messageArray[tempApplicant.messageArray.length - 1],
                // messageArray: index === 0 ? tempApplicant.messageArray : [],
                job: data.jobID,
              };
            }

            // delete applicant.job.applicants;
            if (applicant) {
              // get single array of relevent skills either >0 (must have) or most relevent based on searchstring
              tempData.push(applicant);
            }
          });
        });
        sortedByScore =
          tempData && tempData.length > 0
            ? tempData.sort(
                (a, b) => parseFloat(b.score.final_score) - parseFloat(a.score.final_score)
              )
            : [];
        // const messagesArrayProfiles = sortedByScore.filter(
        //   data => data.messageArray && data.messageArray.length > 0
        // );

        const sendApplicants = {
          applicants: pagination ? sortedByScore.slice(start, end) : sortedByScore,
          // tempData,
          TotalApplicants: sortedByScore.length,
        };
        return sendApplicants;
      }
      const jobExist = await jobModel.findOne({ _id: applicantData.jobID });
      // console.log(jobExist);
      let job;
      let jobData;
      if (jobExist && jobExist.recruiterID === applicantData.recruiterID) {
        job = await jobModel.findOne({ _id: applicantData.jobID }).populate('applicants');
        jobData = await jobModel
          .findOne({ _id: applicantData.jobID })
          .select('_id  recruiterID   jobLocation   created_at updatedAt');
        job = JSON.parse(JSON.stringify(job));
      }

      const applicantsLength =
        job.applicants.length > 0 ? job.applicants[0].profileArray.length : 0;
      const sortedByScore =
        job.applicants[0] && job.applicants[0].profileArray.length > 0
          ? job.applicants[0].profileArray.sort(
              (a, b) => parseFloat(b.score.final_score) - parseFloat(a.score.final_score)
            )
          : [];
      const messagesArrayProfiles = sortedByScore.filter(
        data => data.messageArray && data.messageArray.length > 0
      );
      const applicants =
        messagesArrayProfiles.length > 0
          ? messagesArrayProfiles.map((data, index) => ({
              full_name: data.full_name,
              entityUrn: data.entityUrn,
              image_url: data.image_url,
              score: data.score,
              locality: data.locality,
              lastMessage: data.messageArray[data.messageArray.length - 1],
              job: jobData,
              selected: false,
              messageArray: index === 0 ? data.messageArray : [],
            }))
          : [];

      const sendApplicants = {
        TotalApplicants: applicantsLength,
        applicants: pagination ? applicants.slice(start, end) : applicants,
      };
      return sendApplicants;
    }
    return { notFound: true };
  } catch (err) {
    // return null;
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};
module.exports = {
  getApplicants,
};
