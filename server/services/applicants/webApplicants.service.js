const jobModel = require('../../models/job');
const webApplicantModel = require('../../models/webApplicants');
const userModel = require('../../models/user.js');
const commonService = require('../comon.service');
// save web applicants
const saveApplicant = async applicantData => {
  console.log('save WebApplicant Data INPUT ====>>>>>', JSON.stringify(applicantData));
  const job = await jobModel.findOne({ _id: applicantData.jobID });
  if (job) {
    try {
      const saveData = { ...applicantData };
      saveData.recruiter = JSON.parse(JSON.stringify(job.recruiter));
      saveData.recruiterID = job.recruiterID;
      const webApplicants = await webApplicantModel.create({ ...saveData });
      const updatedjob = await jobModel.update(
        { _id: job._id },
        { $addToSet: { webApplicants: [webApplicants._id] } }
      );
      if (saveData) {
        const reciveWebToken = await commonService.emailWebUserForVerification(
          saveData.emailAddress,
          saveData.name
        );
        const updateCode = await webApplicantModel.findOneAndUpdate(
          { _id: webApplicants._id },
          { $set: { verificationCode: reciveWebToken } },
          { useFindAndModify: false }
        );
        console.log(updateCode);
      }
      const sendData = {
        ...saveData,
        webId: webApplicants._id,
      };
      return { data: sendData, saved: true };
    } catch (err) {
      console.log('error =====>>>>>', JSON.stringify(err));
      console.log('error while addig web applicants', err);
      return { err };
    }
  }
  if (!job) {
    return null;
  }
};

// Get Web Applicants service
const getApplicants = async applicantData => {
  console.log('get WebApplicant Data INPUT ====>>>>>', JSON.stringify(applicantData));
  const user = await userModel.findOne({ recruiterID: applicantData.recruiterID });
  try {
    if (user) {
      if (applicantData.jobID.toUpperCase() === 'ALL') {
        let webApplicants = await webApplicantModel
          .find({ recruiterID: applicantData.recruiterID })
          .populate(
            'jobID',
            '_id jobType jobStatus recruiterID jobTitle companyName jobLocation jdType jobLinkedInSearchKeyword created_at updatedAt jobLink'
          );
        const TotalApplicants = await webApplicantModel.count({
          recruiterID: applicantData.recruiterID,
        });
        webApplicants = JSON.parse(JSON.stringify(webApplicants));
        const tempData = [];
        const applicantsNew = webApplicants.forEach(data => {
          const applicant = {
            ...data,
            job: data.jobID,
          };
          delete applicant.jobID;
          if (applicant) {
            tempData.push(applicant);
          }
        });
        const sendApplicants = {
          applicants: tempData,
          TotalApplicants,
        };
        return sendApplicants;
      }
      const jobExist = await jobModel.findOne({ _id: applicantData.jobID });
      // console.log(jobExist);
      if (jobExist && jobExist.recruiterID === applicantData.recruiterID) {
        let job = await jobModel.findOne({ _id: applicantData.jobID }).populate('webApplicants');
        const jobData = await jobModel
          .findOne({ _id: applicantData.jobID })
          .select(
            '_id jobType jobStatus recruiterID jobTitle companyName jobLocation jdType jobLinkedInSearchKeyword created_at updatedAt jobLink'
          );
        job = JSON.parse(JSON.stringify(job));
        const webApplicantsLength = job.webApplicants.length > 0 ? job.webApplicants.length : 0;
        const applicants =
          job.webApplicants.length > 0
            ? job.webApplicants.map(data => ({ ...data, job: jobData.toObject() }))
            : [];
        // delete job.applicants;
        const sendApplicants = {
          TotalApplicants: webApplicantsLength,
          applicants,
        };
        return sendApplicants;
      }
      return { notFound: true };
    }
    return null;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};

// verify web applicants
const verifyApplicant = async applicantData => {
  console.log('veriy WebApplicant Data INPUT ====>>>>>', JSON.stringify(applicantData));

  const applicant = await webApplicantModel.findOne({ _id: applicantData.webId });
  if (applicant) {
    try {
      if (applicant.verified === true) {
        return { alreadyVerified: true };
      }
      if (applicant.verificationCode === applicantData.verificationCode) {
        const updatedUser = await webApplicantModel.updateOne(
          { _id: applicant._id },
          { $set: { verificationCode: '', verified: true } }
        );
        return { verified: true };
      }
      return { err: 'incorrect Code', verified: false };
    } catch (err) {
      console.log('error =====>>>>>', JSON.stringify(err));
      console.log('error while vrifying web applicants', err);
      return { err };
    }
  }
  if (!applicant) {
    return null;
  }
};

// get single webapplicant
const getApplicant = async applicantData => {
  console.log('get single WebApplicant Data INPUT ====>>>>>', JSON.stringify(applicantData));
  try {
    const applicant = await webApplicantModel.findOne({ _id: applicantData.id });
    if (applicant) {
      const sendData = {
        emailAddress: applicant.emailAddress,
      };
      return { sendData, found: true };
    }
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    console.log('error while getting applicant', err);
    return { err: 'User Not found', found: false };
  }
};

module.exports = {
  saveApplicant,
  getApplicants,
  verifyApplicant,
  getApplicant,
};
