const uuidv1 = require('uuid/v1');

const userModel = require('../../models/user.js');
const jobModel = require('../../models/job');
const applicantModel = require('../../models/applicants');
const fakeUser = require('./fakeProfileObj');

const addProfile = async userData => {
  const user = await userModel.findOne({ recruiterID: userData.recruiterID });
  try {
    if (user) {
      let jobFound = {};
      if (userData.jobID) {
        jobFound = await jobModel.findOne({ _id: userData.jobID });
        if (!jobFound) {
          return { notFound: true };
        }
      }
      const DefaultMessage = `Hi ${userData.firstName},
      ${
        userData.companyName || userData.schoolName
          ? `I see that you ${
              fakeUser.fakeProfile.companyName
                ? `work with ${fakeUser.fakeProfile.companyName} and`
                : ``
            } ${
              fakeUser.fakeProfile.schoolName ? `went to ${fakeUser.fakeProfile.schoolName}, ` : ``
            } and`
          : ``
      }I’m impressed with your work and education. My business is looking for ${
        jobFound.jobTitle
      }, and you appear to be a great fit. Let me know if you’re interested.
      Thanks,
      ${user.name}`;
      const objToPush = {
        dateTime: Date.now(),
        linkedinMessageID: '',
        serverMessageID: '',
        messageText: DefaultMessage,
        type: 'RECEIVED',
      };
      fakeUser.fakeProfile.messageArray = [];
      fakeUser.fakeProfile.firstName = `${userData.firstName}`;
      fakeUser.fakeProfile.lastName = `${userData.lastName}`;
      fakeUser.fakeProfile.full_name = `${userData.firstName} ${userData.lastName}`;
      fakeUser.fakeProfile.publicIdentifier = `${userData.lastName}`;
      fakeUser.fakeProfile.connection_degree = `${userData.connection_degree}`;
      fakeUser.fakeProfile.status = `${userData.status}`;
      fakeUser.fakeProfile.score.final_score = `${userData.final_score}`;
      fakeUser.fakeProfile.entityUrn = uuidv1();
      if (fakeUser.fakeProfile.connection_degree !== '1st') {
        fakeUser.fakeProfile.messageArray.push(objToPush);
      }
      //   const ApplicantsData = await applicantModel.findOne({ jobID: userData.jobID });
      const UpdateProfileData = await applicantModel.updateOne(
        { jobID: userData.jobID },
        { $push: { profileArray: fakeUser.fakeProfile } }
      );
      return { updated: true, id: fakeUser.fakeProfile.entityUrn };
    }
    return null;
  } catch (err) {
    return { err };
  }
};

module.exports = {
  addProfile,
};
