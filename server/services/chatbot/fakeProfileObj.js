const fakeProfile = {
  experience: [
    {
      start: '1-2018',
      title: 'Program Architect',
      end: '',
      industries: [],
      description: 'Designing and creating Text Analytics solutions from customer reviews.\n',
      company_name: 'IBEX | Global Pakistan',
    },
    {
      start: '8-2013',
      title: 'Assistant Professor',
      end: '12-2018',
      industries: [],
      description: 'Research, Teaching',
      company_name: 'Karachi Institute of Economics and Technology',
    },
    {
      start: '8-2015',
      title: 'Postdoctoral Research Fellow',
      end: '8-2016',
      industries: ['Higher Education'],
      description: 'Predictive Safety Assessment for Cyber Physical Systems',
      company_name: 'Singapore University of Technology and Design (SUTD)',
    },
    {
      start: '7-2011',
      title: 'Assistant Professor',
      end: '8-2013',
      industries: [],
      description: 'Teaching, Research, Committies',
      company_name: 'FAST-NUCES',
    },
    {
      start: '1-2011',
      title: 'Visiting Faculty',
      end: '6-2011',
      industries: ['Higher Education'],
      description: 'Teaching',
      company_name: 'University of Karachi',
    },
  ],
  publications: [
    {
      authors: [
        {
          full_name: 'Khurum Nazir Junejo',
        },
      ],
      url: 'https://scholar.google.com.pk/citations?user=gsbePboAAAAJ&hl=en',
      title: 'For my list of publications visit my Google Scholar profile.',
      summary: '',
    },
  ],
  education: [
    {
      schoolName: 'Lahore University of Management Sciences',
      fieldOfStudy: 'Machine Learning and Data Mining',
      degreeName: 'Doctor of Philosophy (PhD)',
    },
    {
      schoolName: 'LUMS',
      fieldOfStudy:
        'Artificial Intelligence, Information Engineering, Theoretical Computer Science,',
      degreeName: 'Master of Science (MS)',
    },
    {
      schoolName: 'Karachi University',
      fieldOfStudy: 'Computer Science',
      degreeName: 'BS',
    },
  ],
  skills: ['Machine Learning', 'Data Mining', 'Text Mining', 'Text Classification'],
  publicIdentifier: '',
  industryCode: '97',
  picture: '',
  scrapeType: 'DEEP',
  trackingId: 'vjWCC2quQ2eKgESq8397tQ==',
  locationName: 'Pakistan',
  postalCode: '75500',
  versionTag: '2958457866',
  schoolName: 'Lahore University of Management Sciences',
  fieldOfStudy: 'Machine Learning and Data Mining',
  title: 'Program Architect',
  companyName: 'IBEX | Global Pakistan',
  languages: 'English,Urdu',
  firstName: '',
  lastName: '',
  full_name: '',
  entityUrn: '',
  headline: 'Data Scientist / Machine Learning Engineer',
  summary:
    'Assistant Professor with specialty in Data Mining and Machine Learning. Areas of interest, Cyber Physical Systems Security, Text Classification, Text Mining, Sentiment Analysis, Natural Language Processing.',
  industry: 'Market Research',
  image_url: '',
  locality: 'Pakistan',
  country_code: 'pk',
  connection_degree: '',
  profileUrl: '',
  also_viewed: [],
  createdAt: 1568013210620.0,
  updatedAt: 1568013210620.0,
  score: {
    education_profile: ['The prospect went to Karachi University'],
    education_required: ['tmasters maths with an importance score of 3/5'],
    education_score: 0.18,
    education_total: 12,
    experience_found: [
      'The prospect has previously worked as a visiting faculty',
      'The prospect has previously worked at University of Karachi',
    ],
    experience_required: ['t2 years of research experience with an importance score of 3/5'],
    experience_score: 2.49172,
    experience_total: 12,
    final_score: 0,
    industries_profile: [],
    industries_required: ['Computer Science with an importance score of 1/5'],
    industry_score: 0,
    industry_total: 4,
    skill_score: 9.7098,
    skill_total: 20,
    skills_profile: ['machine learning', 'data mining'],
    skills_required: [
      'machine learning with an importance score of 5/5',
      'data science with an importance score of 5/5',
      'data mining with an importance score of 3/5',
      'design and evaluate algorithms tprovide technical guidance t100000 to 120000 dollars per annum t t t t t t t t t t t t t t t t t t t t with an importance score of 3/5',
      'python with an importance score of 1/5',
    ],
  },
  status: '',
  requestTime: 0,
  connectionDate: 0,
  batchMessagesCompleted: false,
  hoursPassed: 0,
  intent: '',
  intent_info:
    'This shows the current intent of an applicant. They might be interested in applying for the position posted, interested in learning more or not interested.',
  message_info:
    'This shows the template that the system is about to send to a user. This is based on scoring, intent and other attributes the system detects.',
  readMessages: 1,
  sendMessages: 1,
  messageVariantsArray: [],
  messageArray: [],
};
module.exports = {
  fakeProfile,
};
