const userModel = require('../models/user.js');
const messageToSend = require('./batchMessagingObj');
const defaultMessage = require('./batchMessaging_keys_obj');
const jobModel = require('../models/job');

const getTemplate = (originalTemplates, key) => {
  const messageToReplace = originalTemplates.filter(keyObjects => keyObjects.keys.includes(key));
  return messageToReplace[0].message;
};
const setTemplatesKeyObj = (originalTemplates, newTemplateObj) => {
  originalTemplates.map(template => {
    if (template.keys.includes(newTemplateObj.newtemplateKey)) {
      template.message = newTemplateObj.message;
    }
  });
  return originalTemplates;
};

const sendAbleTemplates = (orignalTemplates, FrontendTemplate, type) => {
  FrontendTemplate.map(personas => {
    personas.interestedThread.map(template => {
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'NPIHS') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'I-HS-NC');
            template.message = newTemplate;
            template.newtemplateKey = 'I-HS-NC';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'OPIHS') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'I-HS-OC');
            template.message = newTemplate;
            template.newtemplateKey = 'I-HS-OC';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'NPILS') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'I-LS-NC');
            template.message = newTemplate;
            template.newtemplateKey = 'I-LS-NC';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'OPILS') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'I-LS-OC');
            template.message = newTemplate;
            template.newtemplateKey = 'I-LS-OC';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'OPHS') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'OCHS');
            template.message = newTemplate;
            template.newtemplateKey = 'OCHS';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'OPLS') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'OCLS');
            template.message = newTemplate;
            template.newtemplateKey = 'OCLS';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      // if (template.type === 'dnnae') {
      //   if (template.oldKeyMap === 'NPIHS') {
      //     if (type === 'frontEnd') {
      //       const newTemplate = getTemplate(orignalTemplates, 'I-HS-NC');
      //       template.message = newTemplate;
      //       template.newtemplateKey = 'I-HS-NC';
      //     }
      //     if (type === 'backEnd') {
      //       const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
      //       return newTemplate;
      //     }
      //   }
      // }
    });

    // not interested Thread Mapping

    personas.notInterestedThread.map(template => {
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'NPHSNI') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'NI-HS-NC');
            template.message = newTemplate;
            template.newtemplateKey = 'NI-HS-NC';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'NPLSNI') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'NI-LS-NC');
            template.message = newTemplate;
            template.newtemplateKey = 'NI-LS-NC';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'OPHSNI') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'NI-HS-OC');
            template.message = newTemplate;
            template.newtemplateKey = 'NI-HS-OC';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'OPLSNI') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'NI-LS-OC');
            template.message = newTemplate;
            template.newtemplateKey = 'NI-LS-OC';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'OPHS') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'OCHS');
            template.message = newTemplate;
            template.newtemplateKey = 'OCHS';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'OPLS') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'OCLS');
            template.message = newTemplate;
            template.newtemplateKey = 'OCLS';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
    });
    // 24 hrs
    personas.NoReplyThread24.map(template => {
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'NPHSNM24') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'NCHSNM24');
            template.message = newTemplate;
            template.newtemplateKey = 'NCHSNM24';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'NPLSNM24') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'NCLSNM24');
            template.message = newTemplate;
            template.newtemplateKey = 'NCLSNM24';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'OPHSNM24') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'OCHSNM24');
            template.message = newTemplate;
            template.newtemplateKey = 'OCHSNM24';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'OPLSNM24') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'OCLSNM24');
            template.message = newTemplate;
            template.newtemplateKey = 'OCLSNM24';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
    });

    // 48 hrs
    personas.NoReplyThread48.map(template => {
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'NPHSNM48') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'NCHSNM48');
            template.message = newTemplate;
            template.newtemplateKey = 'NCHSNM48';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'NPLSNM48') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'NCLSNM48');
            template.message = newTemplate;
            template.newtemplateKey = 'NCLSNM48';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'OPHSNM48') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'OCHSNM48');
            template.message = newTemplate;
            template.newtemplateKey = 'OCHSNM48';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'OPLSNM48') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'OCLSNM48');
            template.message = newTemplate;
            template.newtemplateKey = 'OCLSNM48';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
    });
    // 7days
    personas.NoReplyThread7.map(template => {
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'NPHSNM7d') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'NCHSNM7d');
            template.message = newTemplate;
            template.newtemplateKey = 'NCHSNM7d';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'NPLSNM7d') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'NCLSNM7d');
            template.message = newTemplate;
            template.newtemplateKey = 'NCLSNM7d';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'OPHSNM7d') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'OCHSNM7d');
            template.message = newTemplate;
            template.newtemplateKey = 'OCHSNM7d';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
      if (template.type === 'dnnae') {
        if (template.oldKeyMap === 'OPLSNM7d') {
          if (type === 'frontEnd') {
            const newTemplate = getTemplate(orignalTemplates, 'OCLSNM7d');
            template.message = newTemplate;
            template.newtemplateKey = 'OCLSNM7d';
          }
          if (type === 'backEnd') {
            const newTemplate = setTemplatesKeyObj(orignalTemplates, template);
          }
        }
      }
    });
  });
  if (type === 'frontEnd') {
    return FrontendTemplate;
  }
  if (type === 'backEnd') {
    return orignalTemplates;
  }
};

// const saveableTemplates = (orignalTemplates, FrontendTemplate) => {
//   FrontendTemplate.map(personas => {
//     personas.interestedThread.map(template => {});
//   });
// };
const getSettings = async userData => {
  let defaultMessageDeepCopy = [];
  defaultMessageDeepCopy = JSON.parse(JSON.stringify(defaultMessage));
  let messageToSendDeepCopy = [];
  messageToSendDeepCopy = JSON.parse(JSON.stringify(messageToSend));
  console.log('get batchMessage Settings User INPUT ====>>>>>', JSON.stringify(userData));
  const user = await userModel.findOne({ recruiterID: userData.recruiterID });
  let getJob;
  try {
    if (userData.jobID.toLowerCase() !== 'all') {
      getJob = await jobModel.findOne({ _id: userData.jobID });
    }
    if (user) {
      let sampleMessageTemplate = [];
      if (
        userData.jobID.toLowerCase() !== 'all' &&
        (getJob && getJob.messagingTemplates && getJob.messagingTemplates.length > 0)
      ) {
        sampleMessageTemplate = getJob.messagingTemplates;
      } else if (user.messageTemplatesArray.length > 0) {
        sampleMessageTemplate = user.messageTemplatesArray;
      } else {
        sampleMessageTemplate = defaultMessageDeepCopy.messagingTemplates;
      }
      // if (userData.bot === true) {
      //   sampleMessageTemplate = messageToSend.messagingTemplates;
      // }
      const sampleMessage = sendAbleTemplates(
        sampleMessageTemplate,
        messageToSendDeepCopy.messagingTemplates,
        'frontEnd'
      );
      //   sampleMessage = sampleMessage.replace(/ +/g, ' ');
      return {
        sampleMessage,
      };
    }
    return null;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};

// set settings message coming from api for the user

const setSettings = async userData => {
  const user = await userModel.findOne({ recruiterID: userData.recruiterID });
  let defaultMessageDeepCopy = [];
  defaultMessageDeepCopy = JSON.parse(JSON.stringify(defaultMessage));
  // let messageToSendDeepCopy = [];
  // messageToSendDeepCopy = JSON.parse(JSON.stringify(messageToSend));
  try {
    if (user) {
      const sampleMessage = sendAbleTemplates(
        defaultMessageDeepCopy.messagingTemplates,
        userData.sampleMessage,
        'backEnd'
      );
      if (userData.jobID.toLowerCase() === 'all') {
        const updatedUser = await userModel.updateOne(
          { _id: user._id },
          { messageTemplatesArray: sampleMessage }
        );
      } else {
        const updatedUser = await jobModel.updateOne(
          { _id: userData.jobID },
          { messageTemplatesArray: sampleMessage }
        );
      }
      return { updated: true };
    }
    return null;
  } catch (err) {
    return { err };
  }
};

module.exports = {
  getSettings,
  setSettings,
};
