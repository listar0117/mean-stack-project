const messagingTemplates = [
  {
    keys: ['I-HS-NC-EM-DT', 'I-HS-OC-EM-DT'],
    message:
      "Hey  {First_Name}, hope you're doing well. I just wanted to follow-up on our conversation on LinkedIn. As you know, my company is looking to hire someone for the role of {Job_Title} and you seem like a great fit. I was hoping to talk to you about this opportunity. For our call meeting,I understand that you are available around {Date_Time}. Here is a calendly link {Link_to_calendar} which you can use to book a slot which suits your schedule. Regards, {User_Name}",
  },
  {
    keys: ['I-HS-NC-EM', 'I-HS-OC-EM'],
    message:
      "Hey  {First_Name}, hope you're doing well. I just wanted to follow-up on our conversation on LinkedIn. As you know, my company is looking to hire someone for the role of {Job_Title} and you seem like a great fit. I was hoping to talk to you about this opportunity. If you book a time using the link below, it will book your selected slot on both our calendars with the call-in details. {Link_to_calendar}. Regards, {User_Name}",
  },
  {
    keys: ['I-LS-NC-EM', 'I-LS-OC-EM', 'I-LS-NC-EM-DT', 'I-LS-OC-EM-DT'],
    message:
      "Hey  {First_Name}, hope you're doing well. I just wanted to follow-up on our conversation on LinkedIn. As you know, my company is looking to hire someone for the role of {Job_Title}. The job details can be found at  {Job_Link}. I was hoping that you could use this link to apply with your resume. I look forward to reviewing it and discussing with our team. Regards, {User_Name}",
  },
  {
    keys: [
      'IA-LS-NC-EM',
      'IA-LS-OC-EM',
      'A-LS-NC-EM',
      'A-LS-OC-EM',
      'IA-LS-NC-EM-DT',
      'IA-LS-OC-EM-DT',
      'A-LS-NC-EM-DT',
      'A-LS-OC-EM-DT',
    ],
    message:
      "Hey  {First_Name}, hope you're doing well. I just wanted to follow-up on our conversation on LinkedIn. As you know, my company is looking to hire someone for the role of {Job_Title}. You can visit the job description link to find out more: {Job_Link}. If you're still interested, kindly apply at the given link with your resume. I look forward to reviewing it and discussing with our team. Regards, {User_Name}",
  },
  {
    keys: [
      'I-HS-NC-EM',
      'NI-LS-NC-EM',
      'NI-HS-OC-EM',
      'NI-LS-OC-EM',
      'NI-HS-NC-EM-DT',
      'NI-LS-NC-EM-DT',
      'NI-HS-OC-EM-DT',
      'NI-LS-OC-EM-DT',
    ],
    message:
      "Hey  {First_Name}, hope you're doing well. I just wanted to follow-up on our conversation on LinkedIn. Completely understand that this role might not be a fit right now. Shall I check back in a few months? Do you know anyone in your network that might be interested in this role? Here is the link again: {Job_Link} Regards, {User_Name}",
  },
  {
    keys: ['IA-HS-NC-EM', 'IA-HS-OC-EM', 'A-HS-NC-EM', 'A-HS-OC-EM'],
    message:
      "Hey  {First_Name}, hope you're doing well. I just wanted to follow-up on our conversation on LinkedIn. I look forward to speaking with you and answering your questions. In the meanwhile, please check out the job details here {Job_Link} and it should answer some of your questions. For our call meeting, if you book a time using the link below, it will book your selected slot on both our calendars with the call-in details. {Link_to_calendar}. Regards, {User_Name}",
  },
  {
    keys: ['IA-HS-NC-EM-DT', 'IA-HS-OC-EM-DT', 'A-HS-NC-EM-DT'],
    message:
      "Hey  {First_Name}, hope you're doing well. I just wanted to follow-up on our conversation on LinkedIn. I look forward to speaking with you and answering your questions. In the meanwhile, please check out the job details here {Job_Link} and it should answer some of your questions. For our call meeting,I understand that you are available around {Date_Time}. Here is a calendly link {Link_to_calendar} which you can use to book a slot which suits your schedule. Regards, {User_Name}",
  },
  {
    keys: ['A-LS-OC-EM-DT'],
    message:
      "Hey  {First_Name}, hope you're doing well. I just wanted to follow-up on our conversation on LinkedIn. I look forward to speaking with you and answering your questions. In the meanwhile, please check out the job details here {Job_Link} and it should answer some of your questions. For our call meeting,I understand that you are available around {Date_Time}. Here is a calendly link {Link_to_calendar} which you can use to book a slot which suits your schedule.. Regards, {User_Name}",
  },
  {
    keys: ['I-LS-NC-DT', 'I-LS-OC-DT'],
    message:
      'Hey  {First_Name}, thanks for responding. As you know, my company is looking to hire someone for the role of {Job_Title}. The job details can be found at  {Job_Link}. I was hoping that you could use this link to apply with your resume. I look forward to reviewing it and discussing with our team. Regards, {User_Name}',
  },
  {
    keys: ['NI-HS-NC-DT', 'NI-HS-NC'],
    message:
      'Hey  {First_Name}, thanks for responding. Completely understand that this role might not be a fit right now. Shall I check back in a few months? Do you know anyone in your network that might be interested in this role? Here is the link again: {Job_Link} Regards, {User_Name}',
  },
  {
    keys: ['NI-LS-NC-DT', 'NI-LS-NC'],
    message:
      'Hey  {First_Name}, thanks for responding. Completely understand that this role might not be a fit right now. Shall I check back in a few months? Do you know anyone in your network that might be interested in this role? Here is the link again: {Job_Link} Regards, {User_Name}',
  },
  {
    keys: ['NI-HS-OC-DT', 'NI-HS-OC'],
    message:
      'Hey  {First_Name}, thanks for responding. Completely understand that this role might not be a fit right now. Shall I check back in a few months? Do you know anyone in your network that might be interested in this role? Here is the link again: {Job_Link} Regards, {User_Name}',
  },
  {
    keys: ['NI-LS-OC-DT', 'NI-LS-OC'],
    message:
      'Hey  {First_Name}, thanks for responding. Completely understand that this role might not be a fit right now. Shall I check back in a few months? Do you know anyone in your network that might be interested in this role? Here is the link again: {Job_Link} Regards, {User_Name}',
  },
  {
    keys: ['IA-HS-NC-DT', 'IA-HS-OC-DT', 'A-HS-NC-DT', 'A-HS-OC-DT'],
    message:
      'Hey  {First_Name}, thanks for responding. I look forward to speaking with you and answering your questions. In the meanwhile, please check out the job details here {Job_Link} and it should answer some of your questions.  For our call meeting,I understand that you are available around {Date_Time}. Here is a calendly link {Link_to_calendar} which you can use to book a slot which suits your schedule. Regards, {User_Name}',
  },
  {
    keys: ['I-HS-OC'],
    message:
      'Thanks for responding, {First_Name}. I look forward to speaking with you and discussing more about the role. Will any of these time slots work for you? If you book a time using the link below, it will book your selected slot on both our calendars with the call-in details. Speak soon. {Link_to_calendar}',
  },
  {
    keys: ['I-LS-OC'],
    message:
      'Thanks for responding, {First_Name}. If you could kindly apply at this link with your resume? {Job_Link}. I look forward to reviewing it and discussing with the team. Speak soon',
  },
  {
    keys: ['IA-HS-NC', 'IA-HS-OC', 'A-HS-NC', 'A-HS-OC'],
    message:
      'Thanks {First_Name}, I look forward to speaking with you and answering your questions. In the meanwhile, please check out the job details here {Job_Link} and it should answer some of your questions. As for our call meeting, If you book a time using the link below, it will book your selected slot on both our calendars with the call-in details. Speak soon. {Link_to_calendar}',
  },
  {
    keys: ['I-HS-NC-DT', 'I-HS-OC-DT'],
    message:
      'Thanks {First_Name}, I look forward to speaking with you. I understand that you are available around {Date_Time}. Will any of these time slots work for you? If you book a time using the link below, it will book your selected slot on both our calendars with the call-in details. Speak soon. {Link_to_calendar}',
  },
  {
    keys: ['I-HS-NC'],
    message:
      'Thanks {First_Name}, I look forward to speaking with you. Will any of these time slots work for you? If you book a time using the link below, it will book your selected slot on both our calendars with the call-in details. Speak soon. {Link_to_calendar}',
  },
  {
    keys: ['I-LS-NC'],
    message:
      'Thanks {First_Name}. If you could kindly apply at this link with your resume? {Job_Link}. I look forward to reviewing it and discussing with the team. Speak soon',
  },
  {
    keys: [
      'IA-LS-NC',
      'IA-LS-OC',
      'A-LS-NC',
      'A-LS-OC',
      'IA-LS-NC-DT',
      'IA-LS-OC-DT',
      'A-LS-NC-DT',
      'A-LS-OC-DT',
    ],
    message:
      "Thanks {First_Name}. You can visit the job description link to find out more: {Job_Link}. If you're still interested, kindly apply at the given link with your resume. I look forward to reviewing it and discussing with the team. Speak soon",
  },
  {
    keys: ['OCLS'],
    message:
      'Hi {First_Name}, Hope things are well. I wanted to quickly message you to let you know of a new role I am searching for. If you or anyone in your network seems like a good fit, please let me know. Here are the details: {Job_Link}. Talk to you soon {First_Name}',
  },
  {
    keys: ['OCHS'],
    message:
      'Hi {First_Name}, Hope you are doing well. I am recruiting for this new {Job_Title} role and it seems like you are a really good fit. Please let me know if you have some time to speak about this? Here is the link: {Job_Link}',
  },
  {
    keys: ['NCHSNM7d'],
    message:
      "Hi {First_Name}, I promise I haven't just connected with you to increase my connections on LinkedIn. As you know, I am looking to recruit for this new {Job_Title} role and I believe you are a great fit. Do you have some time to speak this coming week or next?",
  },
  {
    keys: ['NCHSNM24'],
    message:
      'Thanks for connecting with me {First_Name}. I am looking to recruit for this new {Job_Title} role and I believe you are a great fit. Do you have some time to speak this coming week or next?',
  },
  {
    keys: ['NCHSNM48'],
    message:
      'Hi {First_Name}, I hope this note finds you well. As you know, I am looking to recruit for this new {Job_Title} role and I believe you are a great fit. Do you have some time to speak this coming week or next?',
  },
  {
    keys: ['NCLSNM7d'],
    message:
      "Hi {First_Name}, I promise I haven't just connected with you increase my connections on LinkedIn. As you know,  I am searching for a new role. If you or anyone in your network seems like a good fit, please let me know. Here are the details: {Job_Link}. Thanks.",
  },
  {
    keys: ['NCLSNM24'],
    message:
      'Thanks for connecting with me {First_Name}. I wanted to quickly message you to let you know of a new role I am searching for. If you or anyone in your network seems like a good fit, please let me know. Here are the details: {Job_Link}. Talk to you soon {First_Name}',
  },
  {
    keys: ['NCLSNM48'],
    message:
      'Hi {First_Name}, I hOCe this note finds you well. As you know, I am searching for a new role. If you or anyone in your network seems like a good fit, please let me know. Here are the details: {Job_Link}. Talk to you soon {First_Name}',
  },
  {
    keys: ['OCHSNM7d'],
    message:
      "Hi {First_Name}, I promise I haven't just connected with you to increase my connections on LinkedIn. As you know, I am looking to recruit for this new {Job_Title} role and I believe you are a great fit. Do you have some time to speak this coming week or next?",
  },
  {
    keys: ['OCHSNM24'],
    message:
      'Thanks for connecting with me {First_Name}. I am looking to recruit for this new {Job_Title} role and I believe you are a great fit. Do you have some time to speak this coming week or next?',
  },
  {
    keys: ['OCHSNM48'],
    message:
      'Hi {First_Name}, I hope this note finds you well. As you know, I am looking to recruit for this new {Job_Title} role and I believe you are a great fit. Do you have some time to speak this coming week or next?',
  },
  {
    keys: ['OCLSNM7d'],
    message:
      "Hi {First_Name}, I promise I haven't just connected with you increase my connections on LinkedIn. As you know,  I am searching for a new role. If you or anyone in your network seems like a good fit, please let me know. Here are the details: {Job_Link}. Thanks.",
  },
  {
    keys: ['OCLSNM24'],
    message:
      'Thanks for connecting with me {First_Name}. I wanted to quickly message you to let you know of a new role I am searching for. If you or anyone in your network seems like a good fit, please let me know. Here are the details: {Job_Link}. Talk to you soon {First_Name}',
  },
  {
    keys: ['OCLSNM48'],
    message:
      'Hi {First_Name}, I hOCe this note finds you well. As you know, I am searching for a new role. If you or anyone in your network seems like a good fit, please let me know. Here are the details: {Job_Link}. Talk to you soon {First_Name}',
  },
  {
    keys: ['ELM'],
    message: 'Email sent',
  },
  {
    keys: ['subject'],
    message: 'Follow up on LinkedIn conversation about {Job_Title} role',
  },
];
module.exports = {
  messagingTemplates,
};
