const request = require('request-promise-native');
const userModel = require('../models/user.js');
const campaignModel = require('../models/campaign');
const commonService = require('./comon.service');
const linkedinString = require('./linkedinKeywordString.service');

require('dotenv').config();

const createCampaign = async CampaignData => {
  console.log('Create Campaign User INPUT ====>>>>>', JSON.stringify(CampaignData));
  const user = await userModel.findOne({ recruiterID: CampaignData.founderID });
  try {
    if (user) {
      const newCampaignData = {
        ...CampaignData,
        // jobLink: `${commonService.mycurrentURL}/${UserData.name}${newRecId}`,
      };
      delete newCampaignData.founderID;
      // if (
      //   (newCampaignData && !newCampaignData.campaignLinkedInSearchKeyword) ||
      //   (newCampaignData && !newCampaignData.campaignLinkedInSearchKeyword.length.trim() <= 0)
      // ) {
      //   // old logic
      //   // const getSearchData = newJobData.jobArray.filter(
      //   //   jobArray => jobArray.tag === 'TECHNICAL_SKILLS'
      //   // );

      //   // new logic
      //   const IndustrySpecialties = campaignLinkedInSearchKeyword.jobArray.filter(
      //     jobArray => jobArray.tag === 'COMPANY_INDUSTRY_SPECIALTIES'
      //   );
      //   const JobTitle = newJobData.jobArray.filter(jobArray => jobArray.tag === 'JOB_POSITION');
      //   // console.log(JobTitle[0].data[0]);
      //   console.log('IndustrySpecialties', IndustrySpecialties);
      //   // const addedScore = getSearchData[0].data.map(data =>
      //   //   data.score || data.score === 0 ? data : { ...data, score: 0 }
      //   // );
      //   // new logic
      //   const addedScore = IndustrySpecialties[0].data.map(data =>
      //     data.score || data.score === 0 ? data : { ...data, score: 1 }
      //   );
      //   let specialtiesString = '';
      //   addedScore.map((data, index) => {
      //     if (specialtiesString.length === 0) {
      //       specialtiesString = data.name;
      //     } else if (specialtiesString.length > 0) {
      //       specialtiesString = `${specialtiesString} AND ${data.name}`;
      //     }
      //   });
      //   const sortedByScore = addedScore.sort((a, b) => parseFloat(b.score) - parseFloat(a.score));
      //   const searchStringData = sortedByScore.map((data, index) => {
      //     return data.name;
      //   });
      //   const flattenedSearchStringData = [].concat(...searchStringData);
      //   let searchString = '';
      //   for (let i = 0; i <= flattenedSearchStringData.length; i++) {
      //     if (flattenedSearchStringData[i]) {
      //       searchString = `${searchString} ${flattenedSearchStringData[i]}`;
      //     }
      //   }
      //   // const uriString = encodeURI(searchString);
      //   // console.log('qqqqqqqqqqqqqqqqqqqqqqqqq', uriString);

      //   // const uriString1 = await linkedinString.keywordsExtractor(addedScore, JobTitle);
      //   const uriString1 = `${JobTitle[0].data[0]} AND ${specialtiesString}`;
      //   console.log('qqqqqqqq22222222222', uriString1);
      //   newJobData.jobLinkedInSearchKeyword = uriString1;
      // }

      const  addTitle =  CampaignData.campaignTitle.map( title => '(' + title + ')');
      newCampaignData.campaignLinkedInSearchKeyword = addTitle.join(' OR ');
      newCampaignData.founder = user._id;
      let newCampaign;
      let genratedJobLink;
      newCampaignData.founderID = user.recruiterID;
      // check wether to add job or update existing job
      if (CampaignData && CampaignData.campaignID && CampaignData.campaignID !== 'add') {
        try {
          newCampaign = await campaignModel.findOne({ _id: CampaignData.campaignID });
          if (newCampaign) {
            // var replaced = str.split(' ').join('+');
            // genratedJobLink = `${commonService.mycurrentURL}/company/${CampaignData.companyName
            //   .split(' ')
            //   .join('-')}/job/${JobData.jobID}/${JobData.jobTitle.split(' ').join('-')}`;

            genratedJobLink = '';
            const updateData = await campaignModel.findOneAndUpdate(
              { _id: CampaignData.campaignID },
              {
                $set: {
                  campaignTitle: CampaignData.campaignTitle,
                  companyName: CampaignData.companyName,
                  campaignLocation: CampaignData.campaignLocation,
                  investorType: CampaignData.investorType,
                  industries: CampaignData.industries,
                  invest_stage: CampaignData.invest_stage,
                  jobLink: genratedJobLink,
                  campaignLinkedInSearchKeyword: newCampaignData.campaignLinkedInSearchKeyword
                },
              }
            );
          }
          console.log(CampaignData.campaignID, 'Campaign Updated successfully');
        } catch (err) {
          console.log('error =====>>>>>', JSON.stringify(err));
          return { noCampaign: true };
        }
      } else {
        newCampaign = await campaignModel.create({ ...newCampaignData });
        console.log('nwwwwww', newCampaign);
        // const user = await UserModel.findOne({ _id: checkToken });
        const updatedUser = await userModel.updateOne(
          { _id: user._id },
          { $addToSet: { campaigns: [newCampaign._id] } }
        );
        // genratedJobLink = `${commonService.mycurrentURL}/job/${newJob._id}`;
        // genratedJobLink = `${commonService.mycurrentURL}/company/${newJob.companyName
        //   .split(' ')
        //   .join('-')}/job/${newJob._id}/${newJob.jobTitle.split(' ').join('-')}`;
        // const updateJob = await jobModel.updateOne(
        //   { _id: newJob._id },
        //   { $set: { jobLink: genratedJobLink } }
        // );
        genratedJobLink = '';
        console.log('new campaign created');
      }
      const returnCampaignData = {
        campaignID: newCampaign._id,
        jobLink: genratedJobLink,
      };
      return returnCampaignData;
    }
    return null;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};

module.exports = {
  createCampaign,
};
