const { Storage } = require('@google-cloud/storage');
const fs = require('fs');
require('dotenv').config();

const CLOUD_BUCKET = process.env.bucketName;
function getPublicUrl(filename) {
  // eslint-disable-next-line prettier/prettier
  return `https://storage.googleapis.com/${CLOUD_BUCKET}/${filename}`;
}

const storage = new Storage({
  projectId: process.env.Gcloud_Key,
  keyFilename: './keyFile.json',
});

// const uploadImage = async (image, next) => {
//   const storage = new Storage({
//     projectId: process.env.Gcloud_Key,
//     keyFilename: '/keyFile.json',
//   });
//   // let storage = Storage.storage({
//   //     projectId: process.env.Gcloud_Key,
//   //     // keyFilename: 'auth.json'
//   //   });

//   //   const date = Date.now().toString();
//   //   const gcsname = `${date}${image[0].filename}`;
//   const gcsname = image[0].newFileName;
//   const bucket = await storage.bucket(CLOUD_BUCKET);
//   const localReadStream = fs.createReadStream(image[0].path);
//   const remoteWriteStream = await bucket.file(`signUpImages/${gcsname}`).createWriteStream();
//   const newUrl = localReadStream.pipe(remoteWriteStream);

//   const error = await newUrl.on('error', async err => {
//     console.log(err);
//     //   return err;
//     return error;
//   });
//   const done = await newUrl.on('finish', async () => {
//     // The file upload is complete.
//     const url = getPublicUrl(`signUpImages/${gcsname}`);
//     fs.unlink(image[0].path, err => {
//       if (err) throw err;
//       // if no error, file has been deleted successfully
//       console.log('File deleted from temp!');
//     });
//     // await next();
//     return url;
//     //   callback(publicUrl);
//   });
//   return done;
// };

const uploadImage = (image, next) =>
  new Promise(async (resolve, reject) => {
    const gcsname = image[0].newFileName;
    const bucket = await storage.bucket(CLOUD_BUCKET);
    const localReadStream = fs.createReadStream(image[0].path);
    const remoteWriteStream = await bucket.file(`signUpImages/${gcsname}`).createWriteStream();
    const newUrl = localReadStream.pipe(remoteWriteStream);

    const error = await newUrl.on('error', async err => {
      console.log(err);
      reject(err);
    });
    const done = await newUrl.on('finish', async () => {
      // The file upload is complete.
      const url = getPublicUrl(`signUpImages/${gcsname}`);
      fs.unlink(image[0].path, err => {
        if (err) throw err;
        reject(err);
        // if no error, file has been deleted successfully
        console.log('File deleted from temp!');
      });
      resolve(url);
    });
  });

const uploadResume = (file, next) =>
  new Promise(async (resolve, reject) => {
    console.log(file);

    // const storage = new Storage({
    //   projectId: process.env.Gcloud_Key,
    //   keyFilename: './keyFile.json',
    // });
    const date = Date.now().toString();
    const gcsname = `${date}${file[0].filename}`;
    const bucket = await storage.bucket(CLOUD_BUCKET);
    const localReadStream = fs.createReadStream(file[0].path);
    const remoteWriteStream = await bucket.file(`cvUploads/${gcsname}`).createWriteStream();
    const newUrl = localReadStream.pipe(remoteWriteStream);
    const error = newUrl.on('error', err => {
      console.log(err);
      reject(err);
    });

    const done = await newUrl.on('finish', async () => {
      // The file upload is complete.
      const url = getPublicUrl(`cvUploads/${gcsname}`);
      fs.unlink(file[0].path, err => {
        if (err) throw err;
        reject(err);
        // if no error, file has been deleted successfully
        console.log('File deleted from temp!');
      });
      resolve(url);
    });
  });

module.exports = {
  uploadImage,
  getPublicUrl,
  uploadResume,
};
