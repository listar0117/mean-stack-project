const jwt = require('jsonwebtoken');
require('dotenv').config();

const secret = process.env.JWT_SECRET;
module.exports = async (ctx, next) => {
  if (!ctx.headers.authorization) {
    // ctx.throw(403, 'No jwt token.');
    ctx.status = 403;
    ctx.body = {
      isSuccess: false,
      message: 'Authentication failed',
    };
    return;
  }
  // const decodedToken = jwt.verify(ctx.headers.authorization, secret, (err, decoded) => {
  //   console.log(err);
  //   console.log(decoded);
  //   if (err) {
  //     ctx.status = 404;
  //     ctx.body = {
  //       isSuccess: false,
  //       message: err.message,
  //     };
  //   } else {
  //     ctx.status = 200;
  //     ctx.body = {
  //       isSuccess: true,
  //     };
  //   }
  // });
  try {
    const decodedToken = await jwt.verify(ctx.headers.authorization, secret);
    // console.log('decodedToken: ', decodedToken);
    await next();
  } catch (err) {
    console.log('in catch');
    // next(err);
    ctx.status = 404;
    ctx.body = {
      isSuccess: false,
      message: err.message,
    };
  }
};
