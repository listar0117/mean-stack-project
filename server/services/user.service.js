const UserModel = require('../models/user.js');
const counterModel = require('../models/counters');
const commonService = require('./comon.service');
require('dotenv').config();
// this is a test route will be deleted
const getAllUsers = async function getAllUsers() {
  console.log('000000');
  const users = await UserModel.find();
  console.log('here-------', users);
  // const users =  {users:'users'};
  return next();
  // return users;
};

const checkIfExist = async (emailParam, next) => {
  const user = await UserModel.findOne({ emailAddress: emailParam });
  // JSON.stringify(user)
  return user;
};
const getNextSequenceValue = async (sequenceName, val) => {
  let sequenceDocument;
  if (!val) {
    sequenceDocument = await counterModel.findOneAndUpdate(
      {
        _id: sequenceName,
      },
      {
        $inc: {
          sequence_value: 1,
        },
      },
      { new: true }
    );
  } else {
    sequenceDocument = await counterModel.create({
      _id: sequenceName,
      sequence_value: 2,
    });
  }

  return sequenceDocument.sequence_value;
};

const createUser = async UserData => {
  console.log('Create User INPUT ====>>>>>', JSON.stringify(UserData));
  const user = await UserModel.findOne({ emailAddress: UserData.emailAddress});
  if (!user) {
    try {
      // const lasstRecuriterId = await UserModel.findOne({}, {}, { sort: { created_at: -1 } });
      const lasstRecuriterId = await counterModel.findOne({ _id: 'userSequence' });
      console.log('lasttttttttttt', lasstRecuriterId);
      // const newRecId = lasstRecuriterId ? lasstRecuriterId.recruiterID + 1 : 2;
      const newRecId = lasstRecuriterId
        ? await getNextSequenceValue('userSequence', false)
        : await getNextSequenceValue('userSequence', true);
      const imageURL = UserData.image;
      delete UserData.image;
      let newUser = await UserModel.create({
        ...UserData,
        imageURL,
        verified: UserData.type === 'LINKEDIN' ? true : false,
        recruiterLink: `${commonService.mycurrentURL}/${UserData.name.replace(
          /\s/g,
          '_'
        )}${newRecId}`,
        recruiterID: newRecId,
      });
      if (newUser && UserData.type === 'EMAIL') {
        const reciveToken = await commonService.sendEmailonSignUp(newUser);
        addVerificationCode(reciveToken, newUser.emailAddress);
        newUser = JSON.parse(JSON.stringify(newUser));
        delete newUser.gmailAccessToken;
        return { user: newUser, type: 'new', LoginType: UserData.type };
      }
      if (newUser && UserData.type === 'LINKEDIN') {
        const sendData = {
          imageURL: newUser.imageURL ? newUser.imageURL : commonService.defaultImageURL,
          recruiterID: newUser.recruiterID,
          recruiterLink: newUser.recruiterLink,
        };
        return {
          user: sendData,
          type: 'new',
          LoginType: UserData.type,
          allData: newUser.toObject(),
        };
      } else {
        console.log('unable to create new user');
        return { err: 'unable to create user' };
      }
    } catch (err) {
      console.log('error =====>>>>>', JSON.stringify(err));
      return { err };
    }
  } else if (user.verified === false) {
    const reciveToken = await commonService.sendEmailonSignUp(user);
    addVerificationCode(reciveToken, user.emailAddress);
    return { user, type: 'new', LoginType: UserData.type };
  } else {
    return { user, type: 'existing' };
  }
};

const verifyUser = async dataRecived => {
  const checkToken = await verifyCode(dataRecived.emailAddress, dataRecived.verificationCode);
  console.log('tokennnnn', checkToken);
  let getUser = {};
  if (checkToken.verified === false) {
    const updatedUser = await UserModel.updateOne(
      { _id: checkToken._id },
      { $set: { verified: true } }
    );
    getUser = await UserModel.findOne({ _id: checkToken._id });
    console.log('updatedddddddddddddd', updatedUser);
    getUser = JSON.parse(JSON.stringify(getUser));
    delete getUser.gmailAccessToken;
    const returnUser = {
      ...getUser,
      jwt: checkToken.jwt,
    };
    return returnUser;
  }
  return checkToken;
};

const addVerificationCode = async (codeVerification, email) => {
  const user = await UserModel.findOneAndUpdate(
    { emailAddress: email },
    { $set: { verificationCode: codeVerification } },
    { useFindAndModify: false }
  );
};

const signdupAndVerified = async params => {
  console.log("login====>" + params);
  const user = await UserModel.findOne({ emailAddress: params.emailAddress, accountType: params.accountType });
  console.log("user====>" + user);
  if (user) {
    return { user, type: 'old', LoginType: user.type };
  }
  if (params.type === 'LINKEDIN') {
    const createNewUser = await createUser(params);
    return {
      user: createNewUser.user,
      type: 'new',
      LoginType: createNewUser.allData.type,
      allData: createNewUser.allData,
    };
  }
};
// verify on login
const verifyCode = async (email, code) => {
  try {
    const user = await UserModel.findOne({ emailAddress: email });
    if (user.verificationCode == code) {
      const updatedUser = await UserModel.updateOne(
        { _id: user._id },
        { $set: { verificationCode: '' } }
      );
      if (updatedUser) {
        let getUser = await UserModel.findOne({ _id: user._id }).select('-verificationCode');
        const jwtToken = commonService.createJwt(getUser._id);
        getUser = JSON.parse(JSON.stringify(getUser));
        delete getUser.gmailAccessToken;
        const sendResponse = {
          ...getUser,
          jwt: jwtToken,

          // name: getUser.name || '',
          // emailAddress: getUser.emailAddress || '',
          // hasImage: getUser.hasImage || '',
          // imageURL: getUser.image || commonService.defaultImageURL,
          // linkedInID: getUser.linkedInPersonID || '',
          // phone: getUser.phone || '',
          // recruiterID: getUser.recruiterID || '',
          // recruiterLink: getUser.recruiterLink || '',
        };
        return sendResponse;
      }
    } else {
      return { err: 'not a valid Key' };
    }
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};

module.exports = {
  getAllUsers,
  checkIfExist,
  createUser,
  verifyUser,
  addVerificationCode,
  verifyCode,
  signdupAndVerified,
};
