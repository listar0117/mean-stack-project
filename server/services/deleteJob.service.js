const userModel = require('../models/user.js');
const jobModel = require('../models/job');
const commonService = require('./comon.service');

const deleteJob = async JobData => {
  console.log('Delete job User INPUT ====>>>>>', JSON.stringify(JobData));
  const getJob = await jobModel.findOne({ _id: JobData.jobID });
  if (getJob) {
    if (getJob.recruiterID === JobData.recruiterID) {
      try {
        const updateJob = await jobModel.updateOne(
          { _id: getJob._id },
          { $set: { jobStatus: 'DELETED', deletedBy: JobData.recruiterID } }
        );
        console.log(updateJob);
        return updateJob;
      } catch (err) {
        console.log('error =====>>>>>', JSON.stringify(err));
        return { err };
      }
    }
    return 'wrongRecruiter';
  }
  return null;
};

module.exports = {
  deleteJob,
};
