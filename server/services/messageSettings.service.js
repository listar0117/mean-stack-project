const userModel = require('../models/user.js');
const jobModel = require('../models/job');

const getSettings = async userData => {
  console.log('get Settings User INPUT ====>>>>>', JSON.stringify(userData));
  const user = await userModel.findOne({ recruiterID: userData.recruiterID });
  let getJob;
  try {
    if (userData.jobID.toLowerCase() !== 'all') {
      getJob = await jobModel.findOne({ _id: userData.jobID });
    }
    if (user) {
      const keywords = [
        'First_Name',
        'Last_Name',
        'Company_Name',
        'School_Name',
        'Job_Title',
        'User_Name',
      ];
      let sampleMessage = '';
      if (userData.jobID.toLowerCase() !== 'all' && (getJob && getJob.defaultMesage)) {
        sampleMessage = getJob.defaultMesage;
      } else if (user.defaultMesage) {
        sampleMessage = user.defaultMesage;
      } else {
        sampleMessage = `Hi {{.First_Name}},
          I see that you work with {{.Company_Name}} and went to {{.School_Name}}, and I’m impressed with your work and education. My business is looking for {{.Job_Title}}, and you appear to be a great fit. Let me know if you’re interested.
          Thanks,
          {{.User_Name}}`;
      }
      sampleMessage = sampleMessage.replace(/ +/g, ' ');
      return {
        keywords,
        sampleMessage,
      };
    }
    return null;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
};

// set settings message coming from api for the user

const setSettings = async userData => {
  const user = await userModel.findOne({ recruiterID: userData.recruiterID });
  try {
    if (user) {
      if (userData.jobID.toLowerCase() === 'all') {
        const updatedUser = await userModel.updateOne(
          { _id: user._id },
          { defaultMesage: userData.sampleMessage }
        );
      } else {
        const updatedUser = await jobModel.updateOne(
          { _id: userData.jobID },
          { defaultMesage: userData.sampleMessage }
        );
      }
      return { updated: true };
    }
    return null;
  } catch (err) {
    return { err };
  }
};

module.exports = {
  getSettings,
  setSettings,
};
