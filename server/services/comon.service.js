const sgMail = require('@sendgrid/mail');
require('dotenv').config();
const jsonwebtoken = require('jsonwebtoken');
let randomstring = require('randomstring');
const fs = require('fs');

const envKey = process.env.SENDGRID_API_KEY;
const secret = process.env.JWT_SECRET;
sgMail.setApiKey(envKey);
const defaultImageURL =
  'https://upload.wikimedia.org/wikipedia/commons/a/a1/Wikimedia_Foundation_office_camera_shy_250px.png';
const mycurrentURL = `${process.env.URL}${process.env.PORT}`;

const sendEmailonSignUp = async userData => {
  let randomString = '';
  if (userData.emailAddress === 'manush@dnnae.com' && process.env.NODE_ENV === 'development') {
    randomString = 1234;
  } else if (
    userData.emailAddress === 'uridahs@gmail.com' ||
    userData.emailAddress === 'Rbtestingid@gmail.com' ||
    userData.emailAddress === 'rbtestingid@gmail.com' ||
    userData.emailAddress === 'affan@redbuffer.net' ||
    userData.emailAddress === 'affanakhter5@gmail.com'
  ) {
    randomString = 8209;
    return randomString;
  } else {
    randomString = genrateRandomString();
    // randomString = genrateSignUpLink(userData);
  }
  console.log('random String', randomString);
  const msg = {
    to: userData.emailAddress,
    from: process.env.SENDGRID_From,
    subject: 'Activate your account',
    text: ` Thank you ${userData.name} for joining DNNae!,
    Please use the following Pin Code to activate your account: 
    ${randomString}

    DNNae Support
    admin@dnnae.com
    `,
  };
  sgMail.send(msg);
  console.log('sent Email on signup');
  return randomString;
};
// function to encode file data to base64 encoded string
const base64_encode = file => {
  // read binary data
  const bitmap = fs.readFileSync(file);
  // convert binary data to base64 encoded string
  return new Buffer(bitmap).toString('base64');
};
// send email with attachment
const sendEmailWithFile = async (sendTo, name, filePath, fileName) => {
  const file = base64_encode(filePath);
  const msg = {
    to: sendTo,
    from: process.env.SENDGRID_From,
    subject: 'Complete your DNNae sign up',
    text: ` Hello ${name},
   Please See the attached file`,
    attachments: [
      {
        content: file,
        filename: fileName,
        type: 'text/csv',
        disposition: 'attachment',
        contentId: 'SignUp Repote',
      },
    ],
  };
  sgMail.send(msg);
  console.log(`Email with attachment send at ${Date.now()}`);
  fs.unlinkSync(filePath);
  // return randomString;
};

const sendEmailOnLogin = async (sendTo, name) => {
  let randomString = '';
  if (sendTo === 'manush@dnnae.com' && process.env.NODE_ENV === 'development') {
    randomString = 1234;
  } else if (
    sendTo === 'uridahs@gmail.com' ||
    sendTo === 'Rbtestingid@gmail.com' ||
    sendTo === 'rbtestingid@gmail.com' ||
    sendTo === 'affan@redbuffer.net' ||
    sendTo === 'affanakhter5@gmail.com'
  ) {
    randomString = 8209;
    return randomString;
  } else {
    randomString = genrateRandomString();
  }
  console.log('random String', randomString);
  const msg = {
    to: sendTo,
    from: process.env.SENDGRID_From,
    subject: 'Sign in  Credentials',
    text: `Welcome Back ${name},
    Use following verification code to Sign in 

    ${randomString}`,
  };
  sgMail.send(msg);
  console.log('sent Email on Login');
  return randomString;
};

const emailWebUserForVerification = async (sendTo, name) => {
  let randomString = '';
  randomString = genrateRandomString();
  console.log('random String --web', randomString);
  const msg = {
    to: sendTo,
    from: process.env.SENDGRID_From,
    subject: 'Verify your Email Address',
    text: ` Hello ${name},
    Use following verification code to verify your email 
    
    ${randomString}`,
  };
  sgMail.send(msg);
  console.log('sent Email for web');
  return randomString;
};

const createJwt = userId => {
  const token = jsonwebtoken.sign(
    {
      data: userId,
      // exp in seconds
      exp: Math.floor(Date.now() / 1000) + 3 * (60 * 60), // 60 seconds * 60 minutes = 1 hour
    },
    secret
  );
  return token;
};

const authToken = recivedToken => {
  const decodedToken = jsonwebtoken.verify(recivedToken, secret);
  return decodedToken.data;
  // console.log("this is decoded token ", decodedToken);
};

const genrateRandomString = () => {
  return randomstring.generate({
    length: 4,
    charset: 'numeric',
  });
};
const genrateSignUpLink = userData => {
  const signupUrl = `${mycurrentURL}/signup/activate/${userData.name.replace(/\s/g, '_')}.${
    userData._id
  }`;
  return signupUrl;
};
const genrateRandomStringForApplicants = () => {
  return randomstring.generate({
    length: 15,
    charset: 'alphanumeric',
  });
};
const generateHighlightBlock = data => {
  return `<font color='red'>${data}</font>`;
};
module.exports = {
  sendEmailonSignUp,
  sendEmailWithFile,
  createJwt,
  authToken,
  sendEmailOnLogin,
  defaultImageURL,
  mycurrentURL,
  genrateRandomStringForApplicants,
  emailWebUserForVerification,
  generateHighlightBlock,
};
