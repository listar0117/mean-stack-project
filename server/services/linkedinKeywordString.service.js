const techSkills = require('../dataStore/techSkillsForWhiteListing');
const modefiers = require('../dataStore/modefiers');

const getSkills = unfilteredArray => {
  const StringSet = unfilteredArray.map(skillSet => skillSet.name);
  let setWeGet = new Set(StringSet);
  let SetForMapping = new Set(techSkills.techSkillsList);
  let intersection = new Set([...setWeGet].filter(x => SetForMapping.has(x)));
  console.log(intersection);
  const arr = Array.from(intersection);
  const skills = unfilteredArray.filter(data => {
    if (arr.includes(data.name)) {
      return data;
    }
  });
  console.log(skills);
  return skills;
};

// const makeLinkedinString = (skillsArray, jobTitle) => {
//   let linkString = '';
//   for (let i = 0; i <= 3; i++) {
//     if (skillsArray[i]) {
//       if (i === 0) {
//         linkString = `${String(skillsArray[i].name.replace(/[^a-zA-Z0-9 ]/g, '').trim())}`;
//       } else {
//         linkString = `${linkString} ${String(
//           skillsArray[i].name.replace(/[^a-zA-Z0-9 ]/g, '').trim()
//         )}`;
//       }
//     }
//   }
//   const linkedinConcatString = `(${linkString} ${jobTitle}) OR (${linkString})`;
//   return encodeURIComponent(linkedinConcatString);
// };

const makeLinkedinString = (skillsArray, jobTitle) => {
  const linkString = [];
  let skillsString = '';
  let linkedinConcatString = '';
  if (skillsArray.length > 0) {
    for (let i = 0, len = skillsArray.length; i <= len; i += 2) {
      if (skillsArray[i]) {
        if (i + 1 < len - 1) {
          linkString.push(`(${skillsArray[i].name} ${skillsArray[i + 1].name})`);
          // arr.push([skillsArray[i].name, skillsArray[i + 1].name]);
        } else {
          linkString.push(`(${skillsArray[i].name})`);
        }
      }
    }
    skillsString = linkString.join(' OR ');
  }
  if (skillsArray.length > 0) {
    linkedinConcatString = `(${jobTitle}) OR (${skillsString})`;
  } else {
    linkedinConcatString = `(${jobTitle})`;
  }
  return encodeURIComponent(linkedinConcatString);
};

const keywordsExtractor = (techinalSkillsData, title) => {
  let searchString = '';
  let linkedinConcatString = [];
  console.log(techSkills.techSkillsList);
  console.log('============================tech skills=======================');
  console.log(techinalSkillsData);
  console.log('============================tech skills end=======================');
  console.log('============================tech skills stringify Json=======================');
  console.log(JSON.stringify(techinalSkillsData));

  // const jobTitle = title[0].data[0];
  const jobTitle = title[0].data[0].replace(/[^a-zA-Z0-9 ]/g, '').trim();
  // const dataToworkOn = techinalSkillsData;
  const dataToworkOn = JSON.parse(JSON.stringify(techinalSkillsData));
  const sortedByScore = dataToworkOn.sort((a, b) => parseFloat(b.score) - parseFloat(a.score));
  let mustHaves = [];
  if (sortedByScore[0].score > sortedByScore[sortedByScore.length - 1].score) {
    mustHaves = sortedByScore.filter(mustHave => mustHave.score === sortedByScore[0].score);
  } else {
    mustHaves = [];
  }
  if (mustHaves.length > 0) {
    // const skills = getSkills(mustHaves);
    // const sortSkillByScore = mustHaves.sort((a, b) => parseFloat(b.score) - parseFloat(a.score));

    // if (sortSkillByScore.length > 4) {
    // for (let i = 0; i <= 4; i++) {
    //   if (sortSkillByScore[i]) {
    //     if (i === 0) {
    //       searchString = `${sortSkillByScore[i].name}`;
    //     } else {
    //       searchString = `${searchString} ${sortSkillByScore[i].name}`;
    //     }
    //   }
    // }
    // // }
    // linkedinConcatString = `(${searchString} ${jobTitle}) OR (${searchString})`;
    // return encodeURIComponent(linkedinConcatString);
    linkedinConcatString = makeLinkedinString(mustHaves, jobTitle);
    return linkedinConcatString;
  }

  if (mustHaves.length <= 0) {
    // const anyModefiers = sortedByScore.filter(mustHave => mustHave.modifier !== '');
    const assignScores = sortedByScore.map(data => {
      if (modefiers.REQUIRED_MODIFIERS.includes(data.modifier)) {
        data.score = 5;
      } else if (modefiers.IMPORTANT_MODIFIERS.includes(data.modifier)) {
        data.score = 4;
      } else if (modefiers.NICETOHAVE_MODIFIERS.includes(data.modifier)) {
        data.score = 3;
      }
    });
    const sortedByFakeScore = sortedByScore.sort(
      (a, b) => parseFloat(b.score) - parseFloat(a.score)
    );
    const anyModefiers = sortedByFakeScore.filter(
      mustHave => mustHave.score === sortedByFakeScore[0].score
    );

    if (anyModefiers[0].score === anyModefiers[anyModefiers.length - 1].score) {
      const skills = getSkills(anyModefiers);
      const sortSkillByScore = skills.sort((a, b) => parseFloat(b.score) - parseFloat(a.score));
      linkedinConcatString = makeLinkedinString(sortSkillByScore, jobTitle);
    } else {
      const skills = getSkills(sortedByScore);
      const sortSkillByScore = skills.sort((a, b) => parseFloat(b.score) - parseFloat(a.score));
      if (sortSkillByScore[0].score === sortSkillByScore[sortSkillByScore.length - 1].score) {
        const sortskillByLength = sortSkillByScore.sort((a, b) => {
          // console.log(a.length);
          // ASC  -> a.length - b.length
          // DESC -> b.length - a.length
          return a.length - b.length;
        });
        linkedinConcatString = makeLinkedinString(sortskillByLength, jobTitle);
      } else {
        linkedinConcatString = makeLinkedinString(sortSkillByScore, jobTitle);
      }
    }
  }
  return linkedinConcatString;

  // const stringsOnly = sortedByScore.map(skillSet => skillSet.name);
  // console.log(stringsOnly);

  // let setWeGet = new Set(stringsOnly);
  // let SetForMapping = new Set(techSkills.techSkillsList);
  // let intersection = new Set([...setWeGet].filter(x => SetForMapping.has(x)));
  // console.log(intersection);
  // const arr = Array.from(intersection);

  // const sortedArrayonLength = arr.sort((a, b) => {
  //   console.log(a.length);
  //   // ASC  -> a.length - b.length
  //   // DESC -> b.length - a.length
  //   return a.length - b.length;
  // });
  // console.log(sortedArrayonLength);

  // let i;
  // let j;
  // let temparray;
  // //   let linkedinArrayUrls = [];
  // const chunk = 6;
  // for (i = 0, j = sortedArrayonLength.length; i < j; i += chunk) {
  //   temparray = sortedArrayonLength.slice(i, i + chunk);
  //   for (let k = 0; k <= temparray.length; k++) {
  //     if (temparray[k]) {
  //       if (k === 0) {
  //         searchString = `${temparray[k]}`;
  //       } else {
  //         searchString = `${searchString} ${temparray[k]}`;
  //       }
  //     }
  //   }
  //   // linkedinArrayUrls.push(encodeURIComponent(searchString));
  //   if (i === 0) {
  //     linkedinConcatString = `(${searchString} ${jobTitle}) OR (${searchString})`;
  //   } else {
  //     linkedinConcatString = `${linkedinConcatString} OR (${searchString})`;
  //   }

  //   searchString = '';
  // }
  // console.log('linkedinUrls', linkedinConcatString);

  // return encodeURIComponent(linkedinConcatString);
};

module.exports = {
  keywordsExtractor,
};
