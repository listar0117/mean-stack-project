const request = require('request-promise-native');
require('dotenv').config();

const userModel = require('../../models/user.js');
const jobModel = require('../../models/job');

// const getLink = async userData => {
//   console.log('get calendlyLink User INPUT ====>>>>>', JSON.stringify(userData));
//   const user = await userModel.findOne({ recruiterID: userData.recruiterID });
//   let getJob;
//   try {
//     // if (userData.jobID.toLowerCase() !== 'all') {
//     //   getJob = await jobModel.findOne({ _id: userData.jobID });
//     // }
//     if (user) {
//       let link = '';
//       if (user.calendlyLink) {
//         link = user.calendlyLink;
//       }
//       return {
//         link,
//       };
//     }
//     return null;
//   } catch (err) {
//     console.log('error =====>>>>>', JSON.stringify(err));
//     return { err };
//   }
// };

// set email integration coming from api for the user

const setMail = async userData => {
  const user = await userModel.findOne({ recruiterID: userData.recruiterID });
  try {
    if (user) {
      console.log('1111', process.env.mailServer);
      let response;
      try {
        response = await request({
          method: 'POST',
          // url: `http://34.67.207.137:3002/score_profile`,
          url: `${process.env.mailServer}/gmail/get_access_token.php`,
          body: {
            loginEmail: userData.integratedEmailAddress,
            authCode: userData.integratedEmailToken,
          },
          json: true,
        });
      } catch (err) {
        console.log('error =====>>>>>', JSON.stringify(err));
        console.log('error on messaging Api', err);
        return { mailError: true };
      }
      console.log('resssssssssssssssssssssssss', response);
      const recivedData = JSON.parse(response);
      if (recivedData.isSuccess === false || recivedData.isSuccess === 'FALSE') {
        console.log('error =====>>>>>', JSON.stringify(recivedData));
        console.log('error on messaging Api', recivedData);
        return { mailError: true };
      }

      const updatedUser = await userModel.updateOne(
        { _id: user._id },
        {
          integratedEmailAddress: recivedData.loginEmail,
          integratedEmailToken: userData.integratedEmailToken,
          gmailAccessToken: recivedData.accessToken,
        }
      );
      return { updated: true };
    }
    return null;
  } catch (err) {
    return { err };
  }
};

module.exports = {
  setMail,
};
