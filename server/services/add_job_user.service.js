const request = require('request-promise-native');
const userModel = require('../models/user.js');
const jobModel = require('../models/job');
const commonService = require('./comon.service');
const linkedinString = require('./linkedinKeywordString.service');

require('dotenv').config();

const createjob = async JobData => {
  console.log('Create job User INPUT ====>>>>>', JSON.stringify(JobData));
  const user = await userModel.findOne({ recruiterID: JobData.recruiterID });
  try {
    if (user) {
      const newJobData = {
        ...JobData,
        // jobLink: `${commonService.mycurrentURL}/${UserData.name}${newRecId}`,
      };
      delete newJobData.recruiterID;
      if (
        (newJobData && !newJobData.jobLinkedInSearchKeyword) ||
        (newJobData && !newJobData.jobLinkedInSearchKeyword.length.trim() <= 0)
      ) {
        // old logic
        // const getSearchData = newJobData.jobArray.filter(
        //   jobArray => jobArray.tag === 'TECHNICAL_SKILLS'
        // );

        // new logic
        const IndustrySpecialties = newJobData.jobArray.filter(
          jobArray => jobArray.tag === 'COMPANY_INDUSTRY_SPECIALTIES'
        );
        const JobTitle = newJobData.jobArray.filter(jobArray => jobArray.tag === 'JOB_POSITION');
        // console.log(JobTitle[0].data[0]);
        console.log('IndustrySpecialties', IndustrySpecialties);
        // const addedScore = getSearchData[0].data.map(data =>
        //   data.score || data.score === 0 ? data : { ...data, score: 0 }
        // );
        // new logic
        const addedScore = IndustrySpecialties[0].data.map(data =>
          data.score || data.score === 0 ? data : { ...data, score: 1 }
        );
        let specialtiesString = '';
        addedScore.map((data, index) => {
          if (specialtiesString.length === 0) {
            specialtiesString = data.name;
          } else if (specialtiesString.length > 0) {
            specialtiesString = `${specialtiesString} AND ${data.name}`;
          }
        });
        const sortedByScore = addedScore.sort((a, b) => parseFloat(b.score) - parseFloat(a.score));
        const searchStringData = sortedByScore.map((data, index) => {
          return data.name;
        });
        const flattenedSearchStringData = [].concat(...searchStringData);
        let searchString = '';
        for (let i = 0; i <= flattenedSearchStringData.length; i++) {
          if (flattenedSearchStringData[i]) {
            searchString = `${searchString} ${flattenedSearchStringData[i]}`;
          }
        }
        // const uriString = encodeURI(searchString);
        // console.log('qqqqqqqqqqqqqqqqqqqqqqqqq', uriString);

        // const uriString1 = await linkedinString.keywordsExtractor(addedScore, JobTitle);
        const uriString1 = `${JobTitle[0].data[0]} AND ${specialtiesString}`;
        console.log('qqqqqqqq22222222222', uriString1);
        newJobData.jobLinkedInSearchKeyword = uriString1;
      }
      newJobData.recruiter = user._id;
      let newJob;
      let genratedJobLink;
      newJobData.recruiterID = user.recruiterID;
      // check wether to add job or update existing job
      if (JobData && JobData.jobID && JobData.jobID !== 'add') {
        try {
          newJob = await jobModel.findOne({ _id: JobData.jobID });
          if (newJob) {
            // var replaced = str.split(' ').join('+');
            genratedJobLink = `${commonService.mycurrentURL}/company/${JobData.companyName
              .split(' ')
              .join('-')}/job/${JobData.jobID}/${JobData.jobTitle.split(' ').join('-')}`;
            const updateData = await jobModel.findOneAndUpdate(
              { _id: JobData.jobID },
              {
                $set: {
                  jobDescription: JobData.jobDescription,
                  jobArray: JobData.jobArray,
                  jobLink: genratedJobLink,
                  jobTitle: JobData.jobTitle,
                  companyName: JobData.companyName,
                  jobLocation: JobData.jobLocation,
                  stepperData: JobData.stepperData,
                },
              }
            );
          }
          console.log(JobData.jobID, 'Job Updated successfully');
        } catch (err) {
          console.log('error =====>>>>>', JSON.stringify(err));
          return { noJob: true };
        }
      } else {
        newJob = await jobModel.create({ ...newJobData });
        console.log('nwwwwww', newJob);
        // const user = await UserModel.findOne({ _id: checkToken });
        const updatedUser = await userModel.updateOne(
          { _id: user._id },
          { $addToSet: { jobs: [newJob._id] } }
        );
        // genratedJobLink = `${commonService.mycurrentURL}/job/${newJob._id}`;
        genratedJobLink = `${commonService.mycurrentURL}/company/${newJob.companyName
          .split(' ')
          .join('-')}/job/${newJob._id}/${newJob.jobTitle.split(' ').join('-')}`;
        const updateJob = await jobModel.updateOne(
          { _id: newJob._id },
          { $set: { jobLink: genratedJobLink } }
        );
        console.log('new job created');
      }
      // try {
      //   const JobTitle = newJobData.jobArray.filter(jobArray => jobArray.tag === 'JOB_POSITION');
      //   const response = await request({
      //     method: 'PUT',
      //     url: `https://api.dialogflow.com/v1/entities/a7903cc2-3b7e-4b59-becc-ae05e2b17d41/entries?v=20150910`,
      //     // url: `${process.env.nlpUrl}${process.env.nlpPort}/score_profile`,
      //     body: {
      //       synonyms: [JobTitle[0].data[0]],
      //       value: JobTitle[0].data[0],
      //     },
      //     json: true,
      //     headers: {
      //       Authorization: 'Bearer 81062077c319417597f21f500879720f',
      //       'Content-Type': 'application/json',
      //     },
      //   });
      //   console.log(response.status.code);
      //   if (response.status.code === 200) {
      //     const returnJobData = {
      //       jobID: newJob._id,
      //       jobLink: genratedJobLink,
      //     };
      //     return returnJobData;
      //   }
      //   return { DialogErr: 'Some thing went wrong at dialogFlow end' };
      // } catch (err) {
      //   console.log('error =====>>>>>', JSON.stringify(err));
      //   return { DialogErr: 'Some thing went wrong at dialogFlow end' };
      // }
      const returnJobData = {
        jobID: newJob._id,
        jobLink: genratedJobLink,
      };
      return returnJobData;
    }
    return null;
  } catch (err) {
    console.log('error =====>>>>>', JSON.stringify(err));
    return { err };
  }
  //   return user;
};

module.exports = {
  createjob,
};
