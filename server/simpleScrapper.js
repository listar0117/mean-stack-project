const puppeteer = require('puppeteer');

const companyName = 'redbuffer';
(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto('https://www.google.co.uk/', { waitUntil: 'networkidle2' });
  await page.click('[name=q]');
  await page.keyboard.type(`${companyName} linkedin`);
  // you forgot this
  await page.keyboard.press('Enter');

  await page.waitForSelector('h3.LC20lb', { timeout: 10000 });
  await page.click('h3.LC20lb');
  await page.waitForSelector('label.show-more-less-state__label-more', { timeout: 10000 });
  await page.click('label.show-more-less-state__label-more');

  // await page.evaluate(() => {
  const last = await page.$('p.about__detail-text:last-child');
  const specialitiesText = await (await last.getProperty('innerHTML')).jsonValue();
  console.log(specialitiesText);

  // });

  //   await browser.close();
})();
