const Router = require('koa-router');

const router = new Router();
const User = require('../models/user');
const UserController = require('../controllers/user.controller');
const AddJobsController = require('../controllers/add_job_user.controller');
const GetJobsController = require('../controllers/get_jobs_user.controller');
const AddCampaignsController = require('../controllers/add_campaign_user.controller');
const GetCampaignsController = require('../controllers/get_campaigns_user.controller');
const GetJobsListController = require('../controllers/jobs/get_jobs_list_user.controller');
const DeleteJobController = require('../controllers/deleteJob.controller');

const AddApplicantController = require('../controllers/applicants/add_applicants.controllers');
const GetApplicantController = require('../controllers/applicants/get_applicants.controllers');
const GetInboxApplicantController = require('../controllers/applicants/get_inbox_applicant.controller');
const webApplicants = require('../controllers/applicants/add_webApplicant.controller');
const verifyWebApplicants = require('../controllers/applicants/verify_webApplicant.controller');
const getWebApplicants = require('../controllers/applicants/get_webApplicants.controller');
const updateApplicantStatus = require('../controllers/applicants/update_applicants.controller');
const fetchApplicants = require('../controllers/applicants/fetch_applicants_for_connection.controller');
const getMessageSettings = require('../controllers/message_settings/get_message_settings.controller');
const setMessageSettings = require('../controllers/message_settings/set_message_settings.controller');
const getBatchMessageSettings = require('../controllers/message_settings/get_batchMessage_settings.controller');
const setBatchMessageSettings = require('../controllers/message_settings/set_batchMessage_settings.controller');

const webLeadsController = require('../controllers/leads.controller');
const addMessagesController = require('../controllers/applicants_messages/add_messages.controllers');
const getMessagesController = require('../controllers/applicants_messages/get_messages.controllers');
const GetInboxMessagingController = require('../controllers/applicants_messages/get_inbox_messages.controller');
const getMessagesBotController = require('../controllers/applicants_messages/get_messages_bot.controllers');
const getCalendlyController = require('../controllers/message_settings/get_calendly_settings.controller');
const setCalendlyController = require('../controllers/message_settings/set_calendly_settings.controller');
const fakeProfilesController = require('../controllers/chatbot/create_fake_profiles.controller');
const getAll = require('../controllers/getAll/get_all.controller');
const setEmailData = require('../controllers/mailIntegration/set_mailIntegration_settings.controller');

const authentication = require('../services/authentication');

router.post('/api/user/register', UserController.registerUser);
router.post('/api/user/verify', UserController.verifyRegisteredUser);
router.post('/api/user/login', UserController.login);
router.post('/api/user/verify_email', UserController.verifyEmailLogin);
router.post('/api/user/addJob', AddJobsController.addJob);
router.post('/api/user/getJobs', GetJobsController.getJobs);
router.post('/api/user/addCampaign', AddCampaignsController.addCampaign);
router.post('/api/user/getCampaigns', GetCampaignsController.getCampaigns);
router.post('/api/delete/job', DeleteJobController.deleteJob);
router.post('/api/post-applicants', AddApplicantController.addApplicant);
router.post('/api/get-applicants', GetApplicantController.getApplicants);
router.put('/api/put/applicants-status', updateApplicantStatus.updateApplicant);
router.post('/api/get/connection-applicants', fetchApplicants.getApplicants);
router.post('/api/post/get-message-settings', getMessageSettings.getMessageSettings);
router.post('/api/post/set-message-settings', setMessageSettings.setMessageSettings);

router.post('/api/post/get-batchMessage-settings', getBatchMessageSettings.getMessageSettings);
router.post('/api/post/set-batchMessage-settings', setBatchMessageSettings.setMessageSettings);
router.post('/api/post/get-web-applicants', getWebApplicants.getApplicants);
router.post('/api/post/applicant-messaging', addMessagesController.addMessage);
router.post('/api/get/applicant-messaging', getMessagesController.getMessage);
router.post('/api/get/applicant-messaging-from-bot', getMessagesBotController.getMessage);
router.post('/api/post/get-calendly-link', getCalendlyController.getLink);
router.post('/api/post/set-calendly-link', setCalendlyController.setLink);
// EmailIntegration
router.post('/api/post/save-email-data', setEmailData.setMail);
// inbox
router.post('/api/user/inbox/getJobsList', GetJobsListController.getJobs);
router.post('/api/user/inbox/get-applicants', GetInboxApplicantController.getApplicants);
router.post('/api/user/inbox/get-Messages', GetInboxMessagingController.getMessages);

// web Apis
router.post('/api/get/job', GetJobsController.getJob); // =>> get a single job
router.post('/api/post/jobApplicant', webApplicants.saveApplicant); // =>> add applicant who applied through web
router.put('/api/put/VerifyjobApplicant', verifyWebApplicants.verify); // =>> verify added applicant who applied through web
router.get('/api/get/webApplicant', getWebApplicants.getSingleApplicant); // =>> get data for single applicant web data
router.get('/api/get/singleApplicantData', getWebApplicants.getSingleApplicant); // =>> get data for single applicant web data

// chatBot Api's
router.post('/api/post/chatbot/fakeProfiles', fakeProfilesController.addProfile); // =>> create fake profile
// testing APIs
router.get('/api/get/allJobs', getAll.getAllJobs); // =>> get all jobs Data
router.get('/api/get/allApplicants', getAll.getAllApplicants); // =>> get all jobs Data
// new Routes

router.post('/api/post/applicants', webApplicants.saveApplicant);
router.post('/api/post/lead', webLeadsController.Postlead);

// new Routes
router.post('/api/post/applicants', AddApplicantController.addApplicant);
router.post('/api/get/applicants', GetApplicantController.getApplicants);
router.post('/api/get/singleApplicant', GetApplicantController.getApplicant); // get a single applicant from controller

// router.get('/api/users', async ctx => {
//   await User.find()
//     .then(users => {
//       ctx.body = users
//     })
//     .catch(err => {
//       ctx.body = 'error: ' + err
//     })
// })

// router.get('/api/users', authentication, UserController.GetUsers);

router.post('/api/user', async ctx => {
  if (!ctx.request.body.user_name) {
    ctx.body = {
      error: 'Bad Data',
    };
  } else {
    const user = new User();
    user.user_name = ctx.request.body.user_name;
    await user
      .save()
      .then(data => {
        ctx.body = data;
      })
      .catch(err => {
        ctx.body = `error: ${err}`;
      });
  }
});

module.exports = router;
