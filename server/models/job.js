const mongoose = require('mongoose');

const { Schema } = mongoose;
// Create Schema
const schema = new Schema(
  {
    jobTitle: {
      type: String,
    },
    companyName: {
      type: String,
    },
    jobLocation: {
      type: String,
    },
    jdType: {
      type: String,
    },
    jdText: {
      type: String,
    },
    jdFile: {
      type: String,
      required() {
        return this.jdType === 'PDF' ? true : false;
      },
    },
    jobDescription: {
      type: Array,
    },
    jobLink: {
      type: String,
    },
    jobLinkedInSearchKeyword: {
      type: String,
    },
    jobType: {
      type: String,
      default: 'FULL TIME',
    },
    jobStatus: {
      type: String,
      default: 'OPEN',
    },
    jobArray: {
      type: Array,
    },
    stepperData: {
      type: Array,
    },
    recruiter: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'users',
    },
    recruiterID: {
      type: Number,
      default: 0,
    },
    deletedBy: {
      type: Number,
    },
    defaultMesage: {
      type: String,
    },
    defaultBatchMessages: {
      type: Array,
    },
    applicants: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'applicant',
      },
    ],
    applicantMessages: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'applicantMessage',
      },
    ],
    webApplicants: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'webApplicant',
      },
    ],
    quickApplicants: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'quickApplicant',
      },
    ],
  },
  { timestamps: { createdAt: 'created_at' } }
);
schema.set('toJSON', {
  transform: function (doc, ret, options) {
    ret.jobID = ret._id;
    // delete ret._id;
    // delete ret.__v;
  },
});
module.exports = mongoose.model('job', schema);
