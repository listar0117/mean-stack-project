const mongoose = require('mongoose');

const { Schema } = mongoose;
// Create Schema
const schema = new Schema({
  _id: {
    type: String,
  },
  sequence_value: {
    type: Number,
  },
});

module.exports = mongoose.model('counters', schema);
