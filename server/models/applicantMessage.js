const mongoose = require('mongoose');

const { Schema } = mongoose;
// Create Schema
const schema = new Schema(
  {
    profileArray: {
      type: Array,
    },
    jobID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'job',
    },
    recruiter: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'users',
    },
    applicant: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'applicant',
    },
    recruiterID: {
      type: Number,
      default: 0,
    },
    tempID: {
      type: Number,
      default: 1,
    },
  },
  { timestamps: { createdAt: 'created_at' } }
);

module.exports = mongoose.model('applicantMessage', schema);
