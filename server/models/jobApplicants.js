const mongoose = require("mongoose");
const { Schema } = mongoose;
// Create Schema
const schema = new Schema({
  first_name: {
    type: String
  },
  middle_name: {
    type: String
  },
  last_name: {
    type: String
  },
  full_name: {
    type: String
  },
  email:{
    type:String
  },
  resume_link:{
    type:String
  },
  linkedin_link:{
    type:String
  },
  phoneNumber:{
    type:String
  }
});

module.exports = mongoose.model("job_applicants", schema);
