const mongoose = require('mongoose');

const { Schema } = mongoose;
// Create Schema
const schema = new Schema(
  {
    name: {
      type: String,
    },
    emailAddress: {
      type: String,
    },
    linkedInUrl: {
      type: String,
    },
    cvUrl: {
      type: String,
    },
    phoneNumber: {
      type: String,
    },
    jobID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'job',
    },
    recruiter: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'users',
    },
    recruiterID: {
      type: Number,
      default: 0,
    },
    verified: {
      type: Boolean,
      default: false,
    },
    verificationCode: {
      type: Number,
    },
  },
  { timestamps: { createdAt: 'created_at' } }
);

module.exports = mongoose.model('webApplicant', schema);
