const mongoose = require('mongoose');

const { Schema } = mongoose;
// Create Schema
const schema = new Schema(
  {
    emailAddress: {
      type: String,
      required: [true, 'email required'],
      trim: true,
      unique: true,
    },
  },
  { timestamps: { createdAt: 'created_at' } }
);

module.exports = mongoose.model('leads', schema);
