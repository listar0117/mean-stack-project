
const mongoose = require('mongoose');

const { Schema } = mongoose;
// Create Schema
const schema = new Schema(
  {
    campaignTitle: {
      type: Array,
    },
    companyName: {
      type: String,
    },
    campaignLocation: {
      type: String,
    },
    investorType: {
      type: Array,
    },
    industries: {
      type: String,
    },
    invest_stage: {
      type: Array,
    },
    jobLink: {
      type: String,
    },
    campaignLinkedInSearchKeyword: {
      type: String,
    },
    founder: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'users',
    },
    founderID: {
      type: Number,
      default: 0,
    },
    applicants: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'applicant',
      },
    ],
    applicantMessages: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'applicantMessage',
      },
    ],
    webApplicants: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'webApplicant',
      },
    ],
    quickApplicants: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'quickApplicant',
      },
    ],
  },
  { timestamps: { createdAt: 'created_at' } }
);
schema.set('toJSON', {
  transform: function (doc, ret, options) {
    ret.campaignID = ret._id;
  },
});
module.exports = mongoose.model('campaign', schema);
