const mongoose = require('mongoose');

const { Schema } = mongoose;
// Create Schema
const schema = new Schema(
  {
    message: {
      dateTime: { type: Date },
      linkedinMessageID: { type: String },
      type: { type: String },
      messageText: { type: String },
      entityUrn: { type: String },
      chatBotSuggestion: { type: Boolean },
    },
    recruiter: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'users',
    },
    recruiterID: {
      type: Number,
    },
    jobID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'job',
    },
  },
  { timestamps: { createdAt: 'created_at' } }
);

module.exports = mongoose.model('linkedinMessages', schema);
