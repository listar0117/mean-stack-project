const mongoose = require("mongoose");
const { Schema } = mongoose;
// Create Schema
const schema = new Schema({
 name: {
    type: String
  },
  companyType:{
    type:String
  }
});

module.exports = mongoose.model("companies", schema);
