const mongoose = require('mongoose');

const { Schema } = mongoose;
// Create Schema
const schema = new Schema(
  {
    name: {
      type: String,
    },
    email: {
      type: String,
    },
    linkedInID: {
      type: String,
    },
    profileArray: {
      type: Array,
    },
    jobLinkedInSearchKeyword: {
      type: String,
    },
    jobID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'job',
    },
    recruiter: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'users',
    },
    applicantMessages: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'applicantMessage',
      },
    ],
    recruiterID: {
      type: Number,
      default: 0,
    },
    quickApplicants: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'quickApplicant',
      },
    ],
  },
  { timestamps: { createdAt: 'created_at' } }
);

module.exports = mongoose.model('applicant', schema);
