const mongoose = require('mongoose');

const { Schema } = mongoose;
// Here's the required crypto code
const crypto = require('crypto');
require('dotenv').config();

// turn to lower case before saving
function toLower(v) {
  return v.toLowerCase();
}
// encrypt token seter
function encrypt(text) {
  const cipher = crypto.createCipher('aes-256-cbc', process.env.TOKEN_SECRET);
  let crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}

// decrypt token getter
function decrypt(text) {
  if (text === null || typeof text === 'undefined') {
    return text;
  }
  const decipher = crypto.createDecipher('aes-256-cbc', process.env.TOKEN_SECRET);
  let dec = decipher.update(text, 'hex', 'utf8');
  dec += decipher.final('utf8');
  return dec;
}
// Create Schema
const schema = new Schema(
  {
    name: {
      type: String,
    },
    hasImage: {
      type: Boolean,
    },
    type: {
      type: String,
    },
    signupCompanyName: {
      type: String,
    },
    imageURL: {
      type: String,
      required() {
        return this.hasImage ? true : false;
      },
    },
    image: {
      type: String,
    },
    defaultMesage: {
      type: String,
    },
    calendlyLink: {
      type: String,
    },
    defaultBatchMessages: {
      type: Array,
    },
    messageTemplatesArray: {
      type: Array,
    },
    calledFrom: {
      type: String,
    },
    linkedInID: {
      type: String,
    },
    linkedInAccessToken: {
      type: String,
    },
    linkedInPersonID: {
      type: String,
    },
    phone: {
      type: String,
    },
    userType: {
      type: String,
    },
    recruiterID: {
      type: Number,
      default: 0,
    },
    recruiterLink: {
      type: String,
    },
    user_name: {
      type: String,
    },
    emailAddress: {
      type: String,
      required: [true, 'email required'],
      trim: true,
      unique: true,
      set: toLower,
    },
    integratedEmailAddress: {
      type: String,
      trim: true,
      // unique: true,
      set: toLower,
    },
    integratedEmailToken: {
      type: String,
      get: decrypt,
      set: encrypt,
    },
    gmailAccessToken: {
      access_token: { type: String, get: decrypt, set: encrypt },
      expires_in: { type: String },
      refresh_token: { type: String, get: decrypt, set: encrypt },
      scope: { type: String },
      token_type: { type: String },
      created: { type: String },
    },
    verified: {
      type: Boolean,
      default: false,
    },
    verificationCode: {
      type: Number,
    },
    jobs: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'job',
      },
    ],
    campaigns: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'campaign',
      },
    ],
    accountType: {
      type: String,
    }
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } },
  {
    toObject: { getters: true, setters: true },
    toJSON: { getters: true, setters: true },
    runSettersOnQuery: true,
  }
);

module.exports = mongoose.model('users', schema);
