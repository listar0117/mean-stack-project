const mongoose = require("mongoose");
const { Schema } = mongoose;

const schema = new Schema({
  name: {
    type: String
  }
});

module.exports = mongoose.model("user_types", schema);
