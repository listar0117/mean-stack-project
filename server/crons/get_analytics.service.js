const fs = require('fs');
const path = require('path');
// save web applicants
const json2csv = require('json2csv').parse;
const userModel = require('../models/user.js');
const jobModel = require('../models/job');
const commonService = require('../services/comon.service');

const getAnalytics = async applicantData => {
  const fields = [
    'created_at',
    'name',
    'emailAddress',
    'signupCompanyName',
    'verified',
    'jobsCreated',
    'jobTitle',
    'totalProspectsFound',
    'sentRequests',
    'acceptedRequests',
    'MessageTemplate',
  ];
  // const fieldNames = ['created_at', 'name', 'emailAddress', 'signupCompanyName', 'verified'];
  try {
    let user = await userModel
      .find()
      .select('name emailAddress verified signupCompanyName created_at')
      .populate({
        path: 'jobs',
        select: 'jobTitle',
        sort: { created_at: -1 },
        populate: { path: 'applicants' },
      });
    user = JSON.parse(JSON.stringify(user));
    console.log(user);
    const UserData = [];
    user.map(userData => {
      userData.jobs.map(job => {
        // job.applicants[0].profileArray.filter(x => x.status === 'SENT').length;
        const obj = {
          created_at: userData.created_at,
          name: userData.name,
          email: userData.emailAddress,
          company: userData.signupCompanyName,
          verified: userData.verified,
          jobsCreated: userData.jobs.length,
          jobTitle:job.jobTitle,
          totalProspectsFound:
            job.applicants.length > 0 ? job.applicants[0].profileArray.length : 0,
          sentRequests:
            job.applicants.length > 0
              ? job.applicants[0].profileArray.filter(x => x.status === 'SENT').length
              : 0,
          acceptedRequests:
            job.applicants.length > 0
              ? job.applicants[0].profileArray.filter(x => x.status === 'ACCEPTED').length
              : 0,
          MessageTemplate: job.defaultMesage ? job.defaultMesage : userData.defaultMesage,
        };
        UserData.push(obj);
      });
    });
    try {
      const csv = json2csv(UserData, fields);
      console.log(csv);
      const fileName = `Users_Data_${Date.now()}`;
      const filePath = path.resolve(`./${fileName}`);
      // const filePath = `./${fileName}`;
      console.log(filePath);
      await fs.writeFile(fileName, csv, err => {
        if (err) {
          throw err;
        } else {
          console.log('file saved');
          commonService.sendEmailWithFile('fahadjalal@gmail.com', 'Fahad', filePath, fileName);
          commonService.sendEmailWithFile('affanakhter5@gmail.com', 'Affan', filePath, fileName);
        }
      });
    } catch (err) {
      console.error(err);
    }
  } catch (err) {
    console.error(err);
    return { err };
    // process.exit(1);
    // next(err);
  }
};
module.exports = {
  getAnalytics,
};
