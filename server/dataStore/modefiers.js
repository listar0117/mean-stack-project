const REQUIRED_MODIFIERS = [
  'must',
  'must have',
  'expertise',
  'expert',
  'experience',
  'strong experience',
  'experiences',
  'experienced',
  'master',
  'specialization',
  'mandatory',
];
const IMPORTANT_MODIFIERS = [
  'proficient',
  'proficiency',
  'fluent',
  'fluency',
  'skill',
  'skilled',
  'efficient',
  'eloquent',
  'strong knowledge',
  'knowledge',
  'preferred',
  'preferable',
];
const NICETOHAVE_MODIFIERS = [
  'good',
  'nice',
  'publication',
  'publications',
  'coursework',
  'familiarity',
  'familiar',
  'understanding',
  'exposure',
  'plus',
  'valued',
  'optional',
  'focus',
  'aptitude',
];

module.exports = {
  REQUIRED_MODIFIERS,
  IMPORTANT_MODIFIERS,
  NICETOHAVE_MODIFIERS,
};
