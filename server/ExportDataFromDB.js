const fs = require('fs');
const mongoose = require('mongoose');
// const option = {
//   socketTimeoutMS: 30000,
//   keepAlive: true,
//   reconnectTries: 30000,
//   useNewUrlParser: true,
// };

const options = { keepAlive: 300000, connectTimeoutMS: 30000, useNewUrlParser: true };
const applicantsModel = require('./models/applicants');

let dev_db_url = 'mongodb://localhost:27017/dnNae';
let mongoDB = process.env.MONGODB_URI || dev_db_url;
// mongoose.connect(mongoDB, { useNewUrlParser: true });

mongoose.connect(mongoDB, options);
mongoose.Promise = global.Promise;

const getUnique = (arr, comp) => {
  const listOfTags = arr;
  const keys = [comp];
  const filtered = listOfTags.filter(
    (s => o => (k => !s.has(k) && s.add(k))(keys.map(k => o[k]).join('|')))(new Set())
  );
  return filtered;
};
console.log('here');
// get all data
// applicantsModel.find().then(applicants => {
//   console.log('total applicants: ', applicants.length);
//   let profileArrays = [];

//   applicants.map(applicant => {
//     // console.log(applicant.profileArray);
//     console.log('=========================================');
//     profileArrays = profileArrays.concat(applicant.profileArray);
//   });
//   console.log(profileArrays.length);
//   let removedDupes = [];
//   removedDupes = getUnique(profileArrays, 'entityUrn');
//   console.log(removedDupes.length);
//   fs.writeFile('devDataFiltered.json', JSON.stringify(removedDupes), 'utf8', function (err) {
//     console.log(err);
//   });
// });

// get conversations
applicantsModel.find().then(applicants => {
  console.log('total applicants: ', applicants.length);
  let profileArrays = [];
  // console.log('applicants>>>>>>.....', applicants);
  applicants.map(applicantData => {
    applicantData.profileArray.map(applicant => {
      const messages = {
        entityUrn: applicant.entityUrn,
        messageArray: applicant.messageArray,
      };
      console.log('=========================================');
      // profileArrays = profileArrays.concat(messages);
      profileArrays.push(messages);
    });
  });
  console.log(profileArrays.length);
  let removedDupes = [];
  // removedDupes = getUnique(profileArrays, 'entityUrn');
  removedDupes = profileArrays;
  console.log(removedDupes.length);
  fs.writeFile('devDataFiltered.json', JSON.stringify(removedDupes), 'utf8', function (err) {
    console.log(err);
  });
});
